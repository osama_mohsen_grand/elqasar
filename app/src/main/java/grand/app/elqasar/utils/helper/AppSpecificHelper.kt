package grand.app.elqasar.utils.helper

import android.os.Bundle
import grand.app.elqasar.R
import grand.app.elqasar.pages.makeyourclothes.fabric.model.ClothesModel
import grand.app.elqasar.pages.makeyourclothes.fabric.model.ClothesModelRequest
import grand.app.elqasar.pages.makeyourclothes.fabric.model.Fabric
import grand.app.elqasar.utils.resources.ResourceManager

object AppSpecificHelper {
    fun selectClothesModel(models : ArrayList<ClothesModel>,id: Int, type: Int,qty: Int = -1): ArrayList<ClothesModel> {
        for( model in models){
            if(model.type == type){
                model.id = id
                model.qty = qty
                break
            }
        }
        return models
    }
    fun initClothesModels(models : ArrayList<ClothesModel>) {
        models.clear()
        for(i in 1..8){
            val model = ClothesModel(type = i)
            models.add(model)
        }
    }


    fun getTitleBarType(type: Int): String {
        return when (type) {
            1 -> ResourceManager.getString(R.string.fabric_selection)
            2 -> ResourceManager.getString(R.string.tailor_made)
            3 -> ResourceManager.getString(R.string.neck)
            4 -> ResourceManager.getString(R.string.gibzour)
            5 -> ResourceManager.getString(R.string.sleeves)
            6 -> ResourceManager.getString(R.string.embroidery)
            7 -> ResourceManager.getString(R.string.frame)
            8 -> ResourceManager.getString(R.string.accessory)
//            9 -> ResourceManager.getString(R.string.my_sizes)
            else -> ""
        }
    }

    fun getClothesInfo(clothesModel: ArrayList<ClothesModel>): ArrayList<ClothesModel> {
        for( model in clothesModel)
            model.name = getTitleBarType(model.type)
//        clothesModel.add(ClothesModel(-1,9,-1,ResourceManager.getString(R.string.my_sizes)))
        return clothesModel
    }

    fun getId(type: Int, models: ArrayList<ClothesModel>): Int {
        for(model in models){
            if(model.type == type) return model.id
        }
        return -1
    }

    fun getModel(type: Int, models: ArrayList<ClothesModel>): ClothesModel? {
        for(model in models){
            if(model.type == type) return model
        }
        return null
    }
}