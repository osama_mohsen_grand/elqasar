package grand.app.elqasar.utils.upload

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.util.Log
import java.io.*

//n2es hena 2nna n3mel check 3la type al file swa2 image , video 34an n2dr nb3to be retrofit
class FileUpload(private var paramName: String?, filePath: String?, fileType: Int) : Serializable {
    private var filePath: String? = ""
    private var fileType: Int
    private var uri: Uri? = null
    private var file: File?
    fun getResizedBitmap(file: File): Bitmap? {
        val maxSize = 400
        val bmOptions = BitmapFactory.Options()
        val bitmap = BitmapFactory.decodeFile(file.absolutePath, bmOptions)
        var width = bitmap.width
        var height = bitmap.height
        val bitmapRatio = width as Float / height as Float
        if (bitmapRatio > 1) {
            width = maxSize
            height = (width / bitmapRatio) as Int
        } else {
            height = maxSize
            width = (height * bitmapRatio) as Int
        }
        return Bitmap.createScaledBitmap(bitmap, width, height, true)
    }

    fun compressImage() {
        val imageFile = File(getFilePath())
        val bitmap = getResizedBitmap(imageFile)
        setBitmap(bitmap)
        val os: OutputStream
        try {
            os = FileOutputStream(imageFile)
            bitmap?.compress(Bitmap.CompressFormat.JPEG, 70, os)
            os.flush()
            os.close()
        } catch (e: Exception) {
            Log.e("err_compress_image", e.message)
        }
        setFilePath(imageFile.absolutePath)
        setFile(imageFile)
        setBitmap(bitmap)
    }

    fun setFilePath(filePath: String?) {
        this.filePath = filePath
    }

    fun getFilePath(): String? {
        return filePath
    }

    fun setFileType(fileType: Int) {
        this.fileType = fileType
    }

    fun getFileType(): Int {
        return fileType
    }

    fun getParamName(): String? {
        return paramName
    }

    fun setParamName(paramName: String?) {
        this.paramName = paramName
    }

    fun getBytes(): ByteArray? {
        val file = File(getFilePath())
        val size = file.length() as Int
        val bytes = ByteArray(size)
        try {
            val buf = BufferedInputStream(FileInputStream(file))
            buf.read(bytes, 0, bytes.size)
            buf.close()
        } catch (e: FileNotFoundException) {
            // TODO Auto-generated catch block
            e.printStackTrace()
        } catch (e: IOException) {
            // TODO Auto-generated catch block
            e.printStackTrace()
        }
        return bytes
    }

    private var bitmap: Bitmap? = null
    fun setBitmap(bitmap: Bitmap?) {
        this.bitmap = bitmap
    }

    fun getBitmap(): Bitmap? {
        return bitmap
    }

    fun setUri(uri: Uri?) {
        this.uri = uri
    }

    fun getUri(): Uri? {
        return uri
    }

    fun setFile(file: File?) {
        this.file = file
    }

    fun getFile(): File? {
        return file
    }

    init {
        this.filePath = filePath
        this.fileType = fileType
        file = File(filePath)
        compressImage()
    }
}