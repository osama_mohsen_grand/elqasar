package grand.app.elqasar.utils.popup

import android.app.Activity
import android.view.View
import androidx.appcompat.widget.PopupMenu
import java.util.*

class PopUpMenuHelper() {
    private lateinit var popUpMenu: PopupMenu

    fun openPopUp(
        context: Activity,
        viewAction: View,
        objects: List<String>,
        popUpInterface: PopUpInterface
    ) {
        val list: MutableList<String> =
            ArrayList()
        list.clear()
        list.addAll(objects)
        popUpMenu = PopupMenu(context, viewAction)
        for (i in list.indices) {
            popUpMenu.menu.add(i, i, i, list[i])
        }
        popUpMenu.setOnMenuItemClickListener { item ->
            popUpInterface.submitPopUp(item.itemId)
            false
        }
        popUpMenu.show()
    }
}