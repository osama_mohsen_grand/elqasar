package grand.app.elqasar.utils.session

import android.util.Log
import com.google.gson.Gson
import grand.app.elqasar.pages.auth.login.model.User
import grand.app.elqasar.pages.product.model.list.ProductListResponse
import grand.app.elqasar.pages.splash.models.MainSettingsResponse
import grand.app.elqasar.utils.Constants
import java.lang.reflect.Type

/**
 * Created by mohamedatef on 1/12/19.
 */
object UserHelper {
    fun getUserId(): Int {
        return SharedPreferenceHelper.getSharedPreferenceInstance().getInt("id", -1)
    }

    fun saveUserDetails(userModel: User) {

        val prefsEditor =
            SharedPreferenceHelper.getSharedPreferenceInstance().edit()
        val gson = Gson()
        val json = gson.toJson(userModel)
        saveKey(Constants.CART, "${userModel.cartCount}")
        SharedPreferenceHelper.saveKey(Constants.CART, "${userModel.cartCount}")
        prefsEditor.putString("userDetails", json)
        prefsEditor.putInt("id", userModel.id)
        Log.e("userDetails", json.toString())
        prefsEditor.apply()
        prefsEditor.commit()
    }




    fun saveUserId() {
        val prefsEditor = SharedPreferenceHelper.getSharedPreferenceInstance().edit()

        prefsEditor?.let {
            prefsEditor.putInt("id", -1)
            prefsEditor.apply()
            prefsEditor.commit()
        }

    }

    fun clearUserDetails() {
        val prefsEditor = SharedPreferenceHelper.getSharedPreferenceInstance().edit()
        prefsEditor?.let {
            prefsEditor.putString("userDetails", null)
            prefsEditor.putInt("id", -1)
            prefsEditor.apply()
            prefsEditor.commit()
        }
        saveKey(Constants.CART,"0")
    }

    fun saveKey(key: String?, value: String?) {
        val prefsEditor = SharedPreferenceHelper.getSharedPreferenceInstance().edit()
        prefsEditor?.let {
            prefsEditor.putString(key, value)
            prefsEditor.apply()
            prefsEditor.commit()
        }
    }

    fun retrieveKey(key: String?): String? {
        val preferences = SharedPreferenceHelper.getSharedPreferenceInstance()
        return if (preferences?.contains(key) == true) preferences.getString(key, ""); else ""
    }

    fun saveSize(){
        if(isLogin()) {
            val user = getUserDetails()
            user.isSize = true
            saveUserDetails(user)
        }
    }

    fun getUserDetails(): User {

        val json: String =
            SharedPreferenceHelper.getSharedPreferenceInstance().getString("userDetails", "")
                .toString()
        return if (Gson().fromJson(json, User::class.java) != null) {
            Gson().fromJson(json, User::class.java)
        } else
            User()
    }

    fun isLogin() : Boolean{
        return getUserId() != -1
    }

    fun samePhone(phone : String) : Boolean{
        return getUserDetails().phone == phone
    }



    fun saveJsonResponse(key: String?, value: Any?) {
        val prefsEditor = SharedPreferenceHelper.getSharedPreferenceInstance().edit()
        val gson = Gson()
        val json = gson.toJson(value)
        prefsEditor?.let {
            prefsEditor.putString(key, json)
            prefsEditor.apply()
            prefsEditor.commit()
        }
    }


    fun hasSize():Boolean{
        if(isLogin()){
            return getUserDetails().isSize
        }
        val settingsResponse =retrieveJsonResponse(
            Constants.SETTINGS,
            MainSettingsResponse::class.java
        ) as MainSettingsResponse
        return settingsResponse.isSize
    }

    fun retrieveJsonResponse(key: String?, value: Any?): Any? {
        val gson = Gson()
        val json = SharedPreferenceHelper.getSharedPreferenceInstance().getString(key, "")
        return gson.fromJson(json, value as Type?) ?: return Any()
    }
}