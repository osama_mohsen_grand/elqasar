package grand.app.elqasar.utils.helper

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import grand.app.elqasar.R
import grand.app.elqasar.activity.BaseActivity
import grand.app.elqasar.utils.Constants

import timber.log.Timber

object MovementHelper {
    //---------Fragments----------//
    private const val CONTAINER_ID = R.id.fl_home_container
    fun popAllFragments(context: Context?) {
        val fm = (context as FragmentActivity?)?.supportFragmentManager
        fm?.let {
            for (i in 0 until fm.backStackEntryCount) {
                fm.popBackStack()
            }
        }

    }


    fun replaceFragment(context: Context, fragment: Fragment,tag: String = "") {
        Timber.e("replaceFragment")
        val fragmentManager = (context as FragmentActivity?)?.supportFragmentManager
        val fragmentTransaction = if(tag == "")
            fragmentManager?.beginTransaction()?.replace(CONTAINER_ID, fragment)
        else
            fragmentManager?.beginTransaction()?.replace(CONTAINER_ID, fragment, tag)

        fragmentTransaction?.commit()
    }

    fun addFragmentTag(context: Context?, fragment: Fragment?) {
        val fragmentManager = (context as FragmentActivity?)?.supportFragmentManager
        fragment?.let {
            val fragmentTransaction =
                fragmentManager?.beginTransaction()?.add(CONTAINER_ID, fragment)
            fragmentTransaction?.commit()
        }
    }

    fun popLastFragment(context: Context?) {
        (context as FragmentActivity?)?.supportFragmentManager?.popBackStack()
    }

    //-----------Activities-----------------//
    fun startActivity(context: Context?, activity: Class<*>?) {
        val intent = Intent(context, activity)
        context?.startActivity(intent)
    }

    fun startActivity(context: Context?, activity: Class<*>?,bundle: Bundle = Bundle()) {
        val intent = Intent(context, activity)
        if (bundle.containsKey(Constants.PAGE)) {
            intent.putExtra(Constants.PAGE, bundle.getString(Constants.PAGE))
        }
        intent.putExtra(Constants.BUNDLE, bundle)

        context?.startActivity(intent)
    }

    fun startActivityWithBundle(from: Context?, to: Class<*>?, bundle: Bundle?, name: String?) {
        val intent = Intent(from, to)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        if (name != null) {
            intent.putExtra(Constants.NAME_BAR, name)
        }
        if (bundle != null) {
            intent.putExtra(Constants.BUNDLE, bundle)
        }
        from?.startActivity(intent)
    }

    private val TAG: String? = "MovementHelper"
    fun startActivityBase(context: Context?, bundle: Bundle?, name: String = "") {
        val intent = Intent(context, BaseActivity::class.java)
        //        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        if (name != "") {
            intent.putExtra(Constants.NAME_BAR, name)
        }
        if (bundle != null) {
            Log.d(TAG, "bundle != null")
            if (bundle.containsKey(Constants.PAGE)) {
                intent.putExtra(Constants.PAGE, bundle.getString(Constants.PAGE))
            }
            intent.putExtra(Constants.BUNDLE, bundle)
        }
        context?.startActivity(intent)
    }

    fun startActivityBaseForResult(fragment: Fragment, bundle: Bundle?, result: Int, name: String = "") {
        val intent = Intent(fragment.context, BaseActivity::class.java)
        //        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        if (name != "") {
            intent.putExtra(Constants.NAME_BAR, name)
        }
        if (bundle != null) {
            Log.d(TAG, "bundle != null")
            if (bundle.containsKey(Constants.PAGE)) {
                intent.putExtra(Constants.PAGE, bundle.getString(Constants.PAGE))
            }
            intent.putExtra(Constants.BUNDLE, bundle)
        }
        fragment.startActivityForResult(intent,result)
    }

    fun startWebPage(context: Context?, page: String?) {
        try {
            context?.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(page)))
        } catch (e: Exception) {
            e.stackTrace
        }
    }

    fun finishResult(
        requireActivity: FragmentActivity,
        intent: Intent
    ) {
        requireActivity.setResult(Activity.RESULT_OK, intent)
        requireActivity.finish()
    }

}