package grand.app.elqasar.utils.session

import android.content.Context
import android.content.ContextWrapper
import android.os.Build
import android.os.LocaleList
import java.util.*

object MyContextWrapper {
    fun wrap(context: Context?, language: String?): ContextWrapper? {
        var context = context
        val res = context?.getResources()
        val configuration = res?.configuration
        val newLocale = Locale(language)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            configuration?.setLocale(newLocale)
            val localeList = LocaleList(newLocale)
            LocaleList.setDefault(localeList)
            configuration?.setLocales(localeList)
            context = configuration?.let { context?.createConfigurationContext(it) }
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            configuration?.setLocale(newLocale)
            context = configuration?.let { context?.createConfigurationContext(it) }
        } else {
            configuration?.locale = newLocale
            res?.updateConfiguration(configuration, res.displayMetrics)
        }
        return ContextWrapper(context)
    }
}