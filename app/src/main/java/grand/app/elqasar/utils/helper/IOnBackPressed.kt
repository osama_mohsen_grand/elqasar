package grand.app.elqasar.utils.helper

interface IOnBackPressed {
    fun onBackPressed(): Boolean
}