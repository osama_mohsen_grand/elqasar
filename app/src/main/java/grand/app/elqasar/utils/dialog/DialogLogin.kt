package grand.app.elqasar.utils.dialog

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import grand.app.elqasar.R
import grand.app.elqasar.activity.BaseActivity
import grand.app.elqasar.activity.MainActivity
import grand.app.elqasar.databinding.LayoutDialogLoginRegisterBinding
import grand.app.elqasar.pages.auth.login.LoginFragment
import grand.app.elqasar.pages.auth.register.RegisterFragment
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.helper.MovementHelper

public class DialogLogin(var activity: Activity)  {
    private val TAG = "DialogLogin"
    var dialog: AlertDialog? = null

    fun show() {
        val layoutInflater = LayoutInflater.from(activity)
        val binding: LayoutDialogLoginRegisterBinding = DataBindingUtil.inflate(
            layoutInflater,
            R.layout.layout_dialog_login_register,
            null,
            true
        )
        val dialogView: View = binding.getRoot()
        val builder =
            AlertDialog.Builder(activity, R.style.customDialog)
        dialog = builder.create()
        dialog!!.setView(dialogView)
        dialog!!.show()
        val bundle = Bundle()
        binding.tvRegister.setOnClickListener(View.OnClickListener {
            dialog!!.dismiss()
            bundle.putBoolean(Constants.IS_BACK,true)
            bundle.putString(Constants.PAGE,RegisterFragment::class.java.name)
            MovementHelper.startActivityBase(activity,bundle,activity.getString(R.string.register))
        })
        binding.tvLogin.setOnClickListener(View.OnClickListener {
            dialog!!.dismiss()
            bundle.putBoolean(Constants.IS_BACK,true)
            bundle.putString(Constants.PAGE,LoginFragment::class.java.name)
            MovementHelper.startActivityBase(activity,bundle,activity.getString(R.string.login))
        })
        binding.imgDialogClose.setOnClickListener(View.OnClickListener { dialog!!.dismiss() })
    }

    fun dismiss() {
        if (dialog != null && dialog!!.isShowing) dialog!!.dismiss()
    }
}