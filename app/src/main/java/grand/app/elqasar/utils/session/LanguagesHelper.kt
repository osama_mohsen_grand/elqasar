package grand.app.elqasar.utils.session

import android.content.Context
import android.content.SharedPreferences
import android.content.res.Configuration
import grand.app.elqasar.base.MyApplication
import grand.app.elqasar.utils.Constants
import java.util.*

/**
 * Created by mohamedatef on 1/12/19.
 */
object LanguagesHelper {
    fun changeLanguage(context: Context?, languageToLoad: String?) {
        val locale = Locale(languageToLoad)
        Locale.setDefault(locale)
        val config = Configuration()
        config.locale = locale
        context?.resources?.updateConfiguration(config,
                context.resources?.displayMetrics)
        setLanguage(languageToLoad)
    }

    fun setLanguage(language: String?) {
        val userDetails: SharedPreferences? = MyApplication.instance?.getSharedPreferences(Constants.LANGUAGE, Context.MODE_PRIVATE)
        val editor = userDetails?.edit()
        editor?.let {
            editor.putString(Constants.LANGUAGE, language)
            editor.apply()
            editor.commit()
        }
    }

    fun getCurrentLanguage(): String {
        val preferences: SharedPreferences? = MyApplication.instance?.applicationContext?.getSharedPreferences(Constants.LANGUAGE, Context.MODE_PRIVATE)
        preferences?.let {
            return if(preferences.contains(Constants.LANGUAGE)) preferences.getString(Constants.LANGUAGE, "")
                .toString(); else{
                setLanguage(Constants.DEFAULT_LANGUAGE)
                Constants.DEFAULT_LANGUAGE
            }
        }
        return Constants.DEFAULT_LANGUAGE
    }
}