package grand.app.elqasar.utils

object URLS {
    const val BASE_URL: String = "https://alqassar.my-staff.net/api/"
    const val MAIN_SETTINGS: String = "Main-Setting"
    const val USER_SIZES: String = "user-sizes"
    const val LOGIN: String = "login"
    const val REGISTER: String = "register"
    const val FORGET_PASSWORD: String = "check-phone"
    const val UPDATE_PROFILE: String = "user/update-profile"
    const val UPDATE_TOKEN: String = "user/token"

    //    public  final static String COMPANIES = "company/companies?category_id=5&lat=30.1341689&lng=31.3655877&city_id=1";
    const val ORDERS: String = "order/user/list"
    const val ORDER_STATUS: String = "order/status"
    const val USER_CART: String = "user/cart"
    const val CHECKOUT: String = "order/user/complete"
    const val PROMO: String = "promo/apply"
    const val ADD_TO_CART: String = "user/cart/add_or_update"
    const val ADD_TO_CART_CLOTHES: String = "user/cloth-cart/add_or_update"
    const val DELETE_ITEM_CART: String = "user/cart/delete/"

    const val ADD_FAVOURITE: String = "user/favorite/add_or_remove"
    const val FAVOURITE: String = "user/favorite"

    const val PRODUCT_SEND_MESSAGE: String = "product/send-message"
    const val SETTINGS: String = "settings?status="
    const val NOTIFICATIONS: String = "notifications"
    const val CONTACT_US: String = "contactUs/add"
    const val CHANGE_PASSWORD: String = "forget-password"
    const val CHAT: String = "user/chat"
    const val CHAT_SEND: String = "user/create-chat"
    const val DELETE: String = "order/user/"
    const val ORDER_DETAILS: String = "order/user/order-details?order_id="
    const val PRODUCT_LIST: String = "products/list"
    const val SEARCH: String = "products/search?search="
    const val PRODUCT_DETAILS: String = "products/product-details"

    const val ADDRESS_LIST: String = "user/addresses"
    const val ADD_ADDRESS: String = "user/addresses/store"
    const val CATEGORIES: String = "categories"

    const val MY_SIZES: String = "add-user-sizes"

    const val VERIFY: String = "activate-account"
    const val PAYMENT_ONLINE: String = "go-to-payment?invoice_value="

        //go-to-payment?full_name=Amgad&invoice_value=1
    /*
    {{moata}}/user/variations-calculations?product_id=5&advanced[]=2&advanced[]=3
     */
    const val CALCULATE: String = "user/variations-calculations"
}