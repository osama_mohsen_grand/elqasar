package grand.app.elqasar.utils.session

import android.content.Context
import android.content.SharedPreferences
import grand.app.elqasar.base.MyApplication

object SharedPreferenceHelper {
    //here you can find shared preference operations like get saved data for user
    fun getSharedPreferenceInstance(): SharedPreferences {
        return MyApplication.instance.getSharedPreferences("savedData", Context.MODE_PRIVATE)
    }

    fun getKey(key: String?): String? {
        return MyApplication.instance?.getSharedPreferences("shared", Context.MODE_PRIVATE)?.getString(key, "")
    }

    fun saveKey(key: String?, value: String?) {
        MyApplication.instance?.getSharedPreferences("shared", Context.MODE_PRIVATE)?.edit()?.putString(key, value)?.apply()
    }
}