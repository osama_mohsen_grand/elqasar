package grand.app.elqasar.utils.helper

interface IOBackCommonPressed {
    fun onBackPressed(): Boolean
}