package grand.app.elqasar.utils.popup

interface PopUpInterface {
    fun submitPopUp(position: Int)
}