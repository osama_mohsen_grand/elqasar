package grand.app.elqasar.utils.upload

import android.Manifest
import android.annotation.SuppressLint
import android.content.ContentUris
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.util.Base64
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import grand.app.elqasar.R
import grand.app.elqasar.base.ParentActivity

import grand.app.elqasar.connection.FileObject
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.resources.ResourceManager
import java.io.*
import java.util.*

@SuppressLint("NewApi")
class FileOperations {
    fun getFileAsString(context: Context?, uri: Uri?): String? {
        return fileToString(getPath(context, uri))
    }

    private fun fileToString(selectedPath: String?): String? {
        var inputStream: FileInputStream? = null
        var str_image: String? = ""
        val byteBuffer = ByteArrayOutputStream()
        try {
            inputStream = FileInputStream(File(selectedPath))
            val bufferSize = 16777216
            val buffer = ByteArray(bufferSize)
            var len: Int
            while (inputStream.read(buffer).also { len = it } != -1) {
                byteBuffer.write(buffer, 0, len)
            }
            try {
                inputStream.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }
            str_image = Base64.encodeToString(byteBuffer.toByteArray(), 0)
        } catch (ex: Exception) {
            ex.stackTrace
        }
        return str_image
    }

    fun BitMapToString(bitmap: Bitmap?): String? {
        val baos = ByteArrayOutputStream()
        bitmap?.compress(Bitmap.CompressFormat.PNG, 70, baos)
        val b = baos.toByteArray()
        return Base64.encodeToString(b, Base64.DEFAULT)
    }

    companion object {
        fun getPath(context: Context?, uri: Uri?): String? {
            val isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT

            // DocumentProvider
            if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
                // ExternalStorageProvider
                if (isExternalStorageDocument(uri)) {
                    val docId = DocumentsContract.getDocumentId(uri)
                    val split: Array<String?> = docId.split(":").toTypedArray()
                    val type = split[0]
                    if ("primary".equals(type, ignoreCase = true)) {
                        return Environment.getExternalStorageDirectory().toString() + "/" + split[1]
                    }
                } else if (isDownloadsDocument(uri)) {
                    val id = DocumentsContract.getDocumentId(uri)
                    val contentUri = ContentUris.withAppendedId(
                            Uri.parse("content://downloads/public_downloads"), java.lang.Long.valueOf(id))
                    return getDataColumn(context, contentUri, null, null)
                } else if (isMediaDocument(uri)) {
                    val docId = DocumentsContract.getDocumentId(uri)
                    val split: Array<String?> = docId.split(":").toTypedArray()
                    val type = split[0]
                    val contentUri: Uri?
                    contentUri = if ("image" == type) {
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                    } else if ("video" == type) {
                        MediaStore.Video.Media.EXTERNAL_CONTENT_URI
                    } else if ("audio" == type) {
                        MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
                    } else {
                        MediaStore.getMediaScannerUri()
                    }
                    val selection = "_id=?"
                    val selectionArgs = arrayOf(
                            split[1]
                    )
                    return getDataColumn(context, contentUri, selection, selectionArgs)
                }
            } else if ("content".equals(uri?.scheme, ignoreCase = true)) {
                return getDataColumn(context, uri, null, null)
            } else if ("file".equals(uri?.scheme, ignoreCase = true)) {
                return uri?.path
            }
            return null
        }

        /**
         * Get the value of the data column for this Uri. This is useful for
         * MediaStore Uris, and other file-based ContentProviders.
         *
         * @param context       The context.
         * @param uri           The Uri to query.
         * @param selection     (Optional) Filter used in the query.
         * @param selectionArgs (Optional) Selection arguments used in the query.
         * @return The value of the _data column, which is typically a file path.
         */
        private fun getDataColumn(context: Context?, uri: Uri?, selection: String?,
                                  selectionArgs: Array<String?>?): String? {
            var cursor: Cursor? = null
            val column = "_data"
            val projection = arrayOf<String?>(
                    column
            )
            try {
                cursor = uri?.let {
                    context?.contentResolver?.query(
                        it, projection, selection, selectionArgs,
                        null)
                }
                if (cursor != null && cursor.moveToFirst()) {
                    val columnIndex = cursor.getColumnIndexOrThrow(column)
                    return cursor.getString(columnIndex)
                }
            } finally {
                cursor?.close()
            }
            return null
        }

        /**
         * @param uri The Uri to check.
         * @return Whether the Uri authority is ExternalStorageProvider.
         */
        private fun isExternalStorageDocument(uri: Uri?): Boolean {
            return "com.android.externalstorage.documents" == uri?.authority
        }

        /**
         * @param uri The Uri to check.
         * @return Whether the Uri authority is DownloadsProvider.
         */
        private fun isDownloadsDocument(uri: Uri?): Boolean {
            return "com.android.providers.downloads.documents" == uri?.authority
        }

        /**
         * @param uri The Uri to check.
         * @return Whether the Uri authority is MediaProvider.
         */
        private fun isMediaDocument(uri: Uri?): Boolean {
            return "com.android.providers.media.documents" == uri?.authority
        }

        fun fileToBytes(selectedPath: String?): ByteArray? {
            val inputStream: FileInputStream
            val file = File(selectedPath)
            val size = file.length() as Int
            val byteBuffer = ByteArrayOutputStream()
            try {
                inputStream = FileInputStream(File(selectedPath))
                val buffer = ByteArray(size)
                var len: Int
                while (inputStream.read(buffer).also { len = it } != -1) {
                    byteBuffer.write(buffer, 0, len)
                }
                try {
                    inputStream.close()
                } catch (e: IOException) {
                    e.printStackTrace()
                }
                return buffer
            } catch (ex: Exception) {
                ex.stackTrace
            }
            return null
        }

        private fun specialCameraSelector(inContext: Context?, bitmap: Bitmap?): Uri? {
//        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, bytes);
            val stream = ByteArrayOutputStream()
            bitmap?.compress(Bitmap.CompressFormat.JPEG, 70, stream)
            val byteArray = stream.toByteArray()
            val compressedBitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.size)
            val path = MediaStore.Images.Media.insertImage(inContext?.contentResolver, compressedBitmap, Date(System.currentTimeMillis()).toString() + "photo", null)
            return Uri.parse(path)
        }

        fun getImageUri(inContext: Context?, inImage: Bitmap?): String? {
            val bytes = ByteArrayOutputStream()
            val pathBase = Environment.getExternalStorageDirectory().toString()
            var fOut: OutputStream? = null
            val file = File(pathBase, "image") // the File to save , append increasing numeric counter to prevent files from getting overwritten.
            try {
                fOut = FileOutputStream(file)
                inImage?.compress(Bitmap.CompressFormat.JPEG, 70, fOut) // saving the Bitmap to a file compressed as a JPEG with 85% compression rate
                fOut.flush() // Not really required
                fOut.close() // do not forget to close the stream
                MediaStore.Images.Media.insertImage(inContext?.contentResolver, file.absolutePath, file.name, file.name)
            } catch (e: FileNotFoundException) {
                e.printStackTrace()
            } catch (e: IOException) {
                e.printStackTrace()
            }
            return file.absolutePath
        }

        fun getFileObject(context: Context?, data: Intent?, paramName: String?, fileType: Int): FileObject? {
            if (data == null) return null
            var dataUrl = data.data
            if (fileType == Constants.FILE_TYPE_IMAGE) {
                if (dataUrl == null) {
                    dataUrl = specialCameraSelector(context, Objects.requireNonNull(Objects.requireNonNull(data.extras)?.get("data")) as Bitmap?)
                }
                if (dataUrl == null) {
                    return null
                }
            }
            val filePath = getPath(context, dataUrl)
            //        CompressObject compressObject;
//        Log.e("FilePath", " >> " + filePath);
//        if (fileType == Constants.FILE_TYPE_IMAGE) {
//            compressObject = new ImageCompression().compressImage(filePath);
//            Log.e("FilePathAfterCompress", " >> " + filePath);
//        } else {
//            compressObject = new CompressObject();
//            compressObject.setBytes(fileToBytes(filePath));
//        }
            val volleyFileObject = FileObject(paramName, filePath, fileType)
            //        volleyFileObject.setCompressObject(compressObject);
            if (dataUrl != null) {
                volleyFileObject.uri = dataUrl
            }
            return volleyFileObject
        }

        fun pickImage(context: Context?, fragment: Fragment, requestCode: Int) {
            if (ActivityCompat.checkSelfPermission(fragment.requireActivity(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(fragment.requireActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(fragment.requireActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
//            Timber.e("fragment", "pickImage Done");
                val choiceString = arrayOf(ResourceManager.getString(R.string.gallery), ResourceManager.getString(R.string.camera))
                val dialog = AlertDialog.Builder(fragment.requireActivity())
                dialog.setIcon(R.mipmap.ic_launcher)
                dialog.setTitle(ResourceManager.getString(R.string.select_image_from))
                dialog.setItems(choiceString
                ) { dialog1: DialogInterface?, which: Int ->
                    val intent: Intent
                    intent = if (which == 0) {
                        Intent(
                                Intent.ACTION_PICK,
                                MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                    } else {
                        Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                    }
                    (context as ParentActivity?)?.startActivityForResult(Intent.createChooser(intent, ResourceManager.getString(R.string.select_image_from)), requestCode)
                }.show()
            } else {
                ActivityCompat.requestPermissions(fragment.requireActivity(), arrayOf<String?>(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA), 1007)
            }
        }

        fun pickDocument(context: Context?, fragment: Fragment, requestCode: Int) {
            if (ActivityCompat.checkSelfPermission(fragment.requireActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
//            Intent intent;
//            intent = new Intent(Intent.ACTION_GET_CONTENT);
//            intent.setType("*/*");
//            ((AppCompatActivity) context).startActivityForResult(Intent.createChooser(intent, "Select File"), requestCode);
                val mimeTypes = arrayOf<String?>("application/msword",
                        "text/plain",
                        "application/pdf")
                val intent = Intent(Intent.ACTION_GET_CONTENT)
                intent.addCategory(Intent.CATEGORY_OPENABLE)
                intent.type = "application/pdf"


//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//                intent.setType(mimeTypes.length == 1 ? mimeTypes[0] : "*/*");
//                if (mimeTypes.length > 0) {
//                    intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
//                }
//            } else {
//                String mimeTypesStr = "";
//                for (String mimeType : mimeTypes) {
//                    mimeTypesStr += mimeType + "|";
//                }
//                intent.setType(mimeTypesStr.substring(0,mimeTypesStr.length() - 1));
//            }
                fragment.requireActivity().startActivityForResult(Intent.createChooser(intent, ResourceManager.getString(R.string.select_file)), requestCode)
            } else {
                ActivityCompat.requestPermissions(fragment.requireActivity(), arrayOf<String?>(Manifest.permission.READ_EXTERNAL_STORAGE), 1007)
            }
        }
    }
}