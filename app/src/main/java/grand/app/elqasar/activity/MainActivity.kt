package grand.app.elqasar.activity

import android.app.ActivityManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.util.Log
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import grand.app.elqasar.R
import grand.app.elqasar.base.BaseFragment
import grand.app.elqasar.base.MyApplication
import grand.app.elqasar.base.ParentActivity
import grand.app.elqasar.customViews.actionbar.HomeActionBarView
import grand.app.elqasar.customViews.menu.NavigationDrawerView
import grand.app.elqasar.databinding.ActivityMainBinding
import grand.app.elqasar.pages.account.AccountSettingFragment
import grand.app.elqasar.pages.auth.repository.AuthRepository
import grand.app.elqasar.pages.cart.ui.CartFragment
import grand.app.elqasar.pages.favourite.ui.FavouriteFragment
import grand.app.elqasar.pages.home.HomeFragment
import grand.app.elqasar.pages.size.ui.MySizesFragment
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.dialog.DialogLogin
import grand.app.elqasar.utils.helper.AppHelper
import grand.app.elqasar.utils.helper.MovementHelper
import grand.app.elqasar.utils.session.SharedPreferenceHelper
import grand.app.elqasar.utils.session.UserHelper
import javax.inject.Inject

class MainActivity : ParentActivity() {
    lateinit var homeActionBarView: HomeActionBarView
    var navigationDrawerView: NavigationDrawerView? = null
    lateinit var activityMainBinding: ActivityMainBinding
    private val TAG = "MainActivity"

    @Inject
    lateinit var authRepository: AuthRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dialogLogin = DialogLogin(this)
        initializeLanguage()
        //        initializeToken();
        setContentView(R.layout.activity_main)
        val component = (applicationContext as MyApplication).applicationComponent
        component?.inject(this)
        activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
//        navigationDrawerView = NavigationDrawerView(this)
//        activityMainBinding.llBaseContainer.addView(navigationDrawerView)
        setHomeActionBar()
        setTabListeners()
        activityMainBinding.bnvMainBottom.selectedItemId = R.id.action_home
        if (intent.hasExtra(Constants.DIALOG)) {
            AppHelper.showDialog(this,intent.getStringExtra(Constants.DIALOG))
        } else if (intent.hasExtra(Constants.PAGE)) {
            val name = intent.getStringExtra(Constants.PAGE)
            name?.let {
                if (name == CartFragment::class.java.name) activityMainBinding.bnvMainBottom.selectedItemId =
                    R.id.action_cart
                MovementHelper.replaceFragment(
                    this,
                    Class.forName(name).newInstance() as Fragment,
                    name
                )
            }
        } else
            homePage()

        initializeToken(authRepository)
    }

    private fun homePage() {
        MovementHelper.replaceFragment(this, HomeFragment(), tag = Constants.HOME)
    }

    private fun setHomeActionBar() {
        homeActionBarView = HomeActionBarView(this)
        homeActionBarView.setTitle(getString(R.string.home))
        navigationDrawerView?.let {
            navigationDrawerView?.layoutNavigationDrawerBinding?.llBaseActionBarContainer?.addView(
                homeActionBarView,
                0
            )
            homeActionBarView.setNavigation(navigationDrawerView)
            homeActionBarView.connectDrawer(
                navigationDrawerView?.layoutNavigationDrawerBinding?.dlMainNavigationMenu,
                true
            )
            navigationDrawerView?.setActionBar(homeActionBarView)
        }
    }


    private fun setTabListeners() {

        var checked = true
        activityMainBinding.bnvMainBottom.setOnNavigationItemSelectedListener { item ->

            if (item.itemId == R.id.action_home) {
                MovementHelper.replaceFragment(this, HomeFragment(), tag = Constants.HOME)
                activityMainBinding.bnvMainBottom.isSelected = true
                checked = true
            }
            if (item.itemId == R.id.action_sizes) {
                if(UserHelper.isLogin()) {
                    MovementHelper.replaceFragment(
                        this,
                        MySizesFragment(),
                        MySizesFragment::class.java.name
                    )
                    activityMainBinding.bnvMainBottom.isSelected = true
                    checked = true
                }else{
                    showDialogLogin()
                    checked = false
                }
            }
            if (item.itemId == R.id.action_favourite) {
                if(UserHelper.isLogin()) {
                    MovementHelper.replaceFragment(
                        this,
                        FavouriteFragment(),
                        MySizesFragment::class.java.name
                    )
                    activityMainBinding.bnvMainBottom.isSelected = true
                    checked = true
                }else{
                    showDialogLogin()
                    checked = false

                }
            }
            if (item.itemId == R.id.action_cart) {
                if(UserHelper.isLogin()) {
                    MovementHelper.replaceFragment(this, CartFragment(), tag = Constants.CART)
                    activityMainBinding.bnvMainBottom.isSelected = true
                    checked = true
                }else{
                    showDialogLogin()
                    checked = false
                }
            }
            if (item.itemId == R.id.action_account) {
                    MovementHelper.replaceFragment(
                        this,
                        AccountSettingFragment(),
                        tag = Constants.SETTINGS
                    )
                activityMainBinding.bnvMainBottom.isSelected = true
                checked = true
            }
            checked
        }
    }

    private fun showDialogLogin() {
        dialogLogin.show()
    }

    override fun onBackPressed() {
        val mngr = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        val taskList = mngr.getRunningTasks(10)
        if (taskList[0].numActivities == 1 && taskList[0].topActivity?.className == this.javaClass.name) {
            if (supportFragmentManager.backStackEntryCount > 0) {
                super.onBackPressed()
                return
            }

            val baseFragment: BaseFragment? =
                supportFragmentManager.findFragmentByTag(Constants.HOME) as BaseFragment?
            if (baseFragment != null) {
                if (baseFragment.dialogLoader != null && baseFragment.dialogLoader?.isShowing!!) {
                    baseFragment.dialogLoader?.dismiss()
                    finish()
                    return
                }
                finish()
            } else {
                MovementHelper.replaceFragment(this, HomeFragment(), tag = Constants.HOME)
                activityMainBinding.bnvMainBottom.selectedItemId = R.id.action_home
            }
            return
        } else finish()
    }

    override fun onResume() {
        super.onResume()
        registerReceiver(notify, IntentFilter(Constants.NOTIFICATIONS))
        updateCartCount()
    }

    private val notify: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            updateCartCount()
        }
    }

    fun updateCartCount() {
        val card = SharedPreferenceHelper.getKey(Constants.CART)
        Log.d("cart", "cart_count_main:" + SharedPreferenceHelper.getKey(Constants.CART))
        if (UserHelper.isLogin() && !card.equals("") && !card.equals("0")) {
            activityMainBinding.bnvMainBottom.getOrCreateBadge(R.id.action_cart).number =
                card?.toInt()!!
        } else {
            activityMainBinding.bnvMainBottom.removeBadge(R.id.action_cart)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(notify)
    }
}