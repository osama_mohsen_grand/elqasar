package grand.app.elqasar.activity

import android.os.Bundle
import android.util.Log
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import grand.app.elqasar.R
import grand.app.elqasar.base.BaseFragment
import grand.app.elqasar.base.MyApplication
import grand.app.elqasar.base.ParentActivity
import grand.app.elqasar.customViews.actionbar.BackActionBarView
import grand.app.elqasar.databinding.ActivityBaseBinding
import grand.app.elqasar.pages.order.ui.OrderDetailsFragment
import grand.app.elqasar.pages.settings.notification.NotificationListFragment
import grand.app.elqasar.pages.settings.notification.notify.NotificationGCMModel
import grand.app.elqasar.pages.splash.SplashFragment
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.dialog.DialogLogin
import grand.app.elqasar.utils.helper.MovementHelper
import grand.app.elqasar.utils.resources.ResourceManager
import timber.log.Timber

class BaseActivity : ParentActivity() {
    var activityBaseBinding: ActivityBaseBinding? = null
    var backActionBarView: BackActionBarView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dialogLogin = DialogLogin(this)
        initializeLanguage()
        val component = (applicationContext as MyApplication).applicationComponent
        component?.inject(this)
        activityBaseBinding = DataBindingUtil.setContentView(this, R.layout.activity_base)
        getNotification()
        if (!notification_checked) {
            if (intent.hasExtra(Constants.PAGE)) {
                val fragmentName = intent.getStringExtra(Constants.PAGE)
                if (fragmentName != null) {
                    try {
                        val fragment = Class.forName(fragmentName).newInstance() as Fragment
                        MovementHelper.addFragmentTag(this, getBundle(fragment))
                    } catch (ex: Exception) {
                        ex.printStackTrace()
                    }
                } else Timber.e("Fragment Name is empty")
            } else {
                val fragment = SplashFragment()
                MovementHelper.replaceFragment(
                    this,
                    fragment
                )
            }
        }
    }

    private fun setTitleName() {
        if (intent.hasExtra(Constants.NAME_BAR)) {
            backActionBarView = BackActionBarView(this)
            backActionBarView?.setTitle(intent.getStringExtra(Constants.NAME_BAR))
            activityBaseBinding?.llBaseActionBarContainer?.addView(backActionBarView)
        }
    }

    /*
        notification list   notification_type => 1
        notification order  notification_type => 2
        notification chat   notification_type => 3
     */
    private fun getNotification() {
        Log.d(TAG, "Notification GET")
        if (intent.extras != null && intent.hasExtra(Constants.BUNDLE_NOTIFICATION)) {
            Log.d(TAG, "Notification HAS EXTRA")
            val bundle = intent.getBundleExtra(Constants.BUNDLE_NOTIFICATION)
            val model: NotificationGCMModel? =
                bundle.getSerializable(Constants.BUNDLE_NOTIFICATION) as NotificationGCMModel?
            if (model != null) {
                when (model.notification_type) {
                    1 -> {
                        setTitleName(ResourceManager.getString(R.string.notifications))
                        notification_checked = true
                        MovementHelper.replaceFragment(this, NotificationListFragment(), "")
                        return
                    }
                    2 -> {
                        setTitleName(getString(R.string.order_details))
                        notification_checked = true
                        val orderDetailsFragment = OrderDetailsFragment()
                        bundle.putInt(Constants.ID, model.orderId)
                        orderDetailsFragment.arguments = bundle
                        MovementHelper.replaceFragment(this, orderDetailsFragment, "")
                        return
                    }
                }
            }
        }
    }

    private fun setTitleName(name: String) {
        backActionBarView = BackActionBarView(this)
        backActionBarView?.setTitle(name)
        activityBaseBinding?.llBaseActionBarContainer?.addView(backActionBarView)
    }

    private fun getBundle(fragment: Fragment?): Fragment? {
        val bundle = intent.getBundleExtra(Constants.BUNDLE)
        fragment?.setArguments(bundle)
        if (intent.hasExtra(Constants.NAME_BAR)) {
            setTitleName()
        }
        return fragment
    }

    override fun onBackPressed() {
//        try {
//            if (dialogLoader != null && dialogLoader!!.isShowing) {
//                dialogLoader?.dismiss()
//            }
//        } catch (ex: Exception) {
//            ex.printStackTrace()
//        }
        Log.d(TAG,"backpressed")
        val fragment = this.supportFragmentManager.findFragmentById(R.id.fl_home_container)
        if((fragment as BaseFragment).dialogLoader != null && (fragment as BaseFragment).dialogLoader?.isShowing!!){
            fragment.dialogLoader?.dismiss()
        }

        super.onBackPressed()
    }

    companion object {
        private val TAG: String? = "BaseActivity"
    }
}