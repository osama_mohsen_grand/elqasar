package grand.app.elqasar.connection

interface ConnectionListenerInterFace {
    open fun onRequestResponse(response: Any?)
}