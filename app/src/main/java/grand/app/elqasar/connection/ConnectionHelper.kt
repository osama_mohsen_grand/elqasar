package grand.app.elqasar.connection

import android.graphics.BitmapFactory
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.google.gson.JsonObject
import grand.app.elqasar.R
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.model.base.StatusMessage
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.resources.ResourceManager
import grand.app.elqasar.utils.session.UserHelper
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subscribers.DisposableSubscriber
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.json.JSONObject
import timber.log.Timber
import java.io.*
import java.lang.reflect.Type
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton
import kotlin.collections.ArrayList

@Singleton
class ConnectionHelper {

    var api: Api?

    @Inject
    constructor(api: Api?) {
        this.api = api
    }

    //    @Inject
    var liveData: MutableLiveData<Mutable> = MutableLiveData()

    var gson: Gson? = null

    @Inject
    fun init() {
        gson = Gson()
    }

    private fun prepareImages(volleyFileObjects: ArrayList<FileObject>): MultipartBody? {
        var requestBody: MultipartBody? = null
        val files = ArrayList<File?>()
        val builder = MultipartBody.Builder()
        builder.setType(MultipartBody.FORM)
        volleyFileObjects.let {
            for (volleyFileObject in volleyFileObjects) {
                volleyFileObject.let {
                    if (volleyFileObject.getFileType() == Constants.FILE_TYPE_IMAGE) {
                        Log.e("type", "type:" + Constants.FILE_TYPE_IMAGE)
                        val options = BitmapFactory.Options()
                        options.inSampleSize = 2
                        var os: OutputStream? = null
                        try {
                            os =
                                BufferedOutputStream(FileOutputStream(volleyFileObject.getFilePath()))
                            //                    volleyFileObject.getBitmap().compress(Bitmap.CompressFormat.JPEG, 70, os);
                            os.close()
                            val file = volleyFileObject.getFile()
                            if (file!!.exists()) {
                                files.add(file)
                                Log.e(
                                    "KeyNameImage",
                                    "KeyNameImage:" + volleyFileObject.getParamName()
                                )
                                //                                builder.addFormDataPart(volleyFileObject.getParamName(),
                                //                                    volleyFileObject.getParamName(), create.create(parse.parse("multipart/form-data"), file))
                            }
                        } catch (e: FileNotFoundException) {
                            e.printStackTrace()
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }
                    } else {
                        val file = volleyFileObject.getFile()
                        if (file!!.exists()) {
                            Log.e(TAG, "prepareImages: File Found:" + file.getAbsolutePath())
                            files.add(file)
                            Log.e("KeyNameImage", "KeyNameImage:" + volleyFileObject.getParamName())
                            //                            builder.addFormDataPart(volleyFileObject.getParamName(),
                            //                                volleyFileObject.getParamName(), create.create(parse.parse("multipart/form-data"), file))
                        }
                    }
                }
            }
        }
        if (files.size > 0) requestBody = builder.build()
        return requestBody
    }

    lateinit var map: MutableMap<String?, String?>


    fun requestApi(
        url: String?,
        requestData: Any?,
        fileObjects: ArrayList<FileObject>,
        responseType: Class<*>?,
        constantSuccessResponse: String?,
        showProgress: Boolean
    ): Disposable {

        map = getParameters(requestData)
        val map: MutableMap<String?, String?>? =
            getParameters(requestData)
        var call: Flowable<JsonObject?>? = null
        var file: MultipartBody.Part? = null
        if (fileObjects.size == 1) {
            val myFile = fileObjects[0]!!.getFile()
            Log.d(TAG, "done")
            var mFile: RequestBody? = null
            mFile = if (fileObjects[0].getFileType() === Constants.FILE_TYPE_IMAGE) {
                Log.d(TAG, "fileObjects FILE_TYPE_IMAGE")

                myFile?.let { RequestBody.create("image/*".toMediaTypeOrNull(), it) }
            } else {
                Log.d(TAG, "fileObjects FILE_TYPE_VIDEO")
                myFile?.let { RequestBody.create("video/*".toMediaTypeOrNull(), it) }
            }

            file = fileObjects[0]!!.getParamName()?.let {
                mFile?.let { it1 ->
                    MultipartBody.Part.createFormData(
                        it,
                        myFile!!.name,
                        it1
                    )
                }
            }
            call = api?.post(url, map, file)
        } else if (fileObjects.size > 1) {
            val multipartBody = prepareImages(fileObjects)
            if (multipartBody != null) {
                call = api!!.post(url, map, multipartBody)
            } else {
                Timber.e("error in prepare images to upload")
                call = api!!.post(url, map)
            }
        } else {
            call = api!!.post(url, map)
        }
        if (showProgress) {
            Log.d(TAG, "showProgress")
            liveData.setValue(Mutable(Constants.SHOW_PROGRESS))
        }
        return call!!.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(object : DisposableSubscriber<JsonObject?>() {

                override fun onError(t: Throwable) {
                    hideProgress(showProgress)
                    liveData.value = Mutable(Constants.SERVER_ERROR)
                }

                override fun onComplete() {
                    hideProgress(showProgress)
                }

                override fun onNext(response: JsonObject?) {
                    hideProgress(showProgress)
                    val jsonString = gson!!.toJson(response)
                    val statusMessage: StatusMessage =
                        gson!!.fromJson(jsonString, responseType as Type?)
                    when (statusMessage.mStatus) {
                        Constants.RESPONSE_SUCCESS -> {
                            liveData.setValue(
                                Mutable(
                                    constantSuccessResponse!!,
                                    gson!!.fromJson(jsonString, responseType)
                                )
                            )
                        }
                        Constants.RESPONSE_JWT_EXPIRE -> liveData.setValue(
                            Mutable(
                                Constants.LOGOUT,
                                statusMessage.mMessage
                            )
                        )
                        else -> liveData.setValue(Mutable(Constants.ERROR, statusMessage.mMessage))
                    }
                }
            })

    }

    /***
     *
     * @param method (method POST-> 200 , GET->201)
     * @param url (url end point)
     * @param requestData (json object for request your data)
     * @param responseType (json object for delivery your response)
     * @param constantSuccessResponse (constant value for delivery in view example (mutable.message == constantSuccessResponse) => success call)
     * @param showProgress (boolean take true or false)
     */
    fun requestApi(
        method: Int,
        url: String?,
        requestData: Any?,
        responseType: Class<*>?,
        constantSuccessResponse: String,
        showProgress: Boolean
    ): Disposable {
        Log.e(TAG, "requestApi: start" + responseType?.getName())
        var call: Flowable<JsonObject?>? = null
        val map = getParameters(requestData)
        call = if (method == Constants.POST_REQUEST) {
            api?.post(url, requestData) //here
        } else if (method == Constants.GET_REQUEST) {
            api?.get(url, map)
        } else if (method == Constants.DELETE_REQUEST) {
            api?.delete(url, map)
        } else api?.get(url, map)
        if (showProgress) {
            Log.d(TAG, "showProgress")
            liveData.setValue(Mutable(Constants.SHOW_PROGRESS))
        }
        return call?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribeWith(object : DisposableSubscriber<JsonObject?>() {
                override fun onNext(response: JsonObject?) {
                    hideProgress(showProgress)
                    Log.d(TAG, "Success Response")
                    if (liveData == null) Log.d(TAG, "null") else Log.d(TAG, "not null")
                    val jsonString = gson?.toJson(response)
                    val statusMessage =
                        gson?.fromJson<StatusMessage?>(jsonString, responseType as Type?)
                    if (statusMessage?.mStatus == Constants.RESPONSE_SUCCESS) {
                        Log.d(TAG, constantSuccessResponse)
                        liveData.setValue(
                            Mutable(
                                constantSuccessResponse,
                                gson?.fromJson(jsonString, responseType)
                            )
                        )
                    } else if (statusMessage?.mStatus == Constants.RESPONSE_JWT_EXPIRE) liveData.setValue(
                        Mutable(Constants.LOGOUT, statusMessage.mMessage)
                    )else if (statusMessage?.mStatus == Constants.RESPONSE_ENTER_SIZES) liveData.setValue(
                        Mutable(Constants.ENTER_SIZES, statusMessage.mMessage)
                    )
                    else {
                        if (statusMessage == null) {
                            liveData.value = Mutable(
                                Constants.ERROR,
                                ResourceManager.getString(R.string.please_check_your_connection)
                            )
                        } else
                            liveData.value = Mutable(Constants.ERROR, statusMessage.mMessage)
//                        liveData.value = Mutable(constantSuccessResponse, gson?.fromJson(jsonString, responseType))
                    }
                }

                override fun onError(t: Throwable?) {
                    Log.d(TAG, "" + t?.message)
                    hideProgress(showProgress)
                    liveData.value = Mutable(Constants.SERVER_ERROR)
                }

                override fun onComplete() {
                    Log.d(TAG, "complete")
                    hideProgress(showProgress)
                }
            })!!
    }

//    fun requestApiBackground(method: Int, url: String?, requestData: Any?): Disposable? {
//        var call: Flowable<JsonObject?>? = null
//        val map = getParameters(requestData)
//        call = if (method == Constants.POST_REQUEST) {
//            api.post(url, requestData) //here
//        } else if (method == Constants.GET_REQUEST) {
//            api.get(url, map)
//        } else if (method == Constants.DELETE_REQUEST) {
//            api.delete(url, map)
//        } else api.get(url, map)
//        return call.subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribeWith(object : DisposableSubscriber<JsonObject?>() {
//                    override fun onNext(response: JsonObject?) {
//                        Log.d(TAG, "onNext CALL BACKGROUND")
//                    }
//
//                    override fun onError(t: Throwable?) {
//                        Log.d(TAG, "ERROR CALL BACKGROUND")
//                    }
//
//                    override fun onComplete() {
//                        Log.d(TAG, "onComplete CALL BACKGROUND")
//                    }
//                })
//    }

    private fun hideProgress(showProgress: Boolean) {
        if (showProgress) liveData.value = Mutable(Constants.HIDE_PROGRESS)
    }

    private fun getParameters(requestData: Any?): MutableMap<String?, String?> {
        val params: MutableMap<String?, String?> = HashMap()
        if (requestData != null) {
            try {
                val jsonObject = JSONObject(gson!!.toJson(requestData))
                for (i in 0 until jsonObject.names().length()) {
                    params[jsonObject.names().getString(i)] =
                        jsonObject[jsonObject.names().getString(i)].toString() + ""
                }
            } catch (e: Exception) {
                e.stackTrace
            }
        }
        return params
    }


    companion object {
        private val TAG: String? = "ConnectionHelper"
    }

}