package grand.app.elqasar.connection

import com.google.gson.JsonObject
import io.reactivex.Flowable
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*

/**
 * Created by Elmohandes on 20/09/2018.
 */
interface Api {
    @Multipart
    @POST
    fun post(
        @Url url: String?,
        @QueryMap map: MutableMap<String?, String?>?,
        @Part file: MultipartBody.Part?
    ): Flowable<JsonObject?>?

    @POST
    fun post(
        @Url url: String?,
        @QueryMap map: MutableMap<String?, String?>?,
        @Body file: RequestBody?
    ): Flowable<JsonObject?>?


    @POST
    fun post(
        @Url url: String?,
        @QueryMap map: MutableMap<String?, String?>?
    ): Flowable<JsonObject?>?

    @POST
    fun post(@Url url: String?, @Body `object`: Any?): Flowable<JsonObject?>?

    @GET
    operator fun get(
        @Url url: String?,
        @QueryMap map: MutableMap<String?, String?>?
    ): Flowable<JsonObject?>?

    @DELETE
    fun delete(
        @Url url: String?,
        @QueryMap map: MutableMap<String?, String?>?
    ): Flowable<JsonObject?>?



//    fun post(url: String?, map: Map<String?, String?>?, file: MultipartBody.Part?): Flowable<JsonObject?>? {
//
//    }
}