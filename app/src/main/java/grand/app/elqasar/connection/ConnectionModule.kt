package grand.app.elqasar.connection

import android.util.Log
import dagger.Module
import dagger.Provides
import grand.app.elqasar.utils.URLS
import grand.app.elqasar.utils.session.UserHelper

import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class ConnectionModule {
    private var retrofit: Retrofit? = null

    @Singleton
    @Provides
    fun webService(): Api? {
        if (retrofit == null) {
            Log.d(TAG, "retrofit")
            val interceptor = HttpLoggingInterceptor()
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)

            val okHttpClientBuilder = OkHttpClient.Builder() //.addInterceptor(interceptor)
                .connectTimeout(120, TimeUnit.SECONDS)
                .readTimeout(120, TimeUnit.SECONDS)
                .writeTimeout(120, TimeUnit.SECONDS)
                .addInterceptor { chain ->
                    val newRequest = chain.request().newBuilder()
                        .addHeader("jwt", UserHelper.getUserDetails().jwtToken)
                        .build()
                    chain.proceed(newRequest)
                }.addInterceptor(interceptor)

            //                    .connectionSpecs(Collections.singletonList(spec))
//            okHttpClientBuilder.addInterceptor(logging)
//            okHttpClientBuilder.addInterceptor { chain ->
//                val request = chain.request()
//                if (UserHelper.getUserId() != -1) {
////                    Log.d("jwt", UserHelper.getUserDetails().jwt_token);
//                    request.newBuilder().header("jwt",UserHelper.getUserDetails().jwtToken)
//                }
//                request.newBuilder().header("lang", LanguagesHelper.getCurrentLanguage())
//                chain.proceed(request)
//            }
            retrofit = Retrofit.Builder()
                .baseUrl(URLS.BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClientBuilder.build())
                .build()
        }
        return retrofit?.create(Api::class.java)
    }

    companion object {
        var bufferSize = 256 * 1024
        private val TAG: String? = "ConnectionModule"
    }

    /*

     */
}