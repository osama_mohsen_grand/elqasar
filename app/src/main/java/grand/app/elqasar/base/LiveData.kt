package grand.app.elqasar.base

import androidx.lifecycle.MutableLiveData
import dagger.Module
import dagger.Provides
import grand.app.elqasar.model.base.Mutable
import javax.inject.Singleton

@Module
class LiveData {

    @Singleton
    @Provides
    fun getMutableLiveData(): MutableLiveData<Mutable?>? {
        return MutableLiveData()
    }
}