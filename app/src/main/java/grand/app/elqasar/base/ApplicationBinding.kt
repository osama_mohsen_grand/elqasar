package grand.app.elqasar.base

import android.graphics.Color
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.RatingBar
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType
import com.smarteist.autoimageslider.SliderAnimations
import com.smarteist.autoimageslider.SliderView
import grand.app.elqasar.pages.product.model.details.Image
import grand.app.elqasar.pages.product.slider.SliderAdapterExample
import grand.app.elqasar.utils.helper.AppHelper
import java.util.*


private val TAG: String? = "ApplicationBinding"

@BindingAdapter("imageUrl")
fun loadImage(imageView: ImageView?, image: Any?) {

    if (image is String) {
        imageView?.let {
            Glide.with(it.context)
                .load(image)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .fitCenter()
                .into(it)
        }
    } else if (image is Int) {
        imageView?.setImageResource(image)
    }
}


@BindingAdapter("item_width")
fun itemWidth(view: View, divider: String) {
    val layoutParams = view.layoutParams
    val width: Int = AppHelper.getScreenWidth(view.context) / divider.toInt()
    layoutParams.width = width
    layoutParams.height = width
    view.layoutParams = layoutParams
}


@BindingAdapter("item_width_height")
fun itemWidthHeight(view: View, height: Int) {
    Log.d(TAG,"height:${height}")
    val layoutParams = view.layoutParams
    val width: Int = AppHelper.getScreenWidth(view.context) / 2
    layoutParams.width = width
    layoutParams.height = (height/2) - 10
    view.layoutParams = layoutParams
}


@BindingAdapter("imageZoomUrl")
fun loadZoomImage(imageView: ImageView?, image: String?) {
    imageView?.let {
        Glide
            .with(it.context)
            .load(image)
            .fitCenter() //                .placeholder(R.drawable.progress_animation)
            .into(it)
    }
}

@BindingAdapter("color")
fun color(imageView: ImageView?, color: String?) {
    if (color != null && color != "" && color[0] == '#') {
        imageView?.setBackgroundColor(Color.parseColor(color))
    }
}

@BindingAdapter("app:adapter", "app:span", "app:orientation")
fun getItemsV2Binding(
    recyclerView: RecyclerView,
    itemsAdapter: RecyclerView.Adapter<*>?,
    spanCount: String,
    orientation: String?
) {
    recyclerView.apply {
        adapter = itemsAdapter
        setHasFixedSize(true)
        setItemViewCacheSize(30)
        layoutManager = GridLayoutManager(context,spanCount.toInt(),
            if (orientation == "1") LinearLayoutManager.VERTICAL else LinearLayoutManager.HORIZONTAL,
            false)
    }
}

//@BindingAdapter("slider")
//fun setSlider(slider: ImageSlider, images: ArrayList<Image>) {
//    val imageUrls = arrayListOf<String>()
//    for(image in images)
//        imageUrls.add(image.image)
//    slider.adapter = SliderAdapter(
//        slider.context,
//        PicassoImageLoaderFactory(),
//        imageUrls = imageUrls,
//        descriptions = imageUrls
//    )
//}


@BindingAdapter("slider")
fun setSlider(sliderView: SliderView, images: List<Image>) {
    Log.e("slider", "start")
    val sliderItems: MutableList<Image> = ArrayList<Image>()
    for (i in images.indices) {
        sliderItems.add(images[i])
    }
    //        SliderAdapter adapter = new SliderAdapter(sliderView.getContext(), sliderItems);
    val adapter = SliderAdapterExample(sliderView.context, sliderItems)
    sliderView.setSliderAdapter(adapter)
    sliderView.setIndicatorAnimation(IndicatorAnimationType.WORM) //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
    //DEPTHTRANSFORMATION
    sliderView.setSliderTransformAnimation(SliderAnimations.CUBEINDEPTHTRANSFORMATION)
    sliderView.autoCycleDirection = SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH
    sliderView.indicatorSelectedColor = Color.WHITE
    sliderView.indicatorUnselectedColor = Color.GRAY
    sliderView.scrollTimeInSec = 5
    sliderView.isAutoCycle = true
    sliderView.startAutoCycle()
}

@BindingAdapter("rate")
fun setRate(ratingBar: RatingBar?, rate: Float) {
    ratingBar?.let {
        it.rating = rate
    }
}
