package grand.app.elqasar.base.di

import dagger.Component
import grand.app.elqasar.activity.BaseActivity
import grand.app.elqasar.activity.MainActivity
import grand.app.elqasar.base.LiveData
import grand.app.elqasar.pages.auth.code.VerifyCodeFragment
import grand.app.elqasar.pages.auth.forgetpassword.ForgetPasswordFragment
import grand.app.elqasar.pages.auth.login.LoginFragment
import grand.app.elqasar.pages.auth.register.RegisterFragment
import grand.app.elqasar.pages.home.HomeFragment
import grand.app.elqasar.pages.loginorregister.LoginOrRegisterFragment
import grand.app.elqasar.pages.splash.SplashFragment
//import grand.app.elqasar.activity.MainActivity
import grand.app.elqasar.connection.ConnectionModule
import grand.app.elqasar.pages.auth.changepassword.ChangePasswordFragment
import grand.app.elqasar.pages.product.ui.ProductListSliderFragment
import grand.app.elqasar.pages.account.AccountSettingFragment
import grand.app.elqasar.pages.address.add.AddAddressFragment
import grand.app.elqasar.pages.address.list.ui.AddressListFragment
import grand.app.elqasar.pages.cart.ui.CartDetailsFragment
import grand.app.elqasar.pages.cart.ui.CartFragment
import grand.app.elqasar.pages.cart.ui.CartPaymentFragment
import grand.app.elqasar.pages.favourite.ui.FavouriteFragment
import grand.app.elqasar.pages.makeyourclothes.accessories.AccessoriesFragment
import grand.app.elqasar.pages.makeyourclothes.clothesinfo.ClothesInfoFragment
import grand.app.elqasar.pages.makeyourclothes.common.CommonClothesMadeFragment
import grand.app.elqasar.pages.makeyourclothes.fabric.ui.FabricFragment
import grand.app.elqasar.pages.makeyourclothes.tailermade.ui.TailorMadeFragment
import grand.app.elqasar.pages.notes.NotesFragment
import grand.app.elqasar.pages.online.OnlineFragment
import grand.app.elqasar.pages.order.ui.OrderDetailsFragment
import grand.app.elqasar.pages.order.ui.OrderListFragment
import grand.app.elqasar.pages.order.ui.OrderStatusFragment
import grand.app.elqasar.pages.payment.PaymentFragment
import grand.app.elqasar.pages.product.ui.ProductDetailsFragment
import grand.app.elqasar.pages.product.ui.ProductListFragment
import grand.app.elqasar.pages.search.SearchFragment
import grand.app.elqasar.pages.settings.contact.ContactUsFragment
import grand.app.elqasar.pages.settings.notification.NotificationListFragment
import grand.app.elqasar.pages.settings.term.SettingsFragment
import grand.app.elqasar.pages.size.ui.MySizesFragment
import grand.app.elqasar.pages.size.ui.SizesFragment
import grand.app.elqasar.pages.social.SocialFragment
//import grand.app.elqasar.pages.splash.SplashFragment
import javax.inject.Singleton

//Component refer to an interface or waiter for make an coffee cup to me
@Singleton
@Component(modules = [ConnectionModule::class, LiveData::class])
interface IApplicationComponent {
//    open fun inject(mainActivity: MainActivity?)
    open fun inject(tmpActivity: BaseActivity)
    fun inject(tmpActivity: SplashFragment)
    fun inject(mainActivity: MainActivity)
    fun inject(loginOrRegisterFragment: LoginOrRegisterFragment)
    fun inject(registerFragment: RegisterFragment)
    fun inject(forgetPasswordFragment: ForgetPasswordFragment)
    fun inject(loginFragment: LoginFragment)
    fun inject(homeFragment: HomeFragment)
    fun inject(verifyCodeFragment: VerifyCodeFragment)
    fun inject(changePasswordFragment: ChangePasswordFragment)
    fun inject(accountSettingFragment: AccountSettingFragment)
    fun inject(productListSliderFragment: ProductListSliderFragment)
    fun inject(settingsFragment: SettingsFragment)
    fun inject(sizesFragment: SizesFragment)
    fun inject(mySizesFragment: MySizesFragment)
    fun inject(socialFragment: SocialFragment)
    fun inject(contactUsFragment: ContactUsFragment)
    fun inject(productDetailsFragment: ProductDetailsFragment)
    fun inject(productListFragment: ProductListFragment)
    fun inject(addAddressFragment: AddAddressFragment)
    fun inject(addressListFragment: AddressListFragment)
    fun inject(fabricFragment: FabricFragment)
    fun inject(tailorMadeFragment: TailorMadeFragment)
    fun inject(commonClothesMadeFragment: CommonClothesMadeFragment)
    fun inject(accessoriesFragment: AccessoriesFragment)
    fun inject(cartFragment: CartFragment)
    fun inject(cartDetailsFragment: CartDetailsFragment)
    fun inject(cartPaymentFragment: CartPaymentFragment)
    fun inject(notificationListFragment: NotificationListFragment)
    fun inject(orderListFragment: OrderListFragment)
    fun inject(orderStatusFragment: OrderStatusFragment)
    fun inject(orderDetailsFragment: OrderDetailsFragment)
    fun inject(favouriteFragment: FavouriteFragment)
    fun inject(searchFragment: SearchFragment)
    fun inject(notesFragment: NotesFragment)
    fun inject(clothesInfoFragment: ClothesInfoFragment)
    fun inject(onlineFragment: OnlineFragment)
    fun inject(paymentFragment: PaymentFragment)

//    fun inject(baseFragmentBinding: BaseFragmentBinding<ViewDataBinding, ViewModel>)
//    fun inject(layoutRes: Int)
//
//    fun inject(baseFragmentBinding: BaseFragmentBinding<DB, VM>) {
//
//    }

//    fun <DB, VM> inject(baseFragmentBinding: BaseFragmentBinding<DB, VM>)
//    open fun inject(splashFragment: SplashFragment?)

    @Component.Builder
    interface Builder {
        open fun build(): IApplicationComponent?
    }
}