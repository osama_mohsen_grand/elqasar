package grand.app.elqasar.base

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import grand.app.elqasar.R
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.helper.IOBackCommonPressed
import grand.app.elqasar.utils.upload.FileOperations

open class BaseFragment : Fragment() {

    lateinit var context2: Context

    private val TAG = "BaseFragment"

    private fun getContextBase(): Context {
        return context?: context2
    }

    protected fun pickImageDialogSelect() {
        FileOperations.pickImage(getContextBase(), this@BaseFragment, Constants.FILE_TYPE_IMAGE)
    }

    protected fun pickDocument() {
        FileOperations.pickDocument(
            if (context != null) context else context2,
            this@BaseFragment,
            Constants.FILE_TYPE_DOCUMENT
        )
    }

    var dialogLoader: Dialog? = null
    private fun initializeProgress() {
        //TODO loader animation
        val view: View =
            LayoutInflater.from(getContextBase()).inflate(R.layout.loader_animation, null)
        val builder = AlertDialog.Builder(getContextBase(), R.style.customDialog)
        builder.setView(view)
//        dialogLoader?.setCancelable(false);
        dialogLoader = builder.create()
        //        dialogLoader.setCancelable(false);
        dialogLoader?.setOnCancelListener(DialogInterface.OnCancelListener {
            dialogLoader?.dismiss()
            getActivityBase().onBackPressed()
//            finishActivity()
        })
    }

    private fun showProgress() {
        initializeProgress()
        //show dialog
        if (dialogLoader != null) {
            Log.d(TAG, "isFinishing , dialogLoader")
            dialogLoader!!.show()
        } else {
            showProgress()
            Log.d(TAG, "isFinishing $TAG")
        }
    }

    private fun hideProgress() {
        if (dialogLoader != null && dialogLoader!!.isShowing) dialogLoader!!.dismiss()
    }


    fun handleActions(mutable: Mutable) {
        if (mutable.message == Constants.SHOW_PROGRESS) showProgress()
        else if (mutable.message == Constants.HIDE_PROGRESS) hideProgress()
        else if (mutable.message == Constants.SERVER_ERROR && mutable.obj == null) {
            hideProgress()
        } else if (mutable.message == Constants.ERROR && mutable.obj is String) {
            hideProgress()
        } else if (mutable.message == Constants.FAILURE_CONNECTION) {
            hideProgress()
        }

        getActivityBase().handleActions(mutable)
    }

    fun showError(msg: String) {
        getActivityBase().showError(msg)
    }

    private fun getActivityBase(): ParentActivity {
        return (if (context != null) context else context2) as ParentActivity
    }

    fun toastMessage(message: String?, icon: Int, color: Int) {
        getActivityBase().toastMessage(message, icon, color)
    }

    fun toastMessage(message: String?) {

        getActivityBase().toastMessage(
            message,
            R.drawable.ic_check_white_24dp,
            R.color.colorPrimary
        )
    }

    protected fun finishActivity() {
        getActivityBase().finish()
    }

    protected fun finishAllActivities() {
        getActivityBase().finishAffinity()
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        this.context2 = context
    }


}