package grand.app.elqasar.base

import android.content.Context
import androidx.multidex.MultiDexApplication
import grand.app.elqasar.base.di.DaggerIApplicationComponent
import grand.app.elqasar.base.di.IApplicationComponent

class MyApplication : MultiDexApplication() {
    var applicationComponent: IApplicationComponent? = null



    override fun onCreate() {
        super.onCreate()
        instance = this
        applicationComponent = DaggerIApplicationComponent.builder().build()

    }

    fun setConnectivityListener(listener: ConnectivityReceiver.ConnectivityReceiverListener?) {
        ConnectivityReceiver.Companion.connectivityReceiverListener = listener
    }

    companion object {
        lateinit var instance: MyApplication
            private set
    }

    override fun getApplicationContext(): Context {
        return super.getApplicationContext()
    }
}