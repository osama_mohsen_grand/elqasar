package grand.app.elqasar.base

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.location.Location
import android.location.LocationListener
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.MutableLiveData
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.iid.FirebaseInstanceId
import es.dmoral.toasty.Toasty
import grand.app.elqasar.R
import grand.app.elqasar.activity.BaseActivity
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.auth.repository.AuthRepository
import grand.app.elqasar.pages.loginorregister.LoginOrRegisterFragment
import grand.app.elqasar.pages.size.ui.MySizesFragment
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.dialog.DialogLogin
import grand.app.elqasar.utils.helper.AppHelper
import grand.app.elqasar.utils.helper.MovementHelper
import grand.app.elqasar.utils.resources.ResourceManager
import grand.app.elqasar.utils.session.LanguagesHelper
import grand.app.elqasar.utils.session.MyContextWrapper
import grand.app.elqasar.utils.session.UserHelper
import org.imaginativeworld.oopsnointernet.NoInternetDialog

open class ParentActivity : AppCompatActivity(), GoogleApiClient.OnConnectionFailedListener,
    GoogleApiClient.ConnectionCallbacks, LocationListener {
    // No Internet Dialog
    var noInternetDialog: NoInternetDialog? = null
    var ConnectionLiveData: MutableLiveData<Boolean?>? = null
    private val connectivityReceiver: ConnectivityReceiver? = ConnectivityReceiver()
    lateinit var dialogLogin : DialogLogin
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ConnectionLiveData = MutableLiveData()
        //        Thread.setDefaultUncaughtExceptionHandler(new MyExceptionHandler(this,
//                ParentActivity.class));
        initializeLanguage()
//        initializeProgress()
        checkConnection()
    }


    protected var notification_checked = false
    protected fun initializeLanguage() {
        LanguagesHelper.changeLanguage(this, LanguagesHelper.getCurrentLanguage())
        LanguagesHelper.changeLanguage(MyApplication.instance, LanguagesHelper.getCurrentLanguage())
    }

    //    @Inject
    //    AuthRepository repository;
    //
    //    protected void initializeToken() {
    //        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(ParentActivity.this,
    //                new OnSuccessListener<InstanceIdResult>() {
    //                    @Override
    //                    public void onSuccess(InstanceIdResult instanceIdResult) {
    //                        String newToken = instanceIdResult.getToken();
    //                        UserHelper.saveKey(Constants.TOKEN, newToken);
    //                        if (UserHelper.getUserId() != -1) {
    //                            //update token service
    ////                            repository.updateToken(newToken);
    //                        }
    //                    }
    //                });
    //    }


    protected fun initializeToken(authRepository: AuthRepository) {
        if(UserHelper.isLogin()) {
            FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener(
                this@ParentActivity
            ) { instanceIdResult ->
                val newToken = instanceIdResult.token
                UserHelper.saveKey(Constants.TOKEN, newToken)
                authRepository.updateToken(newToken)
            }
        }
    }

    public override fun onResume() {
        super.onResume()
    }


    public override fun onPause() {
        super.onPause()
        // No Internet Dialog
    }

    override fun onStop() {
        super.onStop()
        //        if (null != connectivityReceiver) {
//            unregisterReceiver(connectivityReceiver);
//            connectivityReceiver = null;
//        }
    }

    override fun attachBaseContext(newBase: Context?) {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.N_MR1) {
            super.attachBaseContext(
                MyContextWrapper.wrap(
                    newBase,
                    LanguagesHelper.getCurrentLanguage()
                )
            )
        } else {
            super.attachBaseContext(newBase)
        }
    }

//    protected var dialogLoader: Dialog? = null
//    private fun initializeProgress() {
//        Log.d(TAG,"initializeProgress")
//        //TODO loader animation
//        val view: View =
//            LayoutInflater.from(this).inflate(R.layout.loader_animation,null)
//        val builder = AlertDialog.Builder(this, R.style.customDialog)
//        builder.setView(view)
//        dialogLoader = builder.create()
////        dialogLoader.setCancelable(false);
//        //        dialogLoader.setCancelable(false);
//        dialogLoader?.setOnCancelListener(DialogInterface.OnCancelListener {
//            dialogLoader?.dismiss()
//            onBackPressed()
//        })
//    }

//    fun showProgress() {
//        //show dialog
//        Log.d(TAG,"showProgress")
//        if (dialogLoader != null && !this.isFinishing) {
//            Log.d(TAG,"isFinishing , dialogLoader")
//            dialogLoader!!.show()
//        }else{
//            Log.d(TAG, "isFinishing $TAG")
//        }
//    }

//    fun hideProgress() {
//        if (dialogLoader != null && dialogLoader!!.isShowing && !this.isFinishing) dialogLoader!!.dismiss()
//    }

    fun handleActions(mutable: Mutable?) {
        mutable?.let {
//            if (mutable.message == Constants.SHOW_PROGRESS) showProgress() else if (mutable.message == Constants.HIDE_PROGRESS) hideProgress()
//            else if (mutable.message == Constants.SERVER_ERROR && mutable.obj == null) {
//                hideProgress()
//                //            showError(ResourceManager.getString(R.string.msg_server_error));
//            } else
            if (mutable.message == Constants.ERROR && mutable.obj is String) {
//                hideProgress()
                showError(mutable.obj as String)
            } else if (mutable.message == Constants.ERROR_TOAST && mutable.obj is String) {
                toastError(mutable.obj as String)
            } else if (mutable.message == Constants.FAILURE_CONNECTION) {
//                hideProgress()
                noConnection()
            } else if (mutable.message == Constants.HIDE_KEYBOARD) {
                AppHelper.hideKeyboard(this)
            } else if (mutable.message == Constants.LOGOUT) {
                if (UserHelper.isLogin()) {
                    UserHelper.clearUserDetails()
                    finishAffinity()
                    val bundle: Bundle = Bundle()
                    bundle.putString(Constants.PAGE, LoginOrRegisterFragment::class.java.name)
                    MovementHelper.startActivityBase(this, bundle);
                }else {
                    dialogLogin.show()
                }
            } else if(mutable.message == Constants.ENTER_SIZES){

                val bundle: Bundle = Bundle()
                bundle.putString(Constants.PAGE, MySizesFragment::class.java.name)
                bundle.putString(Constants.TYPE,getString(R.string.cart))
                bundle.putBoolean(Constants.CART,true)
                MovementHelper.startActivityBase(this, bundle);
            }
            if ((mutable.message == Constants.SERVER_ERROR)) {
                showError(ResourceManager.getString(R.string.please_check_your_connection));
            } else if ((mutable.message == Constants.ERROR_RESPONSE)) {
                showError(ResourceManager.getString(R.string.something_was_happen_please_try_again));
            }
        }

    }

    private fun noConnection() {
        showError("no connection")
    }

    // Method to manually check connection status
    fun checkConnection() {
        ConnectionLiveData?.setValue(ConnectivityReceiver.Companion.isConnected())
    }


    fun showError(msg: String) {
        var message: String = msg
        if (msg == Constants.ERROR_LOGIN_RESPONSE) message = getString(R.string.please_login_first)
        val snackBar = Snackbar.make(
            findViewById(R.id.rl_base_container),
            message, Snackbar.LENGTH_LONG
        )
        val view = snackBar.view
        view.setBackgroundColor(ContextCompat.getColor(this, R.color.colorBlack))
        val textView = view.findViewById<TextView>(R.id.snackbar_text)
        textView.gravity = Gravity.CENTER_VERTICAL
        textView.setTextColor(ContextCompat.getColor(this, R.color.colorWhite))
        textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_warning, 0, 0, 0)
        snackBar.show()
    }

    fun toastMessage(message: String?, icon: Int, color: Int) {
        message?.let {
            Toasty.custom(
                this,
                it, icon, color, Toasty.LENGTH_SHORT, true, true
            ).show()
        }
    }

    fun toastError(message: String?) {
        if (message != null) {
            Toasty.error(this, message, Toasty.LENGTH_SHORT, true).show()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.d(TAG, "parentActivity $requestCode")
        Log.d(TAG, "parentActivity $resultCode")
//        try {
        if (resultCode == Activity.RESULT_OK) {
            val fragment = supportFragmentManager.findFragmentById(R.id.fl_home_container)!!
            fragment.onActivityResult(requestCode, resultCode, data)
        }
//        } catch (ex: Exception) {
//            Log.d(TAG,ex.message.toString())
//            Toast.makeText(this, ResourceManager.getString(R.string.please_select_another_file), Toast.LENGTH_SHORT).show()
//        }
    }

    override fun onLocationChanged(location: Location?) {
    }

    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {}
    override fun onProviderEnabled(provider: String?) {}
    override fun onProviderDisabled(provider: String?) {}
    override fun onConnected(bundle: Bundle?) {}
    override fun onConnectionSuspended(i: Int) {}
    override fun onConnectionFailed(connectionResult: ConnectionResult) {}

    companion object {
        private val TAG: String? = "ParentActivity"
    }
}