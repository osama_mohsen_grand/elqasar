package grand.app.elqasar.model.base

import android.view.View

class Mutable {
    var message: String
    var position = 0
    var obj: Any? = null
    var v: View? = null
    var number: Int = 0

    constructor() {
        message = ""
    }

    constructor(message: String) {
        this.message = message
    }

    constructor(message: String, obj: Any?) {
        this.message = message
        this.obj = obj
    }

    constructor(message: String, position: Int) {
        this.message = message
        this.position = position
    }

    constructor(message: String, position: Int, v: View?) {
        this.message = message
        this.position = position
        this.v = v
    }

    constructor(message: String, position: Int, v: Int) {
        this.message = message
        this.position = position
        this.number = number
    }
    
    
}