package grand.app.elqasar.model.base

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

open class StatusMessage {
    @SerializedName("message")
    @Expose
    var mMessage: String = ""

    @SerializedName("status")
    @Expose
    var mStatus = 0
}