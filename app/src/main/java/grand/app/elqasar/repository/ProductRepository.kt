package grand.app.elqasar.repository

import androidx.lifecycle.MutableLiveData
import grand.app.elqasar.connection.ConnectionHelper
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.favourite.model.FavouriteListResponse
import grand.app.elqasar.pages.makeyourclothes.fabric.model.FabricListResponse
import grand.app.elqasar.pages.makeyourclothes.tailermade.model.TailorMadeResponse
import grand.app.elqasar.pages.product.model.FavouriteRequest
import grand.app.elqasar.pages.product.model.FavouriteResponse
import grand.app.elqasar.pages.product.model.details.ProductDetailsResponse
import grand.app.elqasar.pages.product.model.list.ProductListResponse
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.URLS
import io.reactivex.disposables.Disposable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ProductRepository : CartRepository {

    @Inject
    constructor(connectionHelper: ConnectionHelper):super(connectionHelper){
        this.connectionHelper = connectionHelper;
    }

    private var liveData: MutableLiveData<Mutable> = MutableLiveData()
    fun setLiveData(liveData: MutableLiveData<Mutable>) {
        this.liveData = liveData
        connectionHelper.liveData = liveData
    }

    fun getProducts(type : Int) : Disposable {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.PRODUCT_LIST + "?type=$type", Any(), ProductListResponse::class.java,
            URLS.PRODUCT_LIST, true)
    }

    fun getProductDetails(id : Int) : Disposable {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.PRODUCT_DETAILS+ "?id=$id", Any(), ProductDetailsResponse::class.java,
            URLS.PRODUCT_DETAILS, true)
    }

    fun addFavourite(request : FavouriteRequest) : Disposable {
        return connectionHelper.requestApi(Constants.POST_REQUEST, URLS.ADD_FAVOURITE,request, FavouriteResponse::class.java,
            URLS.ADD_FAVOURITE, true)
    }


    fun getFavourite() : Disposable {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.FAVOURITE, Any(), FavouriteListResponse::class.java,
            URLS.FAVOURITE, true)
    }

    fun getCategories(type: Int = -1, cloth_id: Int = -1, detail_id: Int = -1) : Disposable {
        var request = ""
        if(type != -1) request = "?type=$type"
        if(cloth_id != -1) request += "&cloth_id=$cloth_id"
        if(detail_id != -1) request += "&detail_id=$detail_id"

        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.CATEGORIES+request, Any(),
            if(type == 2)  TailorMadeResponse::class.java else FabricListResponse::class.java,
            URLS.CATEGORIES, true)
    }

    fun search(text: String): Disposable {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.SEARCH+text, Any(), ProductListResponse::class.java,
            URLS.SEARCH, true)
    }
}