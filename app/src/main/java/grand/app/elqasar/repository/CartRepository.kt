package grand.app.elqasar.repository

import androidx.lifecycle.MutableLiveData
import grand.app.elqasar.connection.ConnectionHelper
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.model.base.StatusMessage
import grand.app.elqasar.pages.cart.model.*
import grand.app.elqasar.pages.makeyourclothes.fabric.model.ClothesItem
import grand.app.elqasar.pages.makeyourclothes.fabric.model.ClothesModelRequest
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.URLS
import io.reactivex.disposables.Disposable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
open class CartRepository  : BaseRepository {
    @Inject
    lateinit var connectionHelper: ConnectionHelper

    @Inject
    constructor(connectionHelper: ConnectionHelper){
        this.connectionHelper = connectionHelper;
    }

    private var liveData: MutableLiveData<Mutable> = MutableLiveData()

    fun getCart(): Disposable? {
        return connectionHelper.requestApi(
            Constants.GET_REQUEST, URLS.USER_CART, Any(),
            CartResponse::class.java, URLS.USER_CART, true
        )
    }

    fun updateCartClothesInfo(request: ClothesModelRequest): Disposable? {

        return connectionHelper.requestApi(
            Constants.POST_REQUEST, URLS.ADD_TO_CART_CLOTHES, request,
            CartResponse::class.java, URLS.ADD_TO_CART_CLOTHES, true
        )
    }


    fun updateCart(updateCartRequest: UpdateCartRequest): Disposable? {

        return connectionHelper.requestApi(
            Constants.POST_REQUEST, URLS.ADD_TO_CART, updateCartRequest,
            CartResponse::class.java, URLS.ADD_TO_CART, true
        )
    }

    fun deleteItemCart(id: Int): Disposable? {
        return connectionHelper.requestApi(
            Constants.DELETE_REQUEST, URLS.DELETE_ITEM_CART + id, Any(),
            CartResponse::class.java, Constants.DELETE, true
        )
    }
//
    fun checkout(request: CheckoutRequest): Disposable? {
        return connectionHelper.requestApi(
            Constants.POST_REQUEST, URLS.CHECKOUT, request,
            StatusMessage::class.java, URLS.CHECKOUT, true
        )
    }

    fun online(request: OnlineRequest): Disposable? {
        return connectionHelper.requestApi(
            Constants.GET_REQUEST, URLS.PAYMENT_ONLINE+request.invoice, request,
            OnlineResponse::class.java, URLS.PAYMENT_ONLINE, true
        )
    }
}