package grand.app.elqasar.repository

import androidx.lifecycle.MutableLiveData
import grand.app.elqasar.utils.Constants

open class BaseRepository {
    private var status = 0
    private var message: String? = ""
    private val mMutableLiveData: MutableLiveData<Any?>? = null

    //    public BaseRepository(MutableLiveData<Object> mMutableLiveData) {
    //        this.mMutableLiveData = mMutableLiveData;
    //    }
    fun catchErrorResponse(response: Any?): Boolean {
//        Log.d(TAG, "catchErrorResponse: response");
//        if(response instanceof NoConnectivityException) {
//            Log.d(TAG, "NoConnectivityException: ");
//            mMutableLiveData.setValue(Constants.HIDE_PROGRESS);
//            NoConnectivityException throwable = (NoConnectivityException) response;
//            message = throwable.getMessage();
//            Timber.e("NoConnectivityException2:"+message);
//            mMutableLiveData.setValue(Constants.FAILURE_CONNECTION);
//            return true;
//        }else if(response instanceof Throwable){
//            Log.d(TAG, "Throwable: ");
//            mMutableLiveData.setValue(Constants.HIDE_PROGRESS);
//            Throwable throwable = (Throwable) response;
//            message = throwable.getMessage();
//            mMutableLiveData.setValue(Constants.SERVER_ERROR);
//            return true;
//        }
//        Log.d(TAG, "failed: ");
        return false
    }

    fun getMessage(): String? {
        return message
    }

    fun getStatus(): Int {
        return status
    }

    fun setMessage(status: Int, message: String?) {
        this.status = status
        this.message = message
        if (status == Constants.RESPONSE_JWT_EXPIRE) {
            if (mMutableLiveData != null) mMutableLiveData.value = Constants.LOGOUT
        }
    }

    fun getmMutableLiveData(): MutableLiveData<Any?>? {
        return mMutableLiveData
    }

    companion object {
        private val TAG: String? = "BaseRepository"
    }
}