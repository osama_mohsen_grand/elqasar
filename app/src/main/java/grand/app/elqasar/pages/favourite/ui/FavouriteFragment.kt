package grand.app.elqasar.pages.favourite.ui

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import grand.app.elqasar.R
import grand.app.elqasar.base.BaseFragment
import grand.app.elqasar.base.MyApplication
import grand.app.elqasar.base.di.IApplicationComponent
import grand.app.elqasar.databinding.FragmentFavouriteBinding
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.cart.model.CartResponse
import grand.app.elqasar.pages.favourite.model.FavouriteListResponse
import grand.app.elqasar.pages.favourite.viewmodel.FavouriteViewModel
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.URLS
import grand.app.elqasar.utils.session.SharedPreferenceHelper
import javax.inject.Inject

class FavouriteFragment : BaseFragment() {
    private lateinit var binding: FragmentFavouriteBinding

    @Inject
    lateinit var viewModel: FavouriteViewModel

    private val TAG = this::class.java.name

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_favourite, container, false)
        val component: IApplicationComponent? =
            (context!!.applicationContext as MyApplication).applicationComponent
        component?.inject(this)
        binding.viewmodel = viewModel
        // Inflate the layout for this fragment
        setEvent()
        return binding.root
    }

    private fun setEvent() {
        viewModel.liveDataViewModel.observe(requireActivity(), Observer {
            val mutable = it as Mutable
            handleActions(mutable)
            Log.d(TAG,mutable.message)
            if (mutable.message == URLS.FAVOURITE) {
                Log.d(TAG,"done")
                viewModel.response = mutable.obj as FavouriteListResponse
                viewModel.setData()
            }  else if (mutable.message == URLS.ADD_FAVOURITE) {
                viewModel.adapter.removeFavourite()
            }  else if (mutable.message == URLS.ADD_TO_CART) {
                val response = mutable.obj as CartResponse
                toastMessage(response.mMessage)
                SharedPreferenceHelper.saveKey(Constants.CART,response.data.cartCount.toString())
            }

        })
    }




    override fun onResume() {
        super.onResume()
        viewModel.repository.setLiveData(viewModel.liveDataViewModel)
    }
}