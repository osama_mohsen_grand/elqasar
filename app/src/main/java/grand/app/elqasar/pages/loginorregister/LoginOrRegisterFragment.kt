package grand.app.elqasar.pages.loginorregister

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import grand.app.elqasar.R
import grand.app.elqasar.activity.MainActivity
import grand.app.elqasar.base.BaseFragment
import grand.app.elqasar.base.MyApplication
import grand.app.elqasar.base.di.IApplicationComponent
import grand.app.elqasar.databinding.FragmentLoginOrRegisterBinding
import grand.app.elqasar.pages.auth.login.LoginFragment
import grand.app.elqasar.pages.auth.register.RegisterFragment
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.helper.MovementHelper
import javax.inject.Inject

class LoginOrRegisterFragment : BaseFragment() {
    private lateinit var binding: FragmentLoginOrRegisterBinding
    @Inject
    lateinit var viewModel: LoginOrRegisterViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        binding.viewmodel = viewModel
//        setEvent()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_login_or_register, container, false)
        val component: IApplicationComponent? =
            (context!!.applicationContext as MyApplication).applicationComponent
        component?.inject(this)
        binding.viewmodel = viewModel
        setEvent()
        // Inflate the layout for this fragment
        return binding.root
    }


    private fun setEvent() {
        viewModel.liveDataViewModel.observe(requireActivity(), Observer {
            val mutable = it as Mutable
            handleActions(mutable)
            if (mutable.message == Constants.LOGIN) {
                val bundle = Bundle()
                bundle.putString(Constants.PAGE, LoginFragment::class.java.name)
                MovementHelper.startActivityBase(context, bundle, "")
            } else if (mutable.message == Constants.REGISTER) {
                val bundle = Bundle()
                bundle.putString(Constants.PAGE, RegisterFragment::class.java.name)
                MovementHelper.startActivityBase(context, bundle, getString(R.string.register))
            }else if (mutable.message == Constants.SKIP) {
                MovementHelper.startActivity(context,MainActivity::class.java)
            }
        })
    }
    override fun onResume() {
        super.onResume()
        viewModel.liveDataViewModel = viewModel.liveDataViewModel
    }
}