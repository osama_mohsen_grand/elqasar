package grand.app.elqasar.pages.makeyourclothes.accessories.viewmodel

import android.os.Bundle
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import grand.app.elqasar.base.BaseViewModel
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.makeyourclothes.accessories.AccessoriesAdapter
import grand.app.elqasar.pages.makeyourclothes.fabric.model.ClothesItem
import grand.app.elqasar.pages.makeyourclothes.fabric.model.ClothesModelRequest
import grand.app.elqasar.pages.makeyourclothes.fabric.model.FabricListResponse
import grand.app.elqasar.repository.ProductRepository
import grand.app.elqasar.utils.Constants
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class AccessoriesViewModel : BaseViewModel {
    @Inject
    lateinit var repository: ProductRepository;
    private val TAG = this::class.java.name
    lateinit var compositeDisposable: CompositeDisposable
    lateinit var adapter: AccessoriesAdapter
    var liveDataViewModel: MutableLiveData<Mutable>
    lateinit var response: FabricListResponse
    var title : ObservableField<String> = ObservableField("")
//    var cloth_id: Int = 0
//    var detail_id: Int = 0
    @Inject
    constructor(repository: ProductRepository) {
        this.repository = repository
        adapter = AccessoriesAdapter(repository = repository)
        this.liveDataViewModel = MutableLiveData();
        repository.setLiveData(liveDataViewModel)
    }

    fun setArgument(bundle: Bundle) {
        title.set(bundle.getString(Constants.TITLE))
        adapter.type = bundle.getInt(Constants.TYPE)
        adapter.request = bundle.getSerializable(Constants.REQUEST_MODELS) as ClothesItem
        if(bundle.containsKey(Constants.IS_BACK))
            adapter.isBack = bundle.getBoolean(Constants.IS_BACK)
        if(bundle.containsKey(Constants.ALLOW_ACTION))
            adapter.allowAction = bundle.getBoolean(Constants.ALLOW_ACTION)
        repository.getCategories(adapter.type,adapter.request.cloth_id,adapter.request.detail_id)
    }
    fun back(){
        liveDataViewModel.value = Mutable(Constants.BACK)
    }

    fun escape(){
        liveDataViewModel.value = Mutable(Constants.ESCAPE)
    }


    private fun unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }

    override fun onCleared() {
        super.onCleared()
        unSubscribeFromObservable()
    }

    fun setData(response: FabricListResponse) {
        this.response = response
        for(model in response.data) model.qty = 1
        adapter.update(response.data)
        adapter.setSelectedIfExist()
        show.set(true)
    }
}

