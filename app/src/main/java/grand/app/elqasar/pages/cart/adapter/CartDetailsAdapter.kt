package grand.app.elqasar.pages.cart.adapter

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.elqasar.R
import grand.app.elqasar.base.BaseFragment
import grand.app.elqasar.databinding.ItemCartDetailsBinding
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.cart.model.Cart
import grand.app.elqasar.pages.cart.model.UpdateCartRequest
import grand.app.elqasar.pages.cart.viewmodel.ItemCartViewModel
import grand.app.elqasar.pages.makeyourclothes.clothesinfo.ClothesInfoFragment
import grand.app.elqasar.pages.makeyourclothes.fabric.model.ClothesModelRequest
import grand.app.elqasar.pages.product.ui.ProductDetailsFragment
import grand.app.elqasar.repository.ProductRepository
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.helper.MovementHelper
import grand.app.elqasar.utils.resources.ResourceManager

class CartDetailsAdapter(var modelList:ArrayList<Cart> = ArrayList(), private var repository: ProductRepository? = null) : RecyclerView.Adapter<CartDetailsAdapter.ViewHolder>() {
    // fetch list data
    var favPosition : Int = 0
    var selectedItem : Int = -1
    private lateinit var fragment : BaseFragment
    var isOrderDetails = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // binding item_model layout
        val binding: ItemCartDetailsBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_cart_details,
            parent,
            false
        )
        return ViewHolder(
            binding
        )
    }

    fun setFragment(fragment: BaseFragment){
        this.fragment = fragment
    }

    private val TAG = "CartDetailsAdapter"

    val request: ClothesModelRequest = ClothesModelRequest()
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(modelList[position])
        holder.viewModel.mutableLiveData.observeForever {
            val mutable: Mutable = it as Mutable
            selectedItem = it.position
            val model = modelList[selectedItem]

            if(mutable.message == Constants.SUBMIT) {
                val bundle = Bundle()
                if(model.type != 1) {
                    bundle.putString(Constants.PAGE, ProductDetailsFragment::class.java.name)
                    bundle.putInt(Constants.ID, model.productId)
                    bundle.putInt(Constants.TYPE, model.type)
                    bundle.putString(Constants.TITLE, model.name)
                    MovementHelper.startActivityBase(holder.itemView.context, bundle);
                }else{
                    bundle.putString(Constants.PAGE, ClothesInfoFragment::class.java.name)
                    if(isOrderDetails) bundle.putBoolean(Constants.ALLOW_ACTION, false)
                    else bundle.putBoolean(Constants.ALLOW_ACTION, true)
                    bundle.putBoolean(Constants.IS_CREATE, false)
                    bundle.putBoolean(Constants.IS_BACK, true)
                    Log.d(TAG,"${model.id}")

                    bundle.putSerializable(Constants.CART, model)
                    MovementHelper.startActivityBaseForResult(
                        fragment,
                        bundle,
                        Constants.REQUEST_MODELS_RESULT,
                        ResourceManager.getString(R.string.clothe_info))
//                    MovementHelper.startActivityBase(holder.itemView.context, bundle,ResourceManager.getString(R.string.clothe_info));
                }
            }else if(mutable.message == Constants.ADD){
                val model = modelList[mutable.position]
                model.qty++
                repository!!.updateCart(UpdateCartRequest(model.productId,model.qty,model.type,model.id))
            }else if(mutable.message == Constants.MINUS){
                val model = modelList[mutable.position]
                if(model.qty > 1) {
                    model.qty--
                    repository!!.updateCart(UpdateCartRequest(model.productId,model.qty,model.type,model.id))
                }
            }else if(mutable.message == Constants.DELETE){
                val model = modelList[mutable.position]
                repository?.deleteItemCart(model.id)
            }
            notifyItemChanged(selectedItem)
        }
    }

    override fun getItemCount(): Int {
        //return data count if exist or 0
        return modelList.size
    }


    fun updateFavourite(favorite: Int) {
        modelList[favPosition].userFavorite = favorite
        notifyItemChanged(favPosition)
    }


    fun update(modelList: ArrayList<Cart>) {
        //update data after call service again in scroll , and notify list which end with
        this.modelList = modelList
        notifyDataSetChanged()
    }

    class ViewHolder(private val binding: ItemCartDetailsBinding) : RecyclerView.ViewHolder(binding.root) {
        lateinit var viewModel: ItemCartViewModel

        //bint
        fun bind(model: Cart) {
            viewModel = ItemCartViewModel(model, adapterPosition)
            binding.viewmodel = viewModel
        }
    }
}