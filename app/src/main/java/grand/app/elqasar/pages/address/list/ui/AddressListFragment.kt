package grand.app.elqasar.pages.address.list.ui

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import grand.app.elqasar.R
import grand.app.elqasar.base.BaseFragment
import grand.app.elqasar.base.MyApplication
import grand.app.elqasar.base.di.IApplicationComponent
import grand.app.elqasar.databinding.FragmentAddressListBinding
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.address.add.AddAddressFragment
import grand.app.elqasar.pages.address.list.AddressListViewModel
import grand.app.elqasar.pages.address.list.model.Address
import grand.app.elqasar.pages.address.list.model.AddressListResponse
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.URLS
import grand.app.elqasar.utils.helper.MovementHelper
import javax.inject.Inject

class AddressListFragment : BaseFragment() {
    private lateinit var binding: FragmentAddressListBinding

    @Inject
    lateinit var viewModel: AddressListViewModel

    private val TAG = this::class.java.name

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_address_list, container, false)
        val component: IApplicationComponent? =
            (context!!.applicationContext as MyApplication).applicationComponent
        component?.inject(this)
        binding.viewmodel = viewModel
        arguments?.let { viewModel.setArgument(it) }
        // Inflate the layout for this fragment
        setEvent()
        return binding.root
    }

    private fun setEvent() {
        viewModel.liveDataViewModel.observe(requireActivity(), Observer {
            val mutable = it as Mutable
            handleActions(mutable)
            Log.d(TAG,mutable.message)
            if (mutable.message == URLS.ADDRESS_LIST) {
                Log.d(TAG,"done")
                viewModel.response = mutable.obj as AddressListResponse
                viewModel.setData()
            } else if (mutable.message == Constants.BACK) {
                finishActivity()
            } else if (mutable.message == Constants.ADD_ADDRESS) {
                val bundle = Bundle()
                bundle.putString(Constants.PAGE, AddAddressFragment::class.java.name)
                MovementHelper.startActivityBaseForResult(this, bundle,Constants.ADDRESS_RESULT, getString(R.string.add_address))
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.d(TAG,"result:"+resultCode+",request"+requestCode)
        if ( requestCode == Constants.ADDRESS_RESULT && resultCode == RESULT_OK) {
            viewModel.repository.setLiveData(viewModel.liveDataViewModel)
            data?.let {
                if(data.hasExtra(Constants.ADDRESS)){
                    viewModel.adapter.add(data.getSerializableExtra(Constants.ADDRESS) as Address,0)
                }
            }
//            viewModel.getAddressList()
        }
    }



    override fun onResume() {
        super.onResume()
        viewModel.repository.setLiveData(viewModel.liveDataViewModel)
    }
}