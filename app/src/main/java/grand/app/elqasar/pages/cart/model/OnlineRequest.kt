package grand.app.elqasar.pages.cart.model

import com.google.gson.annotations.SerializedName

class OnlineRequest(@SerializedName("address_id") var addressId: Int, @SerializedName("invoice_value") var invoice: Double) {
}