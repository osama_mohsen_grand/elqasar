package grand.app.elqasar.pages.makeyourclothes.accessories

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import grand.app.elqasar.R
import grand.app.elqasar.databinding.ItemAccessoriesBinding
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.makeyourclothes.accessories.viewmodel.ItemAccessoriesViewModel
import grand.app.elqasar.pages.makeyourclothes.fabric.model.ClothesItem
import grand.app.elqasar.pages.makeyourclothes.fabric.model.ClothesModel
import grand.app.elqasar.pages.makeyourclothes.fabric.model.ClothesModelRequest
import grand.app.elqasar.pages.makeyourclothes.fabric.model.Fabric
import grand.app.elqasar.repository.ProductRepository
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.helper.AppSpecificHelper

class AccessoriesAdapter(var modelList:ArrayList<Fabric> = ArrayList(), private var repository: ProductRepository): RecyclerView.Adapter<AccessoriesAdapter.ViewHolder>() {
    // fetch list data
    var qty : Int = 0
    var selectedItem : Int = -1
    var type:Int = 0
    var liveDataViewModel: MutableLiveData<Mutable> = MutableLiveData();
    lateinit var request: ClothesItem
    lateinit var fabric : Fabric
    var isBack: Boolean = true
    var allowAction: Boolean = true

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // binding item_model layout
        val binding: ItemAccessoriesBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_accessories, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(modelList[position])
        holder.viewModel.mutableLiveData.observeForever {
            if(allowAction) {
                val mutable = (it as Mutable)
                selectedItem = mutable.position
                if (mutable.message == Constants.SELECT) {
                    Log.d(TAG,"selected $selectedItem")
                    if (selectedItem != -1) {
                        holder.viewModel.checked = false
//                        notifyItemChanged(selectedItem)
                        notifyDataSetChanged()
                    }
                } else if (mutable.message == Constants.ADD) {
                    val model = modelList[mutable.position]
                    if(model.qty < model.stock) model.qty++

                } else if (mutable.message == Constants.MINUS) {
                    val model = modelList[mutable.position]
                    if (model.qty > 1) model.qty--
                }else if(mutable.message == Constants.SUBMIT){
                    liveDataViewModel.value = mutable
                }
                qty = modelList[selectedItem].qty
                notifyItemChanged(selectedItem)
            }
        }



    }


    private  val TAG = this::class.java.name


    fun setSelectedIfExist(){
        val model: ClothesModel? = AppSpecificHelper.getModel(type,request.models)
        model.let {
            if(it?.id != -1) {
                for (i in 0 until  modelList.size) {
                    if (it?.id == modelList[i].id) {
                        selectedItem = i
                        qty = it.qty
                        Log.d(TAG,"qty: $qty")
                        modelList[i].qty = qty
                        notifyDataSetChanged()
                        return
                    }
                }
            }
        }
    }

    override fun getItemCount(): Int {
        //return data count if exist or 0
        return modelList.size
    }

    fun update(modelList:ArrayList<Fabric>){
        //update data after call service again in scroll , and notify list which end with
        this.modelList = modelList
        notifyItemRangeInserted(getItemCount(), modelList.size - 1);
    }

    inner class ViewHolder(private val binding: ItemAccessoriesBinding):RecyclerView.ViewHolder(binding.root){
        lateinit var viewModel : ItemAccessoriesViewModel
        //bint
        fun bind(model:Fabric){
            viewModel = ItemAccessoriesViewModel(model,adapterPosition,selectedItem == adapterPosition)
            binding.viewmodel = viewModel
        }

    }
}