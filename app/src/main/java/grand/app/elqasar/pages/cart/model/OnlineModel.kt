package grand.app.elqasar.pages.cart.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class OnlineModel (
    @SerializedName("InvoiceId") var invoideId: Int,
    @SerializedName("InvoiceURL") var invoiceUrl: String,
    @SerializedName("CustomerReference") var customReference: String,
    @SerializedName("UserDefinedField") var userDefinedField: String
): Serializable  {
}