package grand.app.elqasar.pages.size.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import grand.app.elqasar.R
import grand.app.elqasar.activity.MainActivity
import grand.app.elqasar.base.BaseFragment
import grand.app.elqasar.base.MyApplication
import grand.app.elqasar.base.di.IApplicationComponent
import grand.app.elqasar.databinding.FragmentHomeBinding
import grand.app.elqasar.databinding.FragmentLoginOrRegisterBinding
import grand.app.elqasar.databinding.FragmentMainSizesImageBinding
import grand.app.elqasar.pages.auth.login.LoginFragment
import grand.app.elqasar.pages.auth.register.RegisterFragment
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.auth.changepassword.ChangePasswordFragment
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.helper.MovementHelper
import javax.inject.Inject

class MainSizesImageFragment : BaseFragment() {
    private lateinit var binding: FragmentMainSizesImageBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_main_sizes_image, container, false)
        // Inflate the layout for this fragment
        binding.tvSubmit.setOnClickListener {
            val bundle = Bundle()
            bundle.putString(Constants.PAGE, MySizesFragment::class.java.name)
            bundle.putString(Constants.TYPE, Constants.REGISTER)
            MovementHelper.startActivityBase(
                context,
                bundle,
                getString(R.string.my_sizes)
            )
        }

        return binding.root
    }
}