package grand.app.elqasar.pages.makeyourclothes.tailermade.ui

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import grand.app.elqasar.R
import grand.app.elqasar.base.BaseFragment
import grand.app.elqasar.base.MyApplication
import grand.app.elqasar.base.di.IApplicationComponent
import grand.app.elqasar.databinding.FragmentTailorMadeBinding
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.makeyourclothes.common.CommonClothesMadeFragment
import grand.app.elqasar.pages.makeyourclothes.tailermade.model.TailorMadeResponse
import grand.app.elqasar.pages.makeyourclothes.tailermade.viewmodel.TailorMadeViewModel
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.URLS
import grand.app.elqasar.utils.helper.AppSpecificHelper
import grand.app.elqasar.utils.helper.MovementHelper
import javax.inject.Inject

class TailorMadeFragment : BaseFragment() {
    private lateinit var binding: FragmentTailorMadeBinding

    @Inject
    lateinit var viewModel: TailorMadeViewModel

    private val TAG = this::class.java.name

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_tailor_made, container, false)
        val component: IApplicationComponent? =
            (context!!.applicationContext as MyApplication).applicationComponent
        component?.inject(this)
        binding.viewmodel = viewModel
        arguments?.let { viewModel.setArgument(it) }
        // Inflate the layout for this fragment
        setEvent()
        return binding.root
    }

    private fun setEvent() {
        viewModel.liveDataViewModel.observe(requireActivity(), Observer {
            val mutable = it as Mutable
            handleActions(mutable)
            Log.d(TAG, mutable.message)
            if (mutable.message == URLS.CATEGORIES) {
                Log.d(TAG, "Found")
                viewModel.setData(mutable.obj as TailorMadeResponse)
            } else if (mutable.message == Constants.SUBMIT) {
                val bundle = Bundle()
                bundle.putString(Constants.PAGE, CommonClothesMadeFragment::class.java.name)
                bundle.putInt(Constants.TYPE, 3)
                bundle.putInt(Constants.CLOTH_ID, viewModel.adapter.cloth_id)
                bundle.putInt(Constants.DETAIL_ID, viewModel.adapter.id)
                bundle.putBoolean(Constants.IS_BACK, false)
                bundle.putBoolean(Constants.HAS_ESCAPE, false)
                bundle.putBoolean(Constants.ALLOW_ACTION, true)
                viewModel.adapter.request.models = AppSpecificHelper.selectClothesModel(
                    viewModel.adapter.request.models, viewModel.adapter.id, 2
                )
                viewModel.adapter.request.detail_id = viewModel.adapter.id
                bundle.putSerializable(Constants.REQUEST_MODELS, viewModel.adapter.request)
                bundle.putString(Constants.TITLE,AppSpecificHelper.getTitleBarType(3))
                if(viewModel.isBack){
                    val intent = Intent()
                    intent.putExtra(Constants.BUNDLE,bundle)
                    requireActivity().setResult(Activity.RESULT_OK, intent);
                    requireActivity().finish()
                }else {
                    MovementHelper.startActivityBase(context, bundle);
                }
            }
        })

    }

    override fun onResume() {
        super.onResume()
        viewModel.repository.setLiveData(viewModel.liveDataViewModel)
    }
}