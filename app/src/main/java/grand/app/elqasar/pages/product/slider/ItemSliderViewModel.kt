package grand.app.elqasar.pages.product.slider

import android.util.Log
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import grand.app.elqasar.R
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.product.model.list.Product
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.resources.ResourceManager
import grand.app.elqasar.utils.session.UserHelper

data class ItemSliderViewModel(val model: Product, var position: Int) {

    var mutableLiveData = MutableLiveData<Any>()

    var price: ObservableField<String> =
        ObservableField("${model.price} ${ResourceManager.getString(R.string.currency)}")

    private val TAG = "ItemSliderViewModel"

    init {
        Log.d(TAG,model.name)
        Log.d(TAG,model.about)
    }

    fun fav(){
        mutableLiveData.value = Mutable(Constants.FAVOURITE , position)
    }
    fun submit(){
        mutableLiveData.value = Mutable(Constants.SUBMIT , position)
    }
}