package grand.app.elqasar.pages.payment

import android.os.Bundle
import android.util.Log
import androidx.lifecycle.MutableLiveData
import grand.app.elqasar.base.BaseViewModel
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.cart.model.CartResponse
import grand.app.elqasar.pages.cart.model.CheckoutRequest
import grand.app.elqasar.pages.cart.model.OnlineRequest
import grand.app.elqasar.repository.ProductRepository
import grand.app.elqasar.utils.Constants
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class PaymentViewModel : BaseViewModel {
    @Inject
    lateinit var repository: ProductRepository;

    var compositeDisposable: CompositeDisposable

    //    private var liveData: LiveData<Mutable> = MutableLiveData()
    var liveDataViewModel: MutableLiveData<Mutable>
    lateinit var cart: CartResponse
    var promo: String = ""

    private val TAG = "PaymentViewModel"

    @Inject
    constructor(repository: ProductRepository) {
        this.repository = repository
        this.liveDataViewModel = MutableLiveData();
        compositeDisposable = CompositeDisposable()
        repository.setLiveData(liveDataViewModel)
    }

    fun setArgument(bundle: Bundle) {
        bundle.let {
            cart = it.getSerializable(Constants.CART) as CartResponse
            promo = it.getString(Constants.PROMO).toString()
            Log.d(TAG,"cart:"+cart.data.totalPrice)
        }
    }

    fun submit(){
        liveDataViewModel.value = Mutable(Constants.PAYMNET)
    }

    protected fun unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }

    override fun onCleared() {
        super.onCleared()
        unSubscribeFromObservable()
    }

    fun submitPayment() {
        repository.checkout(CheckoutRequest(cart.data.addressId, promo))
    }
}