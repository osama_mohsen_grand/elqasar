package grand.app.elqasar.pages.product.viewmodel

import android.os.Bundle
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import grand.app.elqasar.R
import grand.app.elqasar.base.BaseViewModel
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.cart.model.UpdateCartRequest
import grand.app.elqasar.pages.product.model.FavouriteRequest
import grand.app.elqasar.pages.product.model.details.ProductDetailsResponse
import grand.app.elqasar.repository.ProductRepository
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.session.UserHelper
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class ProductDetailsViewModel : BaseViewModel {
    @Inject
    lateinit var repository: ProductRepository;

    var compositeDisposable: CompositeDisposable

    //    private var liveData: LiveData<Mutable> = MutableLiveData()
    var liveDataViewModel: MutableLiveData<Mutable>
    var type: Int = 0
    var id: Int = 0
    private val TAG = "ProductListSliderViewModel"
    var response: ObservableField<ProductDetailsResponse> =
        ObservableField(ProductDetailsResponse())
    var title: ObservableField<String> =
        ObservableField("")
    var price: ObservableField<String> =
        ObservableField("")
    var qty: ObservableField<String> =
        ObservableField("1")
    var allowAction: ObservableField<Boolean> = ObservableField(true)


    @Inject
    constructor(repository: ProductRepository) {
        this.repository = repository
        this.liveDataViewModel = MutableLiveData();
        compositeDisposable = CompositeDisposable()
        repository.setLiveData(liveDataViewModel)
    }

    fun setArgument(bundle: Bundle) {
        type = bundle.getInt(Constants.TYPE)
        id = bundle.getInt(Constants.ID)
        title.set(bundle.getString(Constants.TITLE))
        repository.getProductDetails(id)
    }

    fun back() {
        liveDataViewModel.value = Mutable(Constants.BACK)
    }

    fun favourite() {
        repository.addFavourite(FavouriteRequest(id, type))
    }

    var tmp = 0

    fun add() {
        tmp = qty.get()!!.toInt()
        if (tmp < response.get()!!.productDetails.stock) {
            tmp++
            qty.set(tmp.toString())
        }
    }

    fun minus() {
        tmp = qty.get()!!.toInt()
        if (tmp > 1) {
            tmp--
            qty.set(tmp.toString())
        }
    }

    fun addToCart() {
        repository.updateCart(
            UpdateCartRequest(
                productId = id,
                qty = qty.get()!!.toInt(),
                type = type
            )
        )
    }

    fun share() {
        liveDataViewModel.value = Mutable(Constants.SHARE)
    }

    protected fun unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }

    override fun onCleared() {
        super.onCleared()
        unSubscribeFromObservable()
    }

    fun setData(response: ProductDetailsResponse) {
        this.response.set(response)
        price.set("${response.productDetails.price} ${getString(R.string.currency)}")
        allowAction.set(response.productDetails.stock != 0)
        show.set(true)
    }
}