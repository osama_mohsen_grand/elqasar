package grand.app.elqasar.pages.cart.viewmodel

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.RadioGroup
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.GridLayoutManager
import com.google.gson.Gson
import com.myfatoorah.sdk.model.initiatepayment.MFInitiatePaymentRequest
import com.myfatoorah.sdk.model.initiatepayment.MFInitiatePaymentResponse
import com.myfatoorah.sdk.model.initiatepayment.PaymentMethod
import com.myfatoorah.sdk.utils.MFAPILanguage
import com.myfatoorah.sdk.utils.MFCurrencyISO
import com.myfatoorah.sdk.views.MFResult
import com.myfatoorah.sdk.views.MFSDK
import grand.app.elqasar.R
import grand.app.elqasar.base.BaseViewModel
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.address.list.AddressAdapter
import grand.app.elqasar.pages.address.list.model.AddressListResponse
import grand.app.elqasar.pages.cart.adapter.CartAdapter
import grand.app.elqasar.pages.cart.adapter.CartTransactionAdapter
import grand.app.elqasar.pages.cart.model.CartResponse
import grand.app.elqasar.pages.cart.model.CheckoutRequest
import grand.app.elqasar.pages.cart.model.OnlineRequest
import grand.app.elqasar.pages.payment.MyItemRecyclerViewAdapter
import grand.app.elqasar.pages.payment.OnListFragmentInteractionListener
import grand.app.elqasar.pages.product.adapter.ProductAdapter
import grand.app.elqasar.pages.product.model.list.ProductListResponse
import grand.app.elqasar.repository.CartRepository
import grand.app.elqasar.repository.ProductRepository
import grand.app.elqasar.repository.UserRepository
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.resources.ResourceManager
import io.reactivex.disposables.CompositeDisposable
import java.util.ArrayList
import javax.inject.Inject

class CartPaymentViewModel : BaseViewModel  {
    @Inject
    lateinit var repository: ProductRepository;
    var compositeDisposable: CompositeDisposable

    //    private var liveData: LiveData<Mutable> = MutableLiveData()
    var liveDataViewModel: MutableLiveData<Mutable>
    private val TAG = this::class.java.name
    lateinit var responseCart: CartResponse
    var adapterTransaction = CartTransactionAdapter()
    var promo: String = ""
    var cash: Boolean = false;

    @Inject
    constructor(repository: ProductRepository) {
        this.repository = repository
        this.liveDataViewModel = MutableLiveData();
        compositeDisposable = CompositeDisposable()
        repository.setLiveData(liveDataViewModel)
    }

    fun setArgument(bundle: Bundle) {
        responseCart = bundle.getSerializable(Constants.CART) as CartResponse
        promo = bundle.getString(Constants.PROMO).toString()
        adapterTransaction.setList(responseCart)
//        show.set(true)
    }

    fun submit() {
        if (cash) {
            repository.checkout(CheckoutRequest(responseCart.data.addressId, promo))
        } else {
            liveDataViewModel.value = Mutable(Constants.ONLINE)
//            repository.online(OnlineRequest(responseCart.data.addressId,responseCart.data.totalPrice))
//            payOnline();
        }
    }

    protected fun unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }

    override fun onCleared() {
        super.onCleared()
        unSubscribeFromObservable()
    }
}