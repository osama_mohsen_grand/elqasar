package grand.app.elqasar.pages.cart.model

import com.google.gson.annotations.SerializedName

class CheckoutRequest(@SerializedName("address_id") var addressId: Int,
                      @SerializedName("promo") var promo: String = "",
                      @SerializedName("payment_method") var paymentMethod: String = "2") {
    //1: cash , 2: Online
}