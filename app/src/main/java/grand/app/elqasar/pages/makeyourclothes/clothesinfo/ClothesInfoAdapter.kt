package grand.app.elqasar.pages.makeyourclothes.clothesinfo

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import grand.app.elqasar.R
import grand.app.elqasar.databinding.ItemClothesInfoBinding
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.makeyourclothes.accessories.AccessoriesFragment
import grand.app.elqasar.pages.makeyourclothes.common.CommonClothesMadeFragment
import grand.app.elqasar.pages.makeyourclothes.fabric.model.ClothesItem
import grand.app.elqasar.pages.makeyourclothes.fabric.model.ClothesModel
import grand.app.elqasar.pages.makeyourclothes.fabric.model.ClothesModelRequest
import grand.app.elqasar.pages.makeyourclothes.fabric.ui.FabricFragment
import grand.app.elqasar.pages.makeyourclothes.tailermade.ui.TailorMadeFragment
import grand.app.elqasar.pages.size.ui.MySizesFragment
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.helper.AppSpecificHelper
import grand.app.elqasar.utils.helper.MovementHelper
import grand.app.elqasar.utils.resources.ResourceManager

class ClothesInfoAdapter(var modelList: ArrayList<ClothesModel> = ArrayList()) :
    RecyclerView.Adapter<ClothesInfoAdapter.ViewHolder>() {
    // fetch list data
    private val TAG = this::class.java.name
    var favPosition: Int = 0
    var selectedItem: Int = -1
    lateinit var model: ClothesModel
    var liveDataViewModel: MutableLiveData<Mutable> = MutableLiveData();

    //    var request: ClothesItem = ClothesItem()
    private lateinit var fragment: ClothesInfoFragment
    var allowAction = true
    var isBack = true;
    var isCreate = true
    var requestClothes = ClothesModelRequest()
    var current = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // binding item_model layout
        val binding: ItemClothesInfoBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_clothes_info,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    fun getRequest(position: Int): ClothesItem {
        return requestClothes.clotheItem[position]
    }

    fun getRequest(): ClothesItem {
        return requestClothes.clotheItem[current]
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(modelList[position])
        holder.viewModel.mutableLiveData.observeForever {
            model = modelList[position]
            val mutable = it as Mutable
            val bundle = Bundle()
            if (mutable.message == Constants.SUBMIT) {

                bundle.putString(Constants.TITLE, AppSpecificHelper.getTitleBarType(model.type))
                bundle.putBoolean(Constants.HAS_ESCAPE, model.type >= 6)
                bundle.putBoolean(Constants.IS_BACK, isBack)
                bundle.putBoolean(Constants.ALLOW_ACTION, allowAction)
                bundle.putInt(Constants.CLOTH_ID, getRequest().cloth_id)
                bundle.putInt(Constants.DETAIL_ID, getRequest().detail_id)
                bundle.putSerializable(Constants.REQUEST_MODELS, getRequest())
                bundle.putInt(Constants.TYPE, model.type)
                var name = ""

                Log.d(TAG,"type: ${model.type}")
                Log.d(TAG,"position: ${model.type}")
//                when (position) {
//                    0 -> {
//                        name = model.name
//                        bundle.putString(Constants.PAGE,FabricFragment::class.java.name)
//                    }
//                    1 -> {
//                        name = model.name
//                        bundle.putString(Constants.PAGE, TailorMadeFragment::class.java.name)
//                        bundle.putInt(Constants.CLOTH_ID, getRequest().cloth_id)
//                    }
//                    else -> {
//
//                        }
//                    }
//                }
                when (model.type) {
                    1 -> {
                        bundle.putString(Constants.PAGE, FabricFragment::class.java.name)
                        name = model.name
                    }
                    2 -> {
                        bundle.putString(Constants.PAGE, TailorMadeFragment::class.java.name)
                        name = model.name
                    }
                    in 3..7 -> {
                        bundle.putString(
                            Constants.PAGE,
                            CommonClothesMadeFragment::class.java.name
                        )
                    }
                    8 -> {
                        bundle.putString(Constants.PAGE, AccessoriesFragment::class.java.name)
                    }
                }
                MovementHelper.startActivityBaseForResult(
                    fragment,
                    bundle,
                    Constants.REQUEST_MODELS_RESULT,
                    name
                );
            }
        }
    }


    override fun getItemCount(): Int {
        //return data count if exist or 0
        return modelList.size
    }

    fun update(modelList: ArrayList<ClothesModel>) {
        //update data after call service again in scroll , and notify list which end with
        this.modelList = modelList
        notifyItemRangeInserted(itemCount, modelList.size - 1);
    }

    fun setFragment(fragment: ClothesInfoFragment) {
        this.fragment = fragment
    }

    fun setRequest(clothesItem: ClothesItem) {
        requestClothes.clotheItem[current] = clothesItem
    }

    fun add(request: ClothesItem) {
        current++
        requestClothes.clotheItem.add(request)
    }

    inner class ViewHolder(private val binding: ItemClothesInfoBinding) :
        RecyclerView.ViewHolder(binding.root) {
        lateinit var viewModel: ItemClothesInfoViewModel

        //bint
        fun bind(model: ClothesModel) {
            viewModel = ItemClothesInfoViewModel(model, adapterPosition)
            binding.viewmodel = viewModel
        }

    }
}