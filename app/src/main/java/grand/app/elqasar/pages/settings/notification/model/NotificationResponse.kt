package grand.app.elqasar.pages.settings.notification.model


import com.google.gson.annotations.SerializedName
import grand.app.elqasar.model.base.StatusMessage

data class NotificationResponse(
    @SerializedName("data")
    var data : ArrayList<Notification>
) : StatusMessage()