package grand.app.elqasar.pages.size.adapter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.elqasar.R
import grand.app.elqasar.databinding.ItemOrderListBinding
import grand.app.elqasar.databinding.ItemMySizeBinding
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.makeyourclothes.fabric.model.ClothesModelRequest
import grand.app.elqasar.pages.order.model.Order
import grand.app.elqasar.pages.order.viewmodel.ItemOrderViewModel
import grand.app.elqasar.pages.product.model.FavouriteRequest
import grand.app.elqasar.pages.product.ui.ProductDetailsFragment
import grand.app.elqasar.pages.size.model.MySizeModel
import grand.app.elqasar.pages.size.viewmodel.ItemMySizeViewModel
import grand.app.elqasar.repository.ProductRepository
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.helper.AppHelper
import grand.app.elqasar.utils.helper.MovementHelper

class MySizesAdapter(var modelList:List<MySizeModel> = ArrayList()) : RecyclerView.Adapter<MySizesAdapter.ViewHolder>() {
    // fetch list data
    var favPosition : Int = 0
    var selectedItem : Int = -1
    lateinit var model : MySizeModel
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // binding item_model layout
        val binding: ItemMySizeBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_my_size,
            parent,
            false
        )
        return ViewHolder(
            binding
        )
    }
    val request: ClothesModelRequest = ClothesModelRequest()
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(modelList[position])
        holder.viewModel.mutableLiveData.observeForever {
            val mutable : Mutable = it as Mutable
            model = modelList[mutable.position]
            if(mutable.message == Constants.YOUTUBE){
                AppHelper.openBrowser(holder.itemView.context,model.link)
            }
        }
    }

    override fun getItemCount(): Int {
        //return data count if exist or 0
        return modelList.size
    }



    fun update(modelList: List<MySizeModel>) {
        //update data after call service again in scroll , and notify list which end with
        this.modelList = modelList
        notifyDataSetChanged()
    }

    class ViewHolder(private val binding: ItemMySizeBinding) : RecyclerView.ViewHolder(binding.root) {
        lateinit var viewModel: ItemMySizeViewModel

        //bint
        fun bind(model: MySizeModel) {
            viewModel = ItemMySizeViewModel(model, adapterPosition)
            binding.viewmodel = viewModel
        }
    }
}