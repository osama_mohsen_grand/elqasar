package grand.app.elqasar.pages.account

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import grand.app.elqasar.R
import grand.app.elqasar.base.BaseFragment
import grand.app.elqasar.base.MyApplication
import grand.app.elqasar.base.di.IApplicationComponent
import grand.app.elqasar.databinding.FragmentAccountSettingsBinding
import grand.app.elqasar.databinding.FragmentSettingsBinding
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.auth.code.VerifyCodeFragment
import grand.app.elqasar.pages.auth.forgetpassword.model.ForgetPasswordResponse
import grand.app.elqasar.pages.auth.register.RegisterFragment
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.dialog.DialogLogin
import grand.app.elqasar.utils.helper.IOBackCommonPressed
import grand.app.elqasar.utils.helper.MovementHelper
import grand.app.elqasar.utils.session.UserHelper
import javax.inject.Inject

class AccountSettingFragment : BaseFragment() {
    private lateinit var binding: FragmentAccountSettingsBinding
    @Inject
    lateinit var viewModel: AccountSettingViewModel

    private  val TAG = "AccountSettingFragment"

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_account_settings, container, false)
        val component: IApplicationComponent? =
            (context!!.applicationContext as MyApplication).applicationComponent
        component?.inject(this)
        binding.viewmodel = viewModel
        viewModel.bottomAdapter.setActivity(requireActivity())
        viewModel.topAdapter.setActivity(requireActivity())
        // Inflate the layout for this fragment
        setEvent()
        return binding.root
    }

    private fun setEvent() {
        viewModel.liveDataViewModel.observe(requireActivity(), Observer {
            val mutable = it as Mutable
            handleActions(mutable)
            Log.d(TAG,mutable.message)
            if (mutable.message == Constants.UPDATE_PROFILE) {
                val bundle = Bundle()
                bundle.putString(Constants.PAGE, RegisterFragment::class.java.name)
                MovementHelper.startActivityBase(context, bundle, getString(R.string.update_profile))
            }
        })

    }

    override fun onResume() {
        super.onResume()
        viewModel.liveDataViewModel = viewModel.liveDataViewModel
    }

//    override fun onBackPressed(): Boolean {
//        TODO("Not yet implemented")
//    }
}