package grand.app.elqasar.pages.makeyourclothes.fabric.adapter

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import grand.app.elqasar.R
import grand.app.elqasar.databinding.ItemFabricBinding
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.makeyourclothes.fabric.model.ClothesItem
import grand.app.elqasar.pages.makeyourclothes.fabric.model.ClothesModelRequest
import grand.app.elqasar.pages.makeyourclothes.fabric.viewmodel.ItemFabricViewModel
import grand.app.elqasar.pages.makeyourclothes.fabric.model.Fabric
import grand.app.elqasar.pages.makeyourclothes.tailermade.ui.TailorMadeFragment
import grand.app.elqasar.pages.product.model.FavouriteRequest
import grand.app.elqasar.repository.ProductRepository
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.helper.AppSpecificHelper
import grand.app.elqasar.utils.helper.MovementHelper

class FabricAdapter(
    private var modelList: ArrayList<Fabric> = ArrayList(),
    private var repository: ProductRepository
) : RecyclerView.Adapter<FabricAdapter.ViewHolder>() {

    // fetch list data
    var favPosition: Int = 0
    var selectedItem: Int = -1
    var type: Int = 0
    lateinit var request: ClothesItem
    lateinit var fabric: Fabric
    var editable: Boolean = true
    var isBack: Boolean = false
    var liveDataViewModel: MutableLiveData<Mutable> = MutableLiveData();
    public lateinit var bundle: Bundle

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // binding item_model layout
        val binding: ItemFabricBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_fabric,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(modelList[position])
        holder.viewModel.mutableLiveData.observeForever {
            if (editable) {
                val mutable: Mutable = it as Mutable
                fabric = modelList[mutable.position]
                if (mutable.message == Constants.SUBMIT) {
                    if (selectedItem != -1) {
                        holder.viewModel.checked = false
                        notifyItemChanged(selectedItem)
                    }
                    selectedItem = mutable.position
                    notifyItemChanged(selectedItem)

                    bundle = Bundle()
                    request.models =
                        AppSpecificHelper.selectClothesModel(request.models, fabric.id, type)
                    bundle.putString(Constants.PAGE, TailorMadeFragment::class.java.name)
                    bundle.putInt(Constants.CLOTH_ID, fabric.id)
                    request.cloth_id = fabric.id
                    bundle.putSerializable(Constants.REQUEST_MODELS, request)
                    if (isBack) {
                        liveDataViewModel.value = it
                    }
                    else
                        MovementHelper.startActivityBase(
                            holder.itemView.context,
                            bundle,
                            fabric.name
                        );

                } else if (mutable.message == Constants.FAVOURITE) {
                    favPosition = position
                    repository.addFavourite(FavouriteRequest(fabric.id, type))
                }
            }
        }
    }

    fun setSelectedIfExist() {
        val id = AppSpecificHelper.getId(type, request.models)
        if (id != -1) {
            for (i in 0 until modelList.size) {
                if (id == modelList[i].id) {
                    selectedItem = i
                    notifyDataSetChanged()
                    return
                }
            }
        }
    }


    private val TAG = this::class.java.name


    override fun getItemCount(): Int {
        //return data count if exist or 0
        return modelList.size
    }


    fun updateFavourite(favorite: Int) {
        modelList[favPosition].userFavorite = favorite
        notifyItemChanged(favPosition)
    }

    fun update(modelList: ArrayList<Fabric>) {
        //update data after call service again in scroll , and notify list which end with
        this.modelList = modelList
        notifyItemRangeInserted(itemCount, modelList.size - 1);
    }

    inner class ViewHolder(private val binding: ItemFabricBinding) :
        RecyclerView.ViewHolder(binding.root) {
        lateinit var viewModel: ItemFabricViewModel

        //bint
        fun bind(model: Fabric) {
            viewModel = ItemFabricViewModel(model, adapterPosition, selectedItem == adapterPosition)
            binding.viewmodel = viewModel
        }

    }
}