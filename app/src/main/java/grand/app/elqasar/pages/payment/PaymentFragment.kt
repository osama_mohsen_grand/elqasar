package grand.app.elqasar.pages.payment

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.google.gson.Gson
import com.myfatoorah.sdk.model.executepayment.MFExecutePaymentRequest
import com.myfatoorah.sdk.model.initiatepayment.MFInitiatePaymentRequest
import com.myfatoorah.sdk.model.initiatepayment.MFInitiatePaymentResponse
import com.myfatoorah.sdk.model.initiatepayment.PaymentMethod
import com.myfatoorah.sdk.model.paymentstatus.MFGetPaymentStatusResponse
import com.myfatoorah.sdk.utils.MFAPILanguage
import com.myfatoorah.sdk.utils.MFCurrencyISO
import com.myfatoorah.sdk.views.MFResult
import com.myfatoorah.sdk.views.MFSDK
import grand.app.elqasar.R
import grand.app.elqasar.activity.MainActivity
import grand.app.elqasar.base.BaseFragment
import grand.app.elqasar.base.MyApplication
import grand.app.elqasar.base.di.IApplicationComponent
import grand.app.elqasar.databinding.FragmentPaymentBinding
import grand.app.elqasar.databinding.FragmentProductDetailsBinding
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.model.base.StatusMessage
import grand.app.elqasar.pages.cart.model.CartResponse
import grand.app.elqasar.pages.product.model.details.ProductDetailsResponse
import grand.app.elqasar.pages.product.viewmodel.ProductDetailsViewModel
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.URLS
import grand.app.elqasar.utils.dialog.DialogLogin
import grand.app.elqasar.utils.helper.AppHelper
import grand.app.elqasar.utils.resources.ResourceManager
import grand.app.elqasar.utils.session.SharedPreferenceHelper
import kotlinx.android.synthetic.main.fragment_payment.*
import javax.inject.Inject

class PaymentFragment : BaseFragment(), OnListFragmentInteractionListener {
    val TAG: String = this::class.java.name
    private lateinit var binding: FragmentPaymentBinding
    lateinit var paymentMethods: ArrayList<PaymentMethod>
    private lateinit var adapter: MyItemRecyclerViewAdapter
    private var selectedPaymentMethod: PaymentMethod? = null

    @Inject
    lateinit var viewModel: PaymentViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_payment, container, false)
        val component: IApplicationComponent? =
            (context!!.applicationContext as MyApplication).applicationComponent
        component?.inject(this)
        binding.viewmodel = viewModel
        arguments?.let { viewModel.setArgument(it) }
        initPayment()
        setEvent()
        return binding.root
    }

    private fun setEvent() {
        viewModel.liveDataViewModel.observe(requireActivity(), Observer {
            val mutable = it as Mutable
            Log.d(TAG, mutable.message)
            handleActions(mutable)
            if(mutable.message == Constants.PAYMNET){
                if(selectedPaymentMethod != null)
                    executePayment(selectedPaymentMethod?.paymentMethodId!!)
                else showError(getString(R.string.please_choose_payment_method))
            }else if(mutable.message == URLS.CHECKOUT){
                SharedPreferenceHelper.saveKey(Constants.CART,"")
                finishActivity()
                val intent = Intent(requireActivity(),MainActivity::class.java)
                intent.putExtra(Constants.DIALOG,getString(R.string.your_order_had_been_sent))
                startActivity(intent)
            }
        })
    }

    private fun initPayment() {

        handleActions(Mutable(Constants.SHOW_PROGRESS))

        MFSDK.init(Constants.API_URL, Constants.API_KEY)

        // You can custom your action bar, but this is optional not required to set this line
        MFSDK.setUpActionBar(
            ResourceManager.getString(R.string.app_name), R.color.colorWhite,
            R.color.colorPrimary, true
        )

        val invoiceAmount = viewModel.cart.data.totalPrice
        val request = MFInitiatePaymentRequest(invoiceAmount, MFCurrencyISO.SAUDI_ARABIA_SAR)

        MFSDK.initiatePayment(
            request,
            MFAPILanguage.AR
        ) { result: MFResult<MFInitiatePaymentResponse> ->
            when (result) {
                is MFResult.Success -> {
                    setAvailablePayments(result.response.paymentMethods!!)
                    viewModel.showPage(true)
                    Log.d(TAG, "MFResult Done successfully")
                }
                is MFResult.Fail -> {
                    Log.d(TAG, "Fail: " + Gson().toJson(result.error))
                }
            }
            handleActions(Mutable(Constants.HIDE_PROGRESS))
        }
    }

    private fun executePayment(paymentMethod: Int) {

        val invoiceAmount = viewModel.cart.data.totalPrice
        val request = MFExecutePaymentRequest(paymentMethod, invoiceAmount)
        request.displayCurrencyIso = MFCurrencyISO.SAUDI_ARABIA_SAR

        MFSDK.executePayment(
            requireActivity(),
            request,
            MFAPILanguage.AR
        ) { invoiceId: String, result: MFResult<MFGetPaymentStatusResponse> ->
            when (result) {
                is MFResult.Success -> {
                    Log.d(TAG, "Response: " + Gson().toJson(result.response))
                    Log.d(TAG, "payment Done successfully here")
                    viewModel.submitPayment()
                }
                is MFResult.Fail -> {
                    Log.d(TAG, "Fail: " + Gson().toJson(result.error))
                    Log.d(TAG, "payment failed")
                }
            }
            Log.d(TAG, "invoiceId: $invoiceId")

            pbLoading.visibility = View.GONE
        }
    }


    private fun setAvailablePayments(paymentMethods: java.util.ArrayList<PaymentMethod>) {
        val layoutManager =
            GridLayoutManager(requireContext(), 5)
        adapter = MyItemRecyclerViewAdapter(paymentMethods, this)
        rvPaymentMethod.layoutManager = layoutManager
        rvPaymentMethod.adapter = adapter
    }

    override fun onResume() {
        super.onResume()
        viewModel.repository.setLiveData(viewModel.liveDataViewModel)
    }

    override fun onListFragmentInteraction(position: Int, paymentMethod: PaymentMethod) {
        selectedPaymentMethod = paymentMethod
        Log.d(TAG,"selectedPaymentMethod: "+selectedPaymentMethod)
        adapter.changeSelected(position)
        Log.d(TAG, "payment method: " + paymentMethod.directPayment)

    }
}