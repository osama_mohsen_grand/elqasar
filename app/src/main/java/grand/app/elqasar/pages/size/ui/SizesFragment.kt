package grand.app.elqasar.pages.size.ui

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import grand.app.elqasar.R
import grand.app.elqasar.activity.MainActivity
import grand.app.elqasar.base.BaseFragment
import grand.app.elqasar.base.MyApplication
import grand.app.elqasar.base.di.IApplicationComponent
import grand.app.elqasar.databinding.FragmentSizesBinding
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.size.viewmodel.SizesViewModel
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.URLS
import grand.app.elqasar.utils.helper.MovementHelper
import grand.app.elqasar.utils.session.UserHelper
import javax.inject.Inject

class SizesFragment : BaseFragment() {
    private lateinit var binding: FragmentSizesBinding

    @Inject
    lateinit var viewModel: SizesViewModel


    private val TAG = "SizesFragment"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_sizes, container, false)
        val component: IApplicationComponent? =
            (context!!.applicationContext as MyApplication).applicationComponent
        component?.inject(this)
        binding.viewmodel = viewModel
        setEvent()
        // Inflate the layout for this fragment
        return binding.root
    }


    private fun setEvent() {
        viewModel.liveDataViewModel.observe(requireActivity(), Observer {
            val mutable = it as Mutable
            handleActions(mutable)
            if (mutable.message == URLS.UPDATE_PROFILE) {
                requireActivity().finishAffinity()
                val intent = Intent(requireActivity(),MainActivity::class.java)
                intent.putExtra(Constants.DIALOG,getString(R.string.your_register_had_been_completed))
                startActivity(intent)
            } else if (mutable.message == Constants.SIZE_DISPLAY) {
                val bundle = Bundle()
                bundle.putString(Constants.PAGE, MainSizesImageFragment::class.java.name)
                MovementHelper.startActivityBase(context, bundle, getString(R.string.main_sizes))
            }
        })
    }

    override fun onResume() {
        super.onResume()
        viewModel.repository.setLiveData(viewModel.liveDataViewModel)
    }
}