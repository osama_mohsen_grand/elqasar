package grand.app.elqasar.pages.loginorregister

import android.util.Log
import androidx.lifecycle.MutableLiveData
import grand.app.elqasar.base.BaseViewModel
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.utils.Constants
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class LoginOrRegisterViewModel : BaseViewModel {
//    @Inject
//    lateinit var repository: SettingsRepository;
    lateinit var compositeDisposable: CompositeDisposable
    //    private var liveData: LiveData<Mutable> = MutableLiveData()
    var liveDataViewModel: MutableLiveData<Mutable>
    var x: Int = 6

    @Inject
    constructor(){
//        this.repository = repository
        this.liveDataViewModel = MutableLiveData();
//        compositeDisposable = CompositeDisposable()
//        repository.setLiveData(liveDataViewModel)
    }

    private  val TAG = "LoginOrRegisterViewMode"

    fun login() {
        Log.d(TAG,"login")
        liveDataViewModel.value = Mutable(Constants.LOGIN)
    }

    fun register() {
        Log.d(TAG,"register")
        liveDataViewModel.value = Mutable(Constants.REGISTER)
    }

    fun skip() {
        Log.d(TAG,"register")
        liveDataViewModel.value = Mutable(Constants.SKIP)
    }

    protected fun unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }

    override fun onCleared() {
        super.onCleared()
        unSubscribeFromObservable()
    }
}