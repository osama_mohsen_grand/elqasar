package grand.app.elqasar.pages.settings.notification

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import grand.app.elqasar.R
import grand.app.elqasar.base.BaseFragment
import grand.app.elqasar.base.MyApplication
import grand.app.elqasar.base.di.IApplicationComponent
import grand.app.elqasar.databinding.FragmentNotificationListBinding
import grand.app.elqasar.databinding.FragmentProductListBinding
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.product.model.FavouriteResponse
import grand.app.elqasar.pages.product.model.list.ProductListResponse
import grand.app.elqasar.pages.product.viewmodel.list.ProductListViewModel
import grand.app.elqasar.pages.settings.notification.model.NotificationResponse
import grand.app.elqasar.pages.settings.notification.viewmodel.NotificationListViewModel
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.URLS
import javax.inject.Inject

class NotificationListFragment : BaseFragment() {
    val TAG: String = this::class.java.name

    private lateinit var binding: FragmentNotificationListBinding
    @Inject
    lateinit var viewModel: NotificationListViewModel


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_notification_list, container, false)
        val component: IApplicationComponent? =
            (context!!.applicationContext as MyApplication).applicationComponent
        component?.inject(this)
        binding.viewmodel = viewModel
        setEvent()
        // Inflate the layout for this fragment
        return binding.root
    }
    private fun setEvent() {
        viewModel.liveDataViewModel.observe(requireActivity(), Observer {
            val mutable = it as Mutable
            Log.d(TAG,mutable.message)
            handleActions(mutable)
//            Log.d(TAG,(mutable.obj as StatusMessage).mStatus+"")
            if (mutable.message == URLS.NOTIFICATIONS) {
                viewModel.response = (mutable.obj as NotificationResponse)
                viewModel.setData()
            }
        })
    }
    override fun onResume() {
        super.onResume()
        viewModel.repository.setLiveData(viewModel.liveDataViewModel)
    }
}