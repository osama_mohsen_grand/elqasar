package grand.app.elqasar.pages.order.model.details


import com.google.gson.annotations.SerializedName
import grand.app.elqasar.model.base.StatusMessage

data class OrderDetailsResponse(
    @SerializedName("data")
    var data: OrderDetails = OrderDetails()
): StatusMessage()