package grand.app.elqasar.pages.notes

import android.os.Bundle
import android.util.Log
import androidx.lifecycle.MutableLiveData
import grand.app.elqasar.base.BaseViewModel
import grand.app.elqasar.pages.auth.repository.AuthRepository
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.auth.login.model.LoginRequest
import grand.app.elqasar.pages.makeyourclothes.fabric.model.ClothesItem
import grand.app.elqasar.pages.makeyourclothes.fabric.model.ClothesModelRequest
import grand.app.elqasar.utils.Constants
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class NotesViewModel : BaseViewModel {
    @Inject
    lateinit var repository: AuthRepository;
    var compositeDisposable: CompositeDisposable
    lateinit var request: ClothesItem
    //    private var liveData: LiveData<Mutable> = MutableLiveData()
    var liveDataViewModel: MutableLiveData<Mutable>

    @Inject
    constructor(repository: AuthRepository){
        this.repository = repository
        this.liveDataViewModel = MutableLiveData();
        compositeDisposable = CompositeDisposable()
        repository.setLiveData(liveDataViewModel)
    }

    private  val TAG = "LoginOrRegisterViewMode"

    fun setArgument(bundle: Bundle) {
        request = bundle.getSerializable(Constants.REQUEST_MODELS) as ClothesItem
    }


    fun escape(){
        request.notes = ""
        liveDataViewModel.value = Mutable(Constants.SUBMIT)
    }


    fun submit(){
        liveDataViewModel.value = Mutable(Constants.SUBMIT)
    }

    private fun unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }

    override fun onCleared() {
        super.onCleared()
        unSubscribeFromObservable()
    }
}