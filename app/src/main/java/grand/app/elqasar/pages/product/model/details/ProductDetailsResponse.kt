package grand.app.elqasar.pages.product.model.details


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import grand.app.elqasar.model.base.StatusMessage
import grand.app.elqasar.pages.product.model.details.ProductDetails

data class ProductDetailsResponse(
    @SerializedName("data")
    @Expose
    var productDetails: ProductDetails = ProductDetails()
) : StatusMessage()