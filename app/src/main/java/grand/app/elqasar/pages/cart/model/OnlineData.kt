package grand.app.elqasar.pages.cart.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class OnlineData (
    @SerializedName("IsSuccess") var IsSuccess: Boolean,
    @SerializedName("Message") var message: String,
    @SerializedName("payment_success") var paymentSuccess: String,
    @SerializedName("payment_failed") var paymentFailed: String,
    @SerializedName("Data") var data: OnlineModel
): Serializable  {
}
