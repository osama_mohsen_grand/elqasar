package grand.app.elqasar.pages.cart.ui

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import grand.app.elqasar.R
import grand.app.elqasar.activity.MainActivity
import grand.app.elqasar.base.BaseFragment
import grand.app.elqasar.base.MyApplication
import grand.app.elqasar.base.di.IApplicationComponent
import grand.app.elqasar.databinding.FragmentCartPaymentBinding
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.model.base.StatusMessage
import grand.app.elqasar.pages.cart.model.OnlineResponse
import grand.app.elqasar.pages.cart.viewmodel.CartPaymentViewModel
import grand.app.elqasar.pages.online.OnlineFragment
import grand.app.elqasar.pages.payment.PaymentFragment
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.URLS
import grand.app.elqasar.utils.helper.MovementHelper
import grand.app.elqasar.utils.resources.ResourceManager
import grand.app.elqasar.utils.session.SharedPreferenceHelper
import javax.inject.Inject

class CartPaymentFragment : BaseFragment() {
    val TAG: String = this::class.java.name

    private lateinit var binding: FragmentCartPaymentBinding
    @Inject
    lateinit var viewModel: CartPaymentViewModel


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_cart_payment, container, false)
        val component: IApplicationComponent? =
            (context!!.applicationContext as MyApplication).applicationComponent
        component?.inject(this)
        binding.viewmodel = viewModel
        arguments?.let { viewModel.setArgument(it) }
        setEvent()
        // Inflate the layout for this fragment
        return binding.root
    }
    private fun setEvent() {
        viewModel.liveDataViewModel.observe(requireActivity(), Observer {
            val mutable = it as Mutable
            Log.d(TAG,mutable.message)
            handleActions(mutable)
//            Log.d(TAG,(mutable.obj as StatusMessage).mStatus+"")
            if (mutable.message == URLS.CHECKOUT) {
//                AppHelper.showDialog(requireContext(),(mutable.obj as StatusMessage).mMessage)
                toastMessage((mutable.obj as StatusMessage).mMessage)
                SharedPreferenceHelper.saveKey(Constants.CART,"")
                finishActivity()
                val intent = Intent(requireActivity(),MainActivity::class.java)
                intent.putExtra(Constants.DIALOG,getString(R.string.your_order_had_been_sent))
                startActivity(intent)
            }else if(mutable.message == URLS.PAYMENT_ONLINE){
                val onlineResponse = mutable.obj as OnlineResponse
                val bundle = Bundle()
                bundle.putString(Constants.PAGE,OnlineFragment::class.java.name)
                bundle.putSerializable(Constants.ONLINE,onlineResponse)
                MovementHelper.startActivityBase(requireContext(),bundle)
            }else if(mutable.message == Constants.ONLINE){
                val bundle = Bundle()
                bundle.putString(Constants.PAGE,PaymentFragment::class.java.name)
                bundle.putSerializable(Constants.CART,viewModel.responseCart)
                bundle.putString(Constants.PROMO,viewModel.promo)
                MovementHelper.startActivityBase(requireContext(),bundle,ResourceManager.getString(R.string.payment_method))
            }
        })
    }
    override fun onResume() {
        super.onResume()
        viewModel.repository.setLiveData(viewModel.liveDataViewModel)
    }
}