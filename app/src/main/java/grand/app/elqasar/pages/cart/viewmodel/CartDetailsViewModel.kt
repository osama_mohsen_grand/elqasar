package grand.app.elqasar.pages.cart.viewmodel

import android.os.Bundle
import androidx.lifecycle.MutableLiveData
import grand.app.elqasar.base.BaseViewModel
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.address.list.AddressAdapter
import grand.app.elqasar.pages.address.list.model.AddressListResponse
import grand.app.elqasar.pages.cart.adapter.CartDetailsAdapter
import grand.app.elqasar.pages.cart.adapter.CartTransactionAdapter
import grand.app.elqasar.pages.cart.model.CartResponse
import grand.app.elqasar.pages.cart.model.PromoRequest
import grand.app.elqasar.pages.cart.ui.CartDetailsFragment
import grand.app.elqasar.repository.UserRepository
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.URLS
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class CartDetailsViewModel : BaseViewModel {
    var promoSend: String = ""

    @Inject
    lateinit var repositoryUser: UserRepository;
    var compositeDisposable: CompositeDisposable

    //    private var liveData: LiveData<Mutable> = MutableLiveData()
    var liveDataViewModel: MutableLiveData<Mutable>
    var type: Int = 0
    private val TAG = "ProductListViewModel"
    var responseAddress: AddressListResponse = AddressListResponse()
    lateinit var responseCart: CartResponse
    var adapterAddress: AddressAdapter
    var adapterTransaction = CartTransactionAdapter()
    var adapterCart: CartDetailsAdapter
    var promo = ""

    @Inject
    constructor(repositoryUser: UserRepository) {
        this.repositoryUser = repositoryUser
        this.liveDataViewModel = MutableLiveData();
        compositeDisposable = CompositeDisposable()
        repositoryUser.setLiveData(liveDataViewModel)
        adapterAddress = AddressAdapter()
        adapterCart = CartDetailsAdapter()
    }

    fun setArgument(
        bundle: Bundle,
        fragment: CartDetailsFragment
    ) {
        adapterCart.setFragment(fragment)
        responseCart = bundle.getSerializable(Constants.CART) as CartResponse
        adapterCart.update(responseCart.data.cart)
        if(responseCart.data.addressId != -1){
           adapterAddress.setAddressSelect(responseCart.data.addressId)
        }
        adapterTransaction.setList(responseCart)
        getAddressList()
    }

    fun getAddressList() {
        repositoryUser.getAddresses()
    }

    fun addAddress(){
        liveDataViewModel.value = Mutable(Constants.ADD_ADDRESS)
    }

    fun submit() {
        liveDataViewModel.value = Mutable(Constants.SUBMIT)
    }

    fun submitPromo(){
        if(promo.trim() != "") {
            repositoryUser.addPromo(PromoRequest(promo))
        }
    }

    protected fun unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }

    override fun onCleared() {
        super.onCleared()
        unSubscribeFromObservable()
    }

    fun setData() {
        adapterAddress.update(responseAddress.data)
        show.set(true)
    }

}