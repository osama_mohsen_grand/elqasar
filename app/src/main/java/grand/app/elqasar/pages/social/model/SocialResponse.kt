package grand.app.elqasar.pages.social.model


import com.google.gson.annotations.SerializedName
import grand.app.elqasar.model.base.StatusMessage
import grand.app.elqasar.pages.social.model.Social

data class SocialResponse(
    @SerializedName("data")
    var social: ArrayList<Social> = ArrayList()
) : StatusMessage()