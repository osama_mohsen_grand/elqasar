package grand.app.elqasar.pages.order.ui

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import grand.app.elqasar.R
import grand.app.elqasar.base.BaseFragment
import grand.app.elqasar.base.MyApplication
import grand.app.elqasar.base.di.IApplicationComponent
import grand.app.elqasar.databinding.FragmentNotificationListBinding
import grand.app.elqasar.databinding.FragmentOrderListBinding
import grand.app.elqasar.databinding.FragmentOrderStatusBinding
import grand.app.elqasar.databinding.FragmentProductListBinding
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.order.model.OrderListResponse
import grand.app.elqasar.pages.order.model.OrderStatusResponse
import grand.app.elqasar.pages.order.viewmodel.OrderListViewModel
import grand.app.elqasar.pages.order.viewmodel.OrderStatusViewModel
import grand.app.elqasar.pages.product.model.FavouriteResponse
import grand.app.elqasar.pages.product.model.list.ProductListResponse
import grand.app.elqasar.pages.product.viewmodel.list.ProductListViewModel
import grand.app.elqasar.pages.settings.notification.model.NotificationResponse
import grand.app.elqasar.pages.settings.notification.viewmodel.NotificationListViewModel
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.URLS
import javax.inject.Inject

class OrderStatusFragment : BaseFragment() {

    private lateinit var binding: FragmentOrderStatusBinding
    @Inject
    lateinit var viewModel: OrderStatusViewModel

    private  val TAG = "OrderStatusFragment"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_order_status, container, false)
        val component: IApplicationComponent? =
            (context!!.applicationContext as MyApplication).applicationComponent
        component?.inject(this)
        Log.d(TAG,"class is here")
        binding.viewmodel = viewModel
        arguments?.let { viewModel.setArgument(it) }
        setEvent()
        // Inflate the layout for this fragment
        return binding.root
    }
    private fun setEvent() {
        viewModel.liveDataViewModel.observe(requireActivity(), Observer {
            val mutable = it as Mutable
            Log.d(TAG,mutable.message)
            handleActions(mutable)
//            Log.d(TAG,(mutable.obj as StatusMessage).mStatus+"")
            if (mutable.message == URLS.ORDER_STATUS) {
                Log.d(TAG,"liveDataViewModel is here")
                viewModel.response = (mutable.obj as OrderStatusResponse)
                viewModel.setData()
            }
        })
    }

    override fun onResume() {
        super.onResume()
        viewModel.repository.setLiveData(viewModel.liveDataViewModel)
    }
}