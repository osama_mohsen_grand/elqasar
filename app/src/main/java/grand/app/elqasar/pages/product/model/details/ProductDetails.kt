package grand.app.elqasar.pages.product.model.details


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import grand.app.elqasar.pages.product.model.details.Image

data class ProductDetails(
    @SerializedName("id")
    @Expose
    var id: Int = 0,
    @SerializedName("name")
    @Expose
    var name: String = "",
    @SerializedName("price")
    @Expose
    var price: String = "",
    @SerializedName("about")
    @Expose
    var about: String = "",
    @SerializedName("desc")
    @Expose
    var desc: String = "",
    @SerializedName("qty")
    @Expose
    var qty: Int = 0,
    @SerializedName("stock")
    @Expose
    var stock: Int = 0,
    @SerializedName("user_favorite")
    @Expose
    var userFavorite: Int = 0,
    @SerializedName("images")
    @Expose
    var images: List<Image> = listOf()
)