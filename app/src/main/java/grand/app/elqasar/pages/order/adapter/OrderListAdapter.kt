package grand.app.elqasar.pages.order.adapter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.elqasar.R
import grand.app.elqasar.databinding.ItemOrderListBinding
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.makeyourclothes.fabric.model.ClothesModelRequest
import grand.app.elqasar.pages.order.model.Order
import grand.app.elqasar.pages.order.ui.OrderDetailsFragment
import grand.app.elqasar.pages.order.ui.OrderStatusFragment
import grand.app.elqasar.pages.order.viewmodel.ItemOrderViewModel
import grand.app.elqasar.pages.product.model.FavouriteRequest
import grand.app.elqasar.repository.ProductRepository
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.helper.MovementHelper
import grand.app.elqasar.utils.resources.ResourceManager

class OrderListAdapter(private var modelList:List<Order> = ArrayList(), private var repository: ProductRepository? = null) : RecyclerView.Adapter<OrderListAdapter.ViewHolder>() {
    // fetch list data
    var favPosition : Int = 0
    var selectedItem : Int = -1


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // binding item_model layout
        val binding: ItemOrderListBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_order_list,
            parent,
            false
        )
        return ViewHolder(
            binding
        )
    }
    val request: ClothesModelRequest = ClothesModelRequest()
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(modelList[position])
        holder.viewModel.mutableLiveData.observeForever {
            val mutable: Mutable = it as Mutable
            selectedItem = it.position
            val model = modelList[mutable.position]
            if(mutable.message == Constants.SUBMIT) {
                val bundle = Bundle()
                bundle.putString(Constants.PAGE,OrderDetailsFragment::class.java.name)
                bundle.putInt(Constants.ID,model.id)
                MovementHelper.startActivityBase(holder.itemView.context,bundle,ResourceManager.getString(R.string.order_details))
            }else if(mutable.message == Constants.FOLLOW_ORDER){
                val bundle = Bundle()
                bundle.putString(Constants.PAGE,OrderStatusFragment::class.java.name)
                bundle.putInt(Constants.ID,model.id)
                MovementHelper.startActivityBase(holder.itemView.context,bundle,ResourceManager.getString(R.string.order_status))
            }
            notifyItemChanged(selectedItem)
        }
    }

    override fun getItemCount(): Int {
        //return data count if exist or 0
        return modelList.size
    }



    fun update(modelList: List<Order>) {
        //update data after call service again in scroll , and notify list which end with
        this.modelList = modelList
        notifyDataSetChanged()
    }

    class ViewHolder(private val binding: ItemOrderListBinding) : RecyclerView.ViewHolder(binding.root) {
        lateinit var viewModel: ItemOrderViewModel

        //bint
        fun bind(model: Order) {
            viewModel = ItemOrderViewModel(model, adapterPosition)
            binding.viewmodel = viewModel
        }
    }
}