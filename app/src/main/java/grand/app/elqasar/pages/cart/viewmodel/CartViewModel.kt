package grand.app.elqasar.pages.cart.viewmodel

import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import grand.app.elqasar.base.BaseViewModel
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.cart.adapter.CartAdapter
import grand.app.elqasar.pages.cart.adapter.CartTransactionAdapter
import grand.app.elqasar.pages.cart.model.CartResponse
import grand.app.elqasar.pages.makeyourclothes.fabric.model.ClothesItem
import grand.app.elqasar.pages.makeyourclothes.fabric.model.ClothesModelRequest
import grand.app.elqasar.repository.ProductRepository
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.helper.AppSpecificHelper
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class CartViewModel : BaseViewModel {
    @Inject
    lateinit var repository: ProductRepository;
    var compositeDisposable: CompositeDisposable

    //    private var liveData: LiveData<Mutable> = MutableLiveData()
    var liveDataViewModel: MutableLiveData<Mutable>
    var type: Int = 0
    private val TAG = "CartViewModel"
    lateinit var response: CartResponse
    var adapter: CartAdapter
    var adapterTransaction = CartTransactionAdapter()
    var title: ObservableField<String> =
        ObservableField("")

    @Inject
    constructor(repository: ProductRepository) {
        this.repository = repository
        this.liveDataViewModel = MutableLiveData();
        compositeDisposable = CompositeDisposable()
        repository.setLiveData(liveDataViewModel)
        adapter = CartAdapter(repository = repository)
        adapter.request = ClothesItem()
        AppSpecificHelper.initClothesModels(adapter.request.models)
    }


    fun getCart() {
        show.set(false)
        repository.getCart()
    }

    fun submit(){
        liveDataViewModel.value = Mutable(Constants.SUBMIT)
    }

    protected fun unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }

    override fun onCleared() {
        super.onCleared()
        unSubscribeFromObservable()
    }

    fun setData() {
        adapter.update(response.data.cart)
        adapterTransaction.setList(response)
        show.set(response.data.cart.size > 0)
        if(!show.get()) noData()
    }

    fun updateFavourite(favorite: Int) {
        adapter.updateFavourite(favorite)
    }

    fun updateCart(response: CartResponse) {
        adapter.updateCart()
        this.response = response
        this.response.data.cost = response.data.cost
        this.response.data.discount = response.data.discount
        this.response.data.promoAmount = response.data.promoAmount
        this.response.data.taxesAmount = response.data.taxesAmount
        this.response.data.shipping = response.data.shipping
        this.response.data.totalPrice = response.data.totalPrice
        adapterTransaction.update(response)
        notifyChange()
    }
}