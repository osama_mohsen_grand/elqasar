package grand.app.elqasar.pages.settings.notification.notify

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.util.Log

class NotificationReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
//        playNotificationSound(context);
        val bundle = intent.extras
        if (bundle != null) {
            for (key in bundle.keySet()) {
                Log.e(
                    TAG,
                    key + " : " + if (bundle[key] != null) bundle[key] else "NULL"
                )
            }
        }
    }

    fun playNotificationSound(context: Context?) {
        try {
            val notification =
                RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
            val r = RingtoneManager.getRingtone(context, notification)
            r.play()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    companion object {
        private const val TAG = "NotificationReceiver"
    }
}