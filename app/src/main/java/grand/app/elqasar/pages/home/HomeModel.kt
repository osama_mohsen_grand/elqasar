package grand.app.elqasar.pages.home

import androidx.fragment.app.Fragment

data class HomeModel(var title: String , var drawable : Int , var fragment: String = "" , val type : Int = 0) {
}