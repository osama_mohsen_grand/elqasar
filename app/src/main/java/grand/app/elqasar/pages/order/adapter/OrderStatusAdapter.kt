package grand.app.elqasar.pages.order.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.elqasar.R
import grand.app.elqasar.databinding.ItemOrderListBinding
import grand.app.elqasar.databinding.ItemOrderStatusBinding
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.makeyourclothes.fabric.model.ClothesModelRequest
import grand.app.elqasar.pages.order.model.Order
import grand.app.elqasar.pages.order.model.OrderStatus
import grand.app.elqasar.pages.order.viewmodel.ItemOrderStatusViewModel
import grand.app.elqasar.pages.order.viewmodel.ItemOrderViewModel
import grand.app.elqasar.pages.product.model.FavouriteRequest
import grand.app.elqasar.repository.ProductRepository
import grand.app.elqasar.utils.Constants

class OrderStatusAdapter(private var modelList:List<OrderStatus> = ArrayList()) : RecyclerView.Adapter<OrderStatusAdapter.ViewHolder>() {
    // fetch list data
    var favPosition : Int = 0
    var selectedItem : Int = -1


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // binding item_model layout
        val binding: ItemOrderStatusBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_order_status,
            parent,
            false
        )
        return ViewHolder(
            binding
        )
    }
    val request: ClothesModelRequest = ClothesModelRequest()
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(modelList[position])
    }

    override fun getItemCount(): Int {
        //return data count if exist or 0
        return modelList.size
    }



    fun update(modelList: List<OrderStatus>) {
        //update data after call service again in scroll , and notify list which end with
        this.modelList = modelList
        notifyDataSetChanged()
    }

    class ViewHolder(private val binding: ItemOrderStatusBinding) : RecyclerView.ViewHolder(binding.root) {
        lateinit var viewModel: ItemOrderStatusViewModel

        //bint
        fun bind(model: OrderStatus) {
            viewModel = ItemOrderStatusViewModel(model, adapterPosition)
            binding.viewmodel = viewModel
        }
    }
}