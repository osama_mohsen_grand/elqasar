package grand.app.elqasar.pages.makeyourclothes.fabric.ui

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import grand.app.elqasar.R
import grand.app.elqasar.base.BaseFragment
import grand.app.elqasar.base.MyApplication
import grand.app.elqasar.base.di.IApplicationComponent
import grand.app.elqasar.databinding.FragmentFabricBinding
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.makeyourclothes.fabric.model.FabricListResponse
import grand.app.elqasar.pages.makeyourclothes.fabric.viewmodel.FabricViewModel
import grand.app.elqasar.pages.product.model.FavouriteResponse
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.URLS
import javax.inject.Inject

class FabricFragment : BaseFragment() {
    private lateinit var binding: FragmentFabricBinding
    @Inject
    lateinit var viewModel: FabricViewModel

    private  val TAG = this::class.java.name

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_fabric, container, false)
        val component: IApplicationComponent? =
            (context!!.applicationContext as MyApplication).applicationComponent
        component?.inject(this)
        binding.viewmodel = viewModel
        arguments?.let { viewModel.setArgument(it) }
        // Inflate the layout for this fragment
        setEvent()
        return binding.root
    }

    private fun setEvent() {
        viewModel.liveDataViewModel.observe(requireActivity(), Observer {
            val mutable = it as Mutable
            handleActions(mutable)
            Log.d(TAG,mutable.message)
            if (mutable.message == URLS.CATEGORIES) {
                Log.d(TAG,"Found")
                viewModel.setData( mutable.obj as FabricListResponse)
            }else if (mutable.message == URLS.ADD_FAVOURITE) {
                viewModel.adapter.updateFavourite( (mutable.obj as FavouriteResponse).isFavorite)
            }else if (mutable.message == Constants.BACK) {
                requireActivity().finish()
            }
        })

        viewModel.adapter.liveDataViewModel.observe(requireActivity(), Observer {
            val intent = Intent()
            intent.putExtra(Constants.BUNDLE,viewModel.adapter.bundle)
            requireActivity().setResult(Activity.RESULT_OK, intent);
            requireActivity().finish()
        })

    }

    override fun onResume() {
        super.onResume()
        viewModel.repository.setLiveData(viewModel.liveDataViewModel)
    }
}