package grand.app.elqasar.pages.product.viewmodel.slider

import android.os.Bundle
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import grand.app.elqasar.base.BaseViewModel
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.product.adapter.ProductAdapter
import grand.app.elqasar.pages.product.model.list.ProductListResponse
import grand.app.elqasar.pages.product.slider.SliderAdapter
import grand.app.elqasar.repository.ProductRepository
import grand.app.elqasar.utils.Constants
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class ProductListSliderViewModel : BaseViewModel {
    @Inject
    lateinit var repository: ProductRepository;
    var compositeDisposable: CompositeDisposable
    var currentItem = 0
    //    private var liveData: LiveData<Mutable> = MutableLiveData()
    var liveDataViewModel: MutableLiveData<Mutable>
    var type: Int = 0
    private val TAG = "ProductListSliderViewModel"
    var response: ProductListResponse = ProductListResponse()
    var adapter: SliderAdapter
    var productAdapter: ProductAdapter
    var haveNext: ObservableBoolean = ObservableBoolean(false)
    var haveBefore: ObservableBoolean = ObservableBoolean(false)
    var title: ObservableField<String> =
        ObservableField("")

    @Inject
    constructor(repository: ProductRepository) {
        this.repository = repository
        this.liveDataViewModel = MutableLiveData();
        compositeDisposable = CompositeDisposable()
        repository.setLiveData(liveDataViewModel)
        adapter = SliderAdapter(repository = repository)
        productAdapter = ProductAdapter(repository = repository)
    }

    fun setArgument(bundle: Bundle) {
        type = bundle.getInt(Constants.TYPE)
        title.set(bundle.getString(Constants.TITLE))
        if (type == 4)
            productAdapter.type = type
        else
            adapter.type = type
        getProducts()
    }

    private fun getProducts() {
        repository.getProducts(type)
    }

    fun back() {
        liveDataViewModel.value = Mutable(Constants.BACK)
    }

    fun search() {
        liveDataViewModel.value = Mutable(Constants.SEARCH)
    }

    fun next() {
        if(currentItem < adapter.modelList.size - 1) {
            currentItem++
            liveDataViewModel.value = Mutable(Constants.NEXT)
        }
        checkLimit()
    }

    fun before() {
        if(currentItem > 0) {
            currentItem--
            liveDataViewModel.value = Mutable(Constants.BEFORE)
        }
        checkLimit()
    }

    private fun checkLimit(){
        haveBefore.set(currentItem != 0)
        haveNext.set(currentItem != adapter.modelList.size - 1)
    }

    protected fun unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }

    override fun onCleared() {
        super.onCleared()
        unSubscribeFromObservable()
    }

    fun setData() {
        if (type == 4)
            productAdapter.update(response.data)
        else
            adapter.update(response.data)
        checkLimit()
    }

    fun updateFavourite(favorite: Int) {
        if (type == 4) productAdapter.updateFavourite(favorite)
        else adapter.updateFavourite(favorite)
    }
}