package grand.app.elqasar.pages.auth.repository

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.google.android.gms.tasks.TaskExecutors
import com.google.firebase.FirebaseException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthProvider
import com.google.firebase.auth.PhoneAuthProvider.ForceResendingToken
import com.google.firebase.auth.PhoneAuthProvider.OnVerificationStateChangedCallbacks
import grand.app.elqasar.R
import grand.app.elqasar.connection.ConnectionHelper
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.repository.BaseRepository
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.resources.ResourceManager
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class VerificationFirebaseSMSRepository : BaseRepository {

    @Inject
    lateinit var connectionHelper: ConnectionHelper

    @Inject
    constructor(connectionHelper: ConnectionHelper) {
        this.connectionHelper = connectionHelper;
    }

    private val TAG = "VerificationFirebaseSMS"

    private var liveData: MutableLiveData<Mutable> = MutableLiveData()
    fun setLiveData(liveData: MutableLiveData<Mutable>) {
        this.liveData = liveData
        connectionHelper.liveData = liveData
    }


    private var verificationId = ""
    var mAuthCredentials: PhoneAuthCredential? = null
    private var mAuth: FirebaseAuth? = null


    fun sendVerificationCode(phone: String) {
        Log.e(TAG, "sendVerificationCode: $phone")
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
            phone, 60, TimeUnit.SECONDS,
            TaskExecutors.MAIN_THREAD, verificationStateChangedCallbacks
        )
        liveData.setValue(Mutable(Constants.SHOW_PROGRESS))
    }


    private val verificationStateChangedCallbacks: OnVerificationStateChangedCallbacks =
        object : OnVerificationStateChangedCallbacks() {
            override fun onVerificationCompleted(phoneAuthCredential: PhoneAuthCredential) {
                liveData.setValue(Mutable(Constants.HIDE_PROGRESS))
                mAuthCredentials = phoneAuthCredential
            }

            /* This one is never called: so i assume there's no problem on my part */
            override fun onVerificationFailed(e: FirebaseException) {
                liveData.setValue(Mutable(Constants.HIDE_PROGRESS))
                e.printStackTrace()
                setMessage(401, e.message)
                liveData.setValue(Mutable(Constants.SHOW_PROGRESS))
            }

            /* This one is called */
            override fun onCodeSent(
                s: String,
                forceResendingToken: ForceResendingToken
            ) {
                super.onCodeSent(s, forceResendingToken)
                verificationId = s
                Log.e(
                    TAG,
                    "onCodeSent: $verificationId"
                )
                setMessage(
                    Constants.RESPONSE_SUCCESS,
                    ResourceManager.getString(R.string.code_had_been_sent)
                )
                liveData.value = Mutable(Constants.HIDE_PROGRESS)
                liveData.value = Mutable(Constants.WRITE_CODE)
            }

            /* This one is also called */
            override fun onCodeAutoRetrievalTimeOut(s: String) {
                super.onCodeAutoRetrievalTimeOut(s)
            }
        }

    fun getVerificationId(): String? {
        return verificationId
    }

    fun verifyCode(verify_id: String, checkCode: String) {
        Log.e(TAG, "checkCode: $checkCode")
        Log.e(
            TAG,
            "verificationId: $verificationId"
        )
        verificationId = verify_id
        val credential =
            PhoneAuthProvider.getCredential(verificationId, checkCode)
        mAuth = FirebaseAuth.getInstance()
        signInWithCredentials(credential)
    }

    private fun signInWithCredentials(credential: PhoneAuthCredential) {
        liveData.setValue(Mutable(Constants.SHOW_PROGRESS))
        mAuth!!.signInWithCredential(credential)
            .addOnCompleteListener { task ->
                liveData.setValue(Mutable(Constants.HIDE_PROGRESS))
                if (task.isSuccessful) {
                    liveData.setValue(Mutable(Constants.SUCCESS))
                } else {
                    liveData.setValue(
                        Mutable(
                            Constants.ERROR,
                            ResourceManager.getString(R.string.verification_code_not_valid)
                        )
                    )
                }
            }
    }

}