package grand.app.elqasar.pages.social

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import grand.app.elqasar.R
import grand.app.elqasar.base.BaseFragment
import grand.app.elqasar.base.MyApplication
import grand.app.elqasar.base.di.IApplicationComponent
import grand.app.elqasar.databinding.FragmentSocialBinding
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.social.model.SocialResponse
import grand.app.elqasar.pages.social.viewmodel.SocialViewModel
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.URLS
import javax.inject.Inject

class SocialFragment : BaseFragment() {
    private lateinit var binding: FragmentSocialBinding
    @Inject
    lateinit var viewModel: SocialViewModel

    private  val TAG = "AccountSettingFragment"

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_social, container, false)
        val component: IApplicationComponent? =
            (context!!.applicationContext as MyApplication).applicationComponent
        component?.inject(this)
        binding.viewmodel = viewModel
        arguments?.let { viewModel.setArgument(it) }
        // Inflate the layout for this fragment
        setEvent()
        return binding.root
    }

    private fun setEvent() {
        viewModel.liveDataViewModel.observe(requireActivity(), Observer {
            val mutable = it as Mutable
            handleActions(mutable)
            Log.d(TAG,mutable.message)
            if (mutable.message == URLS.SETTINGS) {
                Log.d(TAG,"Found")
                viewModel.response = mutable.obj as SocialResponse
                viewModel.adapter.update(viewModel.response.social)
            }
        })

    }

    override fun onResume() {
        super.onResume()
        viewModel.repository.setLiveData(viewModel.liveDataViewModel)

    }
}