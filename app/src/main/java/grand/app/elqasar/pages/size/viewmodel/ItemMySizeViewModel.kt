package grand.app.elqasar.pages.size.viewmodel

import android.util.Log
import android.widget.RadioGroup
import androidx.lifecycle.MutableLiveData
import grand.app.elqasar.R
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.address.list.model.Address
import grand.app.elqasar.pages.order.model.OrderStatus
import grand.app.elqasar.pages.size.model.MySizeModel
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.resources.ResourceManager


data class ItemMySizeViewModel(
    val model: MySizeModel,
    var position: Int
) {
    var mutableLiveData = MutableLiveData<Any>()

    fun submit(){
        mutableLiveData.value = Mutable(Constants.YOUTUBE , position)
    }
}