package grand.app.elqasar.pages.product.model

import com.google.gson.annotations.SerializedName
import grand.app.elqasar.model.base.StatusMessage

class FavouriteResponse(@SerializedName("isFavorite") var isFavorite:Int):StatusMessage() {
}