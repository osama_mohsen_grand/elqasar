package grand.app.elqasar.pages.makeyourclothes.accessories.viewmodel

import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import grand.app.elqasar.R
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.makeyourclothes.fabric.model.Fabric
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.resources.ResourceManager

data class ItemAccessoriesViewModel(val model: Fabric, var position: Int, var checked: Boolean) {

    var mutableLiveData = MutableLiveData<Any>()

    var qty = ObservableField<String>(model.qty.toString())

    var price: ObservableField<String> =
        ObservableField("${model.price} ${ResourceManager.getString(R.string.currency)}")

    fun add() {
        mutableLiveData.value = Mutable(Constants.SELECT, position)
        mutableLiveData.value = Mutable(Constants.ADD, position)
    }

    fun minus() {
        mutableLiveData.value = Mutable(Constants.SELECT, position)
        mutableLiveData.value = Mutable(Constants.MINUS, position)
    }

    fun submit() {
        mutableLiveData.value = Mutable(Constants.SUBMIT, position)
    }
}