package grand.app.elqasar.pages.product.adapter

import android.app.Activity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.github.islamkhsh.CardSliderAdapter
import grand.app.elqasar.R
import grand.app.elqasar.databinding.ItemProductBinding
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.product.model.FavouriteRequest
import grand.app.elqasar.pages.product.model.list.Product
import grand.app.elqasar.pages.product.slider.ItemSliderViewModel
import grand.app.elqasar.pages.product.ui.ProductDetailsFragment
import grand.app.elqasar.repository.ProductRepository
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.dialog.DialogLogin
import grand.app.elqasar.utils.helper.MovementHelper

class ProductAdapter(private var modelList:ArrayList<Product> = ArrayList(), private var repository: ProductRepository): CardSliderAdapter<ProductAdapter.ViewHolder>() {
    // fetch list data
    var favPosition : Int = 0
    var type:Int = 0
    lateinit var product : Product
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // binding item_model layout
        val binding: ItemProductBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_product, parent, false)
        return ViewHolder(binding)
    }

    private  val TAG = "ProductAdapter"
    override fun bindVH(holder: ViewHolder, position: Int) {
        holder.bind(modelList[position])
        Log.d(TAG,""+modelList[position].name)
        holder.viewModel.mutableLiveData.observeForever {
            val mutable : Mutable = it as Mutable

            product = modelList[mutable.position]
            if(mutable.message == Constants.SUBMIT){
                val bundle = Bundle()
                bundle.putString(Constants.PAGE, ProductDetailsFragment::class.java.name)
                bundle.putInt(Constants.ID,product.id)
                bundle.putInt(Constants.TYPE,type)
                bundle.putString(Constants.TITLE,product.name)
                MovementHelper.startActivityBase(holder.itemView.context, bundle);

            }else if(mutable.message == Constants.FAVOURITE){
                favPosition = position
                repository.addFavourite(FavouriteRequest(product.id,type))
            }
        }
    }


    override fun getItemCount(): Int {
        //return data count if exist or 0
        return modelList.size
    }

    fun updateFavourite(favorite: Int) {
        modelList[favPosition].userFavorite = favorite
        notifyItemChanged(favPosition)
    }

    fun update(modelList:ArrayList<Product>){
        //update data after call service again in scroll , and notify list which end with
        this.modelList = modelList
        notifyDataSetChanged()
    }

    class ViewHolder(private val binding: ItemProductBinding):RecyclerView.ViewHolder(binding.root){
        lateinit var viewModel : ItemSliderViewModel
        //bint
        fun bind(model:Product){
            viewModel = ItemSliderViewModel(model,adapterPosition)
            binding.viewmodel = viewModel
        }
    }

    /**
     * This method should update the contents of the {@link VH#itemView} to reflect the item at the
     * given position.
     *
     * @param holder The ViewHolder which should be updated to represent the contents of the
     *        item at the given position in the data set.
     * @param position The position of the item within the adapter's data set.
     */

}