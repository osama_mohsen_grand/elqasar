package grand.app.elqasar.pages.auth.forgetpassword

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import grand.app.elqasar.R
import grand.app.elqasar.activity.MainActivity
import grand.app.elqasar.base.BaseFragment
import grand.app.elqasar.base.MyApplication
import grand.app.elqasar.base.di.IApplicationComponent
import grand.app.elqasar.databinding.FragmentForgetPasswordBinding
import grand.app.elqasar.databinding.FragmentLoginBinding
import grand.app.elqasar.databinding.FragmentLoginOrRegisterBinding
import grand.app.elqasar.databinding.FragmentRegisterBinding
import grand.app.elqasar.pages.auth.register.RegisterFragment
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.auth.code.VerifyCodeFragment
import grand.app.elqasar.pages.auth.forgetpassword.model.ForgetPasswordResponse
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.helper.MovementHelper
import javax.inject.Inject

class ForgetPasswordFragment : BaseFragment() {
    private lateinit var binding: FragmentForgetPasswordBinding

    @Inject
    public lateinit var viewModel: ForgetPasswordViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        binding.viewmodel = viewModel
//        setEvent()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_forget_password, container, false)
        val component: IApplicationComponent? =
            (context!!.applicationContext as MyApplication).applicationComponent
        component?.inject(this)
        binding.viewmodel = viewModel
        setEvent()
        // Inflate the layout for this fragment
        return binding.root
    }

    private fun setEvent() {
        viewModel.liveDataViewModel.observe(requireActivity(), Observer {
            val mutable = it as Mutable
            handleActions(mutable)
            if (mutable.message == Constants.FORGET_PASSWORD) {
                if ((mutable.obj as ForgetPasswordResponse).exist) {
                    viewModel.sendFirebase()
                } else
                    showError((mutable.obj as ForgetPasswordResponse).mMessage)
            } else if (mutable.message == Constants.BACK) {
                requireActivity().finish()
            }else if (mutable.message == Constants.ERROR) {
                requireActivity().finish()
            }else if (mutable.message == Constants.WRITE_CODE) {
                val bundle = Bundle()
                bundle.putString(Constants.PAGE, VerifyCodeFragment::class.java.name)
                bundle.putString(Constants.TYPE, Constants.FORGET_PASSWORD)
                bundle.putString(Constants.PHONE, viewModel.request.phone)
                bundle.putString(Constants.VERIFY_ID, "")
                MovementHelper.startActivityBase(context, bundle)
            }
        })
    }

    override fun onResume() {
        super.onResume()
        viewModel.repository.setLiveData(viewModel.liveDataViewModel)
    }
}