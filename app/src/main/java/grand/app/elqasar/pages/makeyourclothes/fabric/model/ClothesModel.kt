package grand.app.elqasar.pages.makeyourclothes.fabric.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable


class ClothesModel(@SerializedName("id") var id : Int = -1,@SerializedName("type") var type : Int = -1,
                   @SerializedName("qty") var qty : Int = -1, @Transient var name : String = "") : Serializable{}