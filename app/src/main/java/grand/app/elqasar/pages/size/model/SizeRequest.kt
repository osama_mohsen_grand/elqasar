package grand.app.elqasar.pages.size.model

import android.util.Log
import androidx.databinding.ObservableField
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.session.UserHelper
import grand.app.elqasar.utils.validation.Validate
import timber.log.Timber

class SizeRequest() {

    @SerializedName("size_id")
    @Expose
    var sizeId = 1
        get() = field
        set(value) {
            field = value
        }
}