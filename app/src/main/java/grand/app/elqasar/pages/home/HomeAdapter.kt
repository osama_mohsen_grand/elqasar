package grand.app.elqasar.pages.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.elqasar.R
import grand.app.elqasar.databinding.ItemHomeBinding
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.makeyourclothes.fabric.model.ClothesItem
import grand.app.elqasar.pages.makeyourclothes.fabric.ui.FabricFragment
import grand.app.elqasar.pages.product.ui.ProductListSliderFragment
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.helper.AppSpecificHelper
import grand.app.elqasar.utils.helper.MovementHelper
import grand.app.elqasar.utils.resources.ResourceManager

class HomeAdapter : RecyclerView.Adapter<HomeAdapter.ViewHolder>() {
    var width: Int = 0
    var height: Int = 0

    // fetch list data
    private lateinit var modelList: ArrayList<HomeModel>
    val request: ClothesItem = ClothesItem()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // binding item_model layout
        val binding: ItemHomeBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_home,
            parent,
            false
        )
        return ViewHolder(binding,width,height)
    }
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(modelList[position])
        holder.viewModel.mutableLiveData.observeForever {
            val mutable: Mutable = it as Mutable
            if (mutable.message == Constants.SUBMIT) {
                val bundle = Bundle()
                bundle.putString(
                    Constants.PAGE,
                    if (modelList[mutable.position].fragment == "") ProductListSliderFragment::class.java.name else modelList[mutable.position].fragment
                )
                if(mutable.position == 0){
//                    request.models.clear()
//                    for(i in 1..8){
//                        val model = ClothesModel(type = i)
//                        request.models.add(model)
//                    }
                    AppSpecificHelper.initClothesModels(request.models)
                    bundle.putSerializable(Constants.REQUEST_MODELS,request)
                }
                bundle.putInt(Constants.TYPE, modelList[mutable.position].type)
                bundle.putString(Constants.TITLE, modelList[mutable.position].title)
                if(modelList[mutable.position].fragment == FabricFragment::class.java.name)
                    MovementHelper.startActivityBase(holder.itemView.context, bundle , ResourceManager.getString(R.string.fabric_selection));
                else
                    MovementHelper.startActivityBase(holder.itemView.context, bundle);
            }
        }
    }

    override fun getItemCount(): Int {
        //return data count if exist or 0
        return if (::modelList.isInitialized) modelList.size else 0
    }

    fun update(modelList: ArrayList<HomeModel>) {
        //update data after call service again in scroll , and notify list which end with
        this.modelList = modelList
        notifyDataSetChanged()
    }

    class ViewHolder(
        private val binding: ItemHomeBinding,
        val width: Int,
        val height: Int
    ) : RecyclerView.ViewHolder(binding.root) {
        lateinit var viewModel: ItemHomeViewModel

        //bint
        fun bind(model: HomeModel) {
            viewModel = ItemHomeViewModel(model,width,height, adapterPosition)
            binding.viewmodel = viewModel
        }
    }
}