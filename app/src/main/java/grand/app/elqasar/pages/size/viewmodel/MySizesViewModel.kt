package grand.app.elqasar.pages.size.viewmodel

import android.os.Bundle
import android.util.Log
import android.widget.RadioGroup
import androidx.databinding.ObservableBoolean
import androidx.lifecycle.MutableLiveData
import grand.app.elqasar.R
import grand.app.elqasar.base.BaseViewModel
import grand.app.elqasar.connection.FileObject
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.auth.register.RegisterRequest
import grand.app.elqasar.pages.auth.repository.AuthRepository
import grand.app.elqasar.pages.home.HomeAdapter
import grand.app.elqasar.pages.home.HomeModel
import grand.app.elqasar.pages.makeyourclothes.fabric.model.ClothesItem
import grand.app.elqasar.pages.makeyourclothes.fabric.model.ClothesModelRequest
import grand.app.elqasar.pages.size.adapter.MySizesAdapter
import grand.app.elqasar.pages.size.model.MySizesRequest
import grand.app.elqasar.pages.size.model.MySizesResponse
import grand.app.elqasar.pages.size.model.SizeRequest
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.session.UserHelper
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject


class MySizesViewModel : BaseViewModel {
    lateinit var compositeDisposable: CompositeDisposable
    var adapter = MySizesAdapter()
    var list = arrayListOf<HomeModel>()
    var liveDataViewModel: MutableLiveData<Mutable>
    val request = MySizesRequest()
    var response = MySizesResponse()
    var msg = ""
    var type = ""
    var valid = true
    val cart:ObservableBoolean = ObservableBoolean(false)
    var requestClothes: ClothesItem = ClothesItem()

    @Inject
    lateinit var repository: AuthRepository;
    var isBack = false

    @Inject
    constructor(repository: AuthRepository) {
        this.repository = repository
        this.liveDataViewModel = MutableLiveData();
        compositeDisposable = CompositeDisposable()
        repository.setLiveData(liveDataViewModel)
        repository.getMySizes()
    }

    fun setArgument(bundle: Bundle) {
        if (bundle.containsKey(Constants.TYPE))
            this.type = bundle.getString(Constants.TYPE).toString()
        if (bundle.containsKey(Constants.CART)) {
            cart.set(true)
        }
        if(bundle.containsKey(Constants.IS_BACK)){
            isBack = bundle.getBoolean(Constants.IS_BACK)
        }

    }


    private fun unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }

    private val TAG = "MySizesViewModel"

    fun submit() {
        request.ids.clear()
        request.sizes.clear()
        valid = true
        for (i in adapter.modelList.indices) {
            if (adapter.modelList[i].size.trim() == "") {
                msg = "${getString(R.string.please_enter)} ${adapter.modelList[i].name}"
                liveDataViewModel.value = Mutable(Constants.ERROR)
                valid = false
                return
            }
            request.ids.add(adapter.modelList[i].id)
            request.sizes.add(adapter.modelList[i].size)
        }
        if (valid) {
//            if (!cart.get() && UserHelper.isLogin())
                repository.updateSizes(request)
//            else
//                liveDataViewModel.value = Mutable(Constants.CART)
        }
    }


    fun setData() {
        adapter.update(response.data)
        show.set(true)
    }

    override fun onCleared() {
        super.onCleared()
        unSubscribeFromObservable()
    }
}

