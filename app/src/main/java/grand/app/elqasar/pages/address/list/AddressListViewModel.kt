package grand.app.elqasar.pages.address.list

import android.os.Bundle
import android.util.Log
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import grand.app.elqasar.base.BaseViewModel
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.address.list.model.AddressListResponse
import grand.app.elqasar.pages.product.adapter.ProductAdapter
import grand.app.elqasar.pages.product.model.list.ProductListResponse
import grand.app.elqasar.repository.UserRepository
import grand.app.elqasar.utils.Constants
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class AddressListViewModel : BaseViewModel {
    @Inject
    lateinit var repository: UserRepository;
    var compositeDisposable: CompositeDisposable

    //    private var liveData: LiveData<Mutable> = MutableLiveData()
    var liveDataViewModel: MutableLiveData<Mutable>
    var type: Int = 0
    private val TAG = this::class.java.name
    var response: AddressListResponse = AddressListResponse()
    var adapter: AddressAdapter
    var title: ObservableField<String> =
        ObservableField("")

    @Inject
    constructor(repository: UserRepository) {
        this.repository = repository
        this.liveDataViewModel = MutableLiveData();
        compositeDisposable = CompositeDisposable()
        repository.setLiveData(liveDataViewModel)
        adapter = AddressAdapter()
    }

    fun setArgument(bundle: Bundle) {
        type = bundle.getInt(Constants.TYPE)
        title.set(bundle.getString(Constants.TITLE))
        adapter.type = type
        getAddressList()
    }

    private fun getAddressList() {
        repository.getAddresses()
    }

    fun addAddress(){
        liveDataViewModel.value = Mutable(Constants.ADD_ADDRESS)
    }

    fun back() {
        liveDataViewModel.value = Mutable(Constants.BACK)
    }


    protected fun unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }

    override fun onCleared() {
        super.onCleared()
        unSubscribeFromObservable()
    }

    fun setData() {
        adapter.update(response.data)
        show.set(true)
        Log.d(TAG,"done setData")
    }

}