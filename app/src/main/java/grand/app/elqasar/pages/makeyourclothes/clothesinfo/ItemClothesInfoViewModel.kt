package grand.app.elqasar.pages.makeyourclothes.clothesinfo

import android.util.Log
import android.widget.RadioGroup
import androidx.databinding.ObservableBoolean
import androidx.lifecycle.MutableLiveData
import grand.app.elqasar.R
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.address.list.model.Address
import grand.app.elqasar.pages.makeyourclothes.fabric.model.ClothesModel
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.resources.ResourceManager


data class ItemClothesInfoViewModel(
    val model: ClothesModel,
    var position: Int
) {
    var mutableLiveData = MutableLiveData<Any>()

    private  val TAG = "ItemClothesInfoViewModel"
    fun submit(){
        mutableLiveData.value = Mutable(Constants.SUBMIT , position)
    }

    fun onSplitTypeChanged(radioGroup: RadioGroup?, id: Int) {
        mutableLiveData.value = Mutable(Constants.SUBMIT , position)

    }

}