package grand.app.elqasar.pages.order.model.details


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import grand.app.elqasar.pages.cart.model.Cart

data class OrderDetails(
    @SerializedName("address")
    var address: Address = Address(),
    @SerializedName("discount_fees")
    var discountFees: String = "",
    @SerializedName("id")
    var id: Int = 0,
    @SerializedName("order_number")
    var orderNumber: String = "",
    @SerializedName("order_products")
    var orderProducts: ArrayList<Cart> = arrayListOf(),
    @SerializedName("shipping_fees")
    var shippingFees: String = "",
    @SerializedName("subtotal_fees")
    var subtotalFees: String = "",
    @SerializedName("taxes_fees")
    var taxesFees: String = "",
    @SerializedName("taxes_percent")
    var taxesPercent: String = "",
    @SerializedName("total_price")
    var totalPrice: String = "",
    @SerializedName("promo_percent")
    @Expose
    var promoPercent: String = "",
    @SerializedName("promo_amount")
    @Expose
    var promoAmount: Int = 0
)