package grand.app.elqasar.pages.auth.changepassword

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import grand.app.elqasar.R
import grand.app.elqasar.activity.BaseActivity
import grand.app.elqasar.activity.MainActivity
import grand.app.elqasar.base.BaseFragment
import grand.app.elqasar.base.MyApplication
import grand.app.elqasar.base.di.IApplicationComponent
import grand.app.elqasar.databinding.FragmentChangePasswordBinding
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.model.base.StatusMessage
import grand.app.elqasar.pages.auth.login.LoginFragment
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.helper.MovementHelper
import grand.app.elqasar.utils.session.UserHelper
import javax.inject.Inject

class ChangePasswordFragment : BaseFragment() {
    private lateinit var binding: FragmentChangePasswordBinding
    @Inject
    lateinit var viewModel: ChangePasswordViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_change_password, container, false)
        val component: IApplicationComponent? =
            (context!!.applicationContext as MyApplication).applicationComponent
        component?.inject(this)
        binding.viewmodel = viewModel
        arguments?.let { viewModel.setArgument(it) }
        setEvent()
        // Inflate the layout for this fragment
        return binding.root
    }
    private fun setEvent() {
        viewModel.liveDataViewModel.observe(requireActivity(), Observer {
            val mutable = it as Mutable
            handleActions(mutable)
            if (mutable.message == Constants.CHANGE_PASSWORD) {
                if (viewModel.request.isLogin) {
                    finishActivity()
                } else {
                    finishAllActivities()
                    val bundle = Bundle()
                    bundle.putString(Constants.PAGE, LoginFragment::class.java.name)
                    MovementHelper.startActivityBase(context, bundle, "")
                }
                toastMessage((mutable.obj as StatusMessage).mMessage)
            }else if (mutable.message == Constants.BACK) {
                requireActivity().finish()
            }
        })
    }

    override fun onResume() {
        super.onResume()
        viewModel.repository.setLiveData(viewModel.liveDataViewModel)
    }
}