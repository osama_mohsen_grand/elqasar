package grand.app.elqasar.pages.order.model


import com.google.gson.annotations.SerializedName
import grand.app.elqasar.model.base.StatusMessage

data class OrderListResponse(
    @SerializedName("data")
    var data: List<Order>
) : StatusMessage()