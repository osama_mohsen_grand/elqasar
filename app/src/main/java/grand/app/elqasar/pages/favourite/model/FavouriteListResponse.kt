package grand.app.elqasar.pages.favourite.model


import com.google.gson.annotations.SerializedName
import grand.app.elqasar.model.base.StatusMessage

data class FavouriteListResponse(
    @SerializedName("data")
    var data: ArrayList<FavouriteModel> = arrayListOf()
) : StatusMessage()