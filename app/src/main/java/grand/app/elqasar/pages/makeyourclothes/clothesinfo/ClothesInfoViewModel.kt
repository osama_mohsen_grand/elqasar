package grand.app.elqasar.pages.makeyourclothes.clothesinfo

import android.os.Bundle
import android.util.Log
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import grand.app.elqasar.R
import grand.app.elqasar.base.BaseViewModel
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.cart.model.Cart
import grand.app.elqasar.pages.makeyourclothes.fabric.model.ClothesItem
import grand.app.elqasar.pages.makeyourclothes.fabric.model.ClothesModel
import grand.app.elqasar.pages.makeyourclothes.fabric.model.ClothesModelRequest
import grand.app.elqasar.pages.makeyourclothes.fabric.model.FabricListResponse
import grand.app.elqasar.repository.ProductRepository
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.helper.AppSpecificHelper
import grand.app.elqasar.utils.session.UserHelper
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class ClothesInfoViewModel : BaseViewModel {
    @Inject
    lateinit var repository: ProductRepository;
    private val TAG = "ClothesInfoViewModel"
    lateinit var compositeDisposable: CompositeDisposable
    var adapter: ClothesInfoAdapter = ClothesInfoAdapter()
    var liveDataViewModel: MutableLiveData<Mutable>
    lateinit var response: FabricListResponse
    var title: ObservableField<String> = ObservableField("")
    var type: Int = 0
    var number: Int = 0
    lateinit var cart: Cart
    val dresses: ArrayList<String> = ArrayList()
    var dress: ObservableField<String>
    var isCart: ObservableBoolean = ObservableBoolean(false)

    @Inject
    constructor(repository: ProductRepository) {
        this.repository = repository
        this.liveDataViewModel = MutableLiveData();
        dresses.add(getString(R.string.dress) + " " + (number + 1))
        dress = ObservableField(dresses[0])
        repository.setLiveData(liveDataViewModel)
    }

    fun setArgument(bundle: Bundle, fragment: ClothesInfoFragment) {
        adapter.setFragment(fragment)
        if (bundle.containsKey(Constants.CART)) {
            isCart.set(true)
            // from cart to edit or response
            cart = bundle.getSerializable(Constants.CART) as Cart
            adapter.requestClothes.clotheItem.add(ClothesItem())
            Log.d(TAG,"${cart.id}")
            adapter.getRequest().cartIdemId = cart.id
//            adapter.requestClothes.cartIdemId = cart.id

            adapter.allowAction = false
            adapter.getRequest().notes = cart.about
            adapter.getRequest().cloth_id = cart.cartOptions.ids[0]
            adapter.getRequest().detail_id = cart.cartOptions.ids[1]
//            adapter.getRequest().cartIdemId = cart.getR
            adapter.getRequest().models.clear()
            for (i in cart.cartOptions.ids.indices) {
                adapter.getRequest().models.add(
                    ClothesModel(
                        cart.cartOptions.ids[i],
                        i + 1,
                        -1,
                        AppSpecificHelper.getTitleBarType(i + 1)
                    )
                )
            }
            adapter.getRequest().models[cart.cartOptions.ids.size - 1].qty = cart.cartOptions.qty
            if (bundle.containsKey(Constants.ALLOW_ACTION))
                adapter.allowAction = bundle.getBoolean(Constants.ALLOW_ACTION)
            if (bundle.containsKey(Constants.IS_CREATE))
                adapter.isCreate = bundle.getBoolean(Constants.IS_CREATE)
        } else {
            adapter.requestClothes.clotheItem.add(
                bundle.getSerializable(Constants.REQUEST_MODELS) as ClothesItem
            )
            adapter.getRequest().models =
                AppSpecificHelper.getClothesInfo(adapter.getRequest().models)
            adapter.requestClothes.cloth_id = adapter.getRequest().cloth_id
            adapter.requestClothes.cartIdemId = adapter.getRequest().cartIdemId
            adapter.requestClothes.detail_id = adapter.getRequest().detail_id
//            adapter.requestClothes.clotheItem.add(adapter.getRequest())
        }
//        Log.d(TAG, adapter.getRequest().cartIdemId.toString())
        Log.d(TAG,adapter.requestClothes.clotheItem.size.toString())
        adapter.update(adapter.getRequest().models)
    }

    fun add() {
        addTextDropDown()
    }

    private fun addTextDropDown() {
        val request = adapter.getRequest()
//        adapter.requestClothes.clotheItem[number] = adapter.getRequest()
//        number++
        val text = "${getString(R.string.dress)} ${(adapter.requestClothes.clotheItem.size + 1)}"
        adapter.add(request)
        dresses.add(text)
        dress.set(text)
        liveDataViewModel.value = Mutable(Constants.ADD)
    }

    fun submit() {
//        if(number > 0)

        Log.d(TAG,"${adapter.getRequest().cartIdemId}")

        adapter.requestClothes.clotheItem.forEachIndexed { index, clothesItem ->
            Log.d(TAG, "index $index => ")
            for (model in clothesItem.models) {
                Log.d(TAG, " , " + model.id)
            }
        }

            if (adapter.isCreate) {
                repository.updateCartClothesInfo(adapter.requestClothes)
            } else {
                repository.updateCartClothesInfo(adapter.requestClothes)
            }
    }

    fun back(){
        liveDataViewModel.value = Mutable(Constants.BACK)
    }


    private fun unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }

    override fun onCleared() {
        super.onCleared()
        unSubscribeFromObservable()
    }

    fun setRequest(position: Int, text: String) {
        dress.set(text)
        adapter.current = position
    }

}

