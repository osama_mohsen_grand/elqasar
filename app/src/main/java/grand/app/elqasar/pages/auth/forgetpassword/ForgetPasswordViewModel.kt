package grand.app.elqasar.pages.auth.forgetpassword

import androidx.lifecycle.MutableLiveData
import grand.app.elqasar.base.BaseViewModel
import grand.app.elqasar.pages.auth.forgetpassword.model.ForgetPasswordRequest
import grand.app.elqasar.pages.auth.repository.AuthRepository
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.auth.repository.VerificationFirebaseSMSRepository
import grand.app.elqasar.utils.Constants
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class ForgetPasswordViewModel : BaseViewModel {
    @Inject
    lateinit var repository: AuthRepository;
    @Inject
    lateinit var repositoryFireBase: VerificationFirebaseSMSRepository;
    lateinit var compositeDisposable: CompositeDisposable
    var request: ForgetPasswordRequest
    //    private var liveData: LiveData<Mutable> = MutableLiveData()
    var liveDataViewModel: MutableLiveData<Mutable>

    @Inject
    constructor(repository: AuthRepository, repositoryFireBase: VerificationFirebaseSMSRepository){
        this.repository = repository
        this.repositoryFireBase = repositoryFireBase
        this.liveDataViewModel = MutableLiveData();
        compositeDisposable = CompositeDisposable()
        request =
            ForgetPasswordRequest()
        repository.setLiveData(liveDataViewModel)
        repositoryFireBase.setLiveData(liveDataViewModel)
    }

    fun back() {
        liveDataViewModel.value = Mutable(Constants.BACK)
    }

    fun submit(){
        if(request.isValid()){
            repository.forgetPassword(request)
        }
    }

    fun sendFirebase(){
        liveDataViewModel.value = Mutable(Constants.WRITE_CODE)
//        repositoryFireBase.sendVerificationCode(request.phone)
    }

    protected fun unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }

    override fun onCleared() {
        super.onCleared()
        unSubscribeFromObservable()
    }
}