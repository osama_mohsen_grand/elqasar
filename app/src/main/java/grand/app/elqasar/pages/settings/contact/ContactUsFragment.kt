package grand.app.elqasar.pages.settings.contact

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import grand.app.elqasar.R
import grand.app.elqasar.activity.MainActivity
import grand.app.elqasar.base.BaseFragment
import grand.app.elqasar.base.MyApplication
import grand.app.elqasar.base.di.IApplicationComponent
import grand.app.elqasar.databinding.FragmentContactUsBinding
import grand.app.elqasar.databinding.FragmentLoginBinding
import grand.app.elqasar.pages.auth.forgetpassword.ForgetPasswordFragment
import grand.app.elqasar.pages.auth.login.model.LoginResponse
import grand.app.elqasar.pages.auth.register.RegisterFragment
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.model.base.StatusMessage
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.URLS
import grand.app.elqasar.utils.helper.MovementHelper
import grand.app.elqasar.utils.session.UserHelper
import javax.inject.Inject

class ContactUsFragment : BaseFragment() {
    private  val TAG = this::class.java.name
    private lateinit var binding: FragmentContactUsBinding
    @Inject
    lateinit var viewModel: ContactUsViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_contact_us, container, false)
        val component: IApplicationComponent? =
            (context!!.applicationContext as MyApplication).applicationComponent
        component?.inject(this)
        binding.viewmodel = viewModel
        setEvent()
        // Inflate the layout for this fragment
        return binding.root
    }
    private fun setEvent() {
        viewModel.liveDataViewModel.observe(requireActivity(), Observer {
            val mutable = it as Mutable
            Log.d(TAG,mutable.message)
            handleActions(mutable)
//            Log.d(TAG,(mutable.obj as StatusMessage).mStatus+"")
            if (mutable.message == Constants.CONTACT_US) {
                toastMessage((mutable.obj as StatusMessage).mMessage)
                finishAllActivities()
                MovementHelper.startActivity(context,MainActivity::class.java)
            }
        })
    }

    override fun onResume() {
        super.onResume()
        viewModel.repository.setLiveData(viewModel.liveDataViewModel)
    }
}