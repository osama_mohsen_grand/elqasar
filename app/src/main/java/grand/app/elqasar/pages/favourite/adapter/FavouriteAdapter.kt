package grand.app.elqasar.pages.favourite.adapter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.github.islamkhsh.CardSliderAdapter
import grand.app.elqasar.R
import grand.app.elqasar.databinding.ItemFavouriteBinding
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.cart.model.UpdateCartRequest
import grand.app.elqasar.pages.favourite.model.FavouriteModel
import grand.app.elqasar.pages.favourite.viewmodel.ItemFavouriteViewModel
import grand.app.elqasar.pages.makeyourclothes.fabric.model.ClothesItem
import grand.app.elqasar.pages.makeyourclothes.fabric.model.ClothesModelRequest
import grand.app.elqasar.pages.makeyourclothes.tailermade.ui.TailorMadeFragment
import grand.app.elqasar.pages.product.model.FavouriteRequest
import grand.app.elqasar.pages.product.ui.ProductDetailsFragment
import grand.app.elqasar.repository.ProductRepository
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.helper.AppSpecificHelper
import grand.app.elqasar.utils.helper.MovementHelper

class FavouriteAdapter(
    private var modelList: ArrayList<FavouriteModel> = ArrayList(),
    private var repository: ProductRepository
) : CardSliderAdapter<FavouriteAdapter.ViewHolder>() {
    // fetch list data
    var favPosition: Int = 0
    lateinit var request: ClothesItem

    //    var type: Int = 0
    lateinit var favouriteModel: FavouriteModel
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // binding item_model layout
        val binding: ItemFavouriteBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_favourite,
            parent,
            false
        )
        return ViewHolder(binding)
    }


    override fun bindVH(holder: ViewHolder, position: Int) {
        holder.bind(modelList[position])
        holder.viewModel.mutableLiveData.observeForever {
            val mutable: Mutable = it as Mutable
            favouriteModel = modelList[mutable.position]
            if (mutable.message == Constants.SUBMIT) {
                val bundle = Bundle()
                if (favouriteModel.type != 1) {
                    bundle.putString(Constants.PAGE, ProductDetailsFragment::class.java.name)
                    bundle.putInt(Constants.ID, favouriteModel.productId)
                    bundle.putInt(Constants.TYPE, favouriteModel.type)
                    bundle.putString(Constants.TITLE, favouriteModel.name)
                    MovementHelper.startActivityBase(holder.itemView.context, bundle)
                }else{
                    request.models =
                        AppSpecificHelper.selectClothesModel(request.models, modelList[position].productId, modelList[position].type)
                    bundle.putString(Constants.PAGE, TailorMadeFragment::class.java.name)
                    bundle.putInt(Constants.CLOTH_ID, modelList[position].productId)
                    request.cloth_id = modelList[position].productId
                    bundle.putSerializable(Constants.REQUEST_MODELS, request)
//                bundle.putString(Constants.TITLE,Fabric.name)
                    MovementHelper.startActivityBase(holder.itemView.context, bundle, modelList[position].name);
                }
            } else if (mutable.message == Constants.DELETE) {
                favPosition = position
                repository.addFavourite(FavouriteRequest(favouriteModel.productId, favouriteModel.type))
            } else if (mutable.message == Constants.ADD_TO_CART) {
                repository.updateCart(
                    UpdateCartRequest(
                        favouriteModel.productId,
                        1,
                        favouriteModel.type
                    )
                )
            }
        }
    }


    override fun getItemCount(): Int {
        //return data count if exist or 0
        return modelList.size
    }


    fun update(modelList: ArrayList<FavouriteModel>) {
        //update data after call service again in scroll , and notify list which end with
        this.modelList = modelList
        notifyDataSetChanged()
    }

    fun removeFavourite() {
        modelList.removeAt(favPosition)
        notifyDataSetChanged()
    }

    class ViewHolder(private val binding: ItemFavouriteBinding) :
        RecyclerView.ViewHolder(binding.root) {
        lateinit var viewModel: ItemFavouriteViewModel

        //bint
        fun bind(model: FavouriteModel) {
            viewModel = ItemFavouriteViewModel(model, adapterPosition)
            binding.viewmodel = viewModel
        }
    }
}