package grand.app.elqasar.pages.address.list.model


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Address(
    @SerializedName("address")
    @Expose
    var address: String,
    @SerializedName("id")
    @Expose
    var id: Int,
    @SerializedName("name")
    @Expose
    var name: String,
    @SerializedName("phone")
    @Expose
    var phone: String,
    @SerializedName("shipping")
    @Expose
    var shipping: Int
) : Serializable