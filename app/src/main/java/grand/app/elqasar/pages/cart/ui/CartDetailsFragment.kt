package grand.app.elqasar.pages.cart.ui

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import grand.app.elqasar.R
import grand.app.elqasar.base.BaseFragment
import grand.app.elqasar.base.MyApplication
import grand.app.elqasar.base.di.IApplicationComponent
import grand.app.elqasar.databinding.FragmentCartDetailsBinding
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.model.base.StatusMessage
import grand.app.elqasar.pages.address.add.AddAddressFragment
import grand.app.elqasar.pages.address.list.model.Address
import grand.app.elqasar.pages.address.list.model.AddressListResponse
import grand.app.elqasar.pages.cart.model.CartResponse
import grand.app.elqasar.pages.cart.viewmodel.CartDetailsViewModel
import grand.app.elqasar.pages.makeyourclothes.fabric.model.ClothesItem
import grand.app.elqasar.pages.makeyourclothes.fabric.model.ClothesModelRequest
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.URLS
import grand.app.elqasar.utils.helper.AppHelper
import grand.app.elqasar.utils.helper.IOnBackPressed
import grand.app.elqasar.utils.helper.MovementHelper
import javax.inject.Inject

class CartDetailsFragment : BaseFragment() , IOnBackPressed {
    val TAG: String = this::class.java.name

    private lateinit var binding: FragmentCartDetailsBinding

    @Inject
    lateinit var viewModel: CartDetailsViewModel


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_cart_details, container, false)
        val component: IApplicationComponent? =
            (context!!.applicationContext as MyApplication).applicationComponent
        component?.inject(this)
        binding.viewmodel = viewModel
        arguments?.let { viewModel.setArgument(it, this) }
        setEvent()
        setEventAdapter()
        // Inflate the layout for this fragment
        return binding.root
    }

    private fun setEventAdapter() {
        viewModel.adapterAddress.liveDataViewModel.observe(requireActivity(), Observer {
            val shipping = viewModel.responseAddress.data[it.position].shipping
            Log.d(TAG, shipping.toString())
            Log.d(TAG, viewModel.responseCart.data.shipping.toString())
            viewModel.adapterTransaction.updateSipping(shipping)
            Log.d(TAG, viewModel.responseCart.data.shipping.toString())
            viewModel.notifyChange()
        })
    }

    private fun setEvent() {
        viewModel.liveDataViewModel.observe(requireActivity(), Observer {
            val mutable = it as Mutable
            Log.d(TAG, mutable.message)
            handleActions(mutable)
//            Log.d(TAG,(mutable.obj as StatusMessage).mStatus+"")
            if (mutable.message == URLS.ADDRESS_LIST) {
                viewModel.responseAddress = mutable.obj as AddressListResponse
                viewModel.setData()
            } else if (mutable.message == URLS.PROMO) {
                AppHelper.showDialog(requireContext(), (mutable.obj as CartResponse).mMessage)
                viewModel.promoSend = viewModel.promo
                Log.d(TAG,viewModel.promoSend)
                viewModel.promo = ""
                viewModel.adapterTransaction.update((mutable.obj as CartResponse))
                viewModel.notifyChange()
            } else if (mutable.message == Constants.ADD_ADDRESS) {
                val bundle = Bundle()
                bundle.putString(Constants.PAGE, AddAddressFragment::class.java.name)
                MovementHelper.startActivityBaseForResult(
                    this,
                    bundle,
                    Constants.ADDRESS_RESULT,
                    getString(R.string.add_address)
                )
            } else if (mutable.message == Constants.SUBMIT) {
                if (viewModel.adapterAddress.id == -1 && viewModel.responseCart.data.addressId == -1) {
                    showError(getString(R.string.please_add_your_address_first))
                } else {
                    val bundle = Bundle()
                    bundle.putString(Constants.PAGE, CartPaymentFragment::class.java.name)
                    viewModel.responseCart.data.addressId = viewModel.adapterAddress.id
                    viewModel.adapterTransaction.response.data.promo = viewModel.promo
                    bundle.putSerializable(Constants.CART, viewModel.adapterTransaction.response)
                    Log.d(TAG,viewModel.adapterTransaction.response.data.promo)
                    bundle.putString(Constants.PROMO,viewModel.promoSend)
                    MovementHelper.startActivityBase(
                        context,
                        bundle,
                        getString(R.string.paying_off)
                    )
                }
            }else if(mutable.message == URLS.PROMO){
                toastMessage(((mutable.obj) as StatusMessage).mMessage)

            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.d(TAG, "request:$requestCode")
        Log.d(TAG, "result:$resultCode")
        if (requestCode == Constants.ADDRESS_RESULT && resultCode == Activity.RESULT_OK) {
            viewModel.repositoryUser.setLiveData(viewModel.liveDataViewModel)
            data?.let {
                if(data.hasExtra(Constants.ADDRESS)){
                    val address : Address = data.getSerializableExtra(Constants.ADDRESS) as Address
                    viewModel.adapterAddress.add(address,0)
                    viewModel.adapterTransaction.updateSipping(address.shipping)
                }
            }

        }else if (requestCode == Constants.REQUEST_MODELS_RESULT && resultCode == Activity.RESULT_OK) {
            data?.getBundleExtra(Constants.BUNDLE)?.let {
                val cart =
                    it.getSerializable(Constants.CART) as CartResponse
                val clothesRequest =
                    it.getSerializable(Constants.REQUEST_MODELS) as ClothesItem
                val ids = arrayListOf<Int>()
                for(i in 0 until  clothesRequest.models.size){
                    ids.add(clothesRequest.models[i].id)
                }
                viewModel.adapterCart.modelList[viewModel.adapterCart.selectedItem].cartOptions .ids= ids
                viewModel.adapterCart.modelList[viewModel.adapterCart.selectedItem].cartOptions .qty= clothesRequest.models[clothesRequest.models.size -1].qty
                viewModel.adapterCart.modelList[viewModel.adapterCart.selectedItem].about = clothesRequest.notes

                viewModel.adapterTransaction.update(cart)
                viewModel.adapterCart.update(cart.data.cart);

                viewModel.adapterCart.notifyItemChanged(viewModel.adapterCart.selectedItem)


            }
        }
    }

    override fun onResume() {
        super.onResume()
        viewModel.repositoryUser.setLiveData(viewModel.liveDataViewModel)
    }

    override fun onBackPressed(): Boolean {
        Log.d(TAG,"onBackPressed")
        val intent = Intent()
        intent.putExtra(Constants.RELOAD, true)
        requireActivity().setResult(Activity.RESULT_OK, intent);
        requireActivity().finish()
        return false
    }
}