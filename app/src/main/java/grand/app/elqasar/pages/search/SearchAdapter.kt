package grand.app.elqasar.pages.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.elqasar.R
import grand.app.elqasar.databinding.ItemSearchBinding
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.makeyourclothes.fabric.model.ClothesModelRequest
import grand.app.elqasar.pages.product.model.FavouriteRequest
import grand.app.elqasar.pages.product.model.list.Product
import grand.app.elqasar.pages.product.slider.ItemSliderViewModel
import grand.app.elqasar.pages.product.ui.ProductDetailsFragment
import grand.app.elqasar.repository.ProductRepository
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.helper.MovementHelper

class SearchAdapter(private var modelList:List<Product> = ArrayList()) : RecyclerView.Adapter<SearchAdapter.ViewHolder>() {
    // fetch list data
    var favPosition : Int = 0
    var selectedItem : Int = -1
    var search: String = ""


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // binding item_model layout
        val binding: ItemSearchBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_search,
            parent,
            false
        )
        return ViewHolder(
            binding
        )
    }
    val request: ClothesModelRequest = ClothesModelRequest()
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(modelList[position])
        holder.viewModel.mutableLiveData.observeForever {
            val mutable: Mutable = it as Mutable
            selectedItem = it.position
            val model = modelList[mutable.position]
            val bundle = Bundle()
            bundle.putString(Constants.PAGE, ProductDetailsFragment::class.java.name)
            bundle.putInt(Constants.ID,model.id)
            bundle.putInt(Constants.TYPE,model.type)
            bundle.putString(Constants.TITLE,model.name)
            MovementHelper.startActivityBase(holder.itemView.context, bundle);
            notifyItemChanged(selectedItem)
        }
    }

    override fun getItemCount(): Int {
        //return data count if exist or 0
        return modelList.size
    }


    fun submitSearch(modelList:List<Product>){
        this.modelList = modelList
        notifyDataSetChanged()
    }

    fun update(modelList: ArrayList<Product>) {
        //update data after call service again in scroll , and notify list which end with
        this.modelList = modelList
        notifyDataSetChanged()
    }

    class ViewHolder(private val binding: ItemSearchBinding) : RecyclerView.ViewHolder(binding.root) {
        lateinit var viewModel: ItemSliderViewModel

        //bint
        fun bind(model: Product) {
            viewModel = ItemSliderViewModel(model, adapterPosition)
            binding.viewmodel = viewModel
        }
    }
}