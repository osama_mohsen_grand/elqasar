package grand.app.elqasar.pages.makeyourclothes.accessories

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import grand.app.elqasar.R
import grand.app.elqasar.base.BaseFragment
import grand.app.elqasar.base.MyApplication
import grand.app.elqasar.base.di.IApplicationComponent
import grand.app.elqasar.databinding.FragmentAccessoriesBinding
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.makeyourclothes.accessories.viewmodel.AccessoriesViewModel
import grand.app.elqasar.pages.makeyourclothes.fabric.model.Fabric
import grand.app.elqasar.pages.makeyourclothes.fabric.model.FabricListResponse
import grand.app.elqasar.pages.notes.NotesFragment
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.URLS
import grand.app.elqasar.utils.helper.AppSpecificHelper
import grand.app.elqasar.utils.helper.MovementHelper
import javax.inject.Inject

class AccessoriesFragment : BaseFragment() {
    private lateinit var binding: FragmentAccessoriesBinding

    @Inject
    lateinit var viewModel: AccessoriesViewModel

    private val TAG = this::class.java.name


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_accessories, container, false)
        val component: IApplicationComponent? =
            (context!!.applicationContext as MyApplication).applicationComponent
        component?.inject(this)
        binding.viewmodel = viewModel
        arguments?.let { viewModel.setArgument(it) }
        // Inflate the layout for this fragment
        setEvent()
//        nextPage()
        return binding.root
    }

    private fun setEvent() {
        viewModel.liveDataViewModel.observe(requireActivity(), Observer {
            val mutable = it as Mutable
            handleActions(mutable)
            Log.d(TAG, mutable.message)
            if (mutable.message == URLS.CATEGORIES) {
                viewModel.setData(mutable.obj as FabricListResponse)
            } else if (mutable.message == Constants.ESCAPE) {
                viewModel.adapter.request.models = AppSpecificHelper.selectClothesModel(
                    viewModel.adapter.request.models,
                    -1, -1
                )
                nextPage()
            } else if (mutable.message == Constants.BACK) {
                finishActivity()
            }
        })

        viewModel.adapter.liveDataViewModel.observe(requireActivity(), Observer {
            val mutable = it as Mutable
            if (mutable.message == Constants.SUBMIT) {
                val fabric: Fabric = viewModel.adapter.modelList[viewModel.adapter.selectedItem]
                viewModel.adapter.request.models = AppSpecificHelper.selectClothesModel(
                    viewModel.adapter.request.models,
                    fabric.id, viewModel.adapter.type, viewModel.adapter.qty
                )
                nextPage()
            }
        })

    }

    private fun nextPage() {
        val bundle = Bundle()
        bundle.putSerializable(Constants.REQUEST_MODELS, viewModel.adapter.request)
        if (!viewModel.adapter.isBack) {
            bundle.putString(Constants.PAGE, NotesFragment::class.java.name)
            MovementHelper.startActivityBase(context, bundle, getString(R.string.notes));
        } else {
            val intent = Intent()
            intent.putExtra(Constants.BUNDLE, bundle)
            requireActivity().setResult(Activity.RESULT_OK, intent);
            requireActivity().finish()
        }
    }

    override fun onResume() {
        super.onResume()
        viewModel.liveDataViewModel = viewModel.liveDataViewModel
    }
}