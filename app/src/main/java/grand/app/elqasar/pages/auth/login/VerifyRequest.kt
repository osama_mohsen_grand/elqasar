package grand.app.elqasar.pages.auth.login

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class VerifyRequest(
    @SerializedName("phone")
    @Expose var phone: String,
    @SerializedName("code")
    @Expose var code: String
) {
}