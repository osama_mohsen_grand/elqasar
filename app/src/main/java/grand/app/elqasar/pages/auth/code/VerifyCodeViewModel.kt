package grand.app.elqasar.pages.auth.code

import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import grand.app.elqasar.R
import grand.app.elqasar.base.BaseViewModel
import grand.app.elqasar.connection.FileObject
import grand.app.elqasar.pages.auth.register.RegisterRequest
import grand.app.elqasar.pages.auth.repository.AuthRepository
import grand.app.elqasar.pages.auth.repository.VerificationFirebaseSMSRepository
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.auth.login.VerifyRequest
import grand.app.elqasar.utils.Constants
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

open class VerifyCodeViewModel : BaseViewModel {

    @Inject
    lateinit var repository: AuthRepository;

    @Inject
    lateinit var repositoryFireBase: VerificationFirebaseSMSRepository;
    private var verify: String = ""
    var type: String = ""
    var code: String = ""
    private  val TAG = "VerificationViewModel"
    var timeLeftText : ObservableField<String> = ObservableField("01:00")
    private val TIME_DURATION:Long = 60000
    lateinit var countDownTimer : CountDownTimer
    var timeLeftInMilliSecond : Long = TIME_DURATION
    var timerRunning : Boolean = false
    lateinit var file: FileObject


    var resend : ObservableBoolean = ObservableBoolean(false)

    lateinit var registerRequest : RegisterRequest

    var compositeDisposable: CompositeDisposable

    //    private var liveData: LiveData<Mutable> = MutableLiveData()
    var liveDataViewModel: MutableLiveData<Mutable>
    var x: Int = 6
    var isBack  = false

    @Inject
    constructor(repository: AuthRepository,repositoryFireBase: VerificationFirebaseSMSRepository){
        this.repository = repository
        this.repositoryFireBase = repositoryFireBase
        this.liveDataViewModel = MutableLiveData();
        compositeDisposable = CompositeDisposable()
        repository.setLiveData(liveDataViewModel)
        repositoryFireBase.setLiveData(liveDataViewModel)
    }


    fun setArgument( bundle : Bundle){
        this.type = bundle.getString(Constants.TYPE).toString()
        this.verify = bundle.getString(Constants.VERIFY_ID).toString()
        if(bundle.containsKey(Constants.IS_BACK)){
            isBack = bundle.getBoolean(Constants.IS_BACK)
        }
        if(type != Constants.FORGET_PASSWORD) {
            this.registerRequest = bundle.getSerializable(Constants.REGISTER) as RegisterRequest
            this.file = bundle.getSerializable(Constants.IMAGE) as FileObject
        }else{
            //forgetPassword
            this.registerRequest = RegisterRequest()
            this.registerRequest.phone = bundle.getString(Constants.PHONE).toString()
        }
        startTimer()
    }


    private fun startTimer() {
        resend.set(false)
        timeLeftInMilliSecond = TIME_DURATION
        countDownTimer = object: CountDownTimer(timeLeftInMilliSecond, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                timeLeftInMilliSecond = millisUntilFinished
                updateTimer()
            }

            override fun onFinish() {
                countDownTimer.cancel()
                timerRunning = false
                resend.set(true)
            }
        }
        countDownTimer.start()
        timerRunning = true
    }

    private fun updateTimer() {
        val minute = (timeLeftInMilliSecond / 60000).toInt()
        val second = (timeLeftInMilliSecond % 60000 / 1000).toInt()
        timeLeftText.set("$minute:")
        if(second < 10) timeLeftText .set(timeLeftText.get()+"0")
        timeLeftText.set(timeLeftText.get()+"$second")
    }

    fun back(){
        liveDataViewModel.value = Mutable(Constants.BACK)
    }

    fun stopTimer(){
        countDownTimer.cancel()
        timerRunning = false
        resend.set(false)
    }


//    fun resend(){
//        Log.d(TAG,"resend")
//        startTimer()
//        repositoryFireBase.sendVerificationCode(registerRequest.phone)
//    }

    fun submit(){
//        if(code.length == 6){
//            repositoryFireBase.verifyCode(verify,code)
//        }
        if(code.length == 4) {
//            liveDataViewModel.value = Mutable(Constants.SUCCESS)
            repository.verifyCode(VerifyRequest(registerRequest.phone,code))
        }else {
            liveDataViewModel.value = Mutable(Constants.ERROR,getString(R.string.verification_code_not_correct))
        }
    }

    fun register(){
        repository.register(registerRequest)
    }

    fun updateProfile() {
        repository.updateProfile(registerRequest, file, false)
    }

    protected fun unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }

    override fun onCleared() {
        super.onCleared()
        unSubscribeFromObservable()
    }


}