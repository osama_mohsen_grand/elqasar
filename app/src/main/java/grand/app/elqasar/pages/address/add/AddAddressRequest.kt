package grand.app.elqasar.pages.address.add

import android.util.Log
import androidx.databinding.ObservableField
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.validation.Validate
import java.io.Serializable

class AddAddressRequest() : Serializable {



    @SerializedName("type")
    @Expose
    var type = 2

    @SerializedName("first_name")
    @Expose
    var firstName = ""
        get() = field
        set(value) {
            field = value
            this.firstNameError.set("")
        }


    @SerializedName("last_name")
    @Expose
    var lastName = ""
        get() = field
        set(value) {
            field = value
            lastNameError.set("")
        }

    @SerializedName("phone1")
    @Expose
    var phone1 = ""
        get() = field
        set(value) {
            field = value
            phone1Error.set("")
        }

    @SerializedName("phone2")
    @Expose
    var phone2 = ""
        get() = field
        set(value) {
            field = value
        }

    @SerializedName("country")
    @Expose
    var country = ""
        get() = field
        set(value) {
            field = value
            countryError.set("")
        }

    @SerializedName("gov")
    @Expose
    var gov = "test"
        get() = field
        set(value) {
            field = value
            govError.set("")
        }

    @SerializedName("city")
    @Expose
    var city = ""
        get() = field
        set(value) {
            field = value
            cityError.set("")
        }


    @SerializedName("region")
    @Expose
    var region = ""
        get() = field
        set(value) {
            field = value
            regionError.set("")
        }

    @SerializedName("street_no")
    @Expose
    var street_no = ""
        get() = field
        set(value) {
            field = value
            Log.d(TAG,"field")
            streetNoError.set("")
        }

    @SerializedName("build_no")
    @Expose
    var build_no = ""
        get() = field
        set(value) {
            field = value
            buildNoError.set("")
        }


    @SerializedName("house")
    @Expose
    var house = ""
        get() = field
        set(value) {
            field = value
            houseError.set("")
        }

    @SerializedName("mark")
    @Expose
    var specialArea = ""
        get() = field
        set(value) {
            field = value
            specialAreaError.set("")
        }


    @Transient
    lateinit var lastNameError: ObservableField<String>

    @Transient
    lateinit var firstNameError: ObservableField<String>


    @Transient
    lateinit var phone1Error: ObservableField<String>

    //here init
    @Transient
    lateinit var countryError: ObservableField<String>

    @Transient
    lateinit var govError: ObservableField<String>

    @Transient
    lateinit var cityError: ObservableField<String>

    @Transient
    lateinit var regionError: ObservableField<String>

    @Transient
    lateinit var streetNoError: ObservableField<String>


    @Transient
    lateinit var buildNoError: ObservableField<String>

    @Transient
    lateinit var houseError: ObservableField<String>

    @Transient
    lateinit var specialAreaError: ObservableField<String>


    init {
        firstNameError = ObservableField()
        lastNameError = ObservableField()
        phone1Error = ObservableField()
        countryError = ObservableField()
        govError = ObservableField()
        cityError = ObservableField()
        regionError = ObservableField()
        streetNoError = ObservableField()
        buildNoError = ObservableField()
        specialAreaError = ObservableField()
        houseError = ObservableField()

    }

    private val TAG = "AddAddressRequest"

    fun isValid(): Boolean {
        var valid = true
        if (!Validate.isValid(firstName)) {
            Log.d(TAG,"error:firstName")
            this.firstNameError.set(Validate.error)
            valid = false
        } else this.firstNameError.set("")

        if (!Validate.isValid(lastName)) {
            Log.d(TAG,"error:lastName")
            lastNameError.set(Validate.error)
            valid = false
        } else lastNameError.set("")

        if (!Validate.isValid(country)) {
            Log.d(TAG,"error:country")
            countryError.set(Validate.error)
            valid = false
        } else countryError.set("")

        if (!Validate.isValid(city)) {
            Log.d(TAG,"error:city")
            cityError.set(Validate.error)
            valid = false
        } else cityError.set("")

//        if (!Validate.isValid(gov)) {
//            Log.d(TAG,"error:street_no")
//            this.govError.set(Validate.error)
//            valid = false
//        } else this.govError.set("")

        if (!Validate.isValid(street_no)) {
            Log.d(TAG,"error:street_no")
            streetNoError.set(Validate.error)
            valid = false
        } else streetNoError.set("")


//        if (!Validate.isValid(region)) {
//            Log.d(TAG,"error:region")
//            regionError.set(Validate.error)
//            valid = false
//        } else regionError.set("")

        if (!Validate.isValid(build_no)) {
            Log.d(TAG,"error:build_no")
            buildNoError.set(Validate.error)
            valid = false
        } else buildNoError.set("")

        if (!Validate.isValid(specialArea)) {
            Log.d(TAG,"error:specialArea")
            specialAreaError.set(Validate.error)
            valid = false
        } else specialAreaError.set("")

        if (!Validate.isValid(house)) {
            Log.d(TAG,"error:doorNo")
            houseError.set(Validate.error)
            valid = false
        } else houseError.set("")


        if (!Validate.isValidPhoneRegularExpression(phone1)) {
            Log.d(TAG,"error:phone1")
            phone1Error.set(Validate.error)
            valid = false
        } else phone1Error.set("")

        return valid
    }


}