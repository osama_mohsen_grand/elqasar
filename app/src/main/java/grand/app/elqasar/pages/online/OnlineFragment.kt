package grand.app.elqasar.pages.online

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.databinding.DataBindingUtil
import grand.app.elqasar.R
import grand.app.elqasar.activity.MainActivity
import grand.app.elqasar.base.BaseFragment
import grand.app.elqasar.base.MyApplication
import grand.app.elqasar.base.di.IApplicationComponent
import grand.app.elqasar.databinding.FragmentOnlineBinding
import grand.app.elqasar.model.base.StatusMessage
import grand.app.elqasar.pages.cart.model.OnlineResponse
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.helper.MovementHelper
import grand.app.elqasar.utils.resources.ResourceManager
import grand.app.elqasar.utils.session.SharedPreferenceHelper
import im.delight.android.webview.AdvancedWebView


class OnlineFragment : BaseFragment() , AdvancedWebView.Listener {
    val TAG: String = this::class.java.name

    private lateinit var binding: FragmentOnlineBinding

    lateinit var response: OnlineResponse
    lateinit var mWebView: AdvancedWebView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_online, container, false)
        val component: IApplicationComponent? =
            (context!!.applicationContext as MyApplication).applicationComponent
        component?.inject(this)
        mWebView = binding.webview
        getData()
        mWebView.setMixedContentAllowed(false);

        // Inflate the layout for this fragment
        return binding.root
    }
    private fun getData() {

        arguments?.let {
            response = it.getSerializable(Constants.ONLINE) as OnlineResponse
            mWebView.loadUrl(response.data.data.invoiceUrl);
            mWebView.webViewClient = object : WebViewClient() {
                override fun onPageFinished(view: WebView, url: String) {
                    // do your stuff here
                    if(url.contains(response.data.paymentSuccess)){
                        toastMessage(ResourceManager.getString(R.string.payment_success))
                        SharedPreferenceHelper.saveKey(Constants.CART,"")
                        requireActivity().finishAffinity()
                        val intent = Intent(requireActivity(),MainActivity::class.java)
                        intent.putExtra(Constants.DIALOG,getString(R.string.your_order_had_been_sent))
                        startActivity(intent)
                    }else if(url.contains(response.data.paymentFailed)){
                        requireActivity().finish()
                        showError(ResourceManager.getString(R.string.payment_failed))
                    }
                }
            }

        }
    }
    override fun onPageFinished(url: String?) {
        Log.d(TAG, "url:$url")
        TODO("Not yet implemented")
    }

    override fun onPageError(errorCode: Int, description: String?, failingUrl: String?) {
        TODO("Not yet implemented")
    }

    override fun onDownloadRequested(
        url: String?,
        suggestedFilename: String?,
        mimeType: String?,
        contentLength: Long,
        contentDisposition: String?,
        userAgent: String?
    ) {
        TODO("Not yet implemented")
    }

    override fun onExternalPageRequest(url: String?) {
        TODO("Not yet implemented")
    }

    override fun onPageStarted(url: String?, favicon: Bitmap?) {
        TODO("Not yet implemented")
    }

    @SuppressLint("NewApi")
    override fun onResume() {
        super.onResume()
        mWebView.onResume()
        // ...
    }

    @SuppressLint("NewApi")
    override fun onPause() {
        mWebView.onPause()
        super.onPause()
    }

    override fun onDestroy() {
        mWebView.onDestroy()
        // ...
        super.onDestroy()
    }
}