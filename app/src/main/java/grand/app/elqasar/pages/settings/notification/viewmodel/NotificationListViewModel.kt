package grand.app.elqasar.pages.settings.notification.viewmodel

import android.os.Bundle
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import grand.app.elqasar.base.BaseViewModel
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.product.adapter.ProductAdapter
import grand.app.elqasar.pages.product.model.list.ProductListResponse
import grand.app.elqasar.pages.settings.notification.NotificationAdapter
import grand.app.elqasar.pages.settings.notification.model.NotificationResponse
import grand.app.elqasar.pages.settings.repository.SettingsRepository
import grand.app.elqasar.repository.ProductRepository
import grand.app.elqasar.utils.Constants
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class NotificationListViewModel : BaseViewModel {
    @Inject
    lateinit var repository: SettingsRepository;
    var compositeDisposable: CompositeDisposable

    //    private var liveData: LiveData<Mutable> = MutableLiveData()
    var liveDataViewModel: MutableLiveData<Mutable>
    var type: Int = 0
    private val TAG = "NotificationListViewModel"
    lateinit var response: NotificationResponse
    var adapter: NotificationAdapter
    var title: ObservableField<String> =
        ObservableField("")

    @Inject
    constructor(repository: SettingsRepository) {
        this.repository = repository
        this.liveDataViewModel = MutableLiveData();
        compositeDisposable = CompositeDisposable()
        repository.setLiveData(liveDataViewModel)
        adapter = NotificationAdapter( )
        repository.getNotifications()
    }

    protected fun unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }

    override fun onCleared() {
        super.onCleared()
        unSubscribeFromObservable()
    }

    fun setData() {
        adapter.update(response.data)
        show.set(true)
    }

}