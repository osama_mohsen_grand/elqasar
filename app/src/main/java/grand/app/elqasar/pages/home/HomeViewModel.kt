package grand.app.elqasar.pages.home

import androidx.lifecycle.MutableLiveData
import grand.app.elqasar.R
import grand.app.elqasar.base.BaseViewModel
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.makeyourclothes.fabric.ui.FabricFragment
import grand.app.elqasar.pages.product.ui.ProductListFragment
import grand.app.elqasar.utils.Constants
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class HomeViewModel : BaseViewModel {
    //    @Inject
//    lateinit var repository: SettingsRepository;
    lateinit var compositeDisposable: CompositeDisposable
    var adapter = HomeAdapter()
    var list = arrayListOf<HomeModel>()
    var liveDataViewModel: MutableLiveData<Mutable>
    var x: Int = 6

    @Inject
    constructor() {
        this.liveDataViewModel = MutableLiveData();
    }

    fun add() {
        list.add(HomeModel(getString(R.string.detach_your_address), R.drawable.img_make_clothes, FabricFragment::class.java.name,1))
        list.add(HomeModel(getString(R.string.pyjama), R.drawable.img_home_sleep, "", 2))
        list.add(
            HomeModel(
                getString(R.string.models), R.drawable.img_models,
                "", 3
            )
        )
        list.add(
            HomeModel(
                getString(R.string.another_sales),
                R.drawable.img_another_sales,
                ProductListFragment::class.java.name,
                4
            )
        )
        adapter.update(list)
    }

    fun notification(){
        liveDataViewModel.value = Mutable(Constants.NOTIFICATIONS)
    }
    fun search(){
        liveDataViewModel.value = Mutable(Constants.SEARCH)
    }

    protected fun unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }

    override fun onCleared() {
        super.onCleared()
        unSubscribeFromObservable()
    }
}

