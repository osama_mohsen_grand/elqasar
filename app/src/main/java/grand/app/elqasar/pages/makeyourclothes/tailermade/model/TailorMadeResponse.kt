package grand.app.elqasar.pages.makeyourclothes.tailermade.model


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import grand.app.elqasar.model.base.StatusMessage

data class TailorMadeResponse(
    @SerializedName("data")
    @Expose
    var data: ArrayList<TailorMade>
): StatusMessage()