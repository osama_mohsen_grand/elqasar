package grand.app.elqasar.pages.home

import android.util.Log
import androidx.lifecycle.MutableLiveData
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.utils.Constants

data class ItemHomeViewModel(val model : HomeModel, var width: Int = 0, var height: Int = 0, var position : Int) {
    private  val TAG = "ItemHomeViewModel"
    init {
        Log.d(TAG,"$height")
    }
    var mutableLiveData = MutableLiveData<Any>()
    fun submit(){
        mutableLiveData.value = Mutable(Constants.SUBMIT , position)
    }


}