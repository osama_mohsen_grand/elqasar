package grand.app.elqasar.pages.search

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.TextView.OnEditorActionListener
import androidx.appcompat.widget.SearchView
import androidx.core.widget.doOnTextChanged
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import grand.app.elqasar.R
import grand.app.elqasar.base.BaseFragment
import grand.app.elqasar.base.MyApplication
import grand.app.elqasar.base.di.IApplicationComponent
import grand.app.elqasar.databinding.FragmentSearchBinding
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.product.model.list.ProductListResponse
import grand.app.elqasar.utils.URLS
import grand.app.elqasar.utils.helper.AppHelper
import io.reactivex.Observable
import java.util.concurrent.TimeUnit
import javax.inject.Inject


class SearchFragment : BaseFragment() {
    val TAG: String = this::class.java.name

    private lateinit var binding: FragmentSearchBinding
    @Inject
    lateinit var viewModel: SearchViewModel


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_search, container, false)
        val component: IApplicationComponent? =
            (context!!.applicationContext as MyApplication).applicationComponent
        component?.inject(this)
        binding.viewmodel = viewModel
        init()
        setEvent()
        // Inflate the layout for this fragment
        return binding.root
    }
    private fun setEvent() {
        viewModel.liveDataViewModel.observe(requireActivity(), Observer {
            val mutable = it as Mutable
            Log.d(TAG,mutable.message)
            handleActions(mutable)
//            Log.d(TAG,(mutable.obj as StatusMessage).mStatus+"")
            if (mutable.message == URLS.SEARCH) {
                viewModel.response = (mutable.obj as ProductListResponse)
                viewModel.setData()
            }
        })
    }

    private fun init() {
        Observable.create<Any> {
            binding.search.doOnTextChanged { text, start, before, count ->
                viewModel.search(text.toString())
            }
        }.debounce(2, TimeUnit.SECONDS)
            .subscribe()

    }
    override fun onResume() {
        super.onResume()
        viewModel.repository.setLiveData(viewModel.liveDataViewModel)
    }
}