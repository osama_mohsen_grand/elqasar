package grand.app.elqasar.pages.social.viewmodel

import androidx.lifecycle.MutableLiveData
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.social.model.Social
import grand.app.elqasar.utils.Constants

data class ItemSocialViewModel(val model : Social, var position : Int) {
    var mutableLiveData = MutableLiveData<Any>()
    fun submit(){
        mutableLiveData.value = Mutable(Constants.SUBMIT , position)
    }
}