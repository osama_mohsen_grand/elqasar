package grand.app.elqasar.pages.order.model


import com.google.gson.annotations.SerializedName

data class Order(
    @SerializedName("id")
    var id: Int,
    @SerializedName("status_id")
    var statusId: Int,
    @SerializedName("name")
    var name: String,
    @SerializedName("about")
    var about: String,
    @SerializedName("image")
    var image: String,
    @SerializedName("price")
    var price: String,
    @SerializedName("qty")
    var qty: Int,
    @SerializedName("total_price")
    var totalPrice: String
)