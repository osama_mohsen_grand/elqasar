package grand.app.elqasar.pages.auth.repository

import android.util.Log
import androidx.lifecycle.MutableLiveData
import grand.app.elqasar.pages.auth.forgetpassword.model.ForgetPasswordRequest
import grand.app.elqasar.pages.auth.login.model.LoginRequest
import grand.app.elqasar.pages.auth.login.model.LoginResponse
import grand.app.elqasar.pages.auth.register.RegisterRequest
import grand.app.elqasar.connection.ConnectionHelper
import grand.app.elqasar.connection.FileObject
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.model.base.StatusMessage
import grand.app.elqasar.pages.auth.changepassword.ChangePasswordRequest
import grand.app.elqasar.pages.auth.forgetpassword.model.ForgetPasswordResponse
import grand.app.elqasar.pages.auth.login.VerifyRequest
import grand.app.elqasar.pages.auth.login.model.TokenRequest
import grand.app.elqasar.pages.size.model.MySizesRequest
import grand.app.elqasar.pages.size.model.MySizesResponse
import grand.app.elqasar.pages.size.model.SizeRequest
import grand.app.elqasar.repository.BaseRepository
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.URLS
import io.reactivex.disposables.Disposable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AuthRepository : BaseRepository {

    @Inject
    lateinit var connectionHelper: ConnectionHelper

    @Inject
    constructor(connectionHelper: ConnectionHelper){
        this.connectionHelper = connectionHelper;
    }

    private var liveData: MutableLiveData<Mutable> = MutableLiveData()
    fun setLiveData(liveData: MutableLiveData<Mutable>) {
        this.liveData = liveData
        connectionHelper.liveData = liveData
    }

    fun login(request: LoginRequest?): Disposable? {
        return connectionHelper.requestApi(
            Constants.POST_REQUEST, URLS.LOGIN, request, LoginResponse::class.java,
            Constants.SUCCESS, true
        )
    }

    fun register(request: RegisterRequest?): Disposable? {
        return connectionHelper.requestApi(
            Constants.POST_REQUEST,
            URLS.REGISTER, request, LoginResponse::class.java,
            Constants.WRITE_CODE, true
        )
    }
    private val TAG = "AuthRepository"
    fun updateProfile(
        request: RegisterRequest?,
        file: FileObject,
        haveFile: Boolean
    ): Disposable? {
        return if (!haveFile) {
            Log.d(TAG,"not fileObject")
            connectionHelper.requestApi(
                Constants.POST_REQUEST, URLS.UPDATE_PROFILE, request,
                LoginResponse::class.java,
                Constants.UPDATE_PROFILE, true
            )
        } else {
            Log.d(TAG,"fileObject")
            val fileObjects: ArrayList<FileObject> = ArrayList<FileObject>()
            fileObjects.add(file)
            connectionHelper.requestApi(
                URLS.UPDATE_PROFILE, request, fileObjects, LoginResponse::class.java,
                Constants.UPDATE_PROFILE, true
            )
        }
    }

    fun updateToken(token: String): Disposable? {
        return connectionHelper.requestApi(Constants.POST_REQUEST,
            URLS.UPDATE_TOKEN, TokenRequest(token), StatusMessage::class.java,
            URLS.UPDATE_TOKEN, false
        )
    }

    fun forgetPassword(request: ForgetPasswordRequest?): Disposable? {
        return connectionHelper.requestApi(
            Constants.POST_REQUEST, URLS.FORGET_PASSWORD, request,
            ForgetPasswordResponse::class.java,
            Constants.FORGET_PASSWORD, true
        )
    }


    fun changePassword(request: ChangePasswordRequest?): Disposable? {
        return connectionHelper.requestApi(
            Constants.POST_REQUEST, URLS.CHANGE_PASSWORD, request,
            StatusMessage::class.java,
            Constants.CHANGE_PASSWORD, true
        )
    }


    //unnecessary
    fun updateSize(request: SizeRequest?): Disposable? {
        return connectionHelper.requestApi(
            Constants.POST_REQUEST, URLS.UPDATE_PROFILE, request,
            StatusMessage::class.java,
            URLS.UPDATE_PROFILE, true
        )
    }

    fun getMySizes(): Disposable? {
        return connectionHelper.requestApi(
            Constants.GET_REQUEST, URLS.USER_SIZES, Any(),
            MySizesResponse::class.java,
            URLS.USER_SIZES, true
        )
    }

    fun updateSizes(request: MySizesRequest): Disposable? {
        return connectionHelper.requestApi(
            Constants.POST_REQUEST, URLS.MY_SIZES, request,
            StatusMessage::class.java,
            URLS.MY_SIZES, true
        )
    }

    fun verifyCode(request: VerifyRequest): Disposable? {
        return connectionHelper.requestApi(
            Constants.POST_REQUEST, URLS.VERIFY, request,
            LoginResponse::class.java,
            Constants.REGISTER, true
        )
    }

}