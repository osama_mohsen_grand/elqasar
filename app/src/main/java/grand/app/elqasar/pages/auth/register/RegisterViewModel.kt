package grand.app.elqasar.pages.auth.register

import android.os.Bundle
import androidx.lifecycle.MutableLiveData
import grand.app.elqasar.base.BaseViewModel
import grand.app.elqasar.connection.FileObject
import grand.app.elqasar.pages.auth.forgetpassword.model.ForgetPasswordRequest
import grand.app.elqasar.pages.auth.repository.AuthRepository
import grand.app.elqasar.pages.auth.repository.VerificationFirebaseSMSRepository
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.makeyourclothes.fabric.model.ClothesItem
import grand.app.elqasar.pages.splash.models.MainSettingsResponse
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.session.UserHelper
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class RegisterViewModel : BaseViewModel {
    @Inject
    lateinit var repository: AuthRepository;

    @Inject
    lateinit var repositoryFireBase: VerificationFirebaseSMSRepository;

    var employers = arrayListOf<String>()
    var cities = arrayListOf<String>()
    var mainSettingsResponse: MainSettingsResponse

    var compositeDisposable: CompositeDisposable
    var request: RegisterRequest

    val user = UserHelper.getUserDetails()

    //    private var liveData: LiveData<Mutable> = MutableLiveData()
    var liveDataViewModel: MutableLiveData<Mutable>
    var liveDataViewModelFirebase: MutableLiveData<Mutable>
    var x: Int = 6
    var file : FileObject = FileObject()
    var isBack = false

    @Inject
    constructor(repository: AuthRepository, repositoryFireBase: VerificationFirebaseSMSRepository) {
        this.repository = repository
        this.repositoryFireBase = repositoryFireBase
        this.liveDataViewModel = MutableLiveData()
        this.liveDataViewModelFirebase = MutableLiveData()
        compositeDisposable = CompositeDisposable()
        request = RegisterRequest()
        if(UserHelper.isLogin()){
            request.firstName = user.firstName
            request.secondName = user.secondName
            request.thirdName = user.thirdName
            request.fourthName = user.fourthName
            request.phone = user.phone
        }
        repository.setLiveData(liveDataViewModel)
        repositoryFireBase.setLiveData(liveDataViewModelFirebase)
    }

    fun setArgument(bundle: Bundle) {
        if(bundle.containsKey(Constants.IS_BACK)){
            isBack = bundle.getBoolean(Constants.IS_BACK)
        }
    }



    init {
        mainSettingsResponse =
            UserHelper.retrieveJsonResponse(
                Constants.SETTINGS,
                MainSettingsResponse::class.java
            ) as MainSettingsResponse
        for (employer in mainSettingsResponse.employers)
            employers.add(employer.name)

        for (city in mainSettingsResponse.cities)
            cities.add(city.name)
    }

    fun login() {
        liveDataViewModel.value = Mutable(Constants.LOGIN)
    }

    fun register() {
        liveDataViewModel.value = Mutable(Constants.REGISTER)
    }

    fun changePassword() {
        liveDataViewModel.value = Mutable(Constants.CHANGE_PASSWORD)
    }

    fun image() {
        liveDataViewModel.value = Mutable(Constants.SELECT_IMAGE)
    }


    fun submit() {
        if (request.isValid()) {
            if (!UserHelper.isLogin() || (UserHelper.isLogin() && UserHelper.getUserDetails().phone != request.phone))
//                repository.forgetPassword(
//                    ForgetPasswordRequest(
//                        request.phone
//                    )
//                )
                repository.register(request)
            else
                repository.updateProfile(
                    request,file,file.getParamName() != ""
                )
        }
    }

    fun sendVerificationCode() {
//        repositoryFireBase.sendVerificationCode(request.phone)
        liveDataViewModel.value = Mutable(Constants.WRITE_CODE)
    }

    protected fun unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }

    override fun onCleared() {
        super.onCleared()
        unSubscribeFromObservable()
    }
}