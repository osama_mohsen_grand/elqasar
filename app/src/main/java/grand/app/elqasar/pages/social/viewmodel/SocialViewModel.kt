package grand.app.elqasar.pages.social.viewmodel

import android.os.Bundle
import androidx.lifecycle.MutableLiveData
import grand.app.elqasar.base.BaseViewModel
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.social.SocialAdapter
import grand.app.elqasar.pages.social.model.SocialResponse
import grand.app.elqasar.pages.settings.repository.SettingsRepository
import grand.app.elqasar.utils.Constants
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class SocialViewModel : BaseViewModel {
    @Inject
    lateinit var repository: SettingsRepository;
    private val TAG = "SocialViewModel"
    lateinit var compositeDisposable: CompositeDisposable
    var adapter = SocialAdapter()
    var liveDataViewModel: MutableLiveData<Mutable>
    var type: Int = 0
    lateinit var response: SocialResponse

    @Inject
    constructor(repository: SettingsRepository) {
        this.repository = repository
        this.liveDataViewModel = MutableLiveData();
        repository.setLiveData(liveDataViewModel)
    }

    fun setArgument( bundle : Bundle){
        type = bundle.getInt(Constants.TYPE)
        repository.getSettings(type)
    }



    private fun unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }

    override fun onCleared() {
        super.onCleared()
        unSubscribeFromObservable()
    }

    fun setData(response: SocialResponse) {
        this.response = response
        adapter.update(response.social)
    }
}

