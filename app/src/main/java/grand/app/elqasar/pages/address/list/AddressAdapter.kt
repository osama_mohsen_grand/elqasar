package grand.app.elqasar.pages.address.list

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.github.islamkhsh.CardSliderAdapter
import grand.app.elqasar.R
import grand.app.elqasar.databinding.ItemAddressBinding
import grand.app.elqasar.databinding.ItemHomeBinding
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.address.list.viewmodel.ItemAddressViewModel
import grand.app.elqasar.pages.address.list.model.Address
import grand.app.elqasar.pages.home.HomeAdapter
import grand.app.elqasar.pages.home.HomeModel
import grand.app.elqasar.pages.home.ItemHomeViewModel
import grand.app.elqasar.pages.makeyourclothes.fabric.model.ClothesModelRequest
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.helper.MovementHelper
import java.io.Serializable

class AddressAdapter(private var modelList: ArrayList<Address> = ArrayList()) :
    RecyclerView.Adapter<AddressAdapter.ViewHolder>() {
    // fetch list data
    var selected: Int = 0
    var id: Int = -1
    var type: Int = 0
    lateinit var Address: Address
    var liveDataViewModel: MutableLiveData<Mutable> = MutableLiveData();

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AddressAdapter.ViewHolder {
        // binding item_model layout
        val binding: ItemAddressBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_address,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    private val TAG = "AddressAdapter"
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(modelList[position])
        holder.viewModel.mutableLiveData.observeForever {
            val mutable: Mutable = it as Mutable
            Address = modelList[mutable.position]
            if (mutable.message == Constants.SUBMIT) {
                holder.viewModel.selected = false
                notifyItemChanged(selected)

                selected = mutable.position
                id = modelList[mutable.position].id
                notifyItemChanged(selected)
            }
            liveDataViewModel.value = mutable
            Log.d(TAG, "selected:" + selected)
        }
    }

    val request: ClothesModelRequest = ClothesModelRequest()


    override fun getItemCount(): Int {
        //return data count if exist or 0
        return modelList.size
    }

    fun update(modelList: ArrayList<Address>) {
        //update data after call service again in scroll , and notify list which end with
        this.modelList = modelList
        notifyDataSetChanged()
    }

    fun setAddressSelect(addressId: Int) {
        Log.d(TAG,modelList.size.toString())
        for (i in 0 until modelList.size) {
            if(modelList[i].id == addressId){
                id = addressId
                selected = i
                notifyItemChanged(selected)
            }
        }
    }

    fun add(address: Address, position: Int) {
        modelList.add(position,address)
        selected = 0
        id = address.id
        notifyDataSetChanged()
    }


    inner class ViewHolder(private val binding: ItemAddressBinding) :
        RecyclerView.ViewHolder(binding.root) {
        lateinit var viewModel: ItemAddressViewModel

        //bint
        fun bind(model: Address) {
            if (id == -1) id = modelList[selected].id
            viewModel = ItemAddressViewModel(model, adapterPosition, selected == adapterPosition)
            binding.viewmodel = viewModel

            //            if(id == -1) id = modelList[selected].id
        }
    }

}