package grand.app.elqasar.pages.product.ui

import android.app.Activity
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.viewpager.widget.ViewPager
import grand.app.elqasar.R
import grand.app.elqasar.base.BaseFragment
import grand.app.elqasar.base.MyApplication
import grand.app.elqasar.base.di.IApplicationComponent
import grand.app.elqasar.databinding.FragmentProductListSliderBinding
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.address.list.model.Address
import grand.app.elqasar.pages.product.model.FavouriteResponse
import grand.app.elqasar.pages.product.model.list.ProductListResponse
import grand.app.elqasar.pages.product.viewmodel.slider.ProductListSliderViewModel
import grand.app.elqasar.pages.search.SearchFragment
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.URLS
import grand.app.elqasar.utils.helper.MovementHelper
import grand.app.elqasar.utils.session.SharedPreferenceHelper
import javax.inject.Inject


class ProductListSliderFragment : BaseFragment() {
    private lateinit var binding: FragmentProductListSliderBinding
    @Inject
    lateinit var viewModel: ProductListSliderViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_product_list_slider, container, false)
        val component: IApplicationComponent? =
            (context!!.applicationContext as MyApplication).applicationComponent
        component?.inject(this)
        viewModel.adapter.setFragment(this)
        binding.viewmodel = viewModel
        arguments?.let { viewModel.setArgument(it) }
//        binding.viewPager.setOnScrollChangeListener()

        setEvent()
        // Inflate the layout for this fragment
        return binding.root
    }
    private fun setEvent() {
        viewModel.liveDataViewModel.observe(requireActivity(), Observer {
            val mutable = it as Mutable
            Log.d(TAG,mutable.message)
            handleActions(mutable)
//            Log.d(TAG,(mutable.obj as StatusMessage).mStatus+"")
            if (mutable.message == URLS.PRODUCT_LIST) {
                viewModel.response = (mutable.obj as ProductListResponse)
                viewModel.setData()
            }else if (mutable.message == URLS.ADD_FAVOURITE) {
                viewModel.updateFavourite((mutable.obj as FavouriteResponse).isFavorite)
            }else if (mutable.message == Constants.BACK) {
                requireActivity().finish()
            }else if (mutable.message == Constants.SEARCH) {
                val bundle = Bundle()
                bundle.putString(Constants.PAGE, SearchFragment::class.java.name)
                bundle.putSerializable(Constants.PRODUCTS_RESPONSE, viewModel.response)
                MovementHelper.startActivityBase(context, bundle, getString(R.string.search))
            }else if(mutable.message == Constants.NEXT || mutable.message == Constants.BEFORE){
                binding.viewPager.setCurrentItem(viewModel.currentItem, true);
            }
        })
    }
    val TAG: String = this::class.java.name
    override fun onResume() {
        super.onResume()
        viewModel.repository.setLiveData(viewModel.liveDataViewModel)
        if(!SharedPreferenceHelper.getKey(Constants.FAVOURITE).equals("")){
            Log.d(TAG,"favourite")
            var favourite : Int? = SharedPreferenceHelper.getKey(Constants.FAVOURITE)?.toInt()
            Log.d(TAG, "favourite: $favourite")
            favourite?.let {
                viewModel.adapter.updateFavouriteBack(it) }
            SharedPreferenceHelper.saveKey(Constants.FAVOURITE,"")
        }
    }

    override fun onDetach() {
        super.onDetach()
        SharedPreferenceHelper.saveKey(Constants.FAVOURITE,"")
    }

}