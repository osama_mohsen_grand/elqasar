package grand.app.elqasar.pages.makeyourclothes.common

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import grand.app.elqasar.R
import grand.app.elqasar.base.BaseFragment
import grand.app.elqasar.base.MyApplication
import grand.app.elqasar.base.di.IApplicationComponent
import grand.app.elqasar.databinding.FragmentCommonClothesMadeBinding
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.makeyourclothes.accessories.AccessoriesFragment
import grand.app.elqasar.pages.makeyourclothes.fabric.model.Fabric
import grand.app.elqasar.pages.makeyourclothes.fabric.model.FabricListResponse
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.URLS
import grand.app.elqasar.utils.helper.AppSpecificHelper
import grand.app.elqasar.utils.helper.AppSpecificHelper.getTitleBarType
import grand.app.elqasar.utils.helper.MovementHelper
import javax.inject.Inject

class CommonClothesMadeFragment : BaseFragment() {
    private lateinit var binding: FragmentCommonClothesMadeBinding

    @Inject
    lateinit var viewModel: CommonClothesMadeViewModel

    private val TAG = this::class.java.name


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_common_clothes_made, container, false)
        val component: IApplicationComponent? =
            (context!!.applicationContext as MyApplication).applicationComponent
        component?.inject(this)
        binding.viewmodel = viewModel
        arguments?.let { viewModel.setArgument(it) }
        // Inflate the layout for this fragment
        setEvent()
        return binding.root
    }

    private fun setEvent() {
        viewModel.liveDataViewModel.observe(requireActivity(), Observer {
            val mutable = it as Mutable
            handleActions(mutable)
            Log.d(TAG, mutable.message)
            if (mutable.message == URLS.CATEGORIES) {
                Log.d(TAG, "Found")
                viewModel.setData(mutable.obj as FabricListResponse)
            } else if (mutable.message == Constants.ESCAPE) {
                viewModel.adapter.request.models = AppSpecificHelper.selectClothesModel(
                    viewModel.adapter.request.models,
                    -1, -1
                )
                nextPage()
            } else if (mutable.message == Constants.BACK) {
                finishActivity()
            }
        })

        viewModel.adapter.liveDataViewModel.observe(requireActivity(), Observer {
            val mutable = it as Mutable
            if (mutable.message == Constants.SUBMIT) {
                val fabric : Fabric = viewModel.adapter.modelList[viewModel.adapter.selectedItem]
                viewModel.adapter.request.models = AppSpecificHelper.selectClothesModel(viewModel.adapter.request.models,
                    fabric.id,viewModel.adapter.type)
                nextPage()
            }
        })

    }
    var newType: Int = 0
    private fun nextPage(){
        val bundle = Bundle()
        bundle.putSerializable(Constants.REQUEST_MODELS, viewModel.adapter.request)
        bundle.putBoolean(Constants.ALLOW_ACTION, viewModel.adapter.allowAction)
        bundle.putBoolean(Constants.IS_BACK, viewModel.adapter.isBack)
        if(!viewModel.adapter.isBack) {
            newType = viewModel.adapter.type + 1
            if (newType <= 7) {
                bundle.putString(Constants.PAGE, CommonClothesMadeFragment::class.java.name)
                bundle.putBoolean(Constants.HAS_ESCAPE, newType >= 6)
            } else {
                Log.d(TAG, "newType$newType")
                bundle.putString(Constants.PAGE, AccessoriesFragment::class.java.name)
            }
            bundle.putInt(Constants.TYPE, newType)
//        bundle.putInt(Constants.CLOTH_ID, viewModel.adapter.request.cloth_id)
//        bundle.putInt(Constants.DETAIL_ID, viewModel.detail_id)
            bundle.putString(Constants.TITLE, getTitleBarType(newType))
            MovementHelper.startActivityBase(context, bundle);
        }else{
            val intent = Intent()
            intent.putExtra(Constants.BUNDLE,bundle)
            requireActivity().setResult(RESULT_OK, intent);
            requireActivity().finish()
        }
    }

    override fun onResume() {
        super.onResume()
        viewModel.liveDataViewModel = viewModel.liveDataViewModel
        viewModel.repository.setLiveData(viewModel.liveDataViewModel)
    }

}