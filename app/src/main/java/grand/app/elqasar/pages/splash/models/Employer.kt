package grand.app.elqasar.pages.splash.models


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Employer(
    @SerializedName("id")
    @Expose
    var id: Int,
    @SerializedName("name")
    @Expose
    var name: String
)