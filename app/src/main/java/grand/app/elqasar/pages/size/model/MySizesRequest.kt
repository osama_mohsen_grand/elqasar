package grand.app.elqasar.pages.size.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class MySizesRequest {

    @SerializedName("id")
    @Expose
    var ids = arrayListOf<Int>()

    @SerializedName("size")
    @Expose
    var sizes = arrayListOf<String>()

}