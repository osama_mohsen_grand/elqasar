package grand.app.elqasar.pages.settings.term


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import grand.app.elqasar.model.base.StatusMessage
import grand.app.elqasar.pages.product.model.details.ProductDetails

data class SettingsResponse(
    @SerializedName("data")
    @Expose
    var data: String = ""
) : StatusMessage()