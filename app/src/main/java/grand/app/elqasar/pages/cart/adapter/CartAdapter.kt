package grand.app.elqasar.pages.cart.adapter

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.elqasar.R
import grand.app.elqasar.databinding.ItemCartBinding
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.cart.model.Cart
import grand.app.elqasar.pages.cart.model.UpdateCartRequest
import grand.app.elqasar.pages.cart.viewmodel.ItemCartViewModel
import grand.app.elqasar.pages.makeyourclothes.fabric.model.ClothesItem
import grand.app.elqasar.pages.makeyourclothes.fabric.model.ClothesModelRequest
import grand.app.elqasar.pages.makeyourclothes.tailermade.ui.TailorMadeFragment
import grand.app.elqasar.pages.product.model.FavouriteRequest
import grand.app.elqasar.pages.product.ui.ProductDetailsFragment
import grand.app.elqasar.repository.ProductRepository
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.helper.AppSpecificHelper
import grand.app.elqasar.utils.helper.MovementHelper

class CartAdapter(var modelList:ArrayList<Cart> = ArrayList(), private var repository: ProductRepository? = null) : RecyclerView.Adapter<CartAdapter.ViewHolder>() {
    // fetch list data
    var favPosition : Int = 0
    var selectedItem : Int = -1
    var newValue : Int = -1
    private  val TAG = "CartAdapter"
    lateinit var request: ClothesItem

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // binding item_model layout
        val binding: ItemCartBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_cart,
            parent,
            false
        )
        return ViewHolder(
            binding
        )
    }
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(modelList[position])
        holder.viewModel.mutableLiveData.observeForever {
            val mutable: Mutable = it as Mutable
            selectedItem = it.position
            val model = modelList[mutable.position]
            if(mutable.message == Constants.SUBMIT) {
                val bundle = Bundle()
                if(model.type != 1) {
                    bundle.putString(Constants.PAGE, ProductDetailsFragment::class.java.name)
                    bundle.putInt(Constants.ID, model.productId)
                    bundle.putInt(Constants.TYPE, model.type)
                    bundle.putString(Constants.TITLE, model.name)
                    MovementHelper.startActivityBase(holder.itemView.context, bundle);
                }else{
                    request.models = AppSpecificHelper.selectClothesModel(request.models, modelList[position].productId, modelList[position].type)
                    bundle.putString(Constants.PAGE, TailorMadeFragment::class.java.name)
                    bundle.putInt(Constants.CLOTH_ID, modelList[position].productId)
                    request.cloth_id = modelList[position].productId
                    bundle.putSerializable(Constants.REQUEST_MODELS, request)
//                bundle.putString(Constants.TITLE,Fabric.name)
                    MovementHelper.startActivityBase(holder.itemView.context, bundle, modelList[position].name);
                }
            }else if(mutable.message == Constants.ADD){
//                Log.d(TAG, "qty_tmp:"+model.qty_tmp.toString())
                newValue = model.qty+1
                repository!!.updateCart(UpdateCartRequest(model.productId,model.qty+1,model.type,model.id))
            }else if( mutable.message == Constants.MINUS){
//                Log.d(TAG, "qty_tmp:"+model.qty_tmp.toString())
                newValue = model.qty-1
                repository!!.updateCart(UpdateCartRequest(model.productId,model.qty-1,model.type,model.id))
            }else if(mutable.message == Constants.FAVOURITE){
                favPosition = position
                repository!!.addFavourite(FavouriteRequest(model.productId,model.type))
            }else if(mutable.message == Constants.DELETE){
                repository!!.deleteItemCart(model.id)
            }
            notifyItemChanged(selectedItem)
        }
    }

    fun updateCart(){
        if(selectedItem != -1){
            modelList[selectedItem].qty = newValue
            notifyItemChanged(selectedItem)
        }
    }

    fun deleteItem() {
        if(selectedItem != -1){
            modelList.removeAt(selectedItem)
            notifyDataSetChanged()
        }
    }

    override fun getItemCount(): Int {
        //return data count if exist or 0
        return modelList.size
    }


    fun updateFavourite(favorite: Int) {
        modelList[favPosition].userFavorite = favorite
        notifyItemChanged(favPosition)
    }


    fun update(modelList: ArrayList<Cart>) {
        //update data after call service again in scroll , and notify list which end with
        this.modelList = modelList
        notifyDataSetChanged()
    }



    class ViewHolder(private val binding: ItemCartBinding) : RecyclerView.ViewHolder(binding.root) {
        lateinit var viewModel: ItemCartViewModel

        //bint
        fun bind(model: Cart) {
            viewModel = ItemCartViewModel(model, adapterPosition)
            binding.viewmodel = viewModel
        }
    }
}