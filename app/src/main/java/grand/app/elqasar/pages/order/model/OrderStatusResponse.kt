package grand.app.elqasar.pages.order.model


import com.google.gson.annotations.SerializedName
import grand.app.elqasar.model.base.StatusMessage

data class OrderStatusResponse(
    @SerializedName("data")
    var data: List<OrderStatus>,
    @SerializedName("order_status")
    var orderStatus: Int
) : StatusMessage()