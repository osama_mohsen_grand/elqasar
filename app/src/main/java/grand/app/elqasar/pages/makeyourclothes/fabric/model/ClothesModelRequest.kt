package grand.app.elqasar.pages.makeyourclothes.fabric.model

import com.google.gson.annotations.SerializedName
import grand.app.elqasar.model.base.StatusMessage
import grand.app.elqasar.pages.size.model.MySizeModel
import java.io.Serializable

class ClothesModelRequest(
    @SerializedName("add_cart") var clotheItem : ArrayList<ClothesItem> = arrayListOf(),
    @SerializedName("cloth_id") var cloth_id: Int = 0,
                          @SerializedName("detail_id") var detail_id: Int = 0,
                          @SerializedName("cart_item_id") var cartIdemId: Int = -1) : Serializable{

}