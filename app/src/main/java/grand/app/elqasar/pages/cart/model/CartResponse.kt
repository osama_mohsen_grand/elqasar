package grand.app.elqasar.pages.cart.model


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import grand.app.elqasar.model.base.StatusMessage
import java.io.Serializable


data class CartResponse(
    @SerializedName("data")
    @Expose
    var data: Data
):StatusMessage() , Serializable