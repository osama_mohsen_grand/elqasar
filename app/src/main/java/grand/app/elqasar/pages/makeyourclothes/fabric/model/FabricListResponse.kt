package grand.app.elqasar.pages.makeyourclothes.fabric.model


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import grand.app.elqasar.model.base.StatusMessage

data class FabricListResponse (
    @SerializedName("data")
    @Expose
    var data: ArrayList<Fabric> = arrayListOf()
): StatusMessage()