package grand.app.elqasar.pages.settings.term

import android.os.Bundle
import android.util.Log
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import grand.app.elqasar.R
import grand.app.elqasar.base.BaseViewModel
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.settings.repository.SettingsRepository
import grand.app.elqasar.utils.Constants
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class SettingsViewModel : BaseViewModel {
    @Inject
    lateinit var repository: SettingsRepository;
    var compositeDisposable: CompositeDisposable
    var response : SettingsResponse =
        SettingsResponse()
    //    private var liveData: LiveData<Mutable> = MutableLiveData()
    var liveDataViewModel: MutableLiveData<Mutable>
    var type:Int = 0
    var title: ObservableField<String> =
        ObservableField("")
    @Inject
    constructor(repository: SettingsRepository){
        this.repository = repository
        this.liveDataViewModel = MutableLiveData();
        compositeDisposable = CompositeDisposable()
        repository.setLiveData(liveDataViewModel)
    }

    //terms send status 1 else if about send status 2 else  social media send status 3
    fun setArgument( bundle : Bundle){
        type = bundle.getInt(Constants.TYPE)
        title.set(if(type == 1) getString(R.string.terms_and_conditions) else getString(R.string.about_app))
        repository.getSettings(type)
    }

    fun back(){
        liveDataViewModel.value = Mutable(Constants.BACK)
    }

    private  val TAG = "LoginOrRegisterViewMode"

    protected fun unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }

    override fun onCleared() {
        super.onCleared()
        unSubscribeFromObservable()
    }

    fun setData(response: SettingsResponse) {
        this.response = response
        showPage()
    }
}