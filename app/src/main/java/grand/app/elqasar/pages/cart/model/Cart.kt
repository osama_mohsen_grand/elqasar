package grand.app.elqasar.pages.cart.model


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Cart(
    @SerializedName("id")
    var id: Int,
    @SerializedName("qty")
    var qty: Int,
    @SerializedName("price")
    var price: Int,
    @SerializedName("total_price")
    var totalPrice: Int,
    @SerializedName("name")
    var name: String,
    @SerializedName("product_id")
    var productId: Int,
    @SerializedName("product_stock")
    var productStock: Int,
    @SerializedName("cart_options")
    @Expose
    var cartOptions: CartOptions = CartOptions(),
    @SerializedName("type")
    var type: Int,
    @SerializedName("image")
    var image: String,
    @SerializedName("about")
    var about: String,
    @SerializedName("user_favorite")
    var userFavorite: Int
): Serializable{
    var newValue = 0
}