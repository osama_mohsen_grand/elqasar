package grand.app.elqasar.pages.product.model.list


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Product(
    @SerializedName("id")
    @Expose
    var id: Int = 0,
    @SerializedName("name")
    @Expose
    var name: String = "",
    @SerializedName("price")
    @Expose
    var price: String = "",
    @SerializedName("about")
    @Expose
    var about: String = "",
    @SerializedName("image")
    @Expose
    var image: String = "",
    @SerializedName("type")
    @Expose
    var type: Int = 0,
    @SerializedName("user_favorite")
    @Expose
    var userFavorite: Int = 0
) : Serializable