package grand.app.elqasar.pages.product.model

import com.google.gson.annotations.SerializedName

class FavouriteRequest(@SerializedName("product_id") var id: Int, @SerializedName("type") var type: Int) {
}