package grand.app.elqasar.pages.makeyourclothes.clothesinfo

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import grand.app.elqasar.R
import grand.app.elqasar.activity.MainActivity
import grand.app.elqasar.base.BaseFragment
import grand.app.elqasar.base.MyApplication
import grand.app.elqasar.base.di.IApplicationComponent
import grand.app.elqasar.databinding.FragmentClothesInfoBinding
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.cart.model.CartResponse
import grand.app.elqasar.pages.makeyourclothes.fabric.model.ClothesItem
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.URLS
import grand.app.elqasar.utils.helper.AppHelper
import grand.app.elqasar.utils.popup.PopUpInterface
import grand.app.elqasar.utils.popup.PopUpMenuHelper
import grand.app.elqasar.utils.session.SharedPreferenceHelper
import javax.inject.Inject

class ClothesInfoFragment : BaseFragment() {
    private lateinit var binding: FragmentClothesInfoBinding

    @Inject
    lateinit var viewModel: ClothesInfoViewModel

    private val TAG = this::class.java.name
    val popupMenu = PopUpMenuHelper()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_clothes_info, container, false)
        val component: IApplicationComponent? =
            (context!!.applicationContext as MyApplication).applicationComponent
        component?.inject(this)
        binding.viewmodel = viewModel
        arguments?.let { viewModel.setArgument(it, this) }
        // Inflate the layout for this fragment
        setEvent()
        return binding.root
    }

    private fun setEvent() {
        viewModel.liveDataViewModel.observe(requireActivity(), Observer {
            val mutable = it as Mutable
            handleActions(mutable)
            if (mutable.message == URLS.ADD_TO_CART_CLOTHES) {
                if(viewModel.adapter.isCreate) {
                    toastMessage((mutable.obj as CartResponse).mMessage)
                    SharedPreferenceHelper.saveKey(
                        Constants.CART,
                        (mutable.obj as CartResponse).data.cartCount.toString()
                    )
                    finishAllActivities()
                    startActivity(Intent(context, MainActivity::class.java))
                }else{
                    val intent = Intent()
                    val bundle = Bundle()
                    bundle.putSerializable(Constants.REQUEST_MODELS, viewModel.adapter.getRequest())
                    bundle.putSerializable(Constants.CART, mutable.obj as CartResponse)
                    intent.putExtra(Constants.BUNDLE,bundle)
                    requireActivity().setResult(RESULT_OK, intent);
                    requireActivity().finish()
                }
            }else if(mutable.message == Constants.BACK){
                finishActivity()
            }else if(mutable.message == Constants.ADD){
                AppHelper.startAnimation(binding.llActionBarDresses,requireContext())
            }
        })

        binding.llActionBarDresses.setOnClickListener{
            popupMenu.openPopUp(
                requireActivity(),
                binding.tvActionBarDresses,
                viewModel.dresses,object : PopUpInterface {
                    override fun submitPopUp(position: Int) {
//                        viewModel.adapter.requestClothes.clotheItem[viewModel.number] = viewModel.adapter.request
//                        viewModel.requestClothes.clotheItem[viewModel.position] = viewModel.adapter.request
                        viewModel.adapter.requestClothes.clotheItem.forEachIndexed { index, clothesItem ->
                            Log.d(TAG,"index $index => ")
                            for (model in clothesItem.models) {
                                Log.d(TAG," , "+model.id)
                            }
                        }
                        viewModel.setRequest(position,viewModel.dresses[position])
//                        binding.tvActionBarDresses.text = viewModel.dresses[position]
//                        viewModel.adapter.request = viewModel.requestClothes.clotheItem[position]
//                        viewModel.notifyChange()
                    }
                })
        }

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.d(TAG, "request:$requestCode")
        Log.d(TAG, "result:$resultCode")
        if (requestCode == Constants.REQUEST_MODELS_RESULT && resultCode == RESULT_OK) {
            data?.getBundleExtra(Constants.BUNDLE)?.let {
                viewModel.adapter.setRequest(it.getSerializable(Constants.REQUEST_MODELS) as ClothesItem)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        viewModel.repository.setLiveData(viewModel.liveDataViewModel)
    }
}