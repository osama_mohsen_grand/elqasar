package grand.app.elqasar.pages.favourite.viewmodel

import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import grand.app.elqasar.R
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.cart.model.Cart
import grand.app.elqasar.pages.favourite.model.FavouriteModel
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.resources.ResourceManager

data class ItemFavouriteViewModel(val model : FavouriteModel, var position : Int) {
    var mutableLiveData = MutableLiveData<Any>()

    var price: ObservableField<String> =
        ObservableField("${model.price} ${ResourceManager.getString(R.string.currency)}")

    fun submit(){
        mutableLiveData.value = Mutable(Constants.SUBMIT , position)
    }

    fun addToCart(){
        mutableLiveData.value = Mutable(Constants.ADD_TO_CART , position)
    }

    fun remove(){
        mutableLiveData.value = Mutable(Constants.DELETE , position)
    }
}