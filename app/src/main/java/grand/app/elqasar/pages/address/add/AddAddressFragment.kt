package grand.app.elqasar.pages.address.add

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import grand.app.elqasar.R
import grand.app.elqasar.base.BaseFragment
import grand.app.elqasar.base.MyApplication
import grand.app.elqasar.base.di.IApplicationComponent
import grand.app.elqasar.databinding.FragmentAddressAddBinding
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.model.base.StatusMessage
import grand.app.elqasar.pages.address.list.model.AddAddressResponse
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.URLS
import grand.app.elqasar.utils.helper.MovementHelper
import kotlinx.android.synthetic.main.fragment_address_add.view.*
import kotlinx.android.synthetic.main.fragment_login.view.*
import javax.inject.Inject

class AddAddressFragment : BaseFragment() {
    private lateinit var binding: FragmentAddressAddBinding
    @Inject
    lateinit var viewModel: AddAddressViewModel
    var countryCode = "+9665"

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_address_add, container, false)
        val component: IApplicationComponent? =
            (context!!.applicationContext as MyApplication).applicationComponent
        component?.inject(this)
        binding.viewmodel = viewModel
        init()
        setEvent()
        // Inflate the layout for this fragment
        return binding.root
    }

    private fun init() {
        binding.edtPhone1.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                if (v.edt_phone1.text != null && !v.edt_phone1.text!!.contains(countryCode))
                    v.edt_phone1.setText(countryCode)
            }
        }

        binding.edtPhone2.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                if (v.edt_phone2.text != null && !v.edt_phone2.text!!.contains(countryCode))
                    v.edt_phone2.setText(countryCode)
            }
        }
    }

    private fun setEvent() {
        viewModel.liveDataViewModel.observe(requireActivity(), Observer {
            val mutable = it as Mutable
            handleActions(mutable)
            if (mutable.message == URLS.ADD_ADDRESS) {
                toastMessage((mutable.obj as AddAddressResponse).mMessage)
                val intent = Intent()
                intent.putExtra(Constants.ADDRESS, (mutable.obj as AddAddressResponse).data)
                requireActivity().setResult(Activity.RESULT_OK, intent);
                requireActivity().finish()
            }
        })

    }


    override fun onResume() {
        super.onResume()
        viewModel.liveDataViewModel = viewModel.liveDataViewModel
        viewModel.repository.setLiveData(viewModel.liveDataViewModel)
    }
}