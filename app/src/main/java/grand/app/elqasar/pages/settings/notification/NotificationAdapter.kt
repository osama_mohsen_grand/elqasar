package grand.app.elqasar.pages.settings.notification

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.elqasar.R
import grand.app.elqasar.databinding.ItemNotificationBinding
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.makeyourclothes.fabric.model.ClothesModelRequest
import grand.app.elqasar.pages.product.model.FavouriteRequest
import grand.app.elqasar.pages.settings.notification.model.Notification
import grand.app.elqasar.pages.settings.notification.viewmodel.ItemNotificationViewModel
import grand.app.elqasar.repository.ProductRepository
import grand.app.elqasar.utils.Constants

class NotificationAdapter(private var modelList:List<Notification> = ArrayList()) : RecyclerView.Adapter<NotificationAdapter.ViewHolder>() {
    // fetch list data
    var favPosition : Int = 0
    var selectedItem : Int = -1


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // binding item_model layout
        val binding: ItemNotificationBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_notification,
            parent,
            false
        )
        return ViewHolder(
            binding
        )
    }
    val request: ClothesModelRequest = ClothesModelRequest()
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(modelList[position])
        holder.viewModel.mutableLiveData.observeForever {
            val mutable: Mutable = it as Mutable
            selectedItem = it.position
            val model = modelList[mutable.position]
            notifyItemChanged(selectedItem)
        }
    }

    override fun getItemCount(): Int {
        //return data count if exist or 0
        return modelList.size
    }


    fun update(modelList: ArrayList<Notification>) {
        //update data after call service again in scroll , and notify list which end with
        this.modelList = modelList
        notifyDataSetChanged()
    }

    class ViewHolder(private val binding: ItemNotificationBinding) : RecyclerView.ViewHolder(binding.root) {
        lateinit var viewModel: ItemNotificationViewModel

        //bint
        fun bind(model: Notification) {
            viewModel = ItemNotificationViewModel(model, adapterPosition)
            binding.viewmodel = viewModel
        }
    }
}