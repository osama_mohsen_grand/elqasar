package grand.app.elqasar.pages.cart.model


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class AddToCartModelResponse(
    @SerializedName("address_id")
    @Expose
    var addressId: Int = 0,
    @SerializedName("cart_count")
    @Expose
    var cart_count: Int = 0,
    @SerializedName("cost")
    @Expose
    var cost: Int  = 0,
    @SerializedName("discount")
    @Expose
    var discount: Int  = 0,
    @SerializedName("shipping")
    @Expose
    var shipping: Int  = 0,
    @SerializedName("taxes_amount")
    @Expose
    var taxesAmount: Int  = 0,
    @SerializedName("taxes_percent")
    @Expose
    var taxesPercent: String = "",
    @SerializedName("total_price")
    @Expose
    var totalPrice: Int = 0,
    @SerializedName("promo_percent")
    @Expose
    var promoPercent: String = "",
    @SerializedName("promo_amount")
    @Expose
    var promoAmount: Int = 0
)