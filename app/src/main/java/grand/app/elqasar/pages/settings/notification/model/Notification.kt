package grand.app.elqasar.pages.settings.notification.model


import com.google.gson.annotations.SerializedName

data class Notification(
    @SerializedName("id")
    var id: Int,
    @SerializedName("image")
    var image: String,
    @SerializedName("title")
    var title: String,
    @SerializedName("message")
    var message: String,
    @SerializedName("date")
    var date: String
)