package grand.app.elqasar.pages.address.list.viewmodel

import android.util.Log
import android.widget.RadioGroup
import androidx.lifecycle.MutableLiveData
import grand.app.elqasar.R
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.address.list.model.Address
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.resources.ResourceManager


data class ItemAddressViewModel(
    val model: Address,
    var position: Int,
    var selected: Boolean
) {
    var mutableLiveData = MutableLiveData<Any>()
    var name: String = "${ResourceManager.getString(R.string.name)} : ${model.name}"
    var address: String = "${ResourceManager.getString(R.string.address)} : ${model.address}"
    var phone: String = "${ResourceManager.getString(R.string.phone)} : ${model.phone}"

    private  val TAG = "ItemAddressViewModel"
    fun submit(){
        Log.d(TAG,"submit")
        mutableLiveData.value = Mutable(Constants.SUBMIT , position)
    }

    fun onSplitTypeChanged(radioGroup: RadioGroup?, id: Int) {
        mutableLiveData.value = Mutable(Constants.SUBMIT , position)

    }

}