package grand.app.elqasar.pages.product.viewmodel.list

import android.os.Bundle
import android.util.Log
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import grand.app.elqasar.base.BaseViewModel
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.product.adapter.ProductAdapter
import grand.app.elqasar.pages.product.model.list.ProductListResponse
import grand.app.elqasar.repository.ProductRepository
import grand.app.elqasar.utils.Constants
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class ProductListViewModel : BaseViewModel {
    @Inject
    lateinit var repository: ProductRepository;
    var compositeDisposable: CompositeDisposable

    //    private var liveData: LiveData<Mutable> = MutableLiveData()
    var liveDataViewModel: MutableLiveData<Mutable>
    var type: Int = 0
    private val TAG = "ProductListViewModel"
    var response: ProductListResponse = ProductListResponse()
    var adapter: ProductAdapter
    var title: ObservableField<String> =
        ObservableField("")

    @Inject
    constructor(repository: ProductRepository) {
        this.repository = repository
        this.liveDataViewModel = MutableLiveData();
        compositeDisposable = CompositeDisposable()
        repository.setLiveData(liveDataViewModel)
        adapter = ProductAdapter(repository = repository)
    }

    fun setArgument(bundle: Bundle) {
        type = bundle.getInt(Constants.TYPE)
        title.set(bundle.getString(Constants.TITLE))
        adapter.type = type
        getProducts()
    }

    private fun getProducts() {
        repository.getProducts(type)
    }

    fun back() {
        liveDataViewModel.value = Mutable(Constants.BACK)
    }

    fun search() {
        liveDataViewModel.value = Mutable(Constants.SEARCH)
    }

    protected fun unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }

    override fun onCleared() {
        super.onCleared()
        unSubscribeFromObservable()
    }

    fun setData() {
        adapter.update(response.data)
    }

    fun updateFavourite(favorite: Int) {
        adapter.updateFavourite(favorite)
    }
}