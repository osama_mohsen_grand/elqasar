package grand.app.elqasar.pages.makeyourclothes.tailermade.viewmodel

import android.os.Bundle
import android.util.Log
import androidx.lifecycle.MutableLiveData
import grand.app.elqasar.base.BaseViewModel
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.makeyourclothes.fabric.model.ClothesItem
import grand.app.elqasar.pages.makeyourclothes.fabric.model.ClothesModelRequest
import grand.app.elqasar.pages.makeyourclothes.tailermade.adapter.TailorMadeAdapter
import grand.app.elqasar.pages.makeyourclothes.tailermade.model.TailorMadeResponse
import grand.app.elqasar.repository.ProductRepository
import grand.app.elqasar.utils.Constants
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class TailorMadeViewModel : BaseViewModel {
    @Inject
    lateinit var repository: ProductRepository;
    private val TAG = "TailorMadeViewModel"
    lateinit var compositeDisposable: CompositeDisposable
    var adapter = TailorMadeAdapter()
    var liveDataViewModel: MutableLiveData<Mutable>
    lateinit var response: TailorMadeResponse
    var isBack : Boolean = false

    @Inject
    constructor(repository: ProductRepository) {
        this.repository = repository
        this.liveDataViewModel = MutableLiveData();
        repository.setLiveData(liveDataViewModel)
    }

    fun setArgument( bundle : Bundle){
        adapter.request = bundle.getSerializable(Constants.REQUEST_MODELS) as ClothesItem
        adapter.cloth_id = bundle.getInt(Constants.CLOTH_ID)
        repository.getCategories(2,adapter.cloth_id)
        isBack = bundle.getBoolean(Constants.IS_BACK,false)
    }

    private fun unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }

    fun choose(){
        liveDataViewModel.value = Mutable(Constants.SUBMIT)
    }

    override fun onCleared() {
        super.onCleared()
        unSubscribeFromObservable()
    }

    fun setData(response: TailorMadeResponse) {
        this.response = response
        adapter.update(response.data)
        adapter.setSelectedIfExist()
        show.set(response.data.size != 0)
    }
}

