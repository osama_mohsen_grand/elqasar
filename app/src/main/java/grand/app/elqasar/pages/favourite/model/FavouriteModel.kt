package grand.app.elqasar.pages.favourite.model


import com.google.gson.annotations.SerializedName

data class FavouriteModel(
    @SerializedName("about")
    var about: String = "",
    @SerializedName("code")
    var code: String = "",
    @SerializedName("id")
    var id: Int = 0,
    @SerializedName("image")
    var image: String = "",
    @SerializedName("name")
    var name: String = "",
    @SerializedName("price")
    var price: String = "",
    @SerializedName("product_id")
    var productId: Int = 0,
    @SerializedName("stock")
    var stock: Int = 0,
    @SerializedName("type")
    var type: Int = 0
)