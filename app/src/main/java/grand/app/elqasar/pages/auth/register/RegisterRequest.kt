package grand.app.elqasar.pages.auth.register

import android.util.Log
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.session.UserHelper
import grand.app.elqasar.utils.validation.Validate
import timber.log.Timber
import java.io.Serializable

class RegisterRequest() : Serializable{


    @SerializedName("city_id")
    @Expose
     var city_id = -1
    @SerializedName("employer_id")
    @Expose
     var employer_id = -1

    @Transient
     var city = ""
        get() = field
        set(value) {
            field = value
            cityError.set("")
        }

    @SerializedName("password")
    @Expose
    var password: String = ""
        get() = field
        set(value) {
            field = value
            passwordError.set("")
        }

    @Transient
    var password_confirm = ""
        get() = field
        set(value) {
            field = value
            passwordConfirmError.set("")
        }

    @SerializedName("phone")
    @Expose
    var phone = ""
        get() = field
        set(value) {
            field = value
            phoneError.set("")
        }

    @SerializedName("first_name")
    @Expose
    var firstName = ""
        get() = field
        set(value) {
            field = value
            firstNameError.set("")
        }

    @SerializedName("second_name")
    @Expose
     var secondName = ""
        get() = field
        set(value) {
            field = value
            secondNameError.set("")
        }

    @SerializedName("third_name")
    @Expose
     var thirdName = ""
        get() = field
        set(value) {
            field = value
            thirdNameError.set("")
        }

    @SerializedName("fourth_name")
    @Expose
     var fourthName = ""
        get() = field
        set(value) {
            field = value
            fourthNameError.set("")
        }

    @Transient
     var empolyer = ""
        get() = field
        set(value) {
            field = value
            employerError.set("")
        }


    @Transient
    lateinit var cityError: ObservableField<String>

    @Transient
    lateinit var phoneError: ObservableField<String>

    @Transient
    lateinit var passwordError: ObservableField<String>

    @Transient
    lateinit var passwordConfirmError: ObservableField<String>

    @Transient
    lateinit var firstNameError: ObservableField<String>

    @Transient
    lateinit var secondNameError: ObservableField<String>

    @Transient
    lateinit var thirdNameError: ObservableField<String>

    @Transient
    lateinit var fourthNameError: ObservableField<String>

    @Transient
    lateinit var employerError: ObservableField<String>

    @Transient
    lateinit var isLogin: ObservableBoolean

    init {
        passwordError = ObservableField()
        cityError = ObservableField()
        phoneError = ObservableField()
        passwordConfirmError = ObservableField()
        firstNameError = ObservableField()
        secondNameError = ObservableField()
        thirdNameError = ObservableField()
        fourthNameError = ObservableField()
        employerError = ObservableField()
        isLogin = ObservableBoolean(UserHelper.isLogin())
    }

    fun isValid(): Boolean {
        var valid = true
        if (!Validate.isValid(firstName)) {
            firstNameError.set(Validate.error)
            valid = false
        } else firstNameError.set("")

        if (!Validate.isValid(secondName)) {
            secondNameError.set(Validate.error)
            valid = false
        } else secondNameError.set("")


        if (!Validate.isValid(thirdName)) {
            thirdNameError.set(Validate.error)
            valid = false
        } else thirdNameError.set("")


        if (!Validate.isValid(fourthName)) {
            fourthNameError.set(Validate.error)
            valid = false
        } else fourthNameError.set("")


        if (!isLogin.get() && !Validate.isValid(city)) {
            cityError.set(Validate.error)
            valid = false
        } else cityError.set("")

        if (!Validate.isValidPhoneRegularExpression(phone)) {
            phoneError.set(Validate.error)
            valid = false
        } else phoneError.set("")
        if (!isLogin.get() && !Validate.isValid(empolyer)) {
            employerError.set(Validate.error)
            valid = false
        } else employerError.set("")

        if (!UserHelper.isLogin()) {
            if (!Validate.isValid(password)) {
                passwordError.set(Validate.error)
                valid = false
            } else passwordError.set("")
            if (!Validate.isValid(password_confirm)) {
                passwordConfirmError.set(Validate.error)
                valid = false
                Timber.e("password_confirm:error")
            } else passwordConfirmError.set("")
        }
        if (valid && !Validate.isMatchPassword(
                password,
                password_confirm
            ) && !UserHelper.isLogin()
        ) {
            passwordConfirmError.set(Validate.error)
            passwordError.set(Validate.error)
            valid = false
            Timber.e("password_password_confirm:error")
        }
        return valid
    }


}