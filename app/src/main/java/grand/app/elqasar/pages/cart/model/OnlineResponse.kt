package grand.app.elqasar.pages.cart.model

import com.google.gson.annotations.SerializedName
import grand.app.elqasar.model.base.StatusMessage
import java.io.Serializable

class OnlineResponse(@SerializedName("data") var data: OnlineData) : StatusMessage() , Serializable  {
}