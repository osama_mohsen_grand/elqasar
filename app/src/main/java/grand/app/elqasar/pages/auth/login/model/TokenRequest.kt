package grand.app.elqasar.pages.auth.login.model

import com.google.gson.annotations.SerializedName

class TokenRequest(@SerializedName("token") var token: String)