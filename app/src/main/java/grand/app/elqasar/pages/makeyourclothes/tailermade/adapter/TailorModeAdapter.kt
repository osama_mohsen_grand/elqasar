package grand.app.elqasar.pages.makeyourclothes.tailermade.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import grand.app.elqasar.R
import grand.app.elqasar.databinding.ItemTailorMadeBinding
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.makeyourclothes.fabric.model.ClothesItem
import grand.app.elqasar.pages.makeyourclothes.fabric.model.ClothesModelRequest
import grand.app.elqasar.pages.makeyourclothes.tailermade.model.TailorMade
import grand.app.elqasar.pages.makeyourclothes.tailermade.viewmodel.ItemTailorMadeViewModel
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.helper.AppSpecificHelper

class TailorMadeAdapter(var modelList: ArrayList<TailorMade> = ArrayList()) :
    RecyclerView.Adapter<TailorMadeAdapter.ViewHolder>() {
    // fetch list data
    var selectedItem: Int = 0
    var id: Int = -1
    var cloth_id: Int = 0
    lateinit var request: ClothesItem
    lateinit var TailorMade: TailorMade
    var editable: Boolean = true

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // binding item_model layout
        val binding: ItemTailorMadeBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_tailor_made,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    private  val TAG = this::class.java.name
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if(editable) {
            holder.bind(modelList[position])
            holder.viewModel.mutableLiveData.observeForever {
                val mutable: Mutable = it as Mutable
                TailorMade = modelList[mutable.position]
                if (mutable.message == Constants.SUBMIT) {
                    selectedItem = mutable.position
                    id = modelList[mutable.position].id
                    notifyDataSetChanged()
                }
            }
        }
    }

    fun setSelectedIfExist(){
        val id = AppSpecificHelper.getId(2,request.models)
        if(id != -1) {
            for (i in 0 until  modelList.size) {
                if (id == modelList[i].id) {
                    selectedItem = i
                    notifyDataSetChanged()
                    return
                }
            }
        }
    }

    override fun getItemCount(): Int {
        //return data count if exist or 0
        return modelList.size
    }

    fun update(modelList: ArrayList<TailorMade>) {
        //update data after call service again in scroll , and notify list which end with
        this.modelList = modelList
        notifyItemRangeInserted(itemCount, modelList.size - 1);
    }


    inner class ViewHolder(private val binding: ItemTailorMadeBinding) :
        RecyclerView.ViewHolder(binding.root) {
        lateinit var viewModel: ItemTailorMadeViewModel

        //bint
        fun bind(model: TailorMade) {
            if (id == -1) id = modelList[selectedItem].id
            viewModel = ItemTailorMadeViewModel(model, adapterPosition, selectedItem == adapterPosition)
            binding.viewmodel = viewModel

            //            if(id == -1) id = modelList[selected].id
        }
    }

}