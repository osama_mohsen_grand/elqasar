package grand.app.elqasar.pages.size.model


import android.util.Log
import androidx.databinding.ObservableField
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import grand.app.elqasar.utils.validation.Validate
import java.io.Serializable

data class MySizeModel(
    @SerializedName("id")
    var id: Int = 0,
    @SerializedName("name")
    var name: String = "",
    @SerializedName("link")
    var link: String = "",
    @SerializedName("image")
    var image: String = "",
    @SerializedName("size")
    var size: String = ""
) : Serializable {

    var sizeUpdate: String = ""
        get() = field
        set(value) {
            field = value
            sizeError.set("")
//            if (field != "") passwordError.set(null)
        }

    @Transient
    var sizeError: ObservableField<String>

    init {
        sizeError = ObservableField("")
    }

    fun isValid(): Boolean {
        var valid = true

        if (!Validate.isValid(name)) {
            Log.d("size", "size not valid")
            sizeError.set(Validate.error)
            valid = false
        } else sizeError.set("")

        return valid
    }
}