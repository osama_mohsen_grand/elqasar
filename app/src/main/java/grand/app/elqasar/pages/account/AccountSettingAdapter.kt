package grand.app.elqasar.pages.account

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat.finishAffinity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import grand.app.elqasar.R
import grand.app.elqasar.databinding.ItemSettingBinding
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.auth.login.LoginFragment
import grand.app.elqasar.pages.home.HomeModel
import grand.app.elqasar.pages.home.ItemHomeViewModel
import grand.app.elqasar.pages.loginorregister.LoginOrRegisterFragment
import grand.app.elqasar.pages.order.ui.OrderListFragment
import grand.app.elqasar.pages.social.SocialFragment
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.dialog.DialogLogin
import grand.app.elqasar.utils.helper.AppHelper
import grand.app.elqasar.utils.helper.MovementHelper
import grand.app.elqasar.utils.resources.ResourceManager
import grand.app.elqasar.utils.session.UserHelper

class AccountSettingAdapter : RecyclerView.Adapter<AccountSettingAdapter.ViewHolder>() {
    // fetch list data
    private lateinit var modelList: ArrayList<HomeModel>
    private lateinit var activity: FragmentActivity
    lateinit var dialogLogin: DialogLogin

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // binding item_model layout
        val binding: ItemSettingBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_setting,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    fun getActivity(): FragmentActivity {
        return activity
    }

    fun setActivity(activity: FragmentActivity) {
        this.activity = activity
        dialogLogin = DialogLogin(activity)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(modelList[position])
        holder.viewModel.mutableLiveData.observeForever {
            val mutable: Mutable = it as Mutable
            val model: HomeModel = modelList[mutable.position]
            if (mutable.message == Constants.SUBMIT) {
                if (model.title == ResourceManager.getString(R.string.rate)) {
                    AppHelper.openBrowser(
                        holder.itemView.context,
                        AppHelper.getPlayStoreLink(holder.itemView.context)
                    )
                } else if (model.title == ResourceManager.getString(R.string.logout)) {
                    UserHelper.clearUserDetails()
                    activity.finishAffinity()
                    val bundle: Bundle = Bundle()
                    bundle.putString(Constants.PAGE, LoginOrRegisterFragment::class.java.name)
                    MovementHelper.startActivityBase(activity, bundle);
                } else if (model.title == ResourceManager.getString(R.string.login)) {
                    UserHelper.clearUserDetails()
                    activity.finishAffinity()
                    val bundle: Bundle = Bundle()
                    bundle.putString(Constants.PAGE, LoginFragment::class.java.name)
                    MovementHelper.startActivityBase(activity, bundle);
                } else if (!UserHelper.isLogin() &&
                    (model.title == ResourceManager.getString(R.string.call_us) ||
                            model.title == ResourceManager.getString(R.string.my_requests) ||
                            model.title == ResourceManager.getString(R.string.my_address))
                ) {
                    dialogLogin.show()
                } else {
                    val bundle = Bundle()
                    bundle.putString(Constants.PAGE, model.fragment)
                    bundle.putInt(Constants.TYPE, model.type)
                    bundle.putString(Constants.TITLE, model.title)
                    if (model.fragment == SocialFragment::class.java.name || model.fragment == OrderListFragment::class.java.name) {
                        MovementHelper.startActivityBase(
                            holder.itemView.context,
                            bundle,
                            model.title
                        );
                    } else
                        MovementHelper.startActivityBase(holder.itemView.context, bundle);
                }
            }
        }
    }

    override fun getItemCount(): Int {
        //return data count if exist or 0
        return if (::modelList.isInitialized) modelList.size else 0
    }

    fun update(modelList: ArrayList<HomeModel>) {
        //update data after call service again in scroll , and notify list which end with
        this.modelList = modelList
        notifyDataSetChanged()
    }

    class ViewHolder(private val binding: ItemSettingBinding) :
        RecyclerView.ViewHolder(binding.root) {
        lateinit var viewModel: ItemHomeViewModel
        fun bind(model: HomeModel) {
            viewModel = ItemHomeViewModel(model,position = adapterPosition)
            binding.viewmodel = viewModel
        }
    }
}