package grand.app.elqasar.pages.auth.changepassword

import androidx.databinding.ObservableField
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import grand.app.elqasar.R
import grand.app.elqasar.utils.resources.ResourceManager
import grand.app.elqasar.utils.session.UserHelper
import grand.app.elqasar.utils.validation.Validate
import timber.log.Timber

class ChangePasswordRequest {

    private val TAG = "ChangePasswordRequest"

    var confirmPassword = ""

    @SerializedName("old_password")
    @Expose
    var oldPassword: String = ""
        get() = field
        set(value) {
            field = value
            oldPasswordError.set("")
        }

    @SerializedName("new_password")
    @Expose
    var newPassword = ""
        get() = field
        set(value) {
            field = value
            newPasswordError.set("")
        }

    @SerializedName("phone")
    @Expose
    var phone = ""
        get() = field
        set(value) {
            field = value
            phoneError.set("")
        }

    var isLogin = false

    @Transient
    lateinit var newPasswordError: ObservableField<String>
    @Transient
    lateinit var oldPasswordError: ObservableField<String>
    @Transient
    lateinit var confirmPasswordError: ObservableField<String>
    @Transient
    lateinit var phoneError: ObservableField<String>

    init {
        if (UserHelper.getUserId() != -1) isLogin = true
        newPasswordError = ObservableField("")
        oldPasswordError = ObservableField("")
        confirmPasswordError = ObservableField("")
        phoneError = ObservableField("")
    }

    fun isValid(): Boolean {
        var valid = true
        if (isLogin && !Validate.isValid(oldPassword)) {
            Timber.e("old password")
            oldPasswordError.set(Validate.error)
            valid = false
        } else oldPasswordError.set("")

        if (!Validate.isValid(newPassword)) {
            Timber.e("new password:" + Validate.error)
            newPasswordError.set(Validate.error)
            valid = false
        } else newPasswordError.set("")

        if (!Validate.isValid(confirmPassword)) {
            Timber.e("confirm password:" + Validate.error)
            confirmPasswordError.set(Validate.error)
            valid = false
        } else confirmPasswordError.set("")

        if (!Validate.isValid(phone) && !isLogin) {
            Timber.e("phone:" + Validate.error)
            phoneError.set(Validate.error)
            valid = false
        } else phoneError.set("")

        if (valid && !Validate.isMatchPassword(newPassword, confirmPassword)) {
            newPasswordError.set(ResourceManager.getString(R.string.both_password_are_not_same))
            confirmPasswordError.set(ResourceManager.getString(R.string.both_password_are_not_same))
            valid = false
        } else if (valid) {
            newPasswordError.set("")
            confirmPasswordError.set("")
        }
        return valid
    }

}