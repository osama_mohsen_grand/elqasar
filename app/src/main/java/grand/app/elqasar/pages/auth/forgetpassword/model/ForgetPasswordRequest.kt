package grand.app.elqasar.pages.auth.forgetpassword.model

import android.util.Log
import androidx.databinding.ObservableField
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.session.UserHelper
import grand.app.elqasar.utils.validation.Validate
import timber.log.Timber

class ForgetPasswordRequest() {

    @SerializedName("phone")
    @Expose
    var phone = ""
        get() = field
        set(value) {
            field = value
            phoneError.set(null)
        }


    @Transient
    lateinit var phoneError: ObservableField<String>

    constructor(phone: String) : this() {
        this.phone = phone
    }

    init {
        phoneError = ObservableField()

    }

    fun isValid(): Boolean {
        var valid = true

        if (!Validate.isValidPhoneRegularExpression(phone)) {
            Log.d("valid", "phone")
            phoneError.set(Validate.error)
            valid = false
        } else phoneError.set(null)

        return valid
    }


}