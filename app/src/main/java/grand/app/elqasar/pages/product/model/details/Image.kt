package grand.app.elqasar.pages.product.model.details


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Image(
    @SerializedName("id")
    @Expose
    var id: Int = 0,
    @SerializedName("product_id")
    @Expose
    var productId: Int = 0,
    @SerializedName("image")
    @Expose
    var image: String = ""
){

}