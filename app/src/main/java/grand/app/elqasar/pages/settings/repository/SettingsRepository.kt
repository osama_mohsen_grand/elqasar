package grand.app.elqasar.pages.settings.repository

import androidx.lifecycle.MutableLiveData
import grand.app.elqasar.pages.splash.models.MainSettingsResponse
import grand.app.elqasar.connection.ConnectionHelper
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.model.base.StatusMessage
import grand.app.elqasar.pages.settings.contact.ContactUsRequest
import grand.app.elqasar.pages.settings.notification.model.NotificationResponse
import grand.app.elqasar.pages.settings.term.SettingsResponse
import grand.app.elqasar.pages.social.model.SocialResponse
import grand.app.elqasar.repository.BaseRepository
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.URLS
import io.reactivex.disposables.Disposable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SettingsRepository : BaseRepository {

    @Inject
    lateinit var connectionHelper: ConnectionHelper

    @Inject
    constructor(connectionHelper: ConnectionHelper){
        this.connectionHelper = connectionHelper;
    }

    private var liveData: MutableLiveData<Mutable> = MutableLiveData()
    fun setLiveData(liveData: MutableLiveData<Mutable>) {
        this.liveData = liveData
        connectionHelper.liveData = liveData
    }


    fun getMainSettings() : Disposable {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.MAIN_SETTINGS, Any(), MainSettingsResponse::class.java,
            URLS.MAIN_SETTINGS, false)
    }

    fun contact(request: ContactUsRequest): Disposable {
        return connectionHelper.requestApi(Constants.POST_REQUEST, URLS.CONTACT_US, request, StatusMessage::class.java,
                Constants.CONTACT_US, true)
    }

    fun getSettings(status : Int) : Disposable {
        return connectionHelper.requestApi(Constants.GET_REQUEST,
            URLS.SETTINGS+status, Any(), if(status == 3) SocialResponse::class.java else SettingsResponse::class.java,
            URLS.SETTINGS, true)
    }

    fun getNotifications(): Disposable {
        return connectionHelper.requestApi(Constants.GET_REQUEST,
            URLS.NOTIFICATIONS, Any(), NotificationResponse::class.java,
            URLS.NOTIFICATIONS, true)
    }
}