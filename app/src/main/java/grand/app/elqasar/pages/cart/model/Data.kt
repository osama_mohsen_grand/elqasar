package grand.app.elqasar.pages.cart.model


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Data(
    @SerializedName("cart")
    var cart: ArrayList<Cart> = arrayListOf(),
    @SerializedName("cart_count")
    var cartCount:Int = -1,
    @SerializedName("cost")
    var cost:Int = -1,
    @SerializedName("discount")
    var discount:Int = -1,
    @SerializedName("shipping")
    var shipping:Int = -1,
    @SerializedName("address_id")
    var addressId:Int = -1,
    @SerializedName("taxes_percent")
    var taxesPercent: String = "",
    @SerializedName("taxes_amount")
    var taxesAmount:Double = -1.0,
    @SerializedName("total_price")
    var totalPrice:Double = -1.0,
    @SerializedName("promo")
    @Expose
    var promo: String = "",
    @SerializedName("promo_amount")
    @Expose
    var promoAmount: Double = -1.0
) : Serializable {
    @SerializedName("promo_percent")
    @Expose
    var promoPercent: String = ""
        get() {
            field = when (field) {
                "0" -> ""
                else -> field
            }
            return field
        }
    
}