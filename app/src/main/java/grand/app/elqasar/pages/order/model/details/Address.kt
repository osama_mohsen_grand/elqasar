package grand.app.elqasar.pages.order.model.details


import com.google.gson.annotations.SerializedName
import grand.app.elqasar.R
import grand.app.elqasar.utils.resources.ResourceManager

data class Address(
    @SerializedName("id")
    var id: Int = 0
) {

    @SerializedName("first_name")
    var firstName = ""
        get() = "${ResourceManager.getString(R.string.name)} : $field"

    @SerializedName("phone1")
    var phone1 = ""
        get() = "${ResourceManager.getString(R.string.phone)} : $field"


    @SerializedName("city")
    var city = ""
        get() = "${ResourceManager.getString(R.string.address)} : $field"
}