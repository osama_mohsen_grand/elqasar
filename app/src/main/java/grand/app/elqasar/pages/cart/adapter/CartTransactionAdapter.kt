package grand.app.elqasar.pages.cart.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.elqasar.R
import grand.app.elqasar.databinding.ItemCartTransactionBinding
import grand.app.elqasar.pages.cart.model.CartResponse
import grand.app.elqasar.pages.cart.model.CartTransaction
import grand.app.elqasar.pages.cart.viewmodel.ItemCartTransactionViewModel
import grand.app.elqasar.pages.makeyourclothes.fabric.model.ClothesModelRequest
import grand.app.elqasar.pages.order.model.details.OrderDetailsResponse
import grand.app.elqasar.utils.resources.ResourceManager

class CartTransactionAdapter(var modelList: ArrayList<CartTransaction> = ArrayList()) :
    RecyclerView.Adapter<CartTransactionAdapter.ViewHolder>() {
    // fetch list data
    lateinit var response: CartResponse
    fun setList(response: CartResponse) {
        this.response = response
        modelList.clear()
        modelList.add(
            CartTransaction(
                ResourceManager.getString(R.string.total),
                response.data.cost
            )
        )
        modelList.add(
            CartTransaction(
                ResourceManager.getString(R.string.discount),
                response.data.discount
            )
        )
        modelList.add(
            CartTransaction(
                "${ResourceManager.getString(R.string.code_discount)} ${response.data.promoPercent}",
                response.data.promoAmount.toInt()
            )
        )
        modelList.add(
            CartTransaction(
                "${ResourceManager.getString(R.string.added_taxex)} ${response.data.taxesPercent}",
                response.data.taxesAmount.toInt()
            )
        )
        modelList.add(
            CartTransaction(
                ResourceManager.getString(R.string.shipping),
                response.data.shipping
            )
        )
        modelList.add(
            CartTransaction(
                ResourceManager.getString(R.string.total_summation),
                response.data.totalPrice.toInt()
            )
        )
        notifyDataSetChanged()
    }
    private  val TAG = "CartTransactionAdapter"

    fun updateSipping(newShipping: Int){
        Log.d(TAG,"total ${response.data.totalPrice}")
        Log.d(TAG,"new shipping ${response.data.shipping}")
        response.data.totalPrice -= response.data.shipping
        response.data.totalPrice += newShipping
        response.data.shipping = newShipping

        Log.d(TAG,"total ${response.data.totalPrice}")
        Log.d(TAG,"new shipping ${response.data.shipping}")

        setList(response)
    }


    fun update(response: CartResponse) {
        setList(response)
    }

    fun setList(response: OrderDetailsResponse) {
        modelList.clear()
        modelList.add(
            CartTransaction(
                ResourceManager.getString(R.string.total),
                response.data.subtotalFees.toInt()
            )
        )
        modelList.add(
            CartTransaction(
                ResourceManager.getString(R.string.discount),
                response.data.discountFees.toInt()
            )
        )
        modelList.add(
            CartTransaction(
                "${ResourceManager.getString(R.string.code_discount)} ${response.data.promoPercent}",
                response.data.promoAmount
            )
        )
        modelList.add(
            CartTransaction(
                "${ResourceManager.getString(R.string.added_taxex)} ${response.data.taxesPercent}",
                response.data.taxesFees.toInt()
            )
        )
        modelList.add(
            CartTransaction(
                ResourceManager.getString(R.string.shipping),
                response.data.shippingFees.toInt()
            )
        )
        modelList.add(
            CartTransaction(
                ResourceManager.getString(R.string.total_summation),
                response.data.totalPrice.toInt()
            )
        )
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // binding item_model layout
        val binding: ItemCartTransactionBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_cart_transaction,
            parent,
            false
        )
        return ViewHolder(
            binding
        )
    }

    val request: ClothesModelRequest = ClothesModelRequest()
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(modelList[position])
    }

    override fun getItemCount(): Int {
        //return data count if exist or 0
        return modelList.size
    }

    fun update(modelList: ArrayList<CartTransaction>) {
        //update data after call service again in scroll , and notify list which end with
        this.modelList = modelList
        notifyItemRangeInserted(getItemCount(), modelList.size - 1);
    }

    fun update(total: Int) {
        modelList[0].value = total
        modelList[3].value =
            total + modelList[1].value + modelList[2].value + response.data.taxesAmount.toInt()
        notifyDataSetChanged()
    }


    inner class ViewHolder(private val binding: ItemCartTransactionBinding) :
        RecyclerView.ViewHolder(binding.root) {
        lateinit var viewModel: ItemCartTransactionViewModel

        //bint
        fun bind(model: CartTransaction) {
            viewModel = ItemCartTransactionViewModel(model, adapterPosition, adapterPosition == modelList.size - 1)
            binding.viewmodel = viewModel
        }
    }
}