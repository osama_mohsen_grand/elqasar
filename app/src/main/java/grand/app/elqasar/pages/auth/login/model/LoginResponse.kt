package grand.app.elqasar.pages.auth.login.model


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import grand.app.elqasar.model.base.StatusMessage

data class LoginResponse(
    @SerializedName("data")
    @Expose
    var user: User
) : StatusMessage()