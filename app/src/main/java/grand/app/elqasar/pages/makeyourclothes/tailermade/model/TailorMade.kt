package grand.app.elqasar.pages.makeyourclothes.tailermade.model


import com.google.gson.annotations.SerializedName

data class TailorMade(
    @SerializedName("id")
    var id: Int,
    @SerializedName("name")
    var name: String
)