package grand.app.elqasar.pages.product.ui

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import grand.app.elqasar.R
import grand.app.elqasar.base.BaseFragment
import grand.app.elqasar.base.MyApplication
import grand.app.elqasar.base.di.IApplicationComponent
import grand.app.elqasar.databinding.FragmentProductDetailsBinding
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.model.base.StatusMessage
import grand.app.elqasar.pages.cart.model.CartResponse
import grand.app.elqasar.pages.product.model.details.ProductDetailsResponse
import grand.app.elqasar.pages.product.viewmodel.ProductDetailsViewModel
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.URLS
import grand.app.elqasar.utils.dialog.DialogLogin
import grand.app.elqasar.utils.helper.AppHelper
import grand.app.elqasar.utils.session.SharedPreferenceHelper
import javax.inject.Inject

class
ProductDetailsFragment : BaseFragment() {
    val TAG: String = this::class.java.name
    private lateinit var binding: FragmentProductDetailsBinding
    @Inject
    lateinit var viewModel: ProductDetailsViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_product_details, container, false)
        val component: IApplicationComponent? =
            (context!!.applicationContext as MyApplication).applicationComponent
        component?.inject(this)
        binding.viewmodel = viewModel
        arguments?.let { viewModel.setArgument(it) }
        setEvent()
//        onBackPress()
        // Inflate the layout for this fragment
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        onBackPress()
    }

    private fun setEvent() {
        viewModel.liveDataViewModel.observe(requireActivity(), Observer {
            val mutable = it as Mutable
            Log.d(TAG,mutable.message)
            handleActions(mutable)
//            Log.d(TAG,(mutable.obj as StatusMessage).mStatus+"")
            if (mutable.message == URLS.PRODUCT_DETAILS) {
                viewModel.setData(mutable.obj as ProductDetailsResponse)
            }else if (mutable.message == URLS.ADD_FAVOURITE) {
                Log.d(TAG,"success Favourite")
                toastMessage((mutable.obj as StatusMessage).mMessage)
                viewModel.response.get()?.productDetails?.userFavorite = if(viewModel.response.get()?.productDetails?.userFavorite == 0) 1 else 0
                viewModel.notifyChange()
                SharedPreferenceHelper.saveKey(Constants.FAVOURITE,viewModel.response.get()?.productDetails?.userFavorite.toString())
            }else if (mutable.message == Constants.BACK) {
                requireActivity().finish()
            }else if (mutable.message == Constants.SHARE) {
                AppHelper.shareCustom(requireActivity(),viewModel.response.get()?.productDetails?.name,viewModel.response.get()?.productDetails?.desc)
            }else if (mutable.message == URLS.ADD_TO_CART) {
                val response = mutable.obj as CartResponse
                toastMessage((mutable.obj as StatusMessage).mMessage)
                SharedPreferenceHelper.saveKey(Constants.CART,response.data.cartCount.toString())
                Log.d("cart","cart_count_product_details:"+SharedPreferenceHelper.getKey(Constants.CART))
            }
        })
    }

    override fun onResume() {
        super.onResume()
        viewModel.repository.setLiveData(viewModel.liveDataViewModel)
    }
}