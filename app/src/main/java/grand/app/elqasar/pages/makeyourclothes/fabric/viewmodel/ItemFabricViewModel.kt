package grand.app.elqasar.pages.makeyourclothes.fabric.viewmodel

import androidx.databinding.ObservableDouble
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import grand.app.elqasar.R
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.makeyourclothes.fabric.model.Fabric
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.resources.ResourceManager

data class ItemFabricViewModel(val model: Fabric, var position: Int,var checked: Boolean) {

    var mutableLiveData = MutableLiveData<Any>()

    var price: ObservableField<String> =
        ObservableField("${model.price} ${ResourceManager.getString(R.string.currency)}")

    var priceValue: ObservableDouble = ObservableDouble(0.0)

    var code: ObservableField<String> =
        ObservableField("${ResourceManager.getString(R.string.code)} : ${model.code}")

    init {
        model.price?.let {
            priceValue.set(model.price!!.toDouble())
        }
    }

    fun fav(){
        mutableLiveData.value = Mutable(Constants.FAVOURITE , position)
    }
    fun submit(){
        mutableLiveData.value = Mutable(Constants.SUBMIT , position)
    }
}