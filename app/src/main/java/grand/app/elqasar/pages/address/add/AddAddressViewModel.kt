package grand.app.elqasar.pages.address.add

import android.widget.RadioGroup
import androidx.databinding.ObservableBoolean
import androidx.lifecycle.MutableLiveData
import grand.app.elqasar.R
import grand.app.elqasar.base.BaseViewModel
import grand.app.elqasar.connection.FileObject
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.repository.UserRepository
import grand.app.elqasar.utils.session.UserHelper
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class AddAddressViewModel : BaseViewModel {
    @Inject
    lateinit var repository: UserRepository;

    var editable: ObservableBoolean = ObservableBoolean(true)
    var cities = arrayListOf<String>()

    var compositeDisposable: CompositeDisposable
    var request: AddAddressRequest

    val user = UserHelper.getUserDetails()

    //    private var liveData: LiveData<Mutable> = MutableLiveData()
    var liveDataViewModel: MutableLiveData<Mutable>
    var x: Int = 6
    var file : FileObject? = null

    @Inject
    constructor(repository: UserRepository) {
        this.repository = repository
        this.liveDataViewModel = MutableLiveData()
        compositeDisposable = CompositeDisposable()
        request = AddAddressRequest()
        repository.setLiveData(liveDataViewModel)
    }


    fun onSplitTypeChanged(radioGroup: RadioGroup?, id: Int) {
        when(id){
            R.id.inside_elhasa -> {
                request.type = 1
                request.country = getString(R.string.saudia)
                request.gov = getString(R.string.elhasa)
                editable.set(false)
            }
            R.id.outside_elhasa -> {
                request.type = 2
                request.country = ""
                request.gov = ""
                editable.set(true)
            }
        }
        notifyChange()
    }

    fun submit() {
        if (request.isValid()) {
            repository.addAddress(request)
        }
    }

    private fun unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }

    override fun onCleared() {
        super.onCleared()
        unSubscribeFromObservable()
    }
}