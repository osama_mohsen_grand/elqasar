package grand.app.elqasar.pages.auth.login.model


import com.google.gson.annotations.SerializedName

data class User(
    @SerializedName("cart_count")
    var cartCount: Int?,
    @SerializedName("email")
    var email: String,
    @SerializedName("first_name")
    var firstName: String,
    @SerializedName("fourth_name")
    var fourthName: String,
    @SerializedName("id")
    var id: Int,
    @SerializedName("image")
    var image: String,
    @SerializedName("jwt_token")
    var jwtToken: String,
    @SerializedName("phone")
    var phone: String,
    @SerializedName("second_name")
    var secondName: String,
    @SerializedName("third_name")
    var thirdName: String,
    @SerializedName("is_size")
    var isSize: Boolean = false,
    @SerializedName("size_id")
    var sizeId: Int = 4
) {
    constructor() : this(0,"","","",-1,"","","","","")
}