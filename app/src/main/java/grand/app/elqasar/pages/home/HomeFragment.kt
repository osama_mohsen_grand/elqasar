package grand.app.elqasar.pages.home

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver.OnGlobalLayoutListener
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import grand.app.elqasar.R
import grand.app.elqasar.base.BaseFragment
import grand.app.elqasar.base.MyApplication
import grand.app.elqasar.base.di.IApplicationComponent
import grand.app.elqasar.databinding.FragmentHomeBinding
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.search.SearchFragment
import grand.app.elqasar.pages.settings.notification.NotificationListFragment
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.helper.MovementHelper
import javax.inject.Inject


class HomeFragment : BaseFragment() {
    private lateinit var binding: FragmentHomeBinding
    @Inject
    lateinit var viewModel: HomeViewModel

    private  val TAG = "HomeFragment"

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false)
        val component: IApplicationComponent? =
            (context!!.applicationContext as MyApplication).applicationComponent
        component?.inject(this)
        viewModel.adapter.width =  binding.rvHome.width
        val params: ViewGroup.LayoutParams = binding.rvHome.layoutParams
//        Log.d(TAG,"=<><>=<>=<> ${params.height}")


        binding.rvHome.viewTreeObserver.addOnGlobalLayoutListener(object : OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                binding.rvHome.viewTreeObserver.removeOnGlobalLayoutListener(this)
                binding.rvHome.height //height is ready
                viewModel.adapter.height =  binding.rvHome.height
                viewModel.add()
            }
        })
        binding.viewmodel = viewModel
        setEvent()
        // Inflate the layout for this fragment
        return binding.root
    }


    private fun setEvent() {
        viewModel.liveDataViewModel.observe(requireActivity(), Observer {
            val mutable = it as Mutable
            handleActions(mutable)
//            Log.d(TAG,(mutable.obj as StatusMessage).mStatus+"")
            if (mutable.message == Constants.NOTIFICATIONS) {
                val bundle = Bundle()
                bundle.putString(Constants.PAGE, NotificationListFragment::class.java.name)
                MovementHelper.startActivityBase(context, bundle, getString(R.string.notifications))
            }
            if (mutable.message == Constants.SEARCH) {
                val bundle = Bundle()
                bundle.putString(Constants.PAGE, SearchFragment::class.java.name)
                MovementHelper.startActivityBase(context, bundle, getString(R.string.search))
            }
        })
    }
    override fun onResume() {
        super.onResume()
        viewModel.liveDataViewModel = viewModel.liveDataViewModel
    }
}