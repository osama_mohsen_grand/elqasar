package grand.app.elqasar.pages.payment

import com.myfatoorah.sdk.model.initiatepayment.PaymentMethod

interface OnListFragmentInteractionListener {
    fun onListFragmentInteraction(position: Int, item: PaymentMethod)
}