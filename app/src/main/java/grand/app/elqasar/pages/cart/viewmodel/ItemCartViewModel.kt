package grand.app.elqasar.pages.cart.viewmodel

import android.util.Log
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import grand.app.elqasar.R
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.cart.model.Cart
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.resources.ResourceManager

data class ItemCartViewModel(val model : Cart, var position : Int) {
    var mutableLiveData = MutableLiveData<Any>()

    var qty = ObservableField<String>(model.qty.toString())
    var qty_details = ObservableField<String>("${ResourceManager.getString(R.string.qty)} ${model.qty}")

    var price: ObservableField<String> =
        ObservableField("${model.price * model.qty} ${ResourceManager.getString(R.string.currency)}")

    fun fav(){
        mutableLiveData.value = Mutable(Constants.FAVOURITE , position)
    }
    fun delete(){
        mutableLiveData.value = Mutable(Constants.DELETE , position)
    }

    private  val TAG = "ItemCartViewModel"
    fun add(){
        val qtyTmp: Int = model.qty + 1
        mutableLiveData.value = Mutable(Constants.ADD, position,qtyTmp)
    }
    fun minus(){
        val qtyTmp: Int = model.qty - 1
        if(qtyTmp > 0) {
            mutableLiveData.value = Mutable(Constants.MINUS, position,qtyTmp)
        }
    }
    fun submit(){
        mutableLiveData.value = Mutable(Constants.SUBMIT , position)
    }
}