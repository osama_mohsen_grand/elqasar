package grand.app.elqasar.pages.settings.notification.viewmodel

import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import grand.app.elqasar.R
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.settings.notification.model.Notification
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.resources.ResourceManager

data class ItemNotificationViewModel(val model: Notification, var position: Int) {

    var mutableLiveData = MutableLiveData<Any>()
    
    fun submit(){
        mutableLiveData.value = Mutable(Constants.SUBMIT , position)
    }
}