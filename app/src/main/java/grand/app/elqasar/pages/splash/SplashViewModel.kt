package grand.app.elqasar.pages.splash

import androidx.lifecycle.MutableLiveData
import grand.app.elqasar.base.BaseViewModel
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.settings.repository.SettingsRepository
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class SplashViewModel : BaseViewModel {
    @Inject
    lateinit var repository: SettingsRepository;
//    private var liveData: LiveData<Mutable> = MutableLiveData()
    var liveDataViewModel: MutableLiveData<Mutable>
    var x: Int = 6

    @Inject
    constructor(repository: SettingsRepository){
        this.repository = repository
        this.liveDataViewModel = MutableLiveData();
        repository.setLiveData(liveDataViewModel)
    }

    private val compositeDisposable: CompositeDisposable? = CompositeDisposable()

    fun call() {
        compositeDisposable?.add(repository.getMainSettings())
        notifyChange()
    }

    protected fun unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }

    override fun onCleared() {
        super.onCleared()
        unSubscribeFromObservable()
    }
}