package grand.app.elqasar.pages.product.slider

import android.app.Activity
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.github.islamkhsh.CardSliderAdapter
import grand.app.elqasar.R
import grand.app.elqasar.databinding.ItemSliderBinding
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.product.model.FavouriteRequest
import grand.app.elqasar.pages.product.model.list.Product
import grand.app.elqasar.pages.product.ui.ProductDetailsFragment
import grand.app.elqasar.pages.product.ui.ProductListSliderFragment
import grand.app.elqasar.repository.ProductRepository
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.dialog.DialogLogin
import grand.app.elqasar.utils.helper.MovementHelper

class SliderAdapter(var modelList:ArrayList<Product> = ArrayList(),private var repository: ProductRepository): CardSliderAdapter<SliderAdapter.ViewHolder>() {
    // fetch list data
    var favPosition : Int = 0
    var type:Int = 0
    var selectedPosition : Int = -1
    private lateinit var fragment: Fragment
    lateinit var product : Product
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // binding item_model layout
        val binding: ItemSliderBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_slider, parent, false)
        return ViewHolder(binding)
    }

    fun setFragment(fragment: Fragment){
        this.fragment = fragment
    }

    override fun bindVH(holder: ViewHolder, position: Int) {
        holder.bind(modelList[position])
        holder.viewModel.mutableLiveData.observeForever {
            val mutable : Mutable = it as Mutable
            product = modelList[mutable.position]
            if(mutable.message == Constants.SUBMIT){
                selectedPosition = position
                val bundle = Bundle()
                bundle.putString(Constants.PAGE, ProductDetailsFragment::class.java.name)
                bundle.putInt(Constants.ID,product.id)
                bundle.putInt(Constants.POSITION,selectedPosition)
                bundle.putInt(Constants.TYPE,type)
                bundle.putString(Constants.TITLE,product.name)
//                MovementHelper.startActivityBase(holder.itemView.context, bundle);
                MovementHelper.startActivityBaseForResult(fragment, bundle,Constants.FAVOURITE_RESULT);

            }else if(mutable.message == Constants.FAVOURITE){
                favPosition = position
                repository.addFavourite(FavouriteRequest(product.id,type))
            }
        }
    }


    override fun getItemCount(): Int {
        //return data count if exist or 0
        return modelList.size
    }

    fun updateFavourite(favourite : Int){
        modelList[favPosition].userFavorite = favourite
        notifyItemChanged(favPosition)
    }

    fun updateFavouriteBack(favourite : Int){
        if(selectedPosition != -1 && modelList.size > 0 && selectedPosition < modelList.size)
        modelList[selectedPosition].userFavorite = favourite
        notifyItemChanged(selectedPosition)
    }

    fun update(modelList:ArrayList<Product>){
        //update data after call service again in scroll , and notify list which end with
        this.modelList = modelList
        notifyItemRangeInserted(getItemCount(), modelList.size - 1);
    }

    class ViewHolder(private val binding: ItemSliderBinding):RecyclerView.ViewHolder(binding.root){
        lateinit var viewModel : ItemSliderViewModel
        //bint
        fun bind(model:Product){
            viewModel = ItemSliderViewModel(model,adapterPosition)
            binding.viewmodel = viewModel
        }
    }

    /**
     * This method should update the contents of the {@link VH#itemView} to reflect the item at the
     * given position.
     *
     * @param holder The ViewHolder which should be updated to represent the contents of the
     *        item at the given position in the data set.
     * @param position The position of the item within the adapter's data set.
     */

}