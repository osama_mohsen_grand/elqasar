package grand.app.elqasar.pages.order.model


import com.google.gson.annotations.SerializedName

data class OrderStatus(
    @SerializedName("id")
    var id: Int,
    @SerializedName("image")
    var image: String,
    @SerializedName("name")
    var name: String
)