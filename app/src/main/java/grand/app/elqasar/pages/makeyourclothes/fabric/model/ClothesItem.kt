package grand.app.elqasar.pages.makeyourclothes.fabric.model

import com.google.gson.annotations.SerializedName
import grand.app.elqasar.pages.size.model.MySizeModel
import java.io.Serializable

class ClothesItem(@SerializedName("item") var models : ArrayList<ClothesModel> = arrayListOf(),
                  @SerializedName("notes") var notes: String = "",
                  @SerializedName("cloth_id") var cloth_id: Int = 0,
                  @SerializedName("detail_id") var detail_id: Int = 0,
                  @SerializedName("cart_item_id") var cartIdemId: Int = -1) :
    Serializable {}