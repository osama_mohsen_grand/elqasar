package grand.app.elqasar.pages.size.ui

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import grand.app.elqasar.R
import grand.app.elqasar.activity.MainActivity
import grand.app.elqasar.base.BaseFragment
import grand.app.elqasar.base.MyApplication
import grand.app.elqasar.base.di.IApplicationComponent
import grand.app.elqasar.databinding.FragmentMySizesBinding
import grand.app.elqasar.databinding.FragmentSizesBinding
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.model.base.StatusMessage
import grand.app.elqasar.pages.order.model.OrderStatusResponse
import grand.app.elqasar.pages.size.model.MySizesResponse
import grand.app.elqasar.pages.size.viewmodel.MySizesViewModel
import grand.app.elqasar.pages.size.viewmodel.SizesViewModel
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.URLS
import grand.app.elqasar.utils.helper.MovementHelper
import grand.app.elqasar.utils.session.UserHelper
import javax.inject.Inject

class MySizesFragment : BaseFragment() {
    private lateinit var binding: FragmentMySizesBinding

    @Inject
    lateinit var viewModel: MySizesViewModel

    private val TAG = "MySizesFragment"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_my_sizes, container, false)
        val component: IApplicationComponent? =
            (context!!.applicationContext as MyApplication).applicationComponent
        component?.inject(this)
        binding.viewmodel = viewModel
        arguments?.let { viewModel.setArgument(it) }
        setEvent()
        // Inflate the layout for this fragment
        return binding.root
    }


    private fun setEvent() {
        viewModel.liveDataViewModel.observe(requireActivity(), Observer {
            val mutable = it as Mutable
            handleActions(mutable)
            Log.d("$TAG , ${mutable.message}",mutable.message)
            Log.d(TAG,viewModel.type)
            if (mutable.message == URLS.USER_SIZES) {
                viewModel.response = (mutable.obj as MySizesResponse)
                viewModel.setData()
            } else if (mutable.message == Constants.ERROR) {
                showError(viewModel.msg)
            }else if (mutable.message == URLS.MY_SIZES) {
                UserHelper.saveSize()
                toastMessage( (mutable.obj as StatusMessage).mMessage)
                if(viewModel.type == Constants.REGISTER){
                    requireActivity().finishAffinity()
                    val intent = Intent(requireActivity(),MainActivity::class.java)
                    intent.putExtra(Constants.DIALOG,getString(R.string.your_register_had_been_completed))
                    startActivity(intent)
                }else if(viewModel.cart.get()){
                    requireActivity().finish()
                }
            }else if(mutable.message == Constants.CART){
                val intent = Intent()
                val bundle = Bundle()
                bundle.putSerializable(Constants.REQUEST_MODELS, viewModel.adapter.request)
                intent.putExtra(Constants.BUNDLE,bundle)
                requireActivity().setResult(Activity.RESULT_OK, intent);
                requireActivity().finish()
            }
        })
    }

    override fun onResume() {
        super.onResume()
        viewModel.repository.setLiveData(viewModel.liveDataViewModel)
    }
}