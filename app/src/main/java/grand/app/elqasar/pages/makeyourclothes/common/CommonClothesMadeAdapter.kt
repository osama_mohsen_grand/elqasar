package grand.app.elqasar.pages.makeyourclothes.common

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import grand.app.elqasar.R
import grand.app.elqasar.databinding.ItemCommonClothesMadeBinding
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.makeyourclothes.fabric.model.ClothesItem
import grand.app.elqasar.pages.makeyourclothes.fabric.model.ClothesModelRequest
import grand.app.elqasar.pages.makeyourclothes.fabric.viewmodel.ItemFabricViewModel
import grand.app.elqasar.pages.makeyourclothes.fabric.model.Fabric
import grand.app.elqasar.repository.ProductRepository
import grand.app.elqasar.utils.helper.AppSpecificHelper

class CommonClothesMadeAdapter(var modelList:ArrayList<Fabric> = ArrayList(), private var repository: ProductRepository): RecyclerView.Adapter<CommonClothesMadeAdapter.ViewHolder>() {
    // fetch list data
    var favPosition : Int = 0
    var selectedItem : Int = -1
    lateinit var fabric : Fabric
    var liveDataViewModel: MutableLiveData<Mutable>  = MutableLiveData();
    lateinit var request: ClothesItem
    var type: Int = -1
    var isBack: Boolean = true
    var allowAction = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // binding item_model layout
        val binding: ItemCommonClothesMadeBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_common_clothes_made, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(modelList[position])
        holder.viewModel.mutableLiveData.observeForever {
            if(allowAction) {
                if (selectedItem != -1) {
                    holder.viewModel.checked = false
                    notifyItemChanged(selectedItem)
                }
                selectedItem = (it as Mutable).position
                notifyItemChanged(selectedItem)
                liveDataViewModel.value = it
            }
        }


    }


    fun setSelectedIfExist(){
        val id = AppSpecificHelper.getId(type,request.models)
        if(id != -1) {
            for (i in 0 until  modelList.size) {
                if (id == modelList[i].id) {
                    selectedItem = i
                    notifyDataSetChanged()
                    return
                }
            }
        }
    }


    private  val TAG = this::class.java.name


    override fun getItemCount(): Int {
        //return data count if exist or 0
        return modelList.size
    }


    fun updateFavourite(favorite: Int) {
        modelList[favPosition].userFavorite = favorite
        notifyItemChanged(favPosition)
    }

    fun update(modelList:ArrayList<Fabric>){
        //update data after call service again in scroll , and notify list which end with
        this.modelList = modelList
        notifyItemRangeInserted(getItemCount(), modelList.size - 1);
    }

    inner class ViewHolder(private val binding: ItemCommonClothesMadeBinding):RecyclerView.ViewHolder(binding.root){
        lateinit var viewModel : ItemFabricViewModel
        //bint
        fun bind(model:Fabric){
            viewModel = ItemFabricViewModel(model,adapterPosition,selectedItem == adapterPosition)
            binding.viewmodel = viewModel
        }

    }
}