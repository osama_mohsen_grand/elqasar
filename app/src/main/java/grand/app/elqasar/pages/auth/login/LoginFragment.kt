package grand.app.elqasar.pages.auth.login

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnFocusChangeListener
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import grand.app.elqasar.R
import grand.app.elqasar.activity.MainActivity
import grand.app.elqasar.base.BaseFragment
import grand.app.elqasar.base.MyApplication
import grand.app.elqasar.base.di.IApplicationComponent
import grand.app.elqasar.databinding.FragmentLoginBinding
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.auth.forgetpassword.ForgetPasswordFragment
import grand.app.elqasar.pages.auth.login.model.LoginResponse
import grand.app.elqasar.pages.auth.register.RegisterFragment
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.helper.MovementHelper
import grand.app.elqasar.utils.session.UserHelper
import kotlinx.android.synthetic.main.fragment_login.view.*
import javax.inject.Inject


class LoginFragment : BaseFragment() {
    private lateinit var binding: FragmentLoginBinding
    @Inject
    lateinit var viewModel: LoginViewModel

    var countryCode = "+9665"
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_login, container, false)
        val component: IApplicationComponent? =
            (context!!.applicationContext as MyApplication).applicationComponent
        component?.inject(this)
        binding.viewmodel = viewModel
        arguments?.let { viewModel.setArgument(it) }
        init()
        setEvent()
        // Inflate the layout for this fragment
        return binding.root
    }

    private fun init() {
        binding.edtLoginPhone.onFocusChangeListener = OnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                if(v.edt_login_phone.text != null && !v.edt_login_phone.text!!.contains(countryCode))
                    v.edt_login_phone.setText(countryCode)
            }
        }

    }

    private  val TAG = "LoginFragment"

    private fun setEvent() {
        viewModel.liveDataViewModel.observe(requireActivity(), Observer {
            val mutable = it as Mutable
            Log.d(TAG,mutable.message)
            handleActions(mutable)
//            Log.d(TAG,(mutable.obj as StatusMessage).mStatus+"")
            if (mutable.message == Constants.SUCCESS) {
                toastMessage((mutable.obj as LoginResponse).mMessage)
                UserHelper.saveUserDetails((mutable.obj as LoginResponse).user)
                if(!viewModel.isBack) {
                    finishAllActivities()
                    MovementHelper.startActivity(context, MainActivity::class.java)
                }else
                    requireActivity().finish()
            } else if (mutable.message == Constants.REGISTER) {
                val bundle = Bundle()
                bundle.putString(Constants.PAGE, RegisterFragment::class.java.name)
                MovementHelper.startActivityBase(context, bundle, getString(R.string.register))
            } else if (mutable.message == Constants.FORGET_PASSWORD) {
                val bundle = Bundle()
                bundle.putString(Constants.PAGE, ForgetPasswordFragment::class.java.name)
                MovementHelper.startActivityBase(context, bundle, "")
            } else if (mutable.message == Constants.BACK) {
                requireActivity().finish()
            }
        })
    }

    override fun onResume() {
        super.onResume()
        viewModel.repository.setLiveData(viewModel.liveDataViewModel)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
    }
}