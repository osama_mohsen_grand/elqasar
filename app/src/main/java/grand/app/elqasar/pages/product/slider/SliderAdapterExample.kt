package grand.app.elqasar.pages.product.slider

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.smarteist.autoimageslider.SliderViewAdapter
import grand.app.elqasar.R
import grand.app.elqasar.pages.product.model.details.Image


class SliderAdapterExample(context: Context,var mImages: MutableList<Image> = ArrayList()) :
    SliderViewAdapter<SliderAdapterExample.SliderAdapterVH?>() {
    private val context: Context
    fun renewItems(Images: MutableList<Image>) {
        mImages = Images
        notifyDataSetChanged()
    }

    fun deleteItem(position: Int) {
        mImages.removeAt(position)
        notifyDataSetChanged()
    }

    fun addItem(Image: Image) {
        mImages.add(Image)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup): SliderAdapterVH {
        val inflate: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.image_slider_layout_item, null)
        return SliderAdapterVH(inflate)
    }

    override fun getCount(): Int {
        return mImages.size
    }

    override fun onBindViewHolder(
        viewHolder: SliderAdapterVH?,
        position: Int
    ) {
        val image: Image = mImages[position]
//        viewHolder?.textViewDescription?.setText(Image.getDescription())
        viewHolder?.textViewDescription?.textSize = 16f
        viewHolder?.textViewDescription?.setTextColor(Color.WHITE)
        viewHolder?.itemView?.context?.let {
            Glide.with(it)
                .load(image.image)
                .fitCenter()
                .into(
                    viewHolder.imageViewBackground
                )
        }
//        viewHolder.itemView.setOnClickListener(object : OnClickListener() {
//            fun onClick(v: View?) {
//                Toast.makeText(context, "This is item in position $position", Toast.LENGTH_SHORT)
//                    .show()
//            }
//        })
    }

    //slider view count could be dynamic size

    inner class SliderAdapterVH(itemView: View) :
        SliderViewAdapter.ViewHolder(itemView) {
//        lateinit var itemView: View
        var imageViewBackground: ImageView
        var imageGifContainer: ImageView
        var textViewDescription: TextView

        init {
            imageViewBackground = itemView.findViewById(R.id.iv_auto_image_slider)
            imageGifContainer = itemView.findViewById(R.id.iv_gif_container)
            textViewDescription = itemView.findViewById(R.id.tv_auto_image_slider)
//            this.itemView = itemView
        }
    }

    init {
        this.context = context
    }

//    override fun onBindViewHolder(viewHolder: SliderAdapterVH?, position: Int) {
//        TODO("Not yet implemented")
//    }
}