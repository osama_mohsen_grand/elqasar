package grand.app.elqasar.pages.splash.models

import android.util.Log
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class TestInputRequest {
    @SerializedName("password")
    @Expose
    private var password: String? = null

    @SerializedName("phone")
    @Expose
    private var phone: String? = null

    @SerializedName("email")
    @Expose
    private var email: String? = null
    private var validator = 0
    fun getPassword(): String? {
        return password
    }

    fun setPassword(password: String?) {
        this.password = password
    }

    fun getPhone(): String? {
        return phone
    }

    fun setPhone(phone: String?) {
        this.phone = phone
    }

    fun getEmail(): String? {
        return email
    }

    fun setEmail(email: String?) {
        this.email = email
    }

    fun setValidator(validator: Int) {
        this.validator = validator
    }

    fun getValidator(): Int {
        return validator
    }

    fun validate(): Boolean {
        Log.e("validator", " Validatorz $validator")
        return validator == 3
    }
}