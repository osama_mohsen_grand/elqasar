package grand.app.elqasar.pages.makeyourclothes.fabric.model


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Fabric(
    @SerializedName("code")
    @Expose
    var code: String = "",
    @SerializedName("id")
    @Expose
    var id: Int,
    @SerializedName("image")
    @Expose
    var image: String?,
    @SerializedName("name")
    @Expose
    var name: String ,
    @SerializedName("price")
    @Expose
    var price: String?,
    var qty: Int = 1,
    @SerializedName("stock")
    @Expose
    var stock: Int,
    @SerializedName("user_favorite")
    @Expose
    var userFavorite: Int?
){

}