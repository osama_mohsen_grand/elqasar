package grand.app.elqasar.pages.order.viewmodel

import android.os.Bundle
import android.util.Log
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import grand.app.elqasar.base.BaseViewModel
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.order.OrderRepository
import grand.app.elqasar.pages.order.adapter.OrderListAdapter
import grand.app.elqasar.pages.order.adapter.OrderStatusAdapter
import grand.app.elqasar.pages.order.model.OrderListResponse
import grand.app.elqasar.pages.order.model.OrderStatusResponse
import grand.app.elqasar.pages.product.adapter.ProductAdapter
import grand.app.elqasar.pages.product.model.list.ProductListResponse
import grand.app.elqasar.pages.settings.notification.NotificationAdapter
import grand.app.elqasar.pages.settings.notification.model.NotificationResponse
import grand.app.elqasar.pages.settings.repository.SettingsRepository
import grand.app.elqasar.repository.ProductRepository
import grand.app.elqasar.utils.Constants
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class OrderStatusViewModel : BaseViewModel {
    @Inject
    lateinit var repository: OrderRepository;
    var compositeDisposable: CompositeDisposable

    //    private var liveData: LiveData<Mutable> = MutableLiveData()
    var liveDataViewModel: MutableLiveData<Mutable>
    var orderId: Int = 0
    private val TAG = "OrderStatusViewModel"
    lateinit var response: OrderStatusResponse
    var adapter: OrderStatusAdapter
    var title: ObservableField<String> =
        ObservableField("")

    @Inject
    constructor(repository: OrderRepository) {
        this.repository = repository
        this.liveDataViewModel = MutableLiveData();
        compositeDisposable = CompositeDisposable()
        repository.setLiveData(liveDataViewModel)
        adapter = OrderStatusAdapter()
    }

    fun setArgument( bundle : Bundle){
        orderId = bundle.getInt(Constants.ID)
        repository.getOrderStatus(orderId)
    }

    private fun unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }

    override fun onCleared() {
        super.onCleared()
        unSubscribeFromObservable()
    }

    fun setData() {
        val image  = response.data[0].image
        for(orderStatus in response.data)
            if(response.orderStatus >= orderStatus.id)
                orderStatus.image = image
        adapter.update(response.data)
    }

}