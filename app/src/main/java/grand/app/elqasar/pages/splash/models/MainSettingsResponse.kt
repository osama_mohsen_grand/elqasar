package grand.app.elqasar.pages.splash.models


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import grand.app.elqasar.model.base.StatusMessage

class MainSettingsResponse (
    @SerializedName("cities")
    @Expose
    var cities: List<City>,
    @SerializedName("employers")
    @Expose
    var employers: List<Employer>,
    @SerializedName("sizes_status")
    @Expose
    var sizesStatus: List<SizesStatu>,
    @SerializedName("is_size")
    var isSize: Boolean = false
): StatusMessage()