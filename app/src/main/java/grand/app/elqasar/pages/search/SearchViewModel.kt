package grand.app.elqasar.pages.search

import android.os.Bundle
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import grand.app.elqasar.base.BaseViewModel
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.product.model.list.Product
import grand.app.elqasar.pages.product.model.list.ProductListResponse
import grand.app.elqasar.repository.ProductRepository
import grand.app.elqasar.utils.Constants
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class SearchViewModel : BaseViewModel {
    @Inject
    lateinit var repository: ProductRepository;
    var compositeDisposable: CompositeDisposable

    //    private var liveData: LiveData<Mutable> = MutableLiveData()
    var liveDataViewModel: MutableLiveData<Mutable>
    var type: Int = 0
    private val TAG = "SearchViewModel"
    var response: ProductListResponse = ProductListResponse()
    var adapter: SearchAdapter
    var title: ObservableField<String> =
        ObservableField("")
    var searchResult = ""
    var searchOnline = true

    @Inject
    constructor(repository: ProductRepository) {
        this.repository = repository
        this.liveDataViewModel = MutableLiveData();
        compositeDisposable = CompositeDisposable()
        repository.setLiveData(liveDataViewModel)
        adapter = SearchAdapter()
    }

    fun search(s: String) {
        if (s.trim() != "") {
            adapter.search = s
            if(searchOnline) repository.search(s)
            else{
                submitSearch()
            }
        }
    }

    fun submitSearch() {
        var products = arrayListOf<Product>()
        for(product in response.data){
            if(product.name.contains(adapter.search)){
                products.add(product)
            }
        }
        adapter.update(products)
    }

    private fun unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }

    override fun onCleared() {
        super.onCleared()
        unSubscribeFromObservable()
    }

    fun setData() {
        adapter.update(response.data)
    }
}