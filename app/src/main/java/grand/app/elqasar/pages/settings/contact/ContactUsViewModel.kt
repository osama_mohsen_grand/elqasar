package grand.app.elqasar.pages.settings.contact

import androidx.lifecycle.MutableLiveData
import grand.app.elqasar.base.BaseViewModel
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.settings.repository.SettingsRepository
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class ContactUsViewModel: BaseViewModel {
    private val TAG = "ContactUsViewModel"
    @Inject
    lateinit var repository: SettingsRepository;
    var compositeDisposable: CompositeDisposable
    var liveDataViewModel: MutableLiveData<Mutable>
    var request: ContactUsRequest


    @Inject
    constructor(repository: SettingsRepository) {
        this.repository = repository
        this.liveDataViewModel = MutableLiveData();
        request = ContactUsRequest()
        compositeDisposable = CompositeDisposable()
        repository.setLiveData(liveDataViewModel)
    }

    fun submit(){
        if(request.isValid()){
            repository.contact(request)
        }
    }

    protected fun unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }

    override fun onCleared() {
        super.onCleared()
        unSubscribeFromObservable()
    }
}