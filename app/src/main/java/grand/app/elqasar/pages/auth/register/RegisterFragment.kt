package grand.app.elqasar.pages.auth.register

import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import grand.app.elqasar.R
import grand.app.elqasar.activity.MainActivity
import grand.app.elqasar.base.BaseFragment
import grand.app.elqasar.base.MyApplication
import grand.app.elqasar.base.di.IApplicationComponent
import grand.app.elqasar.connection.FileObject
import grand.app.elqasar.databinding.FragmentRegisterBinding
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.auth.changepassword.ChangePasswordFragment
import grand.app.elqasar.pages.auth.code.VerifyCodeFragment
import grand.app.elqasar.pages.auth.forgetpassword.model.ForgetPasswordResponse
import grand.app.elqasar.pages.auth.login.model.LoginResponse
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.helper.MovementHelper
import grand.app.elqasar.utils.popup.PopUpInterface
import grand.app.elqasar.utils.popup.PopUpMenuHelper
import grand.app.elqasar.utils.session.UserHelper
import grand.app.elqasar.utils.upload.FileOperations
import kotlinx.android.synthetic.main.fragment_login.view.*
import kotlinx.android.synthetic.main.fragment_register.view.*
import timber.log.Timber
import java.io.File
import javax.inject.Inject

class RegisterFragment : BaseFragment() {
    private lateinit var binding: FragmentRegisterBinding
    val popupMenu = PopUpMenuHelper()
    @Inject
    lateinit var viewModel: RegisterViewModel
    var countryCode = "+9665"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_register, container, false)
        val component: IApplicationComponent? =
            (context!!.applicationContext as MyApplication).applicationComponent
        component?.inject(this)
        binding.viewmodel = viewModel
        arguments?.let { viewModel.setArgument(it) }
        init()
        setEvent()
        // Inflate the layout for this fragment
        return binding.root
    }

    private  val TAG = "RegisterFragment"

    private fun init() {
        binding.edtRegisterPhone.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                if (v.edt_register_phone.text != null && !v.edt_register_phone.text!!.contains(countryCode))
                    v.edt_register_phone.setText(countryCode)
            }
        }

    }

    private fun setEvent() {
        viewModel.liveDataViewModel.observe(requireActivity(), Observer {
            val mutable = it as Mutable
            handleActions(mutable)
            Log.d(TAG,mutable.message)
            if (mutable.message == Constants.FORGET_PASSWORD) {
                if ((mutable.obj as ForgetPasswordResponse).exist)
                    showError((mutable.obj as ForgetPasswordResponse).mMessage)
                else {
                    viewModel.sendVerificationCode()
                }
            } else if (mutable.message == Constants.REGISTER) {
                val bundle = Bundle()
                bundle.putString(Constants.PAGE, RegisterFragment::class.java.name)
                MovementHelper.startActivityBase(context, bundle, getString(R.string.register))
            } else if (mutable.message == Constants.LOGIN) {
                finishActivity()
            } else if (mutable.message == Constants.SELECT_IMAGE) {
                pickImageDialogSelect()
            } else if (mutable.message == Constants.CHANGE_PASSWORD) {
                val bundle = Bundle()
                bundle.putString(Constants.PAGE, ChangePasswordFragment::class.java.name)
                MovementHelper.startActivityBase(context, bundle)
            }else if (mutable.message == Constants.UPDATE_PROFILE) {
                toastMessage((mutable.obj as LoginResponse).mMessage)
                UserHelper.saveUserDetails((mutable.obj as LoginResponse).user)
                finishAllActivities()
                MovementHelper.startActivity(context,MainActivity::class.java)
            }else if (mutable.message == Constants.BACK) {
                requireActivity().finish()
            }else if (mutable.message == Constants.WRITE_CODE) {
                val bundle = Bundle()
                bundle.putSerializable(Constants.REGISTER, viewModel.request)
                bundle.putSerializable(Constants.IMAGE, viewModel.file)
                bundle.putBoolean(Constants.IS_BACK, viewModel.isBack)
                bundle.putString(Constants.VERIFY_ID, "")
                bundle.putString(Constants.PAGE, VerifyCodeFragment::class.java.name)
                bundle.putString(Constants.TYPE, if(UserHelper.isLogin()) Constants.UPDATE_PROFILE else Constants.REGISTER )
                MovementHelper.startActivityBase(context, bundle,"")
            }
        })

//        viewModel.liveDataViewModelFirebase.observe(requireActivity(), Observer {
//            val mutable = it as Mutable
//            handleActions(mutable)
//            Log.d(TAG, "setEvent: "+mutable.message)
//            if (mutable.message == Constants.WRITE_CODE) {
//                val bundle = Bundle()
//                bundle.putSerializable(Constants.REGISTER, viewModel.request)
//                bundle.putSerializable(Constants.IMAGE, viewModel.file)
//                bundle.putString(Constants.VERIFY_ID, viewModel.repositoryFireBase.getVerificationId())
//                bundle.putString(Constants.PAGE, VerifyCodeFragment::class.java.name)
//                bundle.putString(Constants.TYPE, if(UserHelper.isLogin()) Constants.UPDATE_PROFILE else Constants.REGISTER )
//                MovementHelper.startActivityBase(context, bundle, getString(R.string.verify))
//            }
//        })
        binding.edtRegisterCity.setOnClickListener{
            popupMenu.openPopUp(
                requireActivity(),
                binding.edtRegisterCity,
                viewModel.cities,object : PopUpInterface{
                    override fun submitPopUp(position: Int) {
                        binding.edtRegisterCity.setText(viewModel.cities[position])
                        viewModel.request.city_id = viewModel.mainSettingsResponse.cities[position].id
                    }
                })
        }
        binding.edtRegisterEmployee.setOnClickListener{
            popupMenu.openPopUp(
                requireActivity(),
                binding.edtRegisterEmployee,
                viewModel.employers,object : PopUpInterface{
                    override fun submitPopUp(position: Int) {
                        binding.edtRegisterEmployee.setText(viewModel.employers[position])
                        viewModel.request.employer_id = viewModel.mainSettingsResponse.employers[position].id
                    }
                })
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        //super method removed
//        Timber.e("onActivityResult:$requestCode")
        Log.d(TAG, "onActivityResult: $resultCode")
        Log.d(TAG, "onActivityResult: $requestCode")
        if (requestCode == Constants.FILE_TYPE_IMAGE) {
            if(data != null)
                Log.d(TAG, "data not null")
            else
                Log.d(TAG, "data null")
            viewModel.file = FileOperations.getFileObject(
                requireActivity(),
                data,
                Constants.IMAGE,
                Constants.FILE_TYPE_IMAGE
            )!!
            binding.imgRegister.setImageURI(
                Uri.parse(
                    File(viewModel.file!!.getFilePath().toString()).toString()
                )
            )
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onResume() {
        super.onResume()
        viewModel.repository.setLiveData(viewModel.liveDataViewModel)
    }
}