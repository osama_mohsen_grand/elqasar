package grand.app.elqasar.pages.address.list.model


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import grand.app.elqasar.model.base.StatusMessage

data class AddressListResponse (
    @SerializedName("data")
    @Expose
    var data: ArrayList<Address> = arrayListOf()
): StatusMessage()