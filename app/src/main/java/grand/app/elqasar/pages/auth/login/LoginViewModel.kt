package grand.app.elqasar.pages.auth.login

import android.os.Bundle
import android.util.Log
import androidx.lifecycle.MutableLiveData
import grand.app.elqasar.base.BaseViewModel
import grand.app.elqasar.pages.auth.repository.AuthRepository
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.auth.login.model.LoginRequest
import grand.app.elqasar.utils.Constants
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class LoginViewModel : BaseViewModel {
    @Inject
    lateinit var repository: AuthRepository;
    var compositeDisposable: CompositeDisposable
    var request: LoginRequest
    //    private var liveData: LiveData<Mutable> = MutableLiveData()
    var liveDataViewModel: MutableLiveData<Mutable>
    var isBack = false

    @Inject
    constructor(repository: AuthRepository){
        this.repository = repository
        this.liveDataViewModel = MutableLiveData();
        compositeDisposable = CompositeDisposable()
        request = LoginRequest()
        repository.setLiveData(liveDataViewModel)
    }

    fun setArgument(bundle: Bundle) {
        if(bundle.containsKey(Constants.IS_BACK)){
            isBack = bundle.getBoolean(Constants.IS_BACK)
        }
    }





    private  val TAG = "LoginOrRegisterViewMode"


    fun forget(){
        liveDataViewModel.value = Mutable(Constants.FORGET_PASSWORD)
    }
    fun register() {
        liveDataViewModel.value = Mutable(Constants.REGISTER)
    }
    fun back() {
        liveDataViewModel.value = Mutable(Constants.BACK)
    }

    fun submit(){
        if(request.isValid()){
            repository.login(request)
        }
        Log.d(TAG,request.phoneError.get().toString())
//        notifyChange()
    }

    protected fun unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }

    override fun onCleared() {
        super.onCleared()
        unSubscribeFromObservable()
    }
}