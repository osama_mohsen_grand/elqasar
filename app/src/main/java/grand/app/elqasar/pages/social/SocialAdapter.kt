package grand.app.elqasar.pages.social

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import grand.app.elqasar.R
import grand.app.elqasar.databinding.ItemSocialBinding
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.social.model.Social
import grand.app.elqasar.pages.social.viewmodel.ItemSocialViewModel
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.helper.AppHelper
import grand.app.elqasar.utils.helper.MovementHelper
import grand.app.elqasar.utils.resources.ResourceManager

class SocialAdapter : RecyclerView.Adapter<SocialAdapter.ViewHolder>() {
    // fetch list data
    private  val TAG = "SocialAdapter"
    private lateinit var modelList: ArrayList<Social>
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // binding item_model layout
        val binding: ItemSocialBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_social,
            parent,
            false
        )
        return ViewHolder(binding)
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(modelList[position])
        Log.d(TAG,modelList[position].value)
        holder.viewModel.mutableLiveData.observeForever {
            val mutable: Mutable = it as Mutable
            val model: Social = modelList[mutable.position]
            if (mutable.message == Constants.SUBMIT) {
                if (model.key == "Whatsapp")
                    AppHelper.openDialNumber(holder.itemView.context, model.value)
                else
                    AppHelper.openBrowser(holder.itemView.context, model.value)
            }
        }
    }

    override fun getItemCount(): Int {
        //return data count if exist or 0
        return if (::modelList.isInitialized) modelList.size else 0
    }

    fun update(modelList: ArrayList<Social>) {
        //update data after call service again in scroll , and notify list which end with
        Log.d(TAG,"update")
        this.modelList = modelList
        Log.d(TAG,"update : "+modelList.size)
        notifyDataSetChanged()
//        notifyItemRangeInserted(getItemCount(), modelList.size - 1);
    }

    class ViewHolder(private val binding: ItemSocialBinding) :
        RecyclerView.ViewHolder(binding.root) {
        lateinit var viewModel: ItemSocialViewModel
        fun bind(model: Social) {
            viewModel =
                ItemSocialViewModel(
                    model,
                    adapterPosition
                )
            binding.viewmodel = viewModel
        }
    }
}