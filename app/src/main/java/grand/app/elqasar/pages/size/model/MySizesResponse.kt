package grand.app.elqasar.pages.size.model


import com.google.gson.annotations.SerializedName
import grand.app.elqasar.model.base.StatusMessage

data class MySizesResponse(
    @SerializedName("is_size")
    var isSize: Boolean = false,
    @SerializedName("data")
    var data: List<MySizeModel> = arrayListOf()
) : StatusMessage()