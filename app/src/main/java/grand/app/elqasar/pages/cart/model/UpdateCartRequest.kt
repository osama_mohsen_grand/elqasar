package grand.app.elqasar.pages.cart.model

import com.google.gson.annotations.SerializedName

class UpdateCartRequest(@SerializedName("product_id") var productId: Int,
                        @SerializedName("qty") var qty: Int,
                        @SerializedName("type") var type: Int,
                        @SerializedName("cart_item_id") var cart_item_id: Int = 0) {
}