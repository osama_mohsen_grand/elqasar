package grand.app.elqasar.pages.product.slider

import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import grand.app.elqasar.R
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.product.model.details.Image
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.resources.ResourceManager

data class ItemSliderImageViewModel(val model: Image) {

    var mutableLiveData = MutableLiveData<Any>()

}