package grand.app.elqasar.pages.splash

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import grand.app.elqasar.R
import grand.app.elqasar.activity.MainActivity
import grand.app.elqasar.base.BaseFragment
import grand.app.elqasar.base.MyApplication
import grand.app.elqasar.base.di.IApplicationComponent
import grand.app.elqasar.databinding.FragmentSplashBinding
import grand.app.elqasar.pages.auth.code.VerifyCodeFragment
import grand.app.elqasar.pages.auth.register.RegisterFragment
import grand.app.elqasar.pages.loginorregister.LoginOrRegisterFragment
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.product.ui.ProductListSliderFragment
import grand.app.elqasar.pages.splash.SplashViewModel
import grand.app.elqasar.pages.splash.models.MainSettingsResponse
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.URLS
import grand.app.elqasar.utils.helper.MovementHelper
import grand.app.elqasar.utils.session.LanguagesHelper
import grand.app.elqasar.utils.session.SharedPreferenceHelper
import grand.app.elqasar.utils.session.UserHelper
import javax.inject.Inject

class SplashFragment : BaseFragment() {
    private lateinit var binding: FragmentSplashBinding
    @Inject
    lateinit var viewModel: SplashViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_splash, container, false)
        val component: IApplicationComponent? =
            (context!!.applicationContext as MyApplication).applicationComponent
        component?.inject(this)
        binding.viewmodel = viewModel
        viewModel.call()
        setEvent()
        LanguagesHelper.setLanguage(Constants.DEFAULT_LANGUAGE)
        // Inflate the layout for this fragment
        return binding.root
    }

    private fun setEvent() {
        viewModel.liveDataViewModel.observe(requireActivity(), Observer {
            val mutable = it as Mutable
            handleActions(mutable)
            if(mutable.message.equals(URLS.MAIN_SETTINGS)){
                finishActivity()
                UserHelper.saveJsonResponse(Constants.SETTINGS,mutable.obj as MainSettingsResponse)
                val bundle: Bundle = Bundle()
                if(UserHelper.getUserId() == -1) {
                    bundle.putString(Constants.PAGE, LoginOrRegisterFragment::class.java.name)
                    MovementHelper.startActivityBase(context, bundle);
                }else{
                    MovementHelper.startActivity(context, MainActivity::class.java);
                }
            }
        })
    }

    override fun onResume() {
        super.onResume()
        viewModel.repository.setLiveData(viewModel.liveDataViewModel)
    }
}