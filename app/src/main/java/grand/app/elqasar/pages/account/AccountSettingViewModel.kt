package grand.app.elqasar.pages.account

import android.util.Log
import androidx.databinding.ObservableBoolean
import androidx.lifecycle.MutableLiveData
import grand.app.elqasar.R
import grand.app.elqasar.base.BaseViewModel
import grand.app.elqasar.pages.home.HomeModel
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.address.list.ui.AddressListFragment
import grand.app.elqasar.pages.auth.login.model.User
import grand.app.elqasar.pages.order.ui.OrderListFragment
import grand.app.elqasar.pages.settings.contact.ContactUsFragment
import grand.app.elqasar.pages.settings.term.SettingsFragment
import grand.app.elqasar.pages.social.SocialFragment
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.dialog.DialogLogin
import grand.app.elqasar.utils.session.UserHelper
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class AccountSettingViewModel : BaseViewModel {
    //    @Inject
//    lateinit var repository: SettingsRepository;
    lateinit var compositeDisposable: CompositeDisposable
    var topAdapter = AccountSettingAdapter()
    var bottomAdapter = AccountSettingAdapter()
    var topList = arrayListOf<HomeModel>()
    var bottomList = arrayListOf<HomeModel>()
    var liveDataViewModel: MutableLiveData<Mutable>
    var user: User
    var x: Int = 6
    val isLogin: ObservableBoolean = ObservableBoolean(UserHelper.isLogin())

    @Inject
    constructor() {
        this.liveDataViewModel = MutableLiveData();
        user = UserHelper.getUserDetails()
    }

    init {
        topList.add(
            HomeModel(
                getString(R.string.my_requests),
                R.mipmap.img_my_requests,
                OrderListFragment::class.java.name
            )
        )
        topList.add(
            HomeModel(
                getString(R.string.my_address),
                R.mipmap.img_pin,
                AddressListFragment::class.java.name
            )
        )
        topAdapter.update(topList)

        bottomList.add(
            HomeModel(
                getString(R.string.call_us),
                R.mipmap.img_call_contact,
                ContactUsFragment::class.java.name
            )
        )
        bottomList.add(HomeModel(getString(R.string.rate), R.mipmap.img_rate))
        bottomList.add(
            HomeModel(
                getString(R.string.about_app), R.mipmap.img_about_app,
                SettingsFragment::class.java.name, 2
            )
        )
        bottomList.add(
            HomeModel(
                getString(R.string.social_contact),
                R.mipmap.img_social_contact,
                SocialFragment::class.java.name,
                3
            )
        )
        bottomList.add(
            HomeModel(
                getString(R.string.terms_and_conditions), R.mipmap.img_terms,
                SettingsFragment::class.java.name, 1
            )
        )
        if (UserHelper.isLogin())
            bottomList.add(
                HomeModel(
                    getString(R.string.logout), R.mipmap.ic_logout,
                    "", -1
                )
            )
        else
            bottomList.add(
                HomeModel(
                    getString(R.string.login), R.mipmap.ic_logout,
                    "", -1
                )
            )
        bottomAdapter.update(bottomList)
    }

    private val TAG = "AccountSettingViewModel"
    fun updateProfile() {
        Log.d(TAG, "update profile")
        liveDataViewModel.value = Mutable(Constants.UPDATE_PROFILE)
    }

    protected fun unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }

    override fun onCleared() {
        super.onCleared()
        unSubscribeFromObservable()
    }
}

