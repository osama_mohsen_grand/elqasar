package grand.app.elqasar.pages.size.viewmodel

import android.util.Log
import android.widget.RadioGroup
import androidx.lifecycle.MutableLiveData
import grand.app.elqasar.R
import grand.app.elqasar.base.BaseViewModel
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.auth.repository.AuthRepository
import grand.app.elqasar.pages.home.HomeAdapter
import grand.app.elqasar.pages.home.HomeModel
import grand.app.elqasar.pages.size.model.SizeRequest
import grand.app.elqasar.pages.splash.models.MainSettingsResponse
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.session.UserHelper
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject


class SizesViewModel : BaseViewModel {
    //    @Inject
    private val TAG = "SizesViewModel"
    lateinit var compositeDisposable: CompositeDisposable
    var adapter = HomeAdapter()
    var list = arrayListOf<HomeModel>()
    var liveDataViewModel: MutableLiveData<Mutable>
    val request = SizeRequest()
    lateinit var settings: MainSettingsResponse

    @Inject
    lateinit var repository: AuthRepository;

    @Inject
    constructor(repository: AuthRepository) {
        this.repository = repository
        this.liveDataViewModel = MutableLiveData();
        settings = UserHelper.retrieveJsonResponse(Constants.SETTINGS,MainSettingsResponse::class.java) as MainSettingsResponse

    }

    private fun unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }

    fun isVisible(id: Int) : Boolean{
        for(index in settings.sizesStatus)
            if(index.id == id) return true
        return false
    }

    fun submit(){
        Log.d(TAG,"${request.sizeId}")
        if(request.sizeId != 4){
            repository.updateSize(request)
        }else
            liveDataViewModel.value = Mutable(Constants.SIZE_DISPLAY)
    }

    fun onSplitTypeChanged(radioGroup: RadioGroup?, id: Int) {

        request.sizeId = when (id) {
            R.id.rb_sizes_have_size_before -> 1
            R.id.rb_sizes_need_home_visit-> 2
            R.id.rb_size_calculate_from_shop-> 3
            R.id.rb_size_enter_my_sizes_myself-> 4
            else -> 1
        }
    }

    override fun onCleared() {
        super.onCleared()
        unSubscribeFromObservable()
    }
}

