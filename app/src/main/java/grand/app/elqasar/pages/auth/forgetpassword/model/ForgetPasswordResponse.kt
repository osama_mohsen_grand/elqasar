package grand.app.elqasar.pages.auth.forgetpassword.model

import android.util.Log
import androidx.databinding.ObservableField
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import grand.app.elqasar.model.base.StatusMessage
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.session.UserHelper
import grand.app.elqasar.utils.validation.Validate
import timber.log.Timber

data class ForgetPasswordResponse(@SerializedName("exist") @Expose val exist: Boolean) :
    StatusMessage()