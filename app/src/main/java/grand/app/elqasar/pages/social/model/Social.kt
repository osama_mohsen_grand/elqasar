package grand.app.elqasar.pages.social.model


import com.google.gson.annotations.SerializedName

data class Social(
    @SerializedName("id")
    var id: Int = 0,
    @SerializedName("key")
    var key: String = "",
    @SerializedName("image")
    var image: String = "",
    @SerializedName("value")
    var value: String = ""
)