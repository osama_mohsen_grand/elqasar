package grand.app.elqasar.pages.settings.contact


import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.session.UserHelper
import grand.app.elqasar.utils.validation.Validate
import timber.log.Timber

class ContactUsRequest() {

    @SerializedName("phone")
    @Expose
    var phone = ""
        get() = field
        set(value) {
            field = value
            phoneError.set("")
        }

    @SerializedName("email")
    @Expose
    var email = ""
        get() = field
        set(value) {
            field = value
            emailError.set("")
        }

    @SerializedName("name")
    @Expose
    var name = ""
        get() = field
        set(value) {
            field = value
            nameError.set("")
        }

    @SerializedName("message")
    @Expose
    var message = ""
        get() = field
        set(value) {
            field = value
            messageError.set("")
        }

    @Transient
    lateinit var nameError: ObservableField<String>

    @Transient
    lateinit var messageError: ObservableField<String>

    @Transient
    lateinit var phoneError: ObservableField<String>
    @Transient
    lateinit var emailError: ObservableField<String>


    init {
        phoneError = ObservableField()
        nameError = ObservableField()
        messageError = ObservableField()
        emailError = ObservableField()
    }

    fun isValid(): Boolean {
        var valid = true
        if (!Validate.isValid(email, Constants.EMAIL)) {
            emailError.set(Validate.error)
            valid = false
        } else emailError.set("")


        if (!Validate.isValidPhoneRegularExpression(phone)) {
            phoneError.set(Validate.error)
            valid = false
        } else phoneError.set("")

        if (!Validate.isValid(name)) {
            nameError.set(Validate.error)
            valid = false
        } else nameError.set("")

        if (!Validate.isValid(message)) {
            messageError.set(Validate.error)
            valid = false
        } else messageError.set("")

        return valid
    }


}