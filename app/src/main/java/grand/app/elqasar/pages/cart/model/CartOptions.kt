package grand.app.elqasar.pages.cart.model


import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class CartOptions(
    @SerializedName("ids")
    var ids: List<Int> = listOf(),
    @SerializedName("qty")
    var qty: Int = 0
): Serializable