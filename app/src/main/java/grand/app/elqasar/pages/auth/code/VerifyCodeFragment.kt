package grand.app.elqasar.pages.auth.code

import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import grand.app.elqasar.R
import grand.app.elqasar.activity.MainActivity
import grand.app.elqasar.base.BaseFragment
import grand.app.elqasar.base.MyApplication
import grand.app.elqasar.base.di.IApplicationComponent
import grand.app.elqasar.databinding.FragmentVerifyCodeBinding
import grand.app.elqasar.pages.auth.login.LoginFragment
import grand.app.elqasar.pages.auth.login.model.LoginResponse
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.model.base.StatusMessage
import grand.app.elqasar.pages.auth.changepassword.ChangePasswordFragment
import grand.app.elqasar.pages.size.ui.SizesFragment
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.URLS
import grand.app.elqasar.utils.helper.MovementHelper
import grand.app.elqasar.utils.session.UserHelper
import javax.inject.Inject

class VerifyCodeFragment : BaseFragment() {
    private lateinit var binding: FragmentVerifyCodeBinding

    @Inject
    lateinit var viewModel: VerifyCodeViewModel


    private val TAG = "VerifyCodeFragment"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_verify_code, container, false)
        val component: IApplicationComponent? =
            (context!!.applicationContext as MyApplication).applicationComponent
        component?.inject(this)
        binding.viewmodel = viewModel
        arguments?.let { viewModel.setArgument(it) }
        setEvent()
        // Inflate the layout for this fragment
        return binding.root
    }


    private fun setEvent() {
        viewModel.liveDataViewModel.observe(requireActivity(), Observer {
            val mutable = it as Mutable
            handleActions(mutable)
            Log.d(TAG,mutable.message)
            if (mutable.message == Constants.SUCCESS) {
                Log.d(TAG,viewModel.type)
                if (viewModel.type == Constants.UPDATE_PROFILE) {
                    viewModel.updateProfile()
                } else {
                    viewModel.register()
                }
            } else if (mutable.message == URLS.REGISTER) {
                finishAllActivities()
                toastMessage((mutable.obj as LoginResponse).mMessage)
                if (viewModel.type == Constants.FORGET_PASSWORD) {
                    val bundle = Bundle()
                    bundle.putString(Constants.PAGE, ChangePasswordFragment::class.java.name)
                    bundle.putString(Constants.PHONE, viewModel.registerRequest.phone)
                    MovementHelper.startActivityBase(
                        context,
                        bundle)
                }else {
                    viewModel.stopTimer()
                    val bundle = Bundle()
                    UserHelper.saveUserDetails((mutable.obj as LoginResponse).user)
                    bundle.putString(Constants.PAGE, SizesFragment::class.java.name)
                    MovementHelper.startActivityBase(
                        context,
                        bundle,
                        getString(R.string.calculate_size)
                    )
                }
            } else if (mutable.message == Constants.BACK) {
                requireActivity().finish()
            }
        })
    }

    override fun onResume() {
        super.onResume()
        viewModel.repository.setLiveData(viewModel.liveDataViewModel)
    }
}