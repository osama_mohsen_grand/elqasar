package grand.app.elqasar.pages.order

import androidx.lifecycle.MutableLiveData
import grand.app.elqasar.pages.splash.models.MainSettingsResponse
import grand.app.elqasar.connection.ConnectionHelper
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.model.base.StatusMessage
import grand.app.elqasar.pages.order.model.OrderListResponse
import grand.app.elqasar.pages.order.model.OrderStatusResponse
import grand.app.elqasar.pages.order.model.details.OrderDetailsResponse
import grand.app.elqasar.pages.settings.contact.ContactUsRequest
import grand.app.elqasar.pages.settings.notification.model.NotificationResponse
import grand.app.elqasar.pages.settings.term.SettingsResponse
import grand.app.elqasar.pages.social.model.SocialResponse
import grand.app.elqasar.repository.BaseRepository
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.URLS
import io.reactivex.disposables.Disposable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class OrderRepository : BaseRepository {

    @Inject
    lateinit var connectionHelper: ConnectionHelper

    @Inject
    constructor(connectionHelper: ConnectionHelper){
        this.connectionHelper = connectionHelper;
    }

    private var liveData: MutableLiveData<Mutable> = MutableLiveData()
    fun setLiveData(liveData: MutableLiveData<Mutable>) {
        this.liveData = liveData
        connectionHelper.liveData = liveData
    }


    fun getOrders() : Disposable {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.ORDERS, Any(), OrderListResponse::class.java,
            URLS.ORDERS, true)
    }

    fun getOrderStatus(orderId: Int) : Disposable {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.ORDER_STATUS+"?order_id=$orderId", Any(), OrderStatusResponse::class.java,
            URLS.ORDER_STATUS, true)
    }

    fun getOrderDetails(id: Int) : Disposable {
        return connectionHelper.requestApi(Constants.GET_REQUEST, "${URLS.ORDER_DETAILS}$id", Any(), OrderDetailsResponse::class.java,
            URLS.ORDER_DETAILS, true)
    }
}