package grand.app.elqasar.pages.cart.viewmodel

import grand.app.elqasar.pages.cart.model.CartTransaction

data class ItemCartTransactionViewModel(
    val model: CartTransaction,
    var position: Int,
    val isLastItem: Boolean
) {
}