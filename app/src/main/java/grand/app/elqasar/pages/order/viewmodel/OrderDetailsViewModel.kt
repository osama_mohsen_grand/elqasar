package grand.app.elqasar.pages.order.viewmodel

import android.os.Bundle
import android.transition.Slide
import android.util.Log
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import grand.app.elqasar.R
import grand.app.elqasar.base.BaseViewModel
import grand.app.elqasar.pages.auth.repository.AuthRepository
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.auth.login.model.LoginRequest
import grand.app.elqasar.pages.auth.register.RegisterRequest
import grand.app.elqasar.pages.cart.adapter.CartAdapter
import grand.app.elqasar.pages.cart.adapter.CartDetailsAdapter
import grand.app.elqasar.pages.cart.adapter.CartTransactionAdapter
import grand.app.elqasar.pages.cart.model.UpdateCartRequest
import grand.app.elqasar.pages.order.OrderRepository
import grand.app.elqasar.pages.order.model.details.OrderDetailsResponse
import grand.app.elqasar.pages.product.model.FavouriteRequest
import grand.app.elqasar.pages.product.model.details.ProductDetailsResponse
import grand.app.elqasar.pages.product.model.list.Product
import grand.app.elqasar.pages.product.model.list.ProductListResponse
import grand.app.elqasar.pages.product.slider.SliderAdapter
import grand.app.elqasar.repository.ProductRepository
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.resources.ResourceManager
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class OrderDetailsViewModel : BaseViewModel {
    @Inject
    lateinit var repository: OrderRepository;

    var compositeDisposable: CompositeDisposable
    //    private var liveData: LiveData<Mutable> = MutableLiveData()
    var liveDataViewModel: MutableLiveData<Mutable>
    var type: Int = 0
    var id : Int = 0
    private  val TAG = "OrderDetailsViewModel"
    val response : ObservableField<OrderDetailsResponse> = ObservableField()
//    var response: OrderDetailsResponse = OrderDetailsResponse()
    var adapterTransaction = CartTransactionAdapter()
    var adapterCart = CartDetailsAdapter()

    @Inject
    constructor(repository: OrderRepository){
        this.repository = repository
        this.liveDataViewModel = MutableLiveData();
        compositeDisposable = CompositeDisposable()
        adapterCart.isOrderDetails = true
        repository.setLiveData(liveDataViewModel)
    }

    fun setArgument( bundle : Bundle){
        id = bundle.getInt(Constants.ID)
        repository.getOrderDetails(id)

    }

    private fun unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }

    override fun onCleared() {
        super.onCleared()
        unSubscribeFromObservable()
    }

    fun setData(response: OrderDetailsResponse) {
        this.response.set(response)
        adapterTransaction.setList(response)
        adapterCart.update(response.data.orderProducts)
        Log.d(TAG,response.data.orderNumber)
        show.set(true)
    }
}