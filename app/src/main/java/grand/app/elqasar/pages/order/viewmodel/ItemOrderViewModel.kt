package grand.app.elqasar.pages.order.viewmodel

import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import grand.app.elqasar.R
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.order.model.Order
import grand.app.elqasar.pages.settings.notification.model.Notification
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.resources.ResourceManager

data class ItemOrderViewModel(val model: Order, var position: Int) {

    var mutableLiveData = MutableLiveData<Any>()

    var qty_details = ObservableField<String>("${ResourceManager.getString(R.string.qty)} ${model.qty}")

    var price: ObservableField<String> =
        ObservableField("${model.totalPrice} ${ResourceManager.getString(R.string.currency)}")

    fun followOrder(){
        mutableLiveData.value = Mutable(Constants.FOLLOW_ORDER , position)
    }

    fun submit(){
        mutableLiveData.value = Mutable(Constants.SUBMIT , position)
    }
}