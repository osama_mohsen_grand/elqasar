package grand.app.elqasar.pages.auth.login.model


import android.util.Log
import androidx.databinding.ObservableField
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import grand.app.elqasar.utils.Constants
import grand.app.elqasar.utils.session.UserHelper
import grand.app.elqasar.utils.validation.Validate
import timber.log.Timber

class LoginRequest() {

    private val TAG = "LoginRequest"


    @SerializedName("password")
    @Expose
    var password: String = ""
        get() = field
        set(value) {
            field = value
            passwordError.set("")
//            if (field != "") passwordError.set(null)
        }

    @SerializedName("firebase_token")
    @Expose
    var firebase_token = "test"

    @SerializedName("phone")
    @Expose
    var phone = ""
        get() = field
        set(value) {
            field = value
            phoneError.set("")
//            if (Validate.isValid(field,Constants.PHONE)) phoneError.set("")
//            else phoneError.set(Validate.error)
        }
    @Transient
    lateinit var phoneError: ObservableField<String>

    var phoneTextError : String = ""

    @Transient
    lateinit var passwordError: ObservableField<String>

    init {
        phoneError = ObservableField("")
        passwordError = ObservableField("")
    }

    fun isValid(): Boolean {
        var valid = true

        Log.d(TAG,"isValid")
        if(!Validate.isValidPhoneRegularExpression(phone)){
            phoneError.set(Validate.error)
            phoneTextError = Validate.error
            valid = false
        } else phoneError.set("")

//        if (!Validate.isValid(phone, Constants.PHONE)) {
//            Log.d("valid", "phone not valid")
//            phoneError.set(Validate.error)
//            phoneTextError = Validate.error
//            valid = false
//        } else phoneError.set("")

        if (!Validate.isValid(password)) {
            Log.d("password", "password not valid")
            passwordError.set(Validate.error)
            valid = false
        } else passwordError.set("")

        return valid
    }


}