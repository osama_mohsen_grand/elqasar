package grand.app.elqasar.pages.makeyourclothes.tailermade.viewmodel

import androidx.lifecycle.MutableLiveData
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.makeyourclothes.tailermade.model.TailorMade
import grand.app.elqasar.pages.social.model.Social
import grand.app.elqasar.utils.Constants

data class ItemTailorMadeViewModel(val model : TailorMade, var position : Int,var selected : Boolean) {
    var mutableLiveData = MutableLiveData<Any>()
    fun submit(){
        mutableLiveData.value = Mutable(Constants.SUBMIT , position)
    }
}