package grand.app.elqasar.pages.makeyourclothes.fabric.viewmodel

import android.os.Bundle
import android.util.Log
import androidx.lifecycle.MutableLiveData
import grand.app.elqasar.base.BaseViewModel
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.pages.makeyourclothes.fabric.adapter.FabricAdapter
import grand.app.elqasar.pages.makeyourclothes.fabric.model.ClothesItem
import grand.app.elqasar.pages.makeyourclothes.fabric.model.ClothesModelRequest
import grand.app.elqasar.pages.makeyourclothes.fabric.model.Fabric
import grand.app.elqasar.pages.makeyourclothes.fabric.model.FabricListResponse
import grand.app.elqasar.repository.ProductRepository
import grand.app.elqasar.utils.Constants
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class FabricViewModel : BaseViewModel {
    @Inject
    lateinit var repository: ProductRepository;
    private val TAG = "FabricViewModel"
    lateinit var compositeDisposable: CompositeDisposable
    lateinit var adapter: FabricAdapter
    var liveDataViewModel: MutableLiveData<Mutable>
    lateinit var response: FabricListResponse

    var code = ""

    @Inject
    constructor(repository: ProductRepository) {
        this.repository = repository
        adapter = FabricAdapter(repository = repository)
        this.liveDataViewModel = MutableLiveData();
        repository.setLiveData(liveDataViewModel)
    }

    fun setArgument(bundle: Bundle) {
        adapter.type = bundle.getInt(Constants.TYPE)
        adapter.request = bundle.getSerializable(Constants.REQUEST_MODELS) as ClothesItem
        adapter.isBack = bundle.getBoolean(Constants.IS_BACK,false)
        repository.getCategories(adapter.type)
    }


    private fun unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }

    private val tmpList = arrayListOf<Fabric>()

    fun submit(){
        Log.d(TAG,"submit")
        if(code.trim() == ""){
            adapter.update(response.data)
        }else {
            tmpList.clear()
            response.let {
                for (model in response.data) {
                    if (model.code.contains(code))
                        tmpList.add(model)
                }
            }
            Log.d(TAG,tmpList.size.toString())
            adapter.update(tmpList)
            notifyChange()
        }
        liveDataViewModel.value = Mutable(Constants.HIDE_KEYBOARD)
    }

    override fun onCleared() {
        super.onCleared()
        unSubscribeFromObservable()
    }

    fun setData(response: FabricListResponse) {
        this.response = response
        adapter.update(response.data)
        adapter.setSelectedIfExist()
        show.set(true)
    }
}

