package grand.app.elqasar.pages.order.model.details


import com.google.gson.annotations.SerializedName

data class OrderProduct(
    @SerializedName("about")
    var about: String = "",
    @SerializedName("id")
    var id: Int = 0,
    @SerializedName("image")
    var image: String = "",
    @SerializedName("name")
    var name: String = "",
    @SerializedName("price")
    var price: String = "",
    @SerializedName("product_id")
    var productId: Int = 0,
    @SerializedName("qty")
    var qty: Int = 0,
    @SerializedName("total_price")
    var totalPrice: String = ""
)