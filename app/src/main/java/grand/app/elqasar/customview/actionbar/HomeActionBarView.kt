package grand.app.elqasar.customViews.actionbar

import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.view.LayoutInflater
import android.widget.RelativeLayout
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import androidx.drawerlayout.widget.DrawerLayout
import grand.app.elqasar.R
import grand.app.elqasar.activity.MainActivity
import grand.app.elqasar.databinding.LayoutActionBarHomeBinding
import grand.app.elqasar.customViews.menu.NavigationDrawerView


class HomeActionBarView : RelativeLayout {

    lateinit var layoutActionBarHomeBinding: LayoutActionBarHomeBinding
    var navigationDrawerView: NavigationDrawerView? = null
    var drawerLayout: DrawerLayout? = null

    constructor(context: Context, attributeSet: AttributeSet) : super(context,attributeSet){
        Log.d(TAG, "constructor: Context attributeSet")
        init()
    }

    constructor(context: Context) : super(context){
        Log.d(TAG, "constructor: Done")
        init()
    }



    fun init(){
        val layoutInflater = LayoutInflater.from(context)
        layoutActionBarHomeBinding = DataBindingUtil.inflate(layoutInflater, R.layout.layout_action_bar_home, this, true)
//        layoutActionBarHomeBinding.homeBarConst.setOnClickListener{
//            Log.d(TAG, "homeBarConst: Done")
//            connectDrawer(drawerLayout, false)
//
//        }
        layoutActionBarHomeBinding.imgHomeBarMenu.setOnClickListener {
            connectDrawer(drawerLayout, false)
        }
        setEvents()
    }

    fun setEvents() {
        Log.d(TAG, "setEvents: Done")

    }


    private val TAG = "HomeActionBarView"
    fun connectDrawer(drawerLayout: DrawerLayout?, firstConnect: Boolean) {
        Log.d(TAG,"connect drawer" +firstConnect)
        if (firstConnect) {
            this.drawerLayout = drawerLayout
            return
        } else {
            Log.d(TAG,"connect drawer else")
            if (drawerLayout!!.isDrawerOpen(GravityCompat.END)) {
                Log.d(TAG,"connect END")
                drawerLayout.closeDrawer(GravityCompat.START)
            } else {
                Log.d(TAG,"connect START")
                drawerLayout.openDrawer(GravityCompat.START)
            }
        }
    }

    fun setNavigation(navigationDrawerView: NavigationDrawerView?) {
        this.navigationDrawerView = navigationDrawerView
    }

    fun setTitle(string: String?) {
        layoutActionBarHomeBinding.tvHomeBarText.text = string
    }

}