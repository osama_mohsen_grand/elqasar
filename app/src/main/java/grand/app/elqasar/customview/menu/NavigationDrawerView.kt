package grand.app.elqasar.customViews.menu

import android.util.AttributeSet
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.RelativeLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.MutableLiveData
import grand.app.elqasar.R
import grand.app.elqasar.databinding.LayoutNavigationDrawerBinding
import grand.app.elqasar.pages.home.HomeFragment
import grand.app.elqasar.customViews.actionbar.HomeActionBarView
import grand.app.elqasar.model.base.Mutable
import grand.app.elqasar.utils.helper.MovementHelper
import grand.app.elqasar.utils.session.UserHelper

/**
 * Created by mohamedatef on 12/30/18.
 */
class NavigationDrawerView : RelativeLayout {
    var liveData: MutableLiveData<Mutable?>? = null
    var layoutNavigationDrawerBinding: LayoutNavigationDrawerBinding? = null
    var homeActionBarView: HomeActionBarView? = null

    //    ServicesResponse servicesResponse;
    private var menuViewModel: MenuViewModel? = null

    constructor(context: AppCompatActivity?) : super(context) {
        liveData = MutableLiveData()
        init()
    }

    constructor(context: AppCompatActivity?, attrs: AttributeSet?) : super(context, attrs) {
        init()
    }

    constructor(context: AppCompatActivity?, attrs: AttributeSet?, defStyle: Int) : super(context, attrs) {
        init()
    }

    private fun init() {
//                MovementHelper.addFragment(getContext(),new Fragment(),"fda");
        val layoutInflater = LayoutInflater.from(context)
        layoutNavigationDrawerBinding = DataBindingUtil.inflate(layoutInflater, R.layout.layout_navigation_drawer, this, true)
        menuViewModel = MenuViewModel()
        layoutNavigationDrawerBinding?.menuViewModel = menuViewModel
        setHeader()
        setEvents()
    }

    fun setHeader() {
        if (UserHelper.getUserId() == -1) {
            layoutNavigationDrawerBinding?.vvNavigationDrawerImage?.setImageResource(R.mipmap.ic_launcher)
            layoutNavigationDrawerBinding?.tvUserLogin?.visibility = View.GONE
        } else {
//            layoutNavigationDrawerBinding.tvUserLogin.setVisibility(VISIBLE);
//            layoutNavigationDrawerBinding.tvUserLogin.setText(UserHelper.getUserDetails().name);
//            Glide
//                    .with(context)
//                    .load(UserHelper.getUserDetails().image)
//                    .centerCrop()
//                    .into(layoutNavigationDrawerBinding.vvNavigationDrawerImage);
//            layoutNavigationDrawerBinding.vvNavigationDrawerImage.setOnClickListener(view -> {
//                goToBasePage(RegisterFragment.class.getName(), context.getString(R.string.profile), new Bundle());
//            });
        }
    }

    fun setActionBar(homeActionBarView: HomeActionBarView?) {
        this.homeActionBarView = homeActionBarView
    }

    //home - profile - ads - stories - chat_history - gallery - orders - reviews - famous_people - credit_card
    //label_notification - photographer_people - help_and_support - label_privacy_policy - label_language
    // label_share_app - rate_app - be_shop - label_logout
    private fun setEvents() {
        menuViewModel?.getMenuAdapter()?.liveDataAdapter?.observeForever { position ->
            Log.e("onChanged", "onChanged: $position")
            layoutNavigationDrawerBinding?.dlMainNavigationMenu?.closeDrawer(GravityCompat.START)
            //                // select menu
//                int pos = position;
//                String id = menu.get(pos).id;
//                Log.d(TAG, "onChanged: " + id);
//                Intent intent = null;
//                Service service = MoataHelper.checkServiceSelect(id);
//                if (service != null) {
//                    Bundle bundle = new Bundle();
//                    bundle.putSerializable(Constants.CATEGORY, service);
//                    bundle.putString(Constants.SEARCH_BAR, Constants.SEARCH_BAR);
//                    bundle.putString(Constants.PAGE, CategoryFragment.class.getName());
//                    MovementHelper.startActivityBase(context, bundle, service.name);
//                } else if (id.equals(Constants.HOME)) {
//                    homePage();
//                } else if (id.equals(Constants.CHAT)) {
//                    if (UserHelper.getUserId() != -1)
//                        goToBasePage(ChatFragment.class.getName(), context.getString(R.string.chat), new Bundle());
//                    else
//                        liveData.setValue(new Mutable(Constants.LOGIN_FIRST));
//                } else if (id.equals(Constants.MYREQUESTS)) {
//                    if (UserHelper.getUserId() != -1)
//                        goToBasePage(OrdersFragment.class.getName(), context.getString(R.string.my_requests), new Bundle());
//                    else
//                        liveData.setValue(new Mutable(Constants.LOGIN_FIRST));
//                } else if (id.equals(Constants.FAVOURITE)) {
//                    if (UserHelper.getUserId() != -1)
//                        goToBasePage(FavouriteFragment.class.getName(), context.getString(R.string.favourite), new Bundle());
//                    else
//                        liveData.setValue(new Mutable(Constants.LOGIN_FIRST));
//                } else if (id.equals(Constants.COMPLAIN)) {
//                    Bundle bundle = new Bundle();
//                    bundle.putInt(Constants.STATUS, 1);
//                    goToBasePage(ContactUsFragment.class.getName(), context.getString(R.string.complain_and_suggest), bundle);
//                } else if (id.equals(Constants.CONTACT_US)) {
//                    goToBasePage(ContactUsFragment.class.getName(), context.getString(R.string.contact_us), new Bundle());
//                } else if (id.equals(Constants.RATE_APP)) {
//                    MovementHelper.startWebPage(context, AppHelper.getPlayStoreLink(context));
//                } else if (id.equals(Constants.SOCIAL)) {
//                    goToBasePage(SocialMediaFragment.class.getName(), context.getString(R.string.social_media), new Bundle());
//                } else if (id.equals(Constants.TERMS_AND_CONDITIONS)) {
//                    Bundle bundle = new Bundle();
//                    bundle.putInt(Constants.STATUS, 1);
//                    goToBasePage(SettingsFragment.class.getName(), context.getString(R.string.terms_and_conditions), bundle);
//                } else if (id.equals(Constants.LANGUAGE)) {
//                    new LanguageDialog(context).show();
//                } else if (id.equals(Constants.NOTIFICATIONS)) {
//                    goToBasePage(NotificationFragment.class.getName(), context.getString(R.string.notifications), new Bundle());
//                }
        }
    }
}