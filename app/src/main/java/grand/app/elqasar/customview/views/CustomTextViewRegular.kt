package grand.app.elqasar.customViews.views

import android.content.Context
import android.graphics.Typeface
import android.text.method.ScrollingMovementMethod
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatTextView

/**
 * Created by mohamedatef on 1/8/19.
 */
class CustomTextViewRegular(context: Context, attributeSet: AttributeSet) :
    AppCompatTextView(context, attributeSet) {

    init {
        var font: Typeface? = null
        font = Typeface.createFromAsset(context.assets, "regular.otf")
        typeface = font

//        movementMethod = ScrollingMovementMethod()
    }

}