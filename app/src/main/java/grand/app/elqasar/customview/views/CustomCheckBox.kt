package grand.app.elqasar.customview.views

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatCheckBox
import androidx.appcompat.widget.AppCompatTextView

class CustomCheckBox (context: Context, attributeSet: AttributeSet) :
    AppCompatCheckBox(context, attributeSet) {

    init {
        var font: Typeface? = null
        font = Typeface.createFromAsset(context.assets, "regular.otf")
        typeface = font

//        movementMethod = ScrollingMovementMethod()
    }

}