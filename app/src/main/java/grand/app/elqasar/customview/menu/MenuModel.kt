package grand.app.elqasar.customViews.menu

class MenuModel {
    var id: String? = null
    var title: String? = null
    var icon = 0

    constructor(id: String?, title: String?, icon: Int) {
        this.id = id
        this.title = title
        this.icon = icon
    }

    constructor() {}
}