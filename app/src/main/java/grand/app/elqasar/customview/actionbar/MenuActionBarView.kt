package grand.app.elqasar.customViews.actionbar

import android.content.Context
import android.view.LayoutInflater
import android.widget.RelativeLayout
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import androidx.drawerlayout.widget.DrawerLayout
import grand.app.elqasar.R
import grand.app.elqasar.databinding.LayoutActionBarBackBinding


class MenuActionBarView(context: Context?) : RelativeLayout(context) {
    var layoutActionBarBackBinding: LayoutActionBarBackBinding? = null
    var drawerLayout: DrawerLayout? = null
    fun init() {
        val layoutInflater = LayoutInflater.from(context)
        layoutActionBarBackBinding = DataBindingUtil.inflate(layoutInflater, R.layout.layout_action_bar_back, this, true)
    }

    fun connectDrawer(drawerLayout: DrawerLayout?) {
        this.drawerLayout = drawerLayout
        drawerLayout?.let {
            if (drawerLayout.isDrawerOpen(GravityCompat.START)) drawerLayout.closeDrawer(GravityCompat.START)
            else drawerLayout.openDrawer(GravityCompat.START)
        }
    }

    init {
        init()
    }
}