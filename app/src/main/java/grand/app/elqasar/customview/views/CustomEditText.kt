package grand.app.elqasar.customViews.views

import android.content.Context
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.util.AttributeSet
import android.util.Log
import android.view.ContextThemeWrapper
import android.view.inputmethod.EditorInfo
import androidx.databinding.BindingAdapter
import androidx.databinding.InverseBindingAdapter
import androidx.databinding.InverseBindingListener
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import grand.app.elqasar.R
import grand.app.elqasar.utils.resources.ResourceManager
import grand.app.elqasar.utils.validation.Validate


class CustomEditText(context: Context, attributeSet: AttributeSet)   : TextInputLayout(context,attributeSet) {
    private var editText: TextInputEditText? = null
    private var inputType = 0
    private var validator = 0
    var text: String? = ""
    var isCome = false
    private var isFirst = false
    var validatorCount = 0
    var lastCaseWrong = false
    var lastCaseCorrect = false

    init {
        val typedArray = context.obtainStyledAttributes(R.styleable.CustomEditText)
        validator = typedArray.getInt(R.styleable.CustomEditText_isValidate, 0)
        typedArray.recycle()
        inputType(context, attributeSet)
    }
//    constructor(context: Context?) : super(context!!) {}
//    constructor(context: Context?, attrs: AttributeSet?) : super(context!!, attrs) {
//
//    }

//    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context!!, attrs, defStyleAttr) {
//        val typedArray = context.obtainStyledAttributes(R.styleable.CustomEditText)
//        typedArray.recycle()
//        inputType(context, attrs)
//    }

    private fun init() {
        setWillNotDraw(false)
        editText = TextInputEditText(ContextThemeWrapper(context, R.style.inputEditTextLayout))
        createEditBox(editText)
    }

    private fun createEditBox(editText: TextInputEditText?) {
        val layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT)
        editText?.setInputType(inputType)
        editText?.setLayoutParams(layoutParams)
        addView(editText)
    }

    fun setFirst(first: Boolean) {
        isFirst = first
    }

    fun isFirst(): Boolean {
        return isFirst
    }

    private fun inputType(context: Context, attrs: AttributeSet?) {
        val a = context.theme.obtainStyledAttributes(
                attrs,
                R.styleable.CustomEditText,
                0, 0)
        // Gets you the 'value' number - 0 or 666 in your example
        val n = a.indexCount
        for (i in 0 until n) {
            val attr = a.getIndex(i)
            //note that you are accessing standard attributes using your attrs identifier
            if (attr == R.styleable.CustomEditText_android_inputType) {
                inputType = a.getInt(attr, EditorInfo.TYPE_TEXT_VARIATION_NORMAL)
            } else if (attr == R.styleable.CustomEditText_phone_expression) {
                phoneExpression = a.getInt(R.styleable.CustomEditText_phone_expression, 0)
            }
        }
        a.recycle()
        init()
    }

    fun setValidator(validator: Int) {
        this.validator = validator
    }

    fun getValidator(): Int {
        return validator
    }

    companion object {
        private var phoneExpression = 0
        private val TAG: String? = "CustomEditText"

        @BindingAdapter(value = ["isValidateAttrChanged"])
        fun setListener(view: CustomEditText?, textAttrChanged: InverseBindingListener?) {
            if (textAttrChanged != null) {
                view?.getEditText()?.addTextChangedListener(object : TextWatcher {
                    override fun beforeTextChanged(charSequence: CharSequence?, i: Int, i1: Int, i2: Int) {}
                    override fun onTextChanged(charSequence: CharSequence?, i: Int, i1: Int, i2: Int) {}
                    override fun afterTextChanged(editable: Editable?) {
                        if (Validate.isEmpty(view.getEditText()!!.text.toString())!!) {
                            view.validatorCount = 0
                            view.setError(ResourceManager.getString(R.string.fieldRequired))
                        } else if (view.getEditText()?.inputType == InputType.TYPE_CLASS_PHONE && !Validate.isPhone(
                                view.getEditText()!!.text.toString())!!
                        ) {
                            view.validatorCount = 0
                            view.error = "Wrong phone"
                        } else if ((view.getEditText()!!.inputType == 33 || view.getEditText()!!
                                .inputType == InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS) && !Validate.isMail(
                                view.getEditText()!!.text.toString())!!
                        ) {
                            view.validatorCount = 0
                            view.error = ResourceManager.getString(R.string.invalidEmail)
                        } else if ((view.getEditText()!!.inputType == 129 || view.getEditText()!!
                                .inputType == InputType.TYPE_TEXT_VARIATION_PASSWORD || view.getEditText()!!
                                .inputType == InputType.TYPE_NUMBER_VARIATION_PASSWORD || view.getEditText()!!
                                .inputType == InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD || view.getEditText()!!
                                .inputType == InputType.TYPE_TEXT_VARIATION_WEB_PASSWORD)
                                && !Validate.isPassword(view.getEditText()!!.text.toString())!!
                        ) {
                            view.validatorCount = 0
                            view.setError(ResourceManager.getString(R.string.invalidPassword))
                        } else {
                            view.validatorCount = 1
                            view.setFirst(true)
                            view.setError(null)
                        }
                        if (view.validatorCount == 0 && !view.lastCaseWrong && view.lastCaseCorrect) {
                            view.setValidator(view.getValidator() - 1)
                            view.lastCaseWrong = true
                            view.lastCaseCorrect = false
                            Log.e("validator", "Wrong " + view.validatorCount + "")
                        } else if (view.validatorCount == 1 && !view.lastCaseCorrect) {
                            Log.e("validator", "Correct " + view.validatorCount + "")
                            view.setValidator(view.getValidator() + 1)
                            view.lastCaseWrong = false
                            view.lastCaseCorrect = true
                        }
                        textAttrChanged.onChange()
                    }
                })
            }
        }

        @BindingAdapter("app:isValidate")
        fun setError(view: CustomEditText?, value: Int) {
            if (view?.isCome!! && Validate.isEmpty(view.getEditText()?.text.toString())!!) {
                view.error = ResourceManager.getString(R.string.fieldRequired)
            }
            view.isCome = true
            view.setValidator(value)
        }

        @InverseBindingAdapter(attribute = "isValidate")
        fun getError(errorInputLayout: CustomEditText?): Int {
            if (errorInputLayout != null) {
                return errorInputLayout.validator
            }
            return -1
        }

        @BindingAdapter("app:text")
        fun getText(view: CustomEditText?, text: String?) {
            view?.getEditText()?.setText(text)
        }

        @InverseBindingAdapter(attribute = "text")
        fun setText(customEditText: CustomEditText?): String? {
            return customEditText?.getEditText()?.text.toString()
        }

        @BindingAdapter(value = ["textAttrChanged"])
        fun setTextListner(view: CustomEditText?, textAttrChanged: InverseBindingListener?) {
            if (textAttrChanged != null) {
                view?.getEditText()?.addTextChangedListener(object : TextWatcher {
                    override fun beforeTextChanged(charSequence: CharSequence?, i: Int, i1: Int, i2: Int) {}
                    override fun onTextChanged(charSequence: CharSequence?, i: Int, i1: Int, i2: Int) {}
                    override fun afterTextChanged(editable: Editable?) {
                        textAttrChanged.onChange()
                    }
                })
            }
        }
    }
}