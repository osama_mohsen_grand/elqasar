package grand.app.elqasar.customViews.menu

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import grand.app.elqasar.R
import grand.app.elqasar.databinding.ItemMenuBinding
import java.util.*

class MenuAdapter : RecyclerView.Adapter<MenuAdapter.MenuView?>() {
    private val menuModels: MutableList<MenuModel?>?
    var selectId: String? = ""
    val liveDataAdapter: MutableLiveData<Int?>? = MutableLiveData()
    private var context: Context? = null

    override fun onCreateViewHolder(parent: ViewGroup, i: Int): MenuView {
        val itemView = LayoutInflater.from(parent.context).inflate(
            R.layout.item_menu,
            parent, false
        )
        context = parent.context
        return MenuView(itemView)
    }

    override fun onBindViewHolder(holder: MenuView, position: Int) {
        val menuModel = menuModels?.get(position)
        val itemMenuViewModel = ItemMenuViewModel(menuModel)
        context?.let {
            holder.itemMenuBinding?.lifecycleOwner?.let { it1 ->
                itemMenuViewModel.getLiveData()
                    ?.observe(it1, Observer { liveDataAdapter?.setValue(position) })
            }
        }
        holder.setViewModel(itemMenuViewModel)
    }

    fun update(dataList: MutableList<MenuModel?>?) {
        menuModels?.clear()
        dataList?.let { menuModels?.addAll(it) }
        notifyDataSetChanged()
    }

    override fun onViewAttachedToWindow(holder: MenuView) {
        super.onViewAttachedToWindow(holder)
        holder.bind()
    }

    override fun onViewDetachedFromWindow(holder: MenuView) {
        super.onViewDetachedFromWindow(holder)
        holder.unbind()
    }

    override fun getItemCount(): Int {
        return menuModels!!.size
    }

    inner class MenuView internal constructor(itemView: View?) : RecyclerView.ViewHolder(itemView!!) {
        var itemMenuBinding: ItemMenuBinding? = null
        fun bind() {
            if (itemMenuBinding == null) {
                itemMenuBinding = DataBindingUtil.bind(itemView)
            }
        }

        fun unbind() {
            itemMenuBinding?.unbind()
        }

        fun setViewModel(itemViewModels: ItemMenuViewModel?) {
            itemMenuBinding?.setItemMenuViewModel(itemViewModels)
        }

        init {
            bind()
        }
    }

    init {
        menuModels = ArrayList()
    }
}