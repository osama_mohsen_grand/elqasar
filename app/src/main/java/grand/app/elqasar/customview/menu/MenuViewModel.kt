package grand.app.elqasar.customViews.menu

import grand.app.elqasar.R
import grand.app.elqasar.base.BaseViewModel
import java.util.*

class MenuViewModel : BaseViewModel() {
    private val menuAdapter: MenuAdapter? = MenuAdapter()
    fun getMenuAdapter(): MenuAdapter? {
        return menuAdapter
    }

    init {
        val list = ArrayList<MenuModel?>()
        list.add(MenuModel("1", "test", R.drawable.ic_back))
        list.add(MenuModel("2", "test2", R.drawable.ic_back))
        menuAdapter?.update(list)
    }
}