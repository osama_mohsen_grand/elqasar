package grand.app.elqasar.customViews.menu

import androidx.databinding.Bindable
import grand.app.elqasar.base.BaseViewModel
import grand.app.elqasar.utils.Constants

class ItemMenuViewModel(private var menuModel: MenuModel?) : BaseViewModel() {

    @Bindable
    fun getMenuModel(): MenuModel? {
        return menuModel
    }

    fun itemAction() {
        //TODO Item Action with liveData
        getLiveData()?.value = Constants.MENu
    }

}