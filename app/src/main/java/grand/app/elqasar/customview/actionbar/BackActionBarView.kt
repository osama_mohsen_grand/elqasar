package grand.app.elqasar.customViews.actionbar

import android.app.Activity
import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.RelativeLayout
import androidx.databinding.DataBindingUtil
import grand.app.elqasar.R
import grand.app.elqasar.databinding.LayoutActionBarBackBinding


class BackActionBarView : RelativeLayout {
    var layoutActionBarBackBinding: LayoutActionBarBackBinding? = null
    var service_id = 0
    var type = 0
    var flag = 0

    constructor(context: Context?) : super(context) {
        init()
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        init()
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyle: Int) : super(context, attrs) {
        init()
    }

    private fun init() {
        val layoutInflater = LayoutInflater.from(context)
        layoutActionBarBackBinding = DataBindingUtil.inflate(layoutInflater, R.layout.layout_action_bar_back, this, true)
        setEvents()
    }

    private fun setEvents() {
        layoutActionBarBackBinding?.imgActionBarCancel?.setOnClickListener { (context as Activity).finish() }
    }

    fun setTitle(title: String?) {
        layoutActionBarBackBinding?.tvActionBarTitle?.text = title
    }
}