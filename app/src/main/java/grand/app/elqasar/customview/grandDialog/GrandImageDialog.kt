package grand.app.elqasar.customViews.grandDialog

import android.content.Context
import android.content.Intent
import android.graphics.Canvas
import android.net.Uri
import android.util.AttributeSet
import android.util.Log
import android.view.LayoutInflater
import android.widget.RelativeLayout
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.AppCompatImageView
import androidx.databinding.DataBindingUtil
import grand.app.elqasar.R
import grand.app.elqasar.databinding.LayoutGrandBinding


class GrandImageDialog(context: Context, attrs: AttributeSet?) : AppCompatImageView(context, attrs) {
    fun setImage() {
        setImageResource(R.drawable.by_grand_black)
        setOnClickListener {
            val grandDialog = GrandDialog(context)
            grandDialog.init()
        }
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
    }

    init {
        setImage()
    }
}

internal class GrandDialog(context: Context) : RelativeLayout(context) {
    fun init() {
        context?.let {
            val layoutInflater = LayoutInflater.from(context)
            val layoutGrandBinding: LayoutGrandBinding = DataBindingUtil.inflate(layoutInflater, R.layout.layout_grand, null, true)
            val grand = layoutGrandBinding.root
            val builder = context?.let { AlertDialog.Builder(it, R.style.customDialog) }
            val dialog = builder?.create()
            dialog?.let {
                dialog.setView(grand)
                dialog.show()
                layoutGrandBinding.tvGrandUrl.setOnClickListener {
                    val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("http://" + layoutGrandBinding.tvGrandUrl.text.toString()))
                    context.startActivity(browserIntent)
                }
                layoutGrandBinding.rlGrandCall.setOnClickListener {
                    val call = Uri.parse("tel:" + layoutGrandBinding.tvGrandPhone.text.toString())
                    val surf = Intent(Intent.ACTION_DIAL, call)
                    context.startActivity(surf)
                }
                layoutGrandBinding.rlGrandWhats.setOnClickListener {
                    try {
                        val uri = Uri.parse("https://api.whatsapp.com/send?phone=" + layoutGrandBinding.tvGrandPhone.text.toString() + "&text=" + "")
                        val sendIntent = Intent(Intent.ACTION_VIEW, uri)
                        context.startActivity(sendIntent)

//                    Intent i = new Intent("android.intent.action.MAIN");
//                    i.setComponent(new ComponentName("com.whatsapp","com.whatsapp.Conversation"));
//                    i.putExtra("jid", PhoneNumberUtils.stripSeparators(layoutGrandBinding.tvGrandPhone.getText().toString())+"@s.whatsapp.net");
//                    context.startActivity(i);
                    } catch (e: Exception) {
                        Log.e("exc", e.message.toString())
                        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=com.whatsapp&hl=en"))
                        context.startActivity(browserIntent)
                    }
                }
                layoutGrandBinding.imgGrandClose.setOnClickListener { dialog.dismiss() }
            }

        }

    }

}